# Geo Visualization

## Descripción

Herramienta para visualizar series teporales de datos climáticos.

## Screens

![Mapa Perú](Screens/Map.png "Mapa")

![Elevaciones](Screens/Elevaciones.png "Elevaciones")

![Barras](Screens/Barras.png "Barras")

![Gráfica de líneas](Screens/GraficaLineas.png "Gráfica de lineas")

![Sistema de ventanas](Screens/Ventanas.png "Sistema de ventanas")

![Gráfica de circular](Screens/GraficaCircular.png "Gráfica de circular")

![NJ](Screens/NJ.png "NJ")

## Video

[https://youtu.be/LuJVTZ-hS80](https://youtu.be/LuJVTZ-hS80)

## Instrucciones

Unreal 4.25

> Si pide Visual Studio 2017, solo hay que entrar a unreal con algun otro proyecto, y cambiar la ocpion de source code: por defecto esta en Visual Studio, cambiar a Visual Studio 2019

Para ejecutar se requiere del backend para poder hacer las consultas de los datos.