// Fill out your copyright notice in the Description page of Project Settings.

#include "Nodo.h"

Nodo::Nodo() {
    //falta agregar el orden de union
    Id = 0;
    Nombre = "";
    Orden = -1;
    Padre = nullptr;
    PadreId = -1;
    HijosId = new int [2];
    Hijos = new Nodo * [2];
    DistanciasHijos = new float [2];
    HijosId[0] = -1;
    HijosId[1] = -1;
    Hijos[0] = nullptr;
    Hijos[1] = nullptr;
    HojasId = nullptr;
    NumeroHojas = 0;//en teoria este siempre deberia tener tama�o 1 ya que el nodo real deberia ser ese represante, los viruales represntanc a 2 o mas
}

Nodo::~Nodo() {
    delete [] HojasId;
    delete [] Hijos;
    delete [] HijosId;
    delete [] DistanciasHijos;
}

void Nodo::AgregarHoja(int n){
    int * temp = HojasId;
    HojasId = new int [NumeroHojas + 1];
    for(int i = 0; i < NumeroHojas; i++){
        *(HojasId + i) = *(temp + i);
    }//abra una mejor forma de realizar esta copia/
    HojasId[NumeroHojas] = n;
    NumeroHojas++;
    delete [] temp;
}
