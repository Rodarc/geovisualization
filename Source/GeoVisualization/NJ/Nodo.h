// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;
/**
 * 
 */
class GEOVISUALIZATION_API Nodo {
public:
    int Id;
    bool Valido;
    string Nombre;//nombre del elemento o dato
    int Orden;
    int * HijosId;
    Nodo ** Hijos;
    float * DistanciasHijos;//deberianser vectores
    int PadreId;
    Nodo * Padre;
    float DistanciaPadre;
    int * HojasId;
    int NumeroHojas;//solo para saber a quienes representa
    void AgregarHoja(int n);

    //para el layout
    float Theta;
    float Phi;
    int Nivel;
    int Altura;
    float WInicio;
    float WTam;
    int Hojas;//lo usa Numero de hojas, por lo visto ya lo tengo calculado

    float X;
    float Y;


    Nodo();
    ~Nodo();
};
