// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include <vector>
#include "Materials/Material.h"
#include "ConexionServidor/ClimateServerConnection.h"
#include "Mapa/Station.h"
#include "Components/StaticMeshComponent.h"
#include "Triangulacion/TriangleFace.h"
#include "Mapa/PaisData.h"
#include "Mapa/DepartamentoData.h"
#include "Mapa/Departamento.h"
#include "Mapa/LimiteData.h"
#include "MapaDepartamentos.generated.h"

UCLASS()
class GEOVISUALIZATION_API AMapaDepartamentos : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapaDepartamentos();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    TArray<FVector> PuntosSCMapa;

    void LeerSCMapa();

    void ConvertirSCMapaToTriangle();

    FVector LatitudLongitudToXY(float Latitud, float Longitud);
    //este es para convertir a triangle, desparecera cuando se tenga el triangulador
	
    PaisData Pais;

    TArray<DepartamentoData> Departamentos;

    TArray<LimiteData> Limites;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stations")
    TSubclassOf<class AStation> StationType;//esto no es practio llenarlo en el cosntructor, cuando esta clase pase a bluprint sera mejor

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Departamentos")
    TSubclassOf<class ADepartamento> DepartamentoType;//esto no es practio llenarlo en el cosntructor, cuando esta clase pase a bluprint sera mejor

    void CrearDepartamentos();
};

//quiza deberia tener objetos deparatemenos, limite, etc, para poder conservar los datos, como nombres, o referencias entre si
//para probar se quedanar asi por ahora
