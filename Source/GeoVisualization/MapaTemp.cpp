// Fill out your copyright notice in the Description page of Project Settings.

#include "MapaTemp.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Kismet/KismetSystemLibrary.h"
#include "Math/Color.h"
#include "Materials/Material.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "Components/StaticMeshComponent.h"
#include "GeoVisualizationGameModeBase.h"


// Sets default values
AMapaTemp::AMapaTemp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    FactorAltura = 100.0f;
	Malla = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = Malla;
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Components|ProceduralMesh", meta = (AutoCreateRefTerm = "Normals,UV0,VertexColors,Tangents"))
	//	void CreateMeshSection(int32 SectionIndex, const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FVector>& Normals,
	// const TArray<FVector2D>& UV0, const TArray<FColor>& VertexColors, const TArray<FProcMeshTangent>& Tangents, bool bCreateCollision);
 
	// New in UE 4.17, multi-threaded PhysX cooking.
	Malla->bUseAsyncCooking = true;

    //static ConstructorHelpers::FObjectFinder<UMaterial> MapaMaterialAsset(TEXT("Material'/Game/GeoVisualizaton/Materials/MapaMaterial.MapaMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    static ConstructorHelpers::FObjectFinder<UMaterial> MapaMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/MapaTransparentMaterial.MapaTransparentMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (MapaMaterialAsset.Succeeded()) {
        MapaMaterial = MapaMaterialAsset.Object;
    }

    static ConstructorHelpers::FClassFinder<AStation> StationClass(TEXT("Class'/Script/GeoVisualization.Station'"));
    if (StationClass.Succeeded()) {
        if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Station type founded."));
        StationType = StationClass.Class;
    }

    static ConstructorHelpers::FObjectFinder<UStaticMesh> MapaTemporalMeshAsset(TEXT("StaticMesh'/Game/Meshes/MapaPeru.MapaPeru'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	static ConstructorHelpers::FObjectFinder<UMaterial> MapaTemporalMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/MapaMaterial.MapaMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera

    MapaTemporal = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MapaTemporal"));
    MapaTemporal->SetupAttachment(RootComponent);
	MapaTemporal->SetRelativeLocation(FVector(0.0f, 0.0f, 0.01f));

    if (MapaTemporalMeshAsset.Succeeded()) {
        MapaTemporal->SetStaticMesh(MapaTemporalMeshAsset.Object);
        if (MapaTemporalMaterialAsset.Succeeded()) {
            MapaTemporal->SetMaterial(0, MapaTemporalMaterialAsset.Object);
        }
    }

}

// Called when the game starts or when spawned
void AMapaTemp::BeginPlay()
{
	Super::BeginPlay();
    LeerTriangulacion();
    CreateMesh();

    //LoadStations();
}

// Called every frame
void AMapaTemp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMapaTemp::CreateMesh() {
	Malla->CreateMeshSection_LinearColor(0, Vertexs, Triangles, Normals, UV0, VertexColors, Tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
    Malla->SetMaterial(0, MapaMaterial);
}

void AMapaTemp::UpdateMesh() {
	Malla->UpdateMeshSection_LinearColor(0, Vertexs, Normals, UV0, VertexColors, Tangents);
}

void AMapaTemp::LeerTriangulacion() {
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruL.1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruM.1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruH.1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruDepH.1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruLnode.txt");

	std::ifstream filevertexs ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruDepHH.1.node");

	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.node");

    UE_LOG(LogClass, Log, TEXT("leyendo triangulacion"));
	if (filevertexs.is_open()) {
        int temp;
        int nvertices;
        filevertexs >> nvertices;
        filevertexs >> temp;
        filevertexs >> temp;
        filevertexs >> temp;
        UE_LOG(LogClass, Log, TEXT("numero de vertices: %d"), nvertices);
        for (int i = 0; i < nvertices; i++) {
            filevertexs >> temp;
            float x;
            filevertexs >> x;
            float y;
            filevertexs >> y;
            filevertexs >> temp;
            filevertexs >> temp;
            Vertexs.Add(FVector(x, y, 0.0f));
        }
		filevertexs.close();
	}
    
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruL.1.ele");
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruM.1.ele");
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruH.1.ele");
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruDepH.1.ele");

	std::ifstream filetriangles ("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruDepHH.1.ele");
    
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.ele");
	if (filetriangles.is_open()) {
        int temp;
        int ntriangles;
        filetriangles >> ntriangles;
        filetriangles >> temp;
        filetriangles >> temp;

        UE_LOG(LogClass, Log, TEXT("numero de triangulos: %d"), ntriangles);
        for (int i = 0; i < ntriangles; i++) {
            filetriangles >> temp;
            int id1;
            int id2;
            int id3;
            filetriangles >> id1;
            filetriangles >> id2;
            filetriangles >> id3;
            Triangles.Add(id3-1);
            Triangles.Add(id2-1);
            Triangles.Add(id1-1);
        }
		filetriangles.close();
	}

	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		Normals.Add(FVector(0, 0, 1));
	}
 
	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		//UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY));
		UV0.Add(FVector2D(Vertexs[i].X/200, Vertexs[i].Y/200));
	}
 
 
	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		Tangents.Add(FProcMeshTangent(0, 1, 0));
	}
 
	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		VertexColors.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));
	}
}

float AMapaTemp::FuncionGauss(FVector b, FVector x) {//x es el punto a compronobar, b es el punto centro de la campana
    //debo tomar en cuanta solo x y y de cada punto
    b.Z = 0.0f;
    x.Z = 0.0f;
    a = 1 / FMath::Sqrt(2 * PI);
    //c = 50.0f;
    float res = a * FMath::Exp(-FMath::Pow((b - x).Size(), 2)/(2*c*c));
    return res;
}

void AMapaTemp::ElevarEn(FVector punto) {
    //se debe establecer un umbral general en esta elevacion, que afectara al calculo
    c2 = 200.0f;
    c = FMath::Sqrt(c2);
    for (int i = 0; i < Vertexs.Num(); i++) {
        if ((Vertexs[i] - punto).Size() < c2) {//en si tal vez esta comprbacion no se deba hacer
            Vertexs[i].Z = FuncionGauss(punto, Vertexs[i]) * FactorAltura;
        }
    }
}

void AMapaTemp::ElevarEn(FVector punto, float altura) {
    //se debe establecer un umbral general en esta elevacion, que afectara al calculo
    UE_LOG(LogClass, Log, TEXT("Elevando %f en : %f, %f, %f"), altura, punto.X, punto.Y, punto.Z);
    c2 = 100.0f;
    c = FMath::Sqrt(c2);
    for (int i = 0; i < Vertexs.Num(); i++) {
        if ((Vertexs[i] - punto).Size() < c2) {//en si tal vez esta comprbacion no se deba hacer
            Vertexs[i].Z = FMath::Max(FuncionGauss(punto, Vertexs[i]) * altura *2.5f, Vertexs[i].Z);
        }
    }
}

FVector AMapaTemp::LatitudLongitudToXY(float Latitud, float Longitud) {//yo usare la y para la latitud, conversion
	//referncia original
	// x = longitud del meridiano central de proyeccion * cos (latitud paralelo estandar);
	//y = latitud;
	//ohi1 toma valores de 0 a 1, depende del tipo de proyeccin, para la que uso toma el valor de 1
	//float DiametroEsfera = 6378137.0f;
	Latitud = FMath::DegreesToRadians(Latitud);
	Longitud = FMath::DegreesToRadians(Longitud);
	float DiametroEsfera = 1500.0f;
	/*float m0 = FMath::Cos(0.0f)/FMath::Sqrt(1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(0.0f), 2));//cual sera el valor de esto?
	m0 = 1;
	float qpart1 = FMath::Loge((1 - FMath::Exp(1)*FMath::Sin(Latitud))/(1 + FMath::Exp(1)*FMath::Sin(Latitud))) / (2*FMath::Exp(1));
	float qpart = (FMath::Sin(Latitud) / (1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(Latitud), 2))) - qpart1;
	float q = (1 - FMath::Exp(2))*qpart;
	float Longitud0 = FMath::DegreesToRadians(102);
	float Y = DiametroEsfera * m0 * (Longitud0 - Longitud);
	float X = DiametroEsfera * q / (2 * m0);*/

	float Y = DiametroEsfera * (Longitud - FMath::DegreesToRadians(-75.0f));//el 0 es el punto en el que es tangnte el cilindro
	float X = DiametroEsfera * FMath::Loge(FMath::Tan(PI/4 + Latitud/2));


	return FVector(X, Y, 0.0f);
	
}

void AMapaTemp::CreateStations(TArray<FStationStruct> NewStations) {
    Stations = NewStations;

	UWorld * const World = GetWorld();

    for (int i = 0; i < Stations.Num(); i++) {
        UE_LOG(LogTemp, Warning, TEXT("id_ghcn is: %s"), *Stations[i].id_ghcn);
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        FVector SpawnLocation(LatitudLongitudToXY(Stations[i].latitud, Stations[i].longitud));
        //convertir la ubicaion de la estacion
        float tempaltura = Stations[i].elevation;
        if (tempaltura > 700) {
            tempaltura -= 1600;
        }
        ElevarEn(SpawnLocation, tempaltura*0.04f);
        SpawnLocation.Z = tempaltura*0.04f;
        //ElevarEn(SpawnLocation, Stations[i].elevation*0.02f);
        //SpawnLocation.Z = Stations[i].elevation*0.02f;
        
        AStation * const InstancedStation = World->SpawnActor<AStation>(StationType, SpawnLocation, FRotator::ZeroRotator, SpawnParams);
        InstancedStation->Nombre->SetText(FText::FromString(Stations[i].name.TrimEnd()));
        InstancedStation->Latitud->SetText(FText::FromString("Latitud: " + FString::SanitizeFloat(Stations[i].latitud)));
        InstancedStation->Longitud->SetText(FText::FromString("Longitud: " + FString::SanitizeFloat(Stations[i].longitud)));
        InstancedStation->MostrarNombre();
        //NodoInstanciado->bActualizado = false;
        InstancedStation->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
    }
    UpdateMesh();
}

TArray<FVector2D> AMapaTemp::ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct>& Datos) {
    TArray<FVector2D> res;
    for (int i = 0; i < Datos.Num(); i++) {
        if (Datos[i].value != -9999) {
            res.Add(FVector2D(Datos[i].day, Datos[i].value));
        }
    }
    return res;
}

void AMapaTemp::GraficarSerieStation(TArray<FDailyDataStruct>& Datos) {
    TArray<FVector2D> puntos = ConvertirArrayDailyDataStructToFVector2D(Datos);
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        UE_LOG(LogClass, Log, TEXT("Graficando serie"));
    if (GeoGameMode) {
        //GeoGameMode->VisualizationHUDReference->CreateGrafica(puntos);
    }
    
}

void AMapaTemp::LoadStations() {
	/*TArray<AActor *> ConexionesEncontradas;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AClimateServerConnection::StaticClass(), ConexionesEncontradas);
	UE_LOG(LogClass, Log, TEXT("Numero de Conexiones Encontradas: %d"), ConexionesEncontradas.Num());
	for (int i = 0; i < ConexionesEncontradas.Num(); i++) {
		//if(GEngine)//no hacer esta verificación provocaba error al iniciar el editor
			//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Blue, TEXT("PARTES ENCONTRADAS"));
		UE_LOG(LogClass, Log, TEXT("Conexiones Encontradas %d"), i);
		AClimateServerConnection * const ConexionEncontrada = Cast<AClimateServerConnection>(ConexionesEncontradas[i]);
        if (ConexionEncontrada) {
            UE_LOG(LogClass, Log, TEXT("Conexiones Encontradas existente %d"), i);
            ConexionEncontrada->GetStations();
        }
	}*/

    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->GetStationsData();
    }
}