// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Components/SphereComponent.h"
#include <vector>
#include "PruebaProceduralMesh.generated.h"

class Triangulo {
    public:
        int IdA;
        int IdB;
        int IdC;
        FVector A;
        FVector B;
        FVector C;
        int Nivel;
        bool Orientacion;// 0 ahcia arriga, 1 hacia abajo, esto no se si lo use
        FVector Centro;
        float Phi;
        float Theta;
        Triangulo() {}
        Triangulo(int a, int b, int c, int niv, bool ori): IdA(a), IdB(b), IdC(c), Nivel(niv), Orientacion(ori) { }
        Triangulo(FVector va, FVector vb, FVector  vc, int niv, bool ori): A(va), B(vb), C(vc), Nivel(niv), Orientacion(ori) { }
        Triangulo(FVector va, FVector vb, FVector  vc, int a, int b, int c, int niv, bool ori): IdA(a), IdB(b), IdC(c), A(va), B(vb), C(vc), Nivel(niv), Orientacion(ori) { }
};

UCLASS()
class GEOVISUALIZATION_API APruebaProceduralMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APruebaProceduralMesh();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<FVector> VerticesNodos;

	TArray<FVector> VerticesPNodos;

	TArray<int32> TrianglesNodos;

	TArray<FVector> NormalsNodos;

	TArray<FVector2D> UV0Nodos;

	TArray<FProcMeshTangent> TangentsNodos;

	TArray<FLinearColor> VertexColorsNodos;

    std::vector<Triangulo> TriangulosNodo;

    std::vector<FVector> VerticesNodoTemplate;

    std::vector<FVector> VerticesPNodoTemplate;// phi, theta, radio

	std::vector<FVector> NormalsNodoTemplate;

	std::vector<FVector2D> UV0NodoTemplate;

	std::vector<FProcMeshTangent> TangentsNodoTemplate;

	std::vector<FLinearColor> VertexColorsNodoTemplate;

	TArray<FVector> VerticesAristas;

	TArray<int32> TrianglesAristas;

	TArray<FVector> NormalsAristas;

	TArray<FVector2D> UV0Aristas;

	TArray<FProcMeshTangent> TangentsAristas;

	TArray<FLinearColor> VertexColorsAristas;

    std::vector<FVector> VerticesAristaTemplate;

	TArray<int32> TrianglesAristaTemplate;

    std::vector<FVector> VerticesPAristaTemplate;// phi, theta, radio, no se ua en aristas

	std::vector<FVector> NormalsAristaTemplate;

	std::vector<FVector2D> UV0AristaTemplate;

	std::vector<FProcMeshTangent> TangentsAristaTemplate;

	std::vector<FLinearColor> VertexColorsAristaTemplate;

	virtual void PostActorCreated() override;

	virtual void PostLoad() override;

	void CreateTriangle();

	void CreateTriangles();

    void CreateSphereTemplate(int Precision);// crea esferas de radio 1

	void CreateMallaRegular(int N, int M);

    void AddArregloNodos(int N, int M);

    void CreateNodos();

	UFUNCTION(BlueprintCallable)
	void UpdateMallaRegular(int N, int M);

    FVector PuntoTFromAToB(FVector a, FVector b, float t);

    FVector PuntoTFromAToBEsferico(FVector a, FVector b, float t);

    void DividirTriangulo(Triangulo t);

    void DividirTriangulos();

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent * Malla;

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent * MallaArista;

    void AddNodo(FVector Posicion, float Radio, int NumNodo);
	
    void AddArista(FVector Source, FVector Target, int Precision, int Radio, int NumArista);// precision es cuanto lados tendra el cilindo, minimo 3, radio, sera el radio del cilindro, este template no es tan adaptable como el de la esfera, por eso necesai dos parametros

    void CreateAristas();

    TArray<USphereComponent *> ColisionesNodos;
	
};
