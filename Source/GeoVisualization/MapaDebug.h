// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Triangulacion/TriangleFace.h"
#include <vector>
#include "MapaDebug.generated.h"

UCLASS()
class GEOVISUALIZATION_API AMapaDebug : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapaDebug();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<FVector> PuntosMapa;

	TArray<FVector> PuntosDepartamento;

	TArray<FString> PuntosSDepartamento;

	TArray<FString> PuntosSMapa;

	std::vector<TriangleFace> Triangulos;

	//TArray<TriangleFace> TriangulosUnreal;//convertir a unreal triangles

	std::vector<FVector> Vertices;

	void LeerPuntos();

	void EscribirPuntos();

	void EscribirPuntosT();

	void LeerPrueba();

	void ConvertirStringstoVectorsPrueba();

	void LeerPuntosUE();

	void ConvertirStringstoVectors();

	void DrawPoints();

	void DrawVertices();

	void DrawTriangles();

	FVector LatitudLongitudToXY(float Latitud, float Longitud);

    //SCMapa

    TArray<FVector> PuntosSCMapa;

    TArray<int> PuntosPais;

    TArray<TArray<int>> PuntosDepartamentos;

    //quiza deberia tener objetos deparatemenos, limite, etc, para poder conservar los datos, como nombres, o referencias entre si
    //para probar se quedanar asi por ahora

    void LeerSCMapa();

    void ConvertirSCMapa();//este es para convertir a triangle, desparecera cuando se tenga el triangulador

	
//horizontal, latitud; verticla longitud
//en los textos, esta primero la latitud y luego la longitud

//para los cordenadas cartesianas, yo usare la latiud en el eje y, y la longitud en el eje X
	
	
};
