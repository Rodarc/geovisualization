// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include <vector>
#include "Materials/Material.h"
#include "ConexionServidor/ClimateServerConnection.h"
#include "Mapa/Station.h"
#include "Triangulacion/TriangleFace.h"
#include "Components/StaticMeshComponent.h"
#include "MapaTemp.generated.h"

UCLASS()
class GEOVISUALIZATION_API AMapaTemp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapaTemp();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<FVector> Vertexs;//Para el procedural mesh

	TArray<int32> Triangles;//Para el procedural mesh

	TArray<FVector> Normals;//Para el procedural mesh

	TArray<FVector2D> UV0;//Para el procedural mesh

	TArray<FProcMeshTangent> Tangents;//Para el procedural mesh

	TArray<FLinearColor> VertexColors;//Para el procedural mesh
	
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent * Malla;

    UPROPERTY(VisibleAnywhere)
    UStaticMeshComponent * MapaTemporal;
	
    void CreateMesh();

    void UpdateMesh();

    void LeerTriangulacion();

    //void ConvertirStringstoVectorsPrueba();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Transformation")
	UMaterial * MapaMaterial;
	
    float c;

    float c2;

    float a;

    float FactorAltura;

    float FuncionGauss(FVector b, FVector x);

    void ElevarEn(FVector punto);

    void ElevarEn(FVector punto, float altura);

	FVector LatitudLongitudToXY(float Latitud, float Longitud);

    void LoadStations();

    void CreateStations(TArray<FStationStruct> NewStations);

    TArray<FStationStruct> Stations;//esto no se si debe etar aqui o no

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stations")
    TSubclassOf<class AStation> StationType;//esto no es practio llenarlo en el cosntructor, cuando esta clase pase a bluprint sera mejor

    //estas funciones deberian pasar al Visualization HUD
    TArray<FVector2D> ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct> & Datos);

    void GraficarSerieStation(TArray<FDailyDataStruct>& Datos);
};
