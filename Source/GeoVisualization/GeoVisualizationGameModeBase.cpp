// Fill out your copyright notice in the Description page of Project Settings.

#include "GeoVisualizationGameModeBase.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

AGeoVisualizationGameModeBase::AGeoVisualizationGameModeBase() {
    PrimaryActorTick.bCanEverTick = false;//esto hacia falta en el tutprial, sin esto unreal no llama a la funcion tick en cada cuadro

    static ConstructorHelpers::FClassFinder<UVisualizationHUD> HUDWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/VisualizationHUDBP.VisualizationHUDBP_C'"));
    //deberia buscar una grafica
    if (HUDWidgetClass.Succeeded()) {
        TypeHUDWidget = HUDWidgetClass.Class;
    }
}

void AGeoVisualizationGameModeBase::BeginPlay() {
    Super::BeginPlay();

    if (TypeHUDWidget) {
        VisualizationHUDReference = CreateWidget<UVisualizationHUD>(GetWorld(), TypeHUDWidget);//le doy el mundo sobre el que se instancia, y lo que instanciare
        if (VisualizationHUDReference) {
            VisualizationHUDReference->AddToViewport(0);
            //VisualizationHUDReference->SetUserFocus()
        }
    }
    UWorld * World = GetWorld();
    if (World) {
        APlayerController * PlayerController = UGameplayStatics::GetPlayerController(World, 0);
        PlayerController->bShowMouseCursor = true;
        FInputModeGameAndUI Mode = FInputModeGameAndUI();
        Mode.SetHideCursorDuringCapture(false);
        //Mode.SetWidgetToFocus(VisualizationHUDReference->TakeWidget());
        //PlayerController->SetInputMode(FInputModeGameAndUI());
        PlayerController->SetInputMode(Mode);
        //para activar los eventos de Over sobre los obejtos 3d
        PlayerController->bEnableMouseOverEvents = true;
    }

	TArray<AActor *> MapasEncontrados;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMapaTemp::StaticClass(), MapasEncontrados);
	UE_LOG(LogClass, Log, TEXT("Numero de Mapas Encontradas: %d"), MapasEncontrados.Num());
    if (MapasEncontrados.Num()) {
		AMapaTemp * const Mapa = Cast<AMapaTemp>(MapasEncontrados[0]);
        if (Mapa) {
            UE_LOG(LogClass, Log, TEXT("Mapas Encontrada"));
            MapaTemp = Mapa;
        }
    }

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMapaDepartamentos::StaticClass(), MapasEncontrados);
	UE_LOG(LogClass, Log, TEXT("Numero de Mapas Encontradas: %d"), MapasEncontrados.Num());
    if (MapasEncontrados.Num()) {
		AMapaDepartamentos * const Mapa = Cast<AMapaDepartamentos>(MapasEncontrados[0]);
        if (Mapa) {
            UE_LOG(LogClass, Log, TEXT("Mapas Encontrada"));
            MapaDepartamentos = Mapa;
        }
    }

	TArray<AActor *> ConexionesExcontradas;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AClimateServerConnection::StaticClass(), ConexionesExcontradas);
	UE_LOG(LogClass, Log, TEXT("Numero de Conexiones encontradas: %d"), ConexionesExcontradas.Num());
    if (ConexionesExcontradas.Num()) {
		AClimateServerConnection * const Server = Cast<AClimateServerConnection>(ConexionesExcontradas[0]);
        if (Server) {
            UE_LOG(LogClass, Log, TEXT("Conexion Encontrada"));
            Conexion = Server;
        }
    }

	TArray<AActor *> MapsExcontrados;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AVisualizationMap::StaticClass(), MapsExcontrados);
	UE_LOG(LogClass, Log, TEXT("Numero de VisualizationMaps encontrados: %d"), MapsExcontrados.Num());
    if (MapsExcontrados.Num()) {
		AVisualizationMap * const VisMap = Cast<AVisualizationMap>(MapsExcontrados[0]);
        if (VisMap) {
            UE_LOG(LogClass, Log, TEXT("VisualizationMap encontrado"));
            VisualizationMapReference = VisMap;
            if (VisualizationHUDReference) {
                VisualizationHUDReference->VisualizationMapa = VisualizationMapReference;
            }
        }
    }
}

void AGeoVisualizationGameModeBase::Tick(float DeltaTime) {
}

void AGeoVisualizationGameModeBase::GetStationsData() {
    if (Conexion) {
        Conexion->GetStations();
    }
}

void AGeoVisualizationGameModeBase::RecibirStationsData(TArray<FStationStruct> Stations) {
    //debo pasarle las estaciones recibidias al mapa, que las pidio, esto vario sagun el nivel, quiza mapa deberia ser una jeraraquia por ahora trabajare solo con mapa temp
    /*if (MapaTemp) {
        MapaTemp->CreateStations(Stations);
    }*/
    /*if (VisualizationMapReference) {
        VisualizationMapReference->CreateStations(Stations);
    }*/
    if (VisualizationHUDReference) {
        VisualizationHUDReference->CreateStations(Stations);
    }
}

void AGeoVisualizationGameModeBase::GetTemperatureAverageDataYearMonthForStation(int idStation, int initialyear, int finalyear, TArray<int> months) {
    if (Conexion) {
        Conexion->GetTemperatureAvergeYearMonthForStation(idStation, initialyear, finalyear, months);
    }
}

void AGeoVisualizationGameModeBase::RecibirTemperatureData(TArray<FDailyDataStruct> Datos) {
    /*if (MapaTemp) {
        MapaTemp->GraficarSerieStation(Datos);
    }*/
    //esto lo hara las visualizacion
    if (VisualizationHUDReference) {
        //VisualizationHUDReference->RecibirConsulta(Datos);
    }
}

void AGeoVisualizationGameModeBase::GetVariableDataYearsMonthsForStation(int idStation, int initialyear, int finalyear, TArray<int> months, FString variable) {
    if (Conexion) {
        Conexion->GetVariableDataYearsMonthsForStation(idStation, initialyear, finalyear, months, variable);
    }
}

void AGeoVisualizationGameModeBase::RecibirVariableData(TArray<FDailyDataStruct> Datos, FString variable) {
    if (VisualizationHUDReference) {
        VisualizationHUDReference->RecibirConsulta(Datos, variable);
    }
}

void AGeoVisualizationGameModeBase::RecibirIntervaloTemperatureData(TArray<FDailyDataStruct> Datos) {
    if (VisualizationHUDReference && Datos.Num() >= 2) {//corregir esto, solo que exita datos, tomo el primer y el ultimo, si es es solo uno, pues temrina siendo el mismo a�o
        VisualizationHUDReference->SetIntervalYears(Datos[0].year, Datos[1].year);
        VisualizationHUDReference->ActualizarOpciones();
    }
}

void AGeoVisualizationGameModeBase::SetDataOptions() {//deberia ser un GetDataOptions
    if (Conexion) {
        Conexion->GetTemperatureAverageInterval();//pido los datos para las opciones, depende de las opciones
        //deberia pedir el primer y ultimo registro para otbetener el primer y ultimo a�o.
        //deberia pedir que variables tendre, y de ahi recien pedir el intervalo
    }
}




