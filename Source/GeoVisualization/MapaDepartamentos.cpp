// Fill out your copyright notice in the Description page of Project Settings.

#include "MapaDepartamentos.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Kismet/KismetSystemLibrary.h"
#include "Math/Color.h"
#include "Materials/Material.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "ConexionServidor/ClimateServerConnection.h"
#include "Engine/Engine.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
AMapaDepartamentos::AMapaDepartamentos() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    static ConstructorHelpers::FClassFinder<AStation> StationClass(TEXT("Class'/Script/GeoVisualization.Station'"));
    if (StationClass.Succeeded()) {
        if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Station type founded."));
        StationType = StationClass.Class;
    }

    static ConstructorHelpers::FClassFinder<ADepartamento> DepartamentoClass(TEXT("Class'/Script/GeoVisualization.Departamento'"));
    if (DepartamentoClass.Succeeded()) {
        if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Departamento type founded."));
        DepartamentoType = DepartamentoClass.Class;
    }
}

// Called when the game starts or when spawned
void AMapaDepartamentos::BeginPlay() {
	Super::BeginPlay();
	
    LeerSCMapa();
    CrearDepartamentos();
    //ConvertirSCMapaToTriangle();// este paso es solo para poder generar los archivos apra triangle, uan vez creados no es necesario
}

// Called every frame
void AMapaDepartamentos::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void AMapaDepartamentos::LeerSCMapa() {
	std::string line;
	std::ifstream myfile ("D:/AM/Circulo/Mapas/SCMapas/Peru.scmapa");
	if (myfile.is_open()) {
        UE_LOG(LogClass, Log, TEXT("Leyendo SC Mapa"));
        string nombrepais;
        getline(myfile, nombrepais);
        //myfile >> nombrepais;
        int numerodep;
        getline(myfile, line);
        numerodep = std::stoi(line);
        //myfile >> numerodep;
        int numerolim;
        getline(myfile, line);
        numerolim = std::stoi(line);
        //myfile >> numerolim;
        int numeropuntos;
        getline(myfile, line);
        numeropuntos = std::stoi(line);
        //myfile >> numeropuntos;
        PuntosSCMapa.SetNum(numeropuntos);
        for (int i = 0; i < numeropuntos; i++) {
            int id;
            getline(myfile, line);
            int space1 = line.find(' ');
            int space2 = line.find(' ', space1+1);
            id = std::stoi(line.substr(0, space1));
            //myfile >> id;
            PuntosSCMapa[id].Y = std::stof(line.substr(space1+1, space2 - space1 - 1));
            //myfile >> PuntosSCMapa[id].X;
            PuntosSCMapa[id].X = std::stof(line.substr(space2+1));//he cambiado este, antes este era Y
            //myfile >> PuntosSCMapa[id].Y;
            //UE_LOG(LogClass, Log, TEXT("Puntos %d: (%f, %f)"), id, PuntosSCMapa[id].X, PuntosSCMapa[id].Y);
        }
        UE_LOG(LogClass, Log, TEXT("Puntos SC Mapa leidos: %d"), numeropuntos);
        UE_LOG(LogClass, Log, TEXT("Leyendo Pais"));
        Pais.Name = FString::FString(nombrepais.c_str());
        getline(myfile, line);
        numeropuntos = std::stoi(line);
        //myfile >> numeropuntos;
        Pais.Puntos.SetNum(numeropuntos);
        for (int i = 0; i < numeropuntos; i++) {
            getline(myfile, line);
            Pais.Puntos[i] = std::stoi(line);
            //myfile >> Pais.Puntos[i];
        }
        UE_LOG(LogClass, Log, TEXT("Leyendo departamentos"));
        Departamentos.SetNum(numerodep);
        for (int k = 0; k < numerodep; k++) {
            string nombredepartamento;
            //myfile >> nombredepartamento;
            getline(myfile, nombredepartamento);
            Departamentos[k].Name = FString::FString(nombredepartamento.c_str());
            getline(myfile, line);
            //myfile >> numeropuntos;
            numeropuntos = std::stoi(line);
            Departamentos[k].Puntos.SetNum(numeropuntos);
            for (int i = 0; i < numeropuntos; i++) {
                getline(myfile, line);
                Departamentos[k].Puntos[i] = std::stoi(line);
            }
        }

        UE_LOG(LogClass, Log, TEXT("Leyendo limites"));
        Limites.SetNum(numerolim);
        for (int k = 0; k < numerolim; k++) {
            string nombrelimite;
            //myfile >> nombrelimite;
            getline(myfile, nombrelimite);
            Limites[k].Name = FString::FString(nombrelimite.c_str());
            //myfile >> nombrelimite;
            getline(myfile, nombrelimite);
            Limites[k].NameL = FString::FString(nombrelimite.c_str());
            //myfile >> nombrelimite;
            getline(myfile, nombrelimite);
            Limites[k].NameR = FString::FString(nombrelimite.c_str());
            getline(myfile, line);
            numeropuntos = std::stoi(line);
            //myfile >> numeropuntos;
            Limites[k].Puntos.SetNum(numeropuntos);
            for (int i = 0; i < numeropuntos; i++) {
                getline(myfile, line);
                Limites[k].Puntos[i] = std::stoi(line);
            }
        }
        UE_LOG(LogClass, Log, TEXT("Finalizado"));
		myfile.close();
	}
}

void AMapaDepartamentos::ConvertirSCMapaToTriangle() {
    std::ofstream myfile("D:/AM/Circulo/Mapas/SCMapas/TriangleInput/Peru.poly");
    if (myfile.is_open()) {
        //impresion de los puntos
        //myfile << PuntosMapa.Num() << " 2 1 0" << endl;
        myfile << PuntosSCMapa.Num() << " 2 1 0" << endl;
        for (int i = 0; i < PuntosSCMapa.Num(); i++) {
            FVector punto = LatitudLongitudToXY(PuntosSCMapa[i].X, PuntosSCMapa[i].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
            myfile << i + 1 << " " << punto.X << " " << punto.Y << " 1" << endl;
        }

        //impresion de las aristas
        //el numero de aristas depende, pero para el hull de una figura es igual al numero de vertices de ese hull
        myfile << Pais.Puntos.Num() << " 0" << endl;
        for (int i = 0; i < Pais.Puntos.Num() - 1; i++) {
            //myfile << i + 1 << " " << i + 1 << " " << i + 2 << endl;
            myfile << i + 1 << " " << Pais.Puntos[i] + 1 << " " << Pais.Puntos[i + 1] + 1 << endl;
        }
        myfile << Pais.Puntos.Num() << " " << Pais.Puntos.Num() << " " << 1 << endl;
        myfile << "0 0 0" << endl;
        myfile.close();
    }
    for (int k = 0; k < Departamentos.Num(); k++) {
        std::ofstream myfiledep("D:/AM/Circulo/Mapas/SCMapas/TriangleInput/Departamento-" + to_string(k) + ".poly");
        if (myfiledep.is_open()) {
            //impresion de los puntos
            //myfile << PuntosMapa.Num() << " 2 1 0" << endl;
            myfiledep << Departamentos[k].Puntos.Num() << " 2 1 0" << endl;
            for (int i = 0; i < Departamentos[k].Puntos.Num(); i++) {
                //FVector punto = LatitudLongitudToXY(PuntosSCMapa[Departamentos[k].Puntos[i]].X, PuntosSCMapa[Departamentos[k].Puntos[i]].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
                FVector punto = LatitudLongitudToXY(PuntosSCMapa[Departamentos[k].Puntos[i]].X, PuntosSCMapa[Departamentos[k].Puntos[i]].Y) - LatitudLongitudToXY(PuntosSCMapa[Departamentos[k].Puntos[0]].X, PuntosSCMapa[Departamentos[k].Puntos[0]].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
                myfiledep << i + 1 << " " << punto.X << " " << punto.Y << " 1" << endl;
            }

            //impresion de las aristas
            //el numero de aristas depende, pero para el hull de una figura es igual al numero de vertices de ese hull
            myfiledep << Departamentos[k].Puntos.Num() << " 0" << endl;
            for (int i = 0; i < Departamentos[k].Puntos.Num() - 1; i++) {
                myfile << i + 1 << " " << i + 1 << " " << i + 2 << endl;
                //myfile << i + 1 << " " << Pais.Puntos[i] + 1 << " " << Pais.Puntos[i + 1] + 1 << endl;
            }
            myfiledep << Departamentos[k].Puntos.Num() << " " << Departamentos[k].Puntos.Num() << " " << 1 << endl;
            myfiledep << "0 0 0" << endl;
            myfiledep.close();
        }
    }
}

FVector AMapaDepartamentos::LatitudLongitudToXY(float Latitud, float Longitud) {//yo usare la y para la latitud, conversion
	//referncia original
	// x = longitud del meridiano central de proyeccion * cos (latitud paralelo estandar);
	//y = latitud;
	//ohi1 toma valores de 0 a 1, depende del tipo de proyeccin, para la que uso toma el valor de 1
	//float DiametroEsfera = 6378137.0f;
	Latitud = FMath::DegreesToRadians(Latitud);
	Longitud = FMath::DegreesToRadians(Longitud);
	float DiametroEsfera = 1500.0f;
	/*float m0 = FMath::Cos(0.0f)/FMath::Sqrt(1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(0.0f), 2));//cual sera el valor de esto?
	m0 = 1;
	float qpart1 = FMath::Loge((1 - FMath::Exp(1)*FMath::Sin(Latitud))/(1 + FMath::Exp(1)*FMath::Sin(Latitud))) / (2*FMath::Exp(1));
	float qpart = (FMath::Sin(Latitud) / (1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(Latitud), 2))) - qpart1;
	float q = (1 - FMath::Exp(2))*qpart;
	float Longitud0 = FMath::DegreesToRadians(102);
	float Y = DiametroEsfera * m0 * (Longitud0 - Longitud);
	float X = DiametroEsfera * q / (2 * m0);*/

	float Y = DiametroEsfera * (Longitud - FMath::DegreesToRadians(-75.0f));//el 0 es el punto en el que es tangnte el cilindro
	float X = DiametroEsfera * FMath::Loge(FMath::Tan(PI/4 + Latitud/2));

	return FVector(X, Y, 0.0f);
}
/*
    //UE_LOG(LogClass, Log, TEXT("%s"), Pais.Name);
    UE_LOG(LogClass, Log, TEXT("Puntos:"));
    for (int i = 0; i < Pais.Puntos.Num(); i++) {
        UE_LOG(LogClass, Log, TEXT("%d"), Pais.Puntos[i]);
    }

    for (int k = 0; k < Departamentos.Num(); k++) {
        //UE_LOG(LogClass, Log, TEXT("%s"), Departamentos[k].Name);
        UE_LOG(LogClass, Log, TEXT("Puntos:"));
        for (int i = 0; i < Departamentos[k].Puntos.Num(); i++) {
            UE_LOG(LogClass, Log, TEXT("%d"), Departamentos[k].Puntos[i]);
        }
    }

    for (int k = 0; k < Limites.Num(); k++) {
        //UE_LOG(LogClass, Log, TEXT("%s"), Limites[k].Name);
        UE_LOG(LogClass, Log, TEXT("Puntos:"));
        for (int i = 0; i < Limites[k].Puntos.Num(); i++) {
            UE_LOG(LogClass, Log, TEXT("%d"), Limites[k].Puntos[i]);
        }
    }
*/

void AMapaDepartamentos::CrearDepartamentos() {
    //esta funcion se ejecuta despues de haber consultado el mapa y obtenido la informacion
	UWorld * const World = GetWorld();

    for (int i = 0; i < Departamentos.Num(); i++) {
        UE_LOG(LogTemp, Warning, TEXT("Departamento: %s"), *Departamentos[i].Name);
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        FVector SpawnLocation(LatitudLongitudToXY(PuntosSCMapa[Departamentos[i].Puntos[0]].X, PuntosSCMapa[Departamentos[i].Puntos[0]].Y));
        //convertir la ubicaion de la estacion
        
        ADepartamento * const InstancedDepartamento = World->SpawnActor<ADepartamento>(DepartamentoType, SpawnLocation, FRotator::ZeroRotator, SpawnParams);
        InstancedDepartamento->File = "Departamento-" + to_string(i);
        InstancedDepartamento->LeerTriangulacion();
        //NodoInstanciado->bActualizado = false;
        InstancedDepartamento->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        InstancedDepartamento->CreateMesh();
    }
}