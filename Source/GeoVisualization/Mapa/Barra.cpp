// Fill out your copyright notice in the Description page of Project Settings.

#include "Barra.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/Material.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/WidgetComponent.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "../GeoVisualizationGameModeBase.h"
#include "Station.h"
#include "../Interfaz/VisualizationHUD.h"


// Sets default values
ABarra::ABarra() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

    static ConstructorHelpers::FObjectFinder<UStaticMesh> BarraMeshAsset(TEXT("StaticMesh'/Game/Meshes/BarraMesh.BarraMesh'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	static ConstructorHelpers::FObjectFinder<UMaterial> BarraMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/StationMaterial.StationMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera

	BarraMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BarraMesh"));
    BarraMesh->SetupAttachment(RootComponent);
	//BarraMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
    BarraMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
    BarraMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);//para poner el tipo de objeto en Station, que es el primero de los personalizables
    //StationMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_EngineTraceChannel2, ECollisionResponse::ECR_Overlap);

    if (BarraMeshAsset.Succeeded()) {
        BarraMesh->SetStaticMesh(BarraMeshAsset.Object);
        if (BarraMaterialAsset.Succeeded()) {
            BarraMesh->SetMaterial(0, BarraMaterialAsset.Object);
            //StationMaterialDynamic = StationMesh->CreateDynamicMaterialInstance(0, StationMaterialAsset.Object);//el puntero lo guardare en una variable
            //StationMaterialDynamic = UMaterialInstanceDynamic::Create(StationMaterialAsset.Object, StationMesh);
            //StationMesh->SetMaterial(0, StationMaterialDynamic);
        }
    }

    ColorComponentH = 224.5f;
    ColorComponentS = 1.0f;
    ColorComponentV = 0.4f;
    ColorComponentVBase = 0.015;
    ColorComponentVDeltaHover = 0.25f;
    Opacity = 1.0f;

    AlturaMaxima = 50.0f;
    AlturaMinima = 0.0f;
    AlturaActual = AlturaMaxima;

    EscalaMinima = 0.05;
    EscalaMaxima = 1.0f;

    TiempoAnimacion = 0.5f;
    bAnimando = false;
    bVisible = true;
}

// Called when the game starts or when spawned
void ABarra::BeginPlay() {
	Super::BeginPlay();
	
    BarraMaterialDynamic = BarraMesh->CreateDynamicMaterialInstance(0, BarraMesh->GetMaterial(0));//el puntero lo guardare en una variable
    BarraMesh->SetMaterial(0, BarraMaterialDynamic);
}

// Called every frame
void ABarra::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void ABarra::CambiarColor(FLinearColor NewColor) {
    if (BarraMaterialDynamic != nullptr) {
        BarraMaterialDynamic->SetVectorParameterValue(TEXT("Color"), NewColor);// a que pareametro del material se le asiganara el nuevo color
    }
}

void ABarra::ActualizarColor() {
    FLinearColor Color = UKismetMathLibrary::HSVToRGB(ColorComponentH, ColorComponentS, ColorComponentV, 1.0f);
    CambiarColor(Color);
}

void ABarra::CambiarOpacidad(float NewOpacity) {
}

void ABarra::ActualizarOpacidad() {
}

void ABarra::Click_Implementation() {
}

void ABarra::DoubleClick_Implementation() {
}

void ABarra::Press_Implementation() {
}

void ABarra::Release_Implementation() {
}

void ABarra::Hover_Implementation() {
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        Station->MostrarHistograma(HistogramaWidget);
        //GeoGameMode->VisualizationHUDReference->StationWidgets[Station->Id]->ReplaceGrafica();
        //UGraficaDetalleScrollItemWidget * DetalleScrollItem = GeoGameMode->VisualizationHUDReference->CreateGraficaDetalle(this);
    }
}

void ABarra::Unhover_Implementation() {
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        //GeoGameMode->VisualizationHUDReference->StationWidgets[Station->Id]->RestoreGrafica();
        Station->OcultarHistograma();
        //UGraficaDetalleScrollItemWidget * DetalleScrollItem = GeoGameMode->VisualizationHUDReference->CreateGraficaDetalle(this);
    }
}

void ABarra::Select_Implementation() {
}

void ABarra::Unselect_Implementation() {
}

void ABarra::Hide_Implementation() {
    BarraMesh->SetVisibility(false);
    bVisible = false;
}

void ABarra::Show_Implementation() {
    BarraMesh->SetVisibility(true);
    bVisible = true;
}

void ABarra::EscalarToAltura(float Altura) {
    float Escala = Altura * (EscalaMaxima - EscalaMinima) + EscalaMinima;
    //BarraMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, Altura));
    BarraMesh->SetRelativeScale3D(FVector(1.0f, 1.0f, Escala));
}

void ABarra::CreateHistograma() {
    HistogramaWidget = CreateWidget<UHistogramaWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeHistograma);
    //debo agregar esta visualizacion al contenedor
}

void ABarra::ShowHistograma() {
    HistogramaWidget->AddToViewport(0);//no ira
    //pasarselo a la estacion o que la escion haga que se muestre
}

void ABarra::HideHistograma() {
}

