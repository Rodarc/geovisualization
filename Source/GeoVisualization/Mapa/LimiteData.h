// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class GEOVISUALIZATION_API LimiteData
{
public:
    FString Name;
    FString NameL;
    FString NameR;
    TArray<int> Puntos;
	LimiteData();
	~LimiteData();
};
