// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class GEOVISUALIZATION_API PaisData
{
public:
    FString Name;
    TArray<int> Puntos;
	PaisData();
	~PaisData();
};
