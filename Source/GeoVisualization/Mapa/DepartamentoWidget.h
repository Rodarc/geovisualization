// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DepartamentoWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UDepartamentoWidget : public UUserWidget
{
	GENERATED_BODY()

public:
    UDepartamentoWidget(const FObjectInitializer & ObjectInitializer);

    //virtual void NativeConstruct() override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mapa")
    FString DepartamentoName;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bVisible;
	
    void SetInformation(FString InDepartamentonName);//o deberia recibir la estructura?
	
};
