// Fill out your copyright notice in the Description page of Project Settings.

#include "VisualizationMap.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/Color.h"
#include "Materials/Material.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "Station.h"
#include "Components/StaticMeshComponent.h"
#include "../GeoVisualizationGameModeBase.h"


// Sets default values
AVisualizationMap::AVisualizationMap() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    TiempoElevacion = 1.0f;

    //creando los componentes de los mesh procedurales
	MeshPais = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("MeshPais"));
	RootComponent = MeshPais;
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Components|ProceduralMesh", meta = (AutoCreateRefTerm = "Normals,UV0,VertexColors,Tangents"))
	//	void CreateMeshSection(int32 SectionIndex, const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FVector>& Normals,
	// const TArray<FVector2D>& UV0, const TArray<FColor>& VertexColors, const TArray<FProcMeshTangent>& Tangents, bool bCreateCollision);
 
	// New in UE 4.17, multi-threaded PhysX cooking.
	MeshPais->bUseAsyncCooking = true;

	MeshPaisElevaciones = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("MeshPaisElevaciones"));
    MeshPaisElevaciones->SetupAttachment(RootComponent);
    MeshPaisElevaciones->SetRelativeLocation(FVector(0.0f, 0.0f, -0.01f));
	MeshPaisElevaciones->bUseAsyncCooking = true;
    MeshPaisElevaciones->SetVisibility(false);


    //buscando los materiales
	static ConstructorHelpers::FObjectFinder<UMaterial> MapaMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/MapaMaterial.MapaMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (MapaMaterialAsset.Succeeded()) {
        PaisMaterial = MapaMaterialAsset.Object;
    }

    static ConstructorHelpers::FObjectFinder<UMaterial> ElevacionesMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/MapaTransparentMaterial.MapaTransparentMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    //static ConstructorHelpers::FObjectFinder<UMaterial> ElevacionesMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/MapaTransparent2Material.MapaTransparent2Material'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (ElevacionesMaterialAsset.Succeeded()) {
        ElevacionesMaterial = ElevacionesMaterialAsset.Object;
    }



    //buscando los tipos de clases
    static ConstructorHelpers::FClassFinder<AStation> StationClass(TEXT("Class'/Script/GeoVisualization.Station'"));
    //static ConstructorHelpers::FClassFinder<AStation> StationClass(TEXT("BlueprintGeneratedClass'/Game/GeoVisualization/Blueprints/Station_BP.Station_BP_C'"));
    if (StationClass.Succeeded()) {
        /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Station type founded."));*/
        StationType = StationClass.Class;
    }

    static ConstructorHelpers::FClassFinder<ADepartamento> DepartamentoClass(TEXT("Class'/Script/GeoVisualization.Departamento'"));
    if (DepartamentoClass.Succeeded()) {
        /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Departamento type founded."));*/
        DepartamentoType = DepartamentoClass.Class;
    }

	PathTriangle = "D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/";
    FileTriangle = "Peru";//deberia ser pais

    AlturaMinima = 0.0f;
    AlturaMaxima = 100.0f;
    ValorMinimo = 0.0f;
    ValorMaximo = 1000.0f;

    HueMinimo = 240.0f;
    HueMaximo = 0.0f;

	/*ColoresElevaciones.Add(FLinearColor(184.0f/256.0f, 224.0f/256.0f, 255.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(244.0f/256.0f, 224.0f/256.0f, 84.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(238.0f/256.0f, 164.0f/256.0f, 84.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(198.0f/256.0f, 84.0f/256.0f, 84.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(151.0f/256.0f, 84.0f/256.0f, 117.0f/256.0f, 1.0f));*/

	ColoresElevaciones.Add(FLinearColor(0.0f/256.0f, 101.0f/256.0f, 217.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(232.0f/256.0f, 151.0f/256.0f, 0.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(230.0f/256.0f, 72.0f/256.0f, 0.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(97.0f/256.0f, 14.0f/256.0f, 12.0f/256.0f, 1.0f));
	ColoresElevaciones.Add(FLinearColor(78.0f/256.0f, 26.0f/256.0f, 69.0f/256.0f, 1.0f));

	RangosElevaciones.Add(0.0f);
	RangosElevaciones.Add(50.0f);
	RangosElevaciones.Add(200.0f);
	RangosElevaciones.Add(1000.0f);
	RangosElevaciones.Add(3000.0f);
	RangosElevaciones.Add(5000.0f);
}

// Called when the game starts or when spawned
void AVisualizationMap::BeginPlay() {
	Super::BeginPlay();
	
    LeerSCMapa();
    //ConvertirSCMapaToTriangle();// este paso es solo para poder generar los archivos apra triangle, uan vez creados no es necesario
    LeerTriangulacionPais();
    LeerTriangulacionPaisElevaciones();
    //CreateMeshPais();
    CreateMeshPaisElevaciones();
    CrearDepartamentos();//cuando no use el mesh del pais usare esta funcion
}

// Called every frame
void AVisualizationMap::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
    if (bAnimando) {
        TiempoActualElevacion += DeltaTime;
        for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {
            //VertexsMeshPaisElevaciones[i].Z = FMath::Lerp(0.0f, ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion);
            //VertexsMeshPaisElevaciones[i].Z = FMath::InterpCircularIn(0.0f, ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion);//error
            //VertexsMeshPaisElevaciones[i].Z = FMath::InterpEaseIn(0.0f, ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion, 2);//bueno
            //VertexsMeshPaisElevaciones[i].Z = FMath::InterpEaseOut(0.0f, ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion, 2);//muy rapido, termina lento, bonito
            //VertexsMeshPaisElevaciones[i].Z = FMath::InterpEaseInOut(0.0f, ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion, 2);//muy bueno
            //VertexsMeshPaisElevaciones[i].Z = FMath::InterpSinIn(0.0f, ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion);

            VertexsMeshPaisElevaciones[i].Z = FMath::InterpEaseInOut(ElevacionesInicio[i], ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion, 2);//muy bueno

            FLinearColor Color = UKismetMathLibrary::HSVToRGB(FMath::Lerp(HueMinimo, HueMaximo, (VertexsMeshPaisElevaciones[i].Z - AlturaMinima) / (AlturaMaxima - AlturaMinima)), 1.0f, 1.0f, 1.0f);
            VertexColorsMeshPaisElevaciones[i] = Color;

            //VertexsMeshPaisElevaciones[i].Z = FMath::InterpEaseInOut(ElevacionesInicio[i], ElevacionesObjetivo[i], TiempoActualElevacion / TiempoElevacion, 2);//muy bueno
        }
        if (TiempoActualElevacion >= TiempoElevacion) {
            bAnimando = false;
        }
        UpdateNormalsMeshPaisElevaciones();
        UpdateMeshPaisElevaciones();
    }

}

void AVisualizationMap::LeerSCMapa() {
	std::string line;
	std::ifstream myfile ("D:/AM/Circulo/Mapas/SCMapas/Peru.scmapa");
	if (myfile.is_open()) {
        UE_LOG(LogClass, Log, TEXT("Leyendo SC Mapa"));
        string nombrepais;
        getline(myfile, nombrepais);
        //myfile >> nombrepais;
        int numerodep;
        getline(myfile, line);
        numerodep = std::stoi(line);
        //myfile >> numerodep;
        int numerolim;
        getline(myfile, line);
        numerolim = std::stoi(line);
        //myfile >> numerolim;
        int numeropuntos;
        getline(myfile, line);
        numeropuntos = std::stoi(line);
        //myfile >> numeropuntos;
        PuntosSCMapa.SetNum(numeropuntos);
        for (int i = 0; i < numeropuntos; i++) {
            int id;
            getline(myfile, line);
            int space1 = line.find(' ');
            int space2 = line.find(' ', space1+1);
            id = std::stoi(line.substr(0, space1));
            //myfile >> id;
            PuntosSCMapa[id].Y = std::stof(line.substr(space1+1, space2 - space1 - 1));
            //myfile >> PuntosSCMapa[id].X;
            PuntosSCMapa[id].X = std::stof(line.substr(space2+1));//he cambiado este, antes este era Y
            //myfile >> PuntosSCMapa[id].Y;
            //UE_LOG(LogClass, Log, TEXT("Puntos %d: (%f, %f)"), id, PuntosSCMapa[id].X, PuntosSCMapa[id].Y);
        }
        UE_LOG(LogClass, Log, TEXT("Puntos SC Mapa leidos: %d"), numeropuntos);
        UE_LOG(LogClass, Log, TEXT("Leyendo Pais"));
        Pais.Name = FString::FString(nombrepais.c_str());
        getline(myfile, line);
        numeropuntos = std::stoi(line);
        //myfile >> numeropuntos;
        Pais.Puntos.SetNum(numeropuntos);
        for (int i = 0; i < numeropuntos; i++) {
            getline(myfile, line);
            Pais.Puntos[i] = std::stoi(line);
            //myfile >> Pais.Puntos[i];
        }
        UE_LOG(LogClass, Log, TEXT("Leyendo departamentos"));
        Departamentos.SetNum(numerodep);
        for (int k = 0; k < numerodep; k++) {
            string nombredepartamento;
            //myfile >> nombredepartamento;
            getline(myfile, nombredepartamento);
            Departamentos[k].Name = FString::FString(nombredepartamento.c_str());
            getline(myfile, line);
            //myfile >> numeropuntos;
            numeropuntos = std::stoi(line);
            Departamentos[k].Puntos.SetNum(numeropuntos);
            for (int i = 0; i < numeropuntos; i++) {
                getline(myfile, line);
                Departamentos[k].Puntos[i] = std::stoi(line);
            }
        }

        UE_LOG(LogClass, Log, TEXT("Leyendo limites"));
        Limites.SetNum(numerolim);
        for (int k = 0; k < numerolim; k++) {
            string nombrelimite;
            //myfile >> nombrelimite;
            getline(myfile, nombrelimite);
            Limites[k].Name = FString::FString(nombrelimite.c_str());
            //myfile >> nombrelimite;
            getline(myfile, nombrelimite);
            Limites[k].NameL = FString::FString(nombrelimite.c_str());
            //myfile >> nombrelimite;
            getline(myfile, nombrelimite);
            Limites[k].NameR = FString::FString(nombrelimite.c_str());
            getline(myfile, line);
            numeropuntos = std::stoi(line);
            //myfile >> numeropuntos;
            Limites[k].Puntos.SetNum(numeropuntos);
            for (int i = 0; i < numeropuntos; i++) {
                getline(myfile, line);
                Limites[k].Puntos[i] = std::stoi(line);
            }
        }
        UE_LOG(LogClass, Log, TEXT("Finalizado"));
		myfile.close();
	}
}

void AVisualizationMap::ConvertirSCMapaToTriangle() {
    std::ofstream myfile("D:/AM/Circulo/Mapas/SCMapas/TriangleInput/Peru.poly");//deberia ser Pais no peru
    if (myfile.is_open()) {
        //impresion de los puntos
        //myfile << PuntosMapa.Num() << " 2 1 0" << endl;
        myfile << PuntosSCMapa.Num() << " 2 1 0" << endl;
        for (int i = 0; i < PuntosSCMapa.Num(); i++) {
            FVector punto = LatitudLongitudToXY(PuntosSCMapa[i].X, PuntosSCMapa[i].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
            myfile << i + 1 << " " << punto.X << " " << punto.Y << " 1" << endl;
        }

        //impresion de las aristas
        //el numero de aristas depende, pero para el hull de una figura es igual al numero de vertices de ese hull
        myfile << Pais.Puntos.Num() << " 0" << endl;
        for (int i = 0; i < Pais.Puntos.Num() - 1; i++) {
            //myfile << i + 1 << " " << i + 1 << " " << i + 2 << endl;
            myfile << i + 1 << " " << Pais.Puntos[i] + 1 << " " << Pais.Puntos[i + 1] + 1 << endl;
        }
        myfile << Pais.Puntos.Num() << " " << Pais.Puntos.Num() << " " << 1 << endl;
        myfile << "0 0 0" << endl;
        myfile.close();
    }
    for (int k = 0; k < Departamentos.Num(); k++) {
        std::ofstream myfiledep("D:/AM/Circulo/Mapas/SCMapas/TriangleInput/Departamento-" + to_string(k) + ".poly");
        if (myfiledep.is_open()) {
            //impresion de los puntos
            //myfile << PuntosMapa.Num() << " 2 1 0" << endl;
            myfiledep << Departamentos[k].Puntos.Num() << " 2 1 0" << endl;
            for (int i = 0; i < Departamentos[k].Puntos.Num(); i++) {
                //FVector punto = LatitudLongitudToXY(PuntosSCMapa[Departamentos[k].Puntos[i]].X, PuntosSCMapa[Departamentos[k].Puntos[i]].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
                FVector punto = LatitudLongitudToXY(PuntosSCMapa[Departamentos[k].Puntos[i]].X, PuntosSCMapa[Departamentos[k].Puntos[i]].Y) - LatitudLongitudToXY(PuntosSCMapa[Departamentos[k].Puntos[0]].X, PuntosSCMapa[Departamentos[k].Puntos[0]].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
                myfiledep << i + 1 << " " << punto.X << " " << punto.Y << " 1" << endl;
            }

            //impresion de las aristas
            //el numero de aristas depende, pero para el hull de una figura es igual al numero de vertices de ese hull
            myfiledep << Departamentos[k].Puntos.Num() << " 0" << endl;
            for (int i = 0; i < Departamentos[k].Puntos.Num() - 1; i++) {
                myfiledep << i + 1 << " " << i + 1 << " " << i + 2 << endl;
                //myfile << i + 1 << " " << Pais.Puntos[i] + 1 << " " << Pais.Puntos[i + 1] + 1 << endl;
            }
            myfiledep << Departamentos[k].Puntos.Num() << " " << Departamentos[k].Puntos.Num() << " " << 1 << endl;
            myfiledep << "0 0 0" << endl;
            myfiledep.close();
        }
    }
}

void AVisualizationMap::CreateMeshPais() {
	MeshPais->CreateMeshSection_LinearColor(0, VertexsMeshPais, TrianglesMeshPais, NormalsMeshPais, UV0MeshPais, VertexColorsMeshPais, TangentsMeshPais, true);
    // Enable collision data
	MeshPais->ContainsPhysicsTriMeshData(true);
    MeshPais->SetMaterial(0, PaisMaterial);
}

void AVisualizationMap::UpdateMeshPais() {
	MeshPais->UpdateMeshSection_LinearColor(0, VertexsMeshPais, NormalsMeshPais, UV0MeshPais, VertexColorsMeshPais, TangentsMeshPais);
}

void AVisualizationMap::LeerTriangulacionPais() {
    std::ifstream filevertexs(PathTriangle + FileTriangle + ".1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Peru.1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.node");

    UE_LOG(LogClass, Log, TEXT("leyendo triangulacion"));
	if (filevertexs.is_open()) {
        int temp;
        int nvertices;
        filevertexs >> nvertices;
        filevertexs >> temp;
        filevertexs >> temp;
        filevertexs >> temp;
        UE_LOG(LogClass, Log, TEXT("numero de vertices: %d"), nvertices);
        for (int i = 0; i < nvertices; i++) {
            filevertexs >> temp;
            float x;
            filevertexs >> x;
            float y;
            filevertexs >> y;
            filevertexs >> temp;
            filevertexs >> temp;
            VertexsMeshPais.Add(FVector(x, y, 0.0f));
        }
		filevertexs.close();
	}

    std::ifstream filetriangles(PathTriangle + FileTriangle + ".1.ele");
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.ele");
    
	if (filetriangles.is_open()) {
        int temp;
        int ntriangles;
        filetriangles >> ntriangles;
        filetriangles >> temp;
        filetriangles >> temp;

        UE_LOG(LogClass, Log, TEXT("numero de triangulos: %d"), ntriangles);
        for (int i = 0; i < ntriangles; i++) {
            filetriangles >> temp;
            int id1;
            int id2;
            int id3;
            filetriangles >> id1;
            filetriangles >> id2;
            filetriangles >> id3;
            TrianglesMeshPais.Add(id3-1);
            TrianglesMeshPais.Add(id2-1);
            TrianglesMeshPais.Add(id1-1);
        }
		filetriangles.close();
	}

	for (int i = 0; i < VertexsMeshPais.Num(); i++) {// o < N*M
		NormalsMeshPais.Add(FVector(0, 0, 1));
	}
 
	for (int i = 0; i < VertexsMeshPais.Num(); i++) {// o < N*M
		//UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY));
		UV0MeshPais.Add(FVector2D(VertexsMeshPais[i].X/200, VertexsMeshPais[i].Y/200));
	}
 
 
	for (int i = 0; i < VertexsMeshPais.Num(); i++) {// o < N*M
		TangentsMeshPais.Add(FProcMeshTangent(0, 1, 0));
	}
 
	for (int i = 0; i < VertexsMeshPais.Num(); i++) {// o < N*M
		VertexColorsMeshPais.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));
	}
}

void AVisualizationMap::CreateMeshPaisElevaciones() {
	MeshPaisElevaciones->CreateMeshSection_LinearColor(0, VertexsMeshPaisElevaciones, TrianglesMeshPaisElevaciones, NormalsMeshPaisElevaciones, UV0MeshPaisElevaciones, VertexColorsMeshPaisElevaciones, TangentsMeshPaisElevaciones, true);
    // Enable collision data
	MeshPaisElevaciones->ContainsPhysicsTriMeshData(false);
    MeshPaisElevaciones->SetMaterial(0, ElevacionesMaterial);
}

void AVisualizationMap::UpdateMeshPaisElevaciones() {
	MeshPaisElevaciones->UpdateMeshSection_LinearColor(0, VertexsMeshPaisElevaciones, NormalsMeshPaisElevaciones, UV0MeshPaisElevaciones, VertexColorsMeshPaisElevaciones, TangentsMeshPaisElevaciones);
}

void AVisualizationMap::UpdateNormalsMeshPaisElevaciones() {
    for(int i = 0; i < NormalsMeshPaisElevaciones.Num(); i++){
        NormalsMeshPaisElevaciones[i] = FVector::ZeroVector;
    }
    for(int i = 0; i < TriangulosMeshPaisElevaciones.Num(); i++){
        FVector V1 = VertexsMeshPaisElevaciones[TriangulosMeshPaisElevaciones[i].IdVertices[1]] - VertexsMeshPaisElevaciones[TriangulosMeshPaisElevaciones[i].IdVertices[0]];
        FVector V2 = VertexsMeshPaisElevaciones[TriangulosMeshPaisElevaciones[i].IdVertices[2]] - VertexsMeshPaisElevaciones[TriangulosMeshPaisElevaciones[i].IdVertices[0]];
        FVector NormalFace = FVector::CrossProduct(V2, V1);
        NormalFace = NormalFace.GetSafeNormal();
        NormalsMeshPaisElevaciones[TriangulosMeshPaisElevaciones[i].IdVertices[0]] += NormalFace;
        NormalsMeshPaisElevaciones[TriangulosMeshPaisElevaciones[i].IdVertices[1]] += NormalFace;
        NormalsMeshPaisElevaciones[TriangulosMeshPaisElevaciones[i].IdVertices[2]] += NormalFace;
    }
    for(int i = 0; i < NormalsMeshPaisElevaciones.Num(); i++){
        NormalsMeshPaisElevaciones[i] = NormalsMeshPaisElevaciones[i].GetSafeNormal();
    }
}

void AVisualizationMap::LeerTriangulacionPaisElevaciones() {
    std::ifstream filevertexs(PathTriangle + FileTriangle + ".1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.node");

    UE_LOG(LogClass, Log, TEXT("leyendo triangulacion"));
	if (filevertexs.is_open()) {
        int temp;
        int nvertices;
        filevertexs >> nvertices;
        filevertexs >> temp;
        filevertexs >> temp;
        filevertexs >> temp;
        UE_LOG(LogClass, Log, TEXT("numero de vertices: %d"), nvertices);
        for (int i = 0; i < nvertices; i++) {
            filevertexs >> temp;
            float x;
            filevertexs >> x;
            float y;
            filevertexs >> y;
            filevertexs >> temp;
            filevertexs >> temp;
            VertexsMeshPaisElevaciones.Add(FVector(x, y, 0.0f));
        }
		filevertexs.close();
	}

    std::ifstream filetriangles(PathTriangle + FileTriangle + ".1.ele");
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.ele");
    
	if (filetriangles.is_open()) {
        int temp;
        int ntriangles;
        filetriangles >> ntriangles;
        filetriangles >> temp;
        filetriangles >> temp;

        UE_LOG(LogClass, Log, TEXT("numero de triangulos: %d"), ntriangles);
        for (int i = 0; i < ntriangles; i++) {
            filetriangles >> temp;
            int id1;
            int id2;
            int id3;
            filetriangles >> id1;
            filetriangles >> id2;
            filetriangles >> id3;
            TrianglesMeshPaisElevaciones.Add(id3-1);
            TrianglesMeshPaisElevaciones.Add(id2-1);
            TrianglesMeshPaisElevaciones.Add(id1-1);
            TriangleFace TF;
            TF.IdVertices[0] = id3-1;
            TF.IdVertices[1] = id2-1;
            TF.IdVertices[2] = id1-1;
            TriangulosMeshPaisElevaciones.Add(TF);
        }
		filetriangles.close();
	}

    ElevacionesObjetivo.SetNumZeroed(VertexsMeshPaisElevaciones.Num());//espero este los llene de 0

	for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {// o < N*M
		NormalsMeshPaisElevaciones.Add(FVector(0, 0, 1));
	}
 
	for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {// o < N*M
		//UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY));
		UV0MeshPaisElevaciones.Add(FVector2D(VertexsMeshPaisElevaciones[i].X/200, VertexsMeshPaisElevaciones[i].Y/200));
	}
 
 
	for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {// o < N*M
		TangentsMeshPaisElevaciones.Add(FProcMeshTangent(0, 1, 0));
	}
 
	for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {// o < N*M
		VertexColorsMeshPaisElevaciones.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));
	}
}

float AVisualizationMap::FuncionGauss(FVector b, FVector x) {//x es el punto a compronobar, b es el punto centro de la campana
    //debo tomar en cuanta solo x y y de cada punto
    b.Z = 0.0f;
    x.Z = 0.0f;
    a = 1 / FMath::Sqrt(2 * PI);
    //c = 50.0f;
    float res = a * FMath::Exp(-FMath::Pow((b - x).Size(), 2)/(2*c*c));
    return res;
}

void AVisualizationMap::ElevarEn(FVector punto) {//estas funciones afectan al procedurla de elevaciones
    //se debe establecer un umbral general en esta elevacion, que afectara al calculo
    c2 = 200.0f;
    c = FMath::Sqrt(c2);
    for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {
        if ((VertexsMeshPaisElevaciones[i] - punto).Size() < c2) {//en si tal vez esta comprbacion no se deba hacer
            VertexsMeshPaisElevaciones[i].Z = FuncionGauss(punto, VertexsMeshPaisElevaciones[i]) * FactorAltura;
        }
    }
}

void AVisualizationMap::ElevarEn(FVector punto, float altura) {
    //se debe establecer un umbral general en esta elevacion, que afectara al calculo
    UE_LOG(LogClass, Log, TEXT("Elevando %f en : %f, %f, %f"), altura, punto.X, punto.Y, punto.Z);
    c2 = 100.0f;
    c = FMath::Sqrt(c2);
    for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {
        if ((VertexsMeshPaisElevaciones[i] - punto).Size() < c2) {//en si tal vez esta comprbacion no se deba hacer
            ElevacionesObjetivo[i] = FMath::Max(FuncionGauss(punto, VertexsMeshPaisElevaciones[i]) * altura *2.5f, ElevacionesObjetivo[i]);
            //VertexsMeshPaisElevaciones[i].Z = FMath::Max(FuncionGauss(punto, VertexsMeshPaisElevaciones[i]) * altura *2.5f, VertexsMeshPaisElevaciones[i].Z);
        }
    }
}

void AVisualizationMap::ElevarEnConLimites(FVector punto, float altura) {//antes de usar esto se deben establecer los valores minimo y maximo que se usaran
    //se debe establecer un umbral general en esta elevacion, que afectara al calculo
    /*float Alpha = (altura - ValorMinimo) / (ValorMaximo - ValorMinimo);
    float AlturaLimitada = FMath::Lerp(AlturaMinima, AlturaMaxima, Alpha);*/
    float AlturaLimitada = CalcularAlturaLimitada(altura);
    UE_LOG(LogClass, Log, TEXT("Elevando %f en : %f, %f, %f"), altura, punto.X, punto.Y, punto.Z);
    c2 = 100.0f;
    c = FMath::Sqrt(c2);
    for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {
        if ((VertexsMeshPaisElevaciones[i] - punto).Size() < c2) {//en si tal vez esta comprbacion no se deba hacer
            ElevacionesObjetivo[i] = FMath::Max(FuncionGauss(punto, VertexsMeshPaisElevaciones[i]) * AlturaLimitada * 2.5f, ElevacionesObjetivo[i]);//esto ahbra que corregir
            //VertexsMeshPaisElevaciones[i].Z = FMath::Max(FuncionGauss(punto, VertexsMeshPaisElevaciones[i]) * altura *2.5f, VertexsMeshPaisElevaciones[i].Z);
        }
    }
}


float AVisualizationMap::CalcularAlturaLimitada(float Altura) {
    float Alpha = (Altura - ValorMinimo) / (ValorMaximo - ValorMinimo);
    return FMath::Lerp(AlturaMinima, AlturaMaxima, Alpha);
}

void AVisualizationMap::Elevar() {//antes de llamar a esta funion se establecio las nuevas elevaciones objetivo
    bAnimando = true;
    bElevando = true;
    TiempoActualElevacion = 0.0f;
    ElevacionesInicio.SetNum(VertexsMeshPaisElevaciones.Num());
    for (int i = 0; i < ElevacionesInicio.Num(); i++) {
        ElevacionesInicio[i] = VertexsMeshPaisElevaciones[i].Z;
    }
}

void AVisualizationMap::Aplanar() {
    for (int i = 0; i < VertexsMeshPaisElevaciones.Num(); i++) {
        ElevacionesObjetivo[i] = 0.0f;
    }
    for (int i = 0; i < StationActors.Num(); i++) {
        StationActors[i]->MoveToAltura(0.0f);
    }
    Elevar();
}

FVector AVisualizationMap::LatitudLongitudToXY(float Latitud, float Longitud) {//yo usare la y para la latitud, conversion
	//referncia original
	// x = longitud del meridiano central de proyeccion * cos (latitud paralelo estandar);
	//y = latitud;
	//ohi1 toma valores de 0 a 1, depende del tipo de proyeccin, para la que uso toma el valor de 1
	//float DiametroEsfera = 6378137.0f;
	Latitud = FMath::DegreesToRadians(Latitud);
	Longitud = FMath::DegreesToRadians(Longitud);
	float DiametroEsfera = 1500.0f;
	/*float m0 = FMath::Cos(0.0f)/FMath::Sqrt(1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(0.0f), 2));//cual sera el valor de esto?
	m0 = 1;
	float qpart1 = FMath::Loge((1 - FMath::Exp(1)*FMath::Sin(Latitud))/(1 + FMath::Exp(1)*FMath::Sin(Latitud))) / (2*FMath::Exp(1));
	float qpart = (FMath::Sin(Latitud) / (1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(Latitud), 2))) - qpart1;
	float q = (1 - FMath::Exp(2))*qpart;
	float Longitud0 = FMath::DegreesToRadians(102);
	float Y = DiametroEsfera * m0 * (Longitud0 - Longitud);
	float X = DiametroEsfera * q / (2 * m0);*/

	float Y = DiametroEsfera * (Longitud - FMath::DegreesToRadians(-75.0f));//el 0 es el punto en el que es tangnte el cilindro
	float X = DiametroEsfera * FMath::Loge(FMath::Tan(PI/4 + Latitud/2));


	return FVector(X, Y, 0.0f);
	
}

void AVisualizationMap::CreateStations(TArray<FStationStruct> NewStations) {
    Stations = NewStations;

	UWorld * const World = GetWorld();


    for (int i = 0; i < Stations.Num(); i++) {
        UE_LOG(LogTemp, Warning, TEXT("id_ghcn is: %s"), *Stations[i].id_ghcn);
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        FVector SpawnLocation(LatitudLongitudToXY(Stations[i].latitud, Stations[i].longitud));
        //convertir la ubicaion de la estacion
        /*float tempaltura = Stations[i].elevation;
        if (tempaltura > 700) {
            tempaltura -= 1600;
        }
        tempaltura = 1000.0f;
        //ElevarEn(SpawnLocation, tempaltura*0.04f);//esto no ira aqui depsues, esto se llenara co la consulta
        //ElevarEnConLimites(SpawnLocation, tempaltura);//esto no ira aqui depsues, esto se llenara co la consulta
        float AlturaLimitada = CalcularAlturaLimitada(tempaltura);
        ElevarEn(SpawnLocation, AlturaLimitada);//esto no ira aqui depsues, esto se llenara co la consulta
        //SpawnLocation.Z = tempaltura*0.08f;
        SpawnLocation.Z = AlturaLimitada;
        //ElevarEn(SpawnLocation, Stations[i].elevation*0.02f);
        //SpawnLocation.Z = Stations[i].elevation*0.02f;
        */
        AStation * const InstancedStation = World->SpawnActor<AStation>(StationType, SpawnLocation, FRotator::ZeroRotator, SpawnParams);
        if (InstancedStation) {
            StationActors.Add(InstancedStation);
            InstancedStation->Id = i;//ide del array
            InstancedStation->IdDataBase = Stations[i].id;
            InstancedStation->bSelected = true;
            InstancedStation->Name = Stations[i].name.TrimEnd();
            InstancedStation->Latitud->SetText(FText::FromString("Latitud: " + FString::SanitizeFloat(Stations[i].latitud)));
            InstancedStation->Longitud->SetText(FText::FromString("Longitud: " + FString::SanitizeFloat(Stations[i].longitud)));
            //InstancedStation->MostrarNombre();
            //NodoInstanciado->bActualizado = false;
            InstancedStation->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
            IBotonInterface::Execute_Unselect(InstancedStation);
            /*for (int i = 0; i < 3; i++) {
                InstancedStation->AddBarra();
            }
            InstancedStation->DistribuirBarras();*/
        }
    }
    //UpdateNormalsMeshPaisElevaciones();
    //UpdateMeshPaisElevaciones();
    //Elevar();

	//PaintDepartamentosElevaciones();
}

void AVisualizationMap::CrearDepartamentos() {
    //esta funcion se ejecuta despues de haber consultado el mapa y obtenido la informacion
	UWorld * const World = GetWorld();

    int variacion = 360 / Departamentos.Num();

	float GrisVariacion = 0.7f/ Departamentos.Num();

    for (int i = 0; i < Departamentos.Num(); i++) {
        UE_LOG(LogTemp, Warning, TEXT("Departamento: %s"), *Departamentos[i].Name);
        FActorSpawnParameters SpawnParams;
        SpawnParams.Owner = this;
        SpawnParams.Instigator = GetInstigator();

        FVector SpawnLocation(LatitudLongitudToXY(PuntosSCMapa[Departamentos[i].Puntos[0]].X, PuntosSCMapa[Departamentos[i].Puntos[0]].Y));
        //convertir la ubicaion de la estacion
        
        ADepartamento * const InstancedDepartamento = World->SpawnActor<ADepartamento>(DepartamentoType, SpawnLocation, FRotator::ZeroRotator, SpawnParams);
        if (InstancedDepartamento) {
            DepartamentoActors.Add(InstancedDepartamento);
            InstancedDepartamento->File = "Departamento-" + to_string(i);
            InstancedDepartamento->Id = i;
            InstancedDepartamento->Name = Departamentos[i].Name;//en realida deberia pasarle la estructura
            InstancedDepartamento->LeerTriangulacion();
            //NodoInstanciado->bActualizado = false;
            InstancedDepartamento->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
            InstancedDepartamento->CreateMesh();
            InstancedDepartamento->CreateMeshBorde();

            //InstancedDepartamento->ColorComponentH = (i*variacion + 330) % 360;
			InstancedDepartamento->ColorComponentH = 212.0f;
			InstancedDepartamento->ColorComponentS = 0.0f;
			InstancedDepartamento->ColorComponentV = i*GrisVariacion;
            InstancedDepartamento->ActualizarColor();
            InstancedDepartamento->ActualizarInformacion();
            IBotonInterface::Execute_Select(InstancedDepartamento);//para que inicie selecconado
        }
    }
}

void AVisualizationMap::PaintDepartamentosElevaciones() {
	/*TArray<int> CountStations;
	CountStations.SetNum(DepartamentoActors.Num());
	TArray<float> PromedioElevaciones;
	PromedioElevaciones.SetNum(DepartamentoActors.Num());
	for (int i = 0; i < CountStations.Num(); i++) {
		CountStations[i] = 0;
		PromedioElevaciones[i] = 0.0f;
	}
	float ElevacionMaxima = 0.0f;
	for (int i = 0; i < Stations.Num(); i++) {
		if (Stations[i].elevation > ElevacionMaxima) {
			ElevacionMaxima = Stations[i].elevation;
		}
		int id = -1;
		for (int j = 0; j < Departamentos.Num(); j++) {
			if (Stations[i].departament.Trim() == Departamentos[j].Name.Trim()) {
				id = j;
			}
		}
		if (id != -1) {
			CountStations[id]++;
			PromedioElevaciones[id] += Stations[i].elevation;
		}
	}
	for(int i = 0; i < PromedioElevaciones.Num(); i++){
		if (CountStations[i]) {
			PromedioElevaciones[i] /= CountStations[i];
		}
	}*/
	for (int i = 0; i < DepartamentoActors.Num(); i++) {
		DepartamentoActors[i]->CambiarColor(FLinearColor(0.25f, 0.25f, 0.25f, 1.0f));
	}

	for (int i = 0; i < Stations.Num(); i++) {
		int CId = 0;
		for (int j = 0; j < RangosElevaciones.Num() - 1; j++) {
			if (Stations[i].elevation >= RangosElevaciones[j] && Stations[i].elevation < RangosElevaciones[j+1]) {
				CId = j;
			}
		}
		int id = -1;
		for (int j = 0; j < Departamentos.Num(); j++) {
			if (Stations[i].departament.TrimStartAndEnd() == Departamentos[j].Name.TrimStartAndEnd()) {
				id = j;
			}
		}
		if (id != -1) {
			DepartamentoActors[id]->CambiarColor(ColoresElevaciones[CId]);
		}
	}

	/*float DeltaElevacion = ElevacionMaxima / ColoresElevaciones.Num();
	for (int i = 0; i < DepartamentoActors.Num(); i++) {
		if (PromedioElevaciones[i]) {
			float ColorId = PromedioElevaciones[i] / DeltaElevacion;
			int ColorI = FMath::CeilToInt(ColorId) - 1;
			if (ColorI < ColoresElevaciones.Num()) {
				if(ColorI < 0){
					ColorI = 0;
				}
				DepartamentoActors[i]->CambiarColor(ColoresElevaciones[ColorI]);
			}
		}
		else {
			DepartamentoActors[i]->CambiarColor(FLinearColor(0.25f, 0.25f, 0.25f, 1.0f));
		}
	}*/
}

void AVisualizationMap::PaintDepartamentosBlanco() {
	for (int i = 0; i < DepartamentoActors.Num(); i++) {
		DepartamentoActors[i]->CambiarColor(FLinearColor(0.8f, 0.8f, 0.8f, 1.0f));
	}
}

