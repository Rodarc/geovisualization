// Fill out your copyright notice in the Description page of Project Settings.

#include "Station.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Materials/Material.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/WidgetComponent.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"
#include "../GeoVisualizationGameModeBase.h"
#include "../Interfaz/VisualizationHUD.h"


// Sets default values
AStation::AStation()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

    static ConstructorHelpers::FObjectFinder<UStaticMesh> StationMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
	static ConstructorHelpers::FObjectFinder<UMaterial> StationMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/StationMaterial.StationMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera

	StationMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StationMesh"));
    StationMesh->SetupAttachment(RootComponent);
	StationMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 2.5f));
    StationMesh->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
    StationMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);//para poner el tipo de objeto en Station, que es el primero de los personalizables
    //StationMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_EngineTraceChannel2, ECollisionResponse::ECR_Overlap);

    if (StationMeshAsset.Succeeded()) {
        StationMesh->SetStaticMesh(StationMeshAsset.Object);
        if (StationMaterialAsset.Succeeded()) {
            StationMesh->SetMaterial(0, StationMaterialAsset.Object);
            //StationMaterialDynamic = StationMesh->CreateDynamicMaterialInstance(0, StationMaterialAsset.Object);//el puntero lo guardare en una variable
            //StationMaterialDynamic = UMaterialInstanceDynamic::Create(StationMaterialAsset.Object, StationMesh);
            //StationMesh->SetMaterial(0, StationMaterialDynamic);
        }
    }

    Nombre = CreateDefaultSubobject<UTextRenderComponent>(TEXT("StationName"));
    Nombre->SetupAttachment(RootComponent);
    Nombre->SetRelativeLocation(FVector(0.0f, 0.0f, 10.0f));
    Nombre->SetRelativeRotation(FRotator(0.0f, 180.0f, 0.0f));
    Nombre->SetTextRenderColor(FColor::Black);
    Nombre->SetWorldSize(3.0f);
    Nombre->SetVisibility(false);
    Nombre->SetHorizontalAlignment(EHorizTextAligment::EHTA_Center);

    Latitud = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Latitud"));
    Latitud->SetupAttachment(RootComponent);
    Latitud->SetRelativeLocation(FVector(0.0f, 0.0f, 7.0f));
    Latitud->SetRelativeRotation(FRotator(0.0f, 180.0f, 0.0f));
    Latitud->SetTextRenderColor(FColor::White);
    Latitud->SetWorldSize(2.0f);
    Latitud->SetVisibility(false);
    Latitud->SetHorizontalAlignment(EHorizTextAligment::EHTA_Left);

    Longitud= CreateDefaultSubobject<UTextRenderComponent>(TEXT("Longitud"));
    Longitud->SetupAttachment(RootComponent);
    Longitud->SetRelativeLocation(FVector(0.0f, 0.0f, 5.0f));
    Longitud->SetRelativeRotation(FRotator(0.0f, 180.0f, 0.0f));
    Longitud->SetTextRenderColor(FColor::White);
    Longitud->SetWorldSize(2.0f);
    Longitud->SetVisibility(false);
    Longitud->SetHorizontalAlignment(EHorizTextAligment::EHTA_Left);


    static ConstructorHelpers::FClassFinder<UUserWidget> WidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/StationBase.StationBase_C'"));
    CirculoStation = CreateDefaultSubobject<UWidgetComponent>(TEXT("CirculoStation"));
    CirculoStation->SetupAttachment(RootComponent);
    CirculoStation->SetWidgetSpace(EWidgetSpace::World);
    //Widget->SetupAttachment(MotionControllerLeft);
    CirculoStation->SetDrawSize(FVector2D(250.0f, 250.0f));
    CirculoStation->SetPivot(FVector2D(0.5f, 0.5f));
    CirculoStation->SetRelativeLocation(FVector(0.0f, 0.0f, 0.01f));
    CirculoStation->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
    CirculoStation->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f));
    if (WidgetClass.Succeeded()) {
        CirculoStation->SetWidgetClass(WidgetClass.Class);
    }
    CirculoStation->SetBlendMode(EWidgetBlendMode::Transparent);
    CirculoStation->SetVisibility(false);

    ColorComponentH = 224.5f;
    ColorComponentS = 0.0f;
    ColorComponentV = 0.015f;
    ColorComponentVBase = 0.015;
    ColorComponentVDeltaHover = 0.25f;
    Opacity = 1.0f;

    TiempoMovimiento = 1.0f;
    bMoving = false;

    //buscando los tipos de clases
    static ConstructorHelpers::FClassFinder<ABarra> BarraClass(TEXT("BlueprintGeneratedClass'/Game/GeoVisualization/Blueprints/BarraBP.BarraBP_C'"));
    //static ConstructorHelpers::FClassFinder<AStation> StationClass(TEXT("BlueprintGeneratedClass'/Game/GeoVisualization/Blueprints/Station_BP.Station_BP_C'"));
    if (BarraClass.Succeeded()) {
        //if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
            //GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Station type founded."));
        BarraType = BarraClass.Class;
    }

    AnchoBarras = 5.0f;
}

// Called when the game starts or when spawned
void AStation::BeginPlay() {
	Super::BeginPlay();
	
    StationMaterialDynamic = StationMesh->CreateDynamicMaterialInstance(0, StationMesh->GetMaterial(0));//el puntero lo guardare en una variable
    StationMesh->SetMaterial(0, StationMaterialDynamic);
}

// Called every frame
void AStation::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

    if (bMoving) {
        TiempoActualMovimiento += DeltaTime;

        FVector Posicion = GetRootComponent()->GetRelativeTransform().GetLocation();

        Posicion.Z = FMath::InterpEaseInOut(AlturaInicial, AlturaFinal, TiempoActualMovimiento / TiempoMovimiento, 2);//muy bueno

        SetActorRelativeLocation(Posicion);

        if (TiempoActualMovimiento >= TiempoMovimiento) {
            bMoving = false;
        }
    }
}

void AStation::MostrarNombre() {
    Nombre->SetVisibility(true);
}

void AStation::OcultarNombre() {
    Nombre->SetVisibility(false);
}

void AStation::ActualizarRotacionNombre(FVector Direccion) {
    //quiza no necesite esto
    FRotator NewRotation = FRotationMatrix::MakeFromX(Direccion - Nombre->GetComponentLocation()).Rotator();
    //NewRotation = FRotationMatrix::MakeFromX(Direccion).Rotator();
    Nombre->SetWorldRotation(NewRotation);
}

void AStation::ActivarResaltado() {
    //usar oulines apra este resaltado
}

void AStation::DesactivarResaltado() {
}

void AStation::Click_Implementation() {
    if (bFijado) {
        bFijado = false;
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->VisualizationHUDReference->LiberarResaltadoStation(this);
        }
    }
    else {
        bFijado = true;
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->VisualizationHUDReference->FijarResaltadoStation(this);
        }
    }
}

void AStation::DoubleClick_Implementation() {
    if (bSelected) {
        IBotonInterface::Execute_Unselect(this);
        /*bSelected = false;
        ColorComponentS = 0.0f;
        Opacity = 0.8f;
        ActualizarColor();
        ActualizarOpacidad();*/
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->VisualizationHUDReference->UnselectStation(Id);
        }
    }
    else {
        IBotonInterface::Execute_Select(this);
        /*bSelected = true;
        ColorComponentS = 1.0f;
        Opacity = 1.0f;
        ActualizarColor();
        ActualizarOpacidad();*/
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->VisualizationHUDReference->SelectStation(Id);
            //GeoGameMode->VisualizationHUDReference->SelectStation(IdDataBase);
        }
    }
}

void AStation::Press_Implementation() {
}

void AStation::Release_Implementation() {
}

void AStation::Hover_Implementation() {
    /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Hover Station."));*/

    ColorComponentV = ColorComponentVBase + ColorComponentVDeltaHover;
    ActualizarColor();
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->VisualizationHUDReference->ActivarResaltadoStation(this);
    }
}

void AStation::Unhover_Implementation() {
    /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Unhover Station."));*/
    ColorComponentV = ColorComponentVBase;
    ActualizarColor();
    if (!bFijado) {
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->VisualizationHUDReference->DesactivarResaltadoStation(this);
        }
    }
}

void AStation::Select_Implementation() {
    bSelected = true;
    //ColorComponentS = 1.0f;
    ColorComponentVBase = 0.015f;
    Opacity = 1.0f;
    ActualizarColor();
    ActualizarOpacidad();
}

void AStation::Unselect_Implementation() {//solo el efecto visual
    bSelected = false;
    //ColorComponentS = 0.0f;
    ColorComponentVBase = 0.5f;
    Opacity = 1.0f;
    ActualizarColor();
    ActualizarOpacidad();
} 

void AStation::Hide_Implementation() {
    StationMesh->SetVisibility(false);
    StationMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    //debo ocultar sus marcas y labels tambien
}

void AStation::Show_Implementation() {//solo el efecto visual
    StationMesh->SetVisibility(true);
    StationMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    //cuando las muestre no necesito mostrar sus marcas ni sus lables
} 

void AStation::CambiarColor(FLinearColor NewColor) {
    if (StationMaterialDynamic != nullptr) {
        StationMaterialDynamic->SetVectorParameterValue(TEXT("Color"), NewColor);// a que pareametro del material se le asiganara el nuevo color
    }
}

void AStation::ActualizarColor() {
    FLinearColor Color = UKismetMathLibrary::HSVToRGB(ColorComponentH, ColorComponentS, ColorComponentV, 1.0f);
    CambiarColor(Color);
}


void AStation::CambiarOpacidad(float NewOpacity) {
    if (StationMaterialDynamic != nullptr) {
        StationMaterialDynamic->SetScalarParameterValue(TEXT("Opacity"), NewOpacity);// a que pareametro del material se le asiganara el nuevo color
    }
}

void AStation::ActualizarOpacidad() {
    CambiarOpacidad(Opacity);
}

void AStation::MoveToAltura(float Altura) {
    bMoving = true;
    AlturaInicial = GetRootComponent()->GetRelativeTransform().GetLocation().Z;
    AlturaFinal = Altura;
    TiempoActualMovimiento = 0.0f;
}

void AStation::MoveToPosition(FVector NewPosition) {
    bMoving = true;
    PosicionInicial = GetRootComponent()->GetRelativeTransform().GetLocation();
    PosicionFinal = NewPosition;
    TiempoActualMovimiento = 0.0f;
}

int AStation::AddBarra() {
	UWorld * const World = GetWorld();
    FActorSpawnParameters SpawnParams;
    SpawnParams.Owner = this;
    SpawnParams.Instigator = GetInstigator();

    //FVector SpawnLocation(LatitudLongitudToXY(Stations[i].latitud, Stations[i].longitud));
    ABarra * const InstancedBarra = World->SpawnActor<ABarra>(BarraType, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
    if (InstancedBarra) {
        Barras.Add(InstancedBarra);
        InstancedBarra->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);//segun el compilador de unral debo usar esto
        InstancedBarra->Station = this;
        InstancedBarra->CreateHistograma();
        //IBotonInterface::Execute_Unselect(InstancedStation);
    }
    //debo llamar a disntrbuir barras
    return Barras.Num() - 1;
}

bool AStation::ExistBarra(int IdBarra) {
    return IdBarra < Barras.Num();
}

void AStation::SetValueBarra(int IdBarra, float NewValue) {
    if (IdBarra >= 0 && IdBarra < Barras.Num()) {
        Barras[IdBarra]->EscalarToAltura(NewValue);
    }
}

void AStation::EliminateBarra(int IdBarra) {//deberia pasar la misma barra
}

void AStation::HideBarra(int IdBarra) {
    if (IdBarra < Barras.Num()) {
        IBotonInterface::Execute_Hide(Barras[IdBarra]);
    }
}

void AStation::ShowBarra(int IdBarra) {
    if (IdBarra < Barras.Num()) {
        IBotonInterface::Execute_Show(Barras[IdBarra]);
    }
}

void AStation::SetColorBarra(int IdBarra, FLinearColor NewColor) {
    if (IdBarra >= 0 && IdBarra < Barras.Num()) {
        Barras[IdBarra]->CambiarColor(NewColor);
    }
}

void AStation::ClearBarras() {
    for (int i = 0; i < Barras.Num(); i++) {
        Barras[i]->Destroy();
    }
    Barras.Empty();
}

void AStation::DistribuirBarras() {
    float radioCentro;//deberia ser una variable global, apra dibujar un circulo que conecte todas las variables de la estacion, y saber de quien es la barra en caso de que haya mucha superposicion
    if (Barras.Num() == 0) {
        return;
    }
    //debo contar las visible, s y hace los calculos con los visibles, por ahora dejarlo asi
    float angulo = 360.0f / Barras.Num();//bariacion del angulo
    //angulo /= 2;
    //angulo = 90 - angulo;
    float distanciaPuntos = 3.0f * AnchoBarras;//esta funcinando bien, ahora solo debo cauadrar junto con rotacion
    if(Barras.Num() == 1){//error al calcular para cudno hay 1 solo nodod, el erro es en el angulo, y usar una distancia para el radio centro
        radioCentro = 0;
    }
    else {
        //radioCentro = 2*distanciaPuntos/(2* cos(angulo * PI / 180.0));//hacer caso especial para 1??
        radioCentro = Barras.Num()*distanciaPuntos / (2 * PI);
    }
    for(int i = 0; i < Barras.Num(); i++){
        float AnguloBarra = angulo * i;
        float x = radioCentro * cos(AnguloBarra * PI / 180);
        float y = radioCentro * sin(AnguloBarra * PI / 180);
        Barras[i]->SetActorRelativeLocation(FVector(x, y, 0.0f));
    }//deberia seguir pero desde mi puntero active, para que ese sea el primer nodo
}

void AStation::MostrarHistograma(UVisualizacionWidget * Histograma) {
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->VisualizationHUDReference->StationWidgets[Id]->MostrarHistograma(Histograma);
    }
}

void AStation::OcultarHistograma() {
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->VisualizationHUDReference->StationWidgets[Id]->OcultarHistograma();
    }
}
