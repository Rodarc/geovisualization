// Fill out your copyright notice in the Description page of Project Settings.

#include "DepartamentoWidget.h"


UDepartamentoWidget::UDepartamentoWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer){
}

void UDepartamentoWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
}

void UDepartamentoWidget::SetInformation(FString InDepartamentoName) {
    DepartamentoName = InDepartamentoName;
}
