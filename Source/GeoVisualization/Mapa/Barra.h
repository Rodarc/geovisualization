// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/WidgetComponent.h"
#include "../Interfaz/BotonInterface.h"
#include "../Visualizaciones/HistogramaWidget.h"
#include "Barra.generated.h"

UCLASS()
class GEOVISUALIZATION_API ABarra : public AActor, public IBotonInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABarra();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    class AStation * Station;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    int Id;// en el array de estaciones recibidas, inicia en 0
    //no sera usaod el id, ya que esta barras se crear y de sactivan segun el filtro

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UStaticMeshComponent * BarraMesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    UMaterialInstanceDynamic * BarraMaterialDynamic;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    bool bVisible;
	
    void CambiarColor(FLinearColor NewColor);

    void ActualizarColor();

    void CambiarOpacidad(float NewOpacity);

    void ActualizarOpacidad();

    virtual void Click_Implementation() override;

    virtual void DoubleClick_Implementation() override;

    virtual void Press_Implementation() override;

    virtual void Release_Implementation() override;
	
    virtual void Hover_Implementation() override;

    virtual void Unhover_Implementation() override;

    virtual void Select_Implementation() override;

    virtual void Unselect_Implementation() override;

    virtual void Hide_Implementation() override;

    virtual void Show_Implementation() override;

    float ColorComponentH;

    float ColorComponentS;

    float ColorComponentV;

    float ColorComponentVBase;

    float ColorComponentVDeltaHover;

    float Opacity;

    float EscalaInicial;

    float EscalaFinal;

    float EscalaMinima;

    float EscalaMaxima;

    float AlturaInicial;

    float AlturaFinal;

    float AlturaMaxima;
    
    float AlturaMinima;

    float AlturaActual;

    bool bAnimando;

    float TiempoAnimacion;

    float TiempoActualAnimacion;

    void EscalarToAltura(float Altura);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UHistogramaWidget> TypeHistograma;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UHistogramaWidget * HistogramaWidget;

    void CreateHistograma();

    void ShowHistograma();

    void HideHistograma();
	
};
