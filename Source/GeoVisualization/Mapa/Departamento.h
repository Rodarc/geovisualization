// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include <vector>
#include "Materials/Material.h"
#include "Components/StaticMeshComponent.h"
#include "../Interfaz/BotonInterface.h"
#include <string>
#include "Components/WidgetComponent.h"
#include "DepartamentoWidget.h"
#include "Departamento.generated.h"

UCLASS()
class GEOVISUALIZATION_API ADepartamento : public AActor, public IBotonInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADepartamento();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    std::string File;//este string debe ser llenado al momento de crear el objeto y antes de llenar el mesh

    std::string Path;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    int Id;// en el array de estaciones recibidas, inicia en 0

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    FString Name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    bool bSelected;

    int NumeroVerticesBorde;

    float AlturaBotonSeleccion;

    float AlturaBase;

    float AlturaNoSeleccionado;

    float AlturaSeleccionado;

	TArray<FVector> Vertexs;//Para el procedural mesh

	TArray<int32> Triangles;//Para el procedural mesh

	TArray<FVector> Normals;//Para el procedural mesh

	TArray<FVector2D> UV0;//Para el procedural mesh

	TArray<FProcMeshTangent> Tangents;//Para el procedural mesh

	TArray<FLinearColor> VertexColors;//Para el procedural mesh
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
	UProceduralMeshComponent * Malla;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    UMaterialInstanceDynamic * MallaMaterialDynamic;

    float GrosorBorde;

	TArray<FVector> VertexsBorde;//Para el procedural mesh

	TArray<int32> TrianglesBorde;//Para el procedural mesh

	TArray<FVector> NormalsBorde;//Para el procedural mesh

	TArray<FVector2D> UV0Borde;//Para el procedural mesh

	TArray<FProcMeshTangent> TangentsBorde;//Para el procedural mesh

	TArray<FLinearColor> VertexColorsBorde;//Para el procedural mesh

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
	UProceduralMeshComponent * Borde;

    void CreateMesh();

    void UpdateMesh();

    void CreateMeshBorde();

    void UpdateMeshBorde();

    void LeerTriangulacion();

    void CambiarColor(FLinearColor NewColor);

    void ActualizarColor();

    void CambiarOpacidad(float NewOpacity);

    void ActualizarOpacidad();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentSBase;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentH;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentS;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentV;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float Opacity;

    void ActualizarInformacion();


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentSBaseBorde;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentHBorde;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentSBorde;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    float ColorComponentVBorde;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Transformation")
	UMaterial * MapaMaterial;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Transformation")
	UMaterial * BordeMaterial;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * LabelDepartamento;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UDepartamentoWidget * DepartamentoWidget;


    virtual void Click_Implementation() override;

    virtual void DoubleClick_Implementation() override;

    virtual void Press_Implementation() override;

    virtual void Release_Implementation() override;
	
    virtual void Hover_Implementation() override;

    virtual void Unhover_Implementation() override;

    virtual void Select_Implementation() override;

    virtual void Unselect_Implementation() override;
};
