// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include <vector>
#include "Materials/Material.h"
#include "Station.h"
#include "Components/StaticMeshComponent.h"
#include "PaisData.h"
#include "DepartamentoData.h"
#include "Departamento.h"
#include "LimiteData.h"
#include "../Triangulacion/TriangleFace.h"
#include "../Estructuras/StationStruct.h"
#include "VisualizationMap.generated.h"

UCLASS()
class GEOVISUALIZATION_API AVisualizationMap : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVisualizationMap();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    //ubicaciones de los archivos para ller la triangulaciones, esto es solo para lo que es lo relacionesado a Triangle
    std::string FileTriangle;//este string debe ser llenado al momento de crear el objeto y antes de llenar el mesh

    std::string PathTriangle;

    /* Datos Mapa */
    //Almacena todos los puntos del mapa, para que puedan ser accedidos por los distitnos elementos, como departamentos, lineas , etc
    TArray<FVector> PuntosSCMapa;

    //Funcion para leer los datos de un mapa en el formato scmapa, y los almancena en las variables para las estructuras y en el array de puntos general
    void LeerSCMapa();

    //Exporta los datos del scmapa al formato de triangle para poder triangularlo
    void ConvertirSCMapaToTriangle();
	
    //Estructura que almacena los datos del pais, nombre e id de puntos de los puntos del PuntosSCMapa
    PaisData Pais;

    //Array de estructuras que almacena los datos de los departamentos: nombre e id de puntos de los puntos del PuntosSCMapa

    TArray<DepartamentoData> Departamentos;

    //Array de estructuras que almacena los datos de los limites de los departamentos: nombre e id de puntos de los puntos del PuntosSCMapa, entre otras cosas
    TArray<LimiteData> Limites;

    /* Elementos Visuales */

    //Tipo de estacion que mostrara el mapa
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mapa - Stations")
    TSubclassOf<AStation> StationType;

    //Tipo de departamento que mostrara el mapa
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mapa - Departamentos")
    TSubclassOf<ADepartamento> DepartamentoType;
	
    /* Mallas procedurales para el mapa del pais */

    //Malla para el pais
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa - Mallas")
	UProceduralMeshComponent * MeshPais;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa - Mallas")
	UMaterial * PaisMaterial;

    //Para el procedural mesh del pais
	TArray<FVector> VertexsMeshPais;

    //Para el procedural mesh del pais
	TArray<int32> TrianglesMeshPais;

    //Para el procedural mesh del pais
	TArray<FVector> NormalsMeshPais;

    //Para el procedural mesh del pais
	TArray<FVector2D> UV0MeshPais;

    //Para el procedural mesh del pais
	TArray<FProcMeshTangent> TangentsMeshPais;

    //Para el procedural mesh del pais
	TArray<FLinearColor> VertexColorsMeshPais;

    //Para el procedural mesh del pais
    void CreateMeshPais();

    //Para el procedural mesh del pais
    void UpdateMeshPais();

    //Para el procedural mesh del pais, lee una triangulacion desde un archivo
    void LeerTriangulacionPais();//cuando el algortimo de triangulacion este listo, ya no usare esto, sera reemplazada por la funcion Triangular();

    //Malla para las elevaciones del pais 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa - Mallas")
	UProceduralMeshComponent * MeshPaisElevaciones;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa - Mallas")
	UMaterial * ElevacionesMaterial;

    //Para el procedural mesh las elevaciones del pais
	TArray<FVector> VertexsMeshPaisElevaciones;

    //Para el procedural mesh las elevaciones del pais
	TArray<int32> TrianglesMeshPaisElevaciones;

    //Para el procedural mesh las elevaciones del pais
	TArray<FVector> NormalsMeshPaisElevaciones;

    //Para el procedural mesh las elevaciones del pais
	TArray<FVector2D> UV0MeshPaisElevaciones;

    //Para el procedural mesh las elevaciones del pais
	TArray<FProcMeshTangent> TangentsMeshPaisElevaciones;

    //Para el procedural mesh las elevaciones del pais
	TArray<FLinearColor> VertexColorsMeshPaisElevaciones;

	TArray<TriangleFace> TriangulosMeshPaisElevaciones;//esto es para conservar la estrucutrura triangular de la mall y hacer calculos de las normales al hacer elevaciones

    //Para el procedural mesh las elevaciones del pais
    void CreateMeshPaisElevaciones();

    //Para el procedural mesh las elevaciones del pais
    void UpdateMeshPaisElevaciones();

    void UpdateNormalsMeshPaisElevaciones();

    //Para el procedural mesh las elevaciones del pais, lee una triangulacion desde un archivo
    void LeerTriangulacionPaisElevaciones();//cuando el algortimo de triangulacion este listo, ya no usare esto, sera reemplazada por la funcion Triangular();

    /* Para el calculo de las Elevaciones */
    float c;

    float c2;

    float a;

    float FactorAltura;

    float FuncionGauss(FVector b, FVector x);

    void ElevarEn(FVector punto);

    void ElevarEn(FVector punto, float altura);

    void ElevarEnConLimites(FVector punto, float altura);

    float CalcularAlturaLimitada(float Altura);

    float AlturaMinima;//marcan el tama�o de la elevaciones en la visualizacion, entre estos valores hay que mapear los datos para qgenerar las elevaciones

    float AlturaMaxima;

    float HueMinimo;

    float HueMaximo;

    float ValorMinimo;//valores que se representan en la visualizacion entre la altura minima y maxima

    float ValorMaximo;

    TArray<float> ElevacionesInicio;

    TArray<float> ElevacionesObjetivo;

    TArray<float> ElevacionesFin;

    float TiempoElevacion;

    float TiempoActualElevacion;

    bool bAnimando;

    bool bElevando;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void Elevar();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void Aplanar();

	FVector LatitudLongitudToXY(float Latitud, float Longitud);

    /* Visualizacion de estaciones */

    //Array de los actores station 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa - Mallas")
    TArray<AStation *> StationActors;

    //Array de las estructruas de la estaciones
    TArray<FStationStruct> Stations;//esto no se si debe etar aqui o no

    //crear las estaciones, recibo un array de estructuras que conitne cada una una estacion
    void CreateStations(TArray<FStationStruct> NewStations);


    /* Visualizacion de departamentos */
    //Array de los actores departamentso
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa - Mallas")
    TArray<ADepartamento *> DepartamentoActors;

    //crea los departamentos a paratir de las estructuras deparatmento leidas
    void CrearDepartamentos();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa")
    TArray<FLinearColor> ColoresElevaciones;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mapa")
    TArray<float> RangosElevaciones;

    void PaintDepartamentosElevaciones();

    void PaintDepartamentosBlanco();
};
