// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/TextRenderComponent.h"
#include "Components/WidgetComponent.h"
#include "../Interfaz/BotonInterface.h"
#include "Barra.h"
#include "Station.generated.h"

UCLASS()
class GEOVISUALIZATION_API AStation : public AActor, public IBotonInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    int Id;// en el array de estaciones recibidas, inicia en 0

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    int IdDataBase;//id que empiza en 1 esto son los usados para realizar las consiultas

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    bool bSelected;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    bool bFijado;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    FString Name;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UStaticMeshComponent * StationMesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization")
    UMaterialInstanceDynamic * StationMaterialDynamic;
	
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UTextRenderComponent * Nombre;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UTextRenderComponent * Latitud;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UTextRenderComponent * Longitud;

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void MostrarNombre();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void OcultarNombre();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void ActualizarRotacionNombre(FVector Direccion);

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Visualization")
    UWidgetComponent * CirculoStation;

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void ActivarResaltado();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void DesactivarResaltado();

    void CambiarColor(FLinearColor NewColor);

    void ActualizarColor();

    void CambiarOpacidad(float NewOpacity);

    void ActualizarOpacidad();

    virtual void Click_Implementation() override;

    virtual void DoubleClick_Implementation() override;

    virtual void Press_Implementation() override;

    virtual void Release_Implementation() override;
	
    virtual void Hover_Implementation() override;

    virtual void Unhover_Implementation() override;

    virtual void Select_Implementation() override;

    virtual void Unselect_Implementation() override;

    virtual void Hide_Implementation() override;

    virtual void Show_Implementation() override;

    float ColorComponentH;

    float ColorComponentS;

    float ColorComponentV;

    float ColorComponentVBase;

    float ColorComponentVDeltaHover;

    float Opacity;

    FVector PosicionInicial;

    FVector PosicionFinal;

    float AlturaInicial;

    float AlturaFinal;

    bool bMoving;

    float TiempoMovimiento;

    float TiempoActualMovimiento;

    void MoveToAltura(float Altura);

    void MoveToPosition(FVector NewPosition);

    float AnchoBarras;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stations - Barra")
    TSubclassOf<ABarra> BarraType;

    TArray<ABarra *> Barras;

    int AddBarra();

    bool ExistBarra(int IdBarra);

    void SetValueBarra(int IdBarra, float NewValue);//valor de 0 a 1 creo, es decir la estacion hace los calculos

    void SetColorBarra(int IdBarra, FLinearColor NewColor);

    void EliminateBarra(int IdBarra);

    void HideBarra(int IdBarra);

    void ShowBarra(int IdBarra);


    void ClearBarras();

    void DistribuirBarras();

    void CalcularHitogramas();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void MostrarHistograma(UVisualizacionWidget * Histograma);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void OcultarHistograma();
}; 