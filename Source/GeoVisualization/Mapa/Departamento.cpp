// Fill out your copyright notice in the Description page of Project Settings.

#include "Departamento.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/Color.h"
#include "Materials/Material.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "Station.h"
#include "Components/StaticMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include <iostream>
#include <fstream>
#include <string>
#include "../GeoVisualizationGameModeBase.h"
#include "VisualizationMap.h"


// Sets default values
ADepartamento::ADepartamento() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Malla = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = Malla;
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Components|ProceduralMesh", meta = (AutoCreateRefTerm = "Normals,UV0,VertexColors,Tangents"))
	//	void CreateMeshSection(int32 SectionIndex, const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FVector>& Normals,
	// const TArray<FVector2D>& UV0, const TArray<FColor>& VertexColors, const TArray<FProcMeshTangent>& Tangents, bool bCreateCollision);
 
	// New in UE 4.17, multi-threaded PhysX cooking.
    Malla->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);//Botonchannel asumo
	Malla->bUseAsyncCooking = true;

    static ConstructorHelpers::FObjectFinder<UMaterial> MapaMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/MapaMaterial.MapaMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    //static ConstructorHelpers::FObjectFinder<UMaterial> MapaMaterialAsset(TEXT("Material'/Game/GeoVisualizaton/Materials/MapaTransparentMaterial.MapaTransparentMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (MapaMaterialAsset.Succeeded()) {
        MapaMaterial = MapaMaterialAsset.Object;
    }


	Borde = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("BordeMesh"));
    Borde->SetupAttachment(RootComponent);
    Borde->SetRelativeLocation(FVector(0.0f, 0.0f, 0.01f));
	Borde->bUseAsyncCooking = true;

    static ConstructorHelpers::FObjectFinder<UMaterial> BordeMapaMaterialAsset(TEXT("Material'/Game/GeoVisualization/Materials/BordeMapaMaterial.BordeMapaMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    //static ConstructorHelpers::FObjectFinder<UMaterial> MapaMaterialAsset(TEXT("Material'/Game/GeoVisualizaton/Materials/MapaTransparentMaterial.MapaTransparentMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (BordeMapaMaterialAsset.Succeeded()) {
        BordeMaterial = BordeMapaMaterialAsset.Object;
    }


    static ConstructorHelpers::FClassFinder<UUserWidget> WidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/DepartamentoWidgetBP.DepartamentoWidgetBP_C'"));
    LabelDepartamento = CreateDefaultSubobject<UWidgetComponent>(TEXT("Label"));
    LabelDepartamento->SetupAttachment(RootComponent);
    LabelDepartamento->SetWidgetSpace(EWidgetSpace::World);
    LabelDepartamento->SetDrawSize(FVector2D(525.0f, 120.0f));
    LabelDepartamento->SetPivot(FVector2D(0.5f, 0.5f));
    LabelDepartamento->SetRelativeLocation(FVector(0.0f, 0.0f, 0.02f));
    LabelDepartamento->SetRelativeScale3D(FVector(0.1f, 0.1f, 0.1f));
    LabelDepartamento->SetRelativeRotation(FRotator(90.0f, 180.0f, 0.0f));
    if (WidgetClass.Succeeded()) {
        LabelDepartamento->SetWidgetClass(WidgetClass.Class);
    }
    LabelDepartamento->SetBlendMode(EWidgetBlendMode::Transparent);
    LabelDepartamento->SetVisibility(false);

	Path = "D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/";


    ColorComponentH = 224.5f;
    ColorComponentS = 1.0f;
    ColorComponentV = 0.70f;

    ColorComponentSBase = 0.5f;
    Opacity = 1.0f;

    GrosorBorde = 1.5f;
    AlturaBotonSeleccion = 7.5f;

    AlturaBase = -15.0f;
    AlturaNoSeleccionado = -10.0f;
    AlturaSeleccionado = 0.0f;

}

// Called when the game starts or when spawned
void ADepartamento::BeginPlay() {
	Super::BeginPlay();
	
}

// Called every frame
void ADepartamento::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void ADepartamento::CreateMesh() {
	Malla->CreateMeshSection_LinearColor(0, Vertexs, Triangles, Normals, UV0, VertexColors, Tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
    Malla->SetMaterial(0, MapaMaterial);
    MallaMaterialDynamic = Malla->CreateDynamicMaterialInstance(0, Malla->GetMaterial(0));//el puntero lo guardare en una variable
    Malla->SetMaterial(0, MallaMaterialDynamic);

    Malla->UpdateBounds();

    FVector Origin;
    FVector Extent;
    float Radius;
    UKismetSystemLibrary::GetComponentBounds(Malla, Origin, Extent, Radius);
    //Origin.Z += 0.01;
    Origin.Z = Malla->GetComponentLocation().Z + 0.02f;
    LabelDepartamento->SetWorldLocation(Origin);

}

void ADepartamento::UpdateMesh() {
	Malla->UpdateMeshSection_LinearColor(0, Vertexs, Normals, UV0, VertexColors, Tangents);
    Malla->UpdateBounds();

    FVector Origin;
    FVector Extent;
    float Radius;
    UKismetSystemLibrary::GetComponentBounds(Malla, Origin, Extent, Radius);
    Origin.Z = Malla->GetComponentLocation().Z + 0.02f;
    LabelDepartamento->SetWorldLocation(Origin);
}

void ADepartamento::CreateMeshBorde() {
	Borde->CreateMeshSection_LinearColor(0, VertexsBorde, TrianglesBorde, NormalsBorde, UV0Borde, VertexColorsBorde, TangentsBorde, true);
 
        // Enable collision data
	Borde->ContainsPhysicsTriMeshData(false);
    Borde->SetMaterial(0, BordeMaterial);

}

void ADepartamento::UpdateMeshBorde() {
	Borde->UpdateMeshSection_LinearColor(0, VertexsBorde, NormalsBorde, UV0Borde, VertexColorsBorde, TangentsBorde);
}

void ADepartamento::LeerTriangulacion() {

    std::ifstream filevertexs(Path + File + ".1.node");
	//std::ifstream filevertexs ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.node");

    UE_LOG(LogClass, Log, TEXT("leyendo triangulacion"));
	if (filevertexs.is_open()) {
        int temp;
        int nvertices;
        filevertexs >> nvertices;
        filevertexs >> temp;
        filevertexs >> temp;
        filevertexs >> temp;
        UE_LOG(LogClass, Log, TEXT("numero de vertices: %d"), nvertices);
        for (int i = 0; i < nvertices; i++) {
            filevertexs >> temp;
            float x;
            filevertexs >> x;
            float y;
            filevertexs >> y;
            filevertexs >> temp;
            filevertexs >> temp;
            Vertexs.Add(FVector(x, y, AlturaSeleccionado));
        }
		filevertexs.close();
	}

    std::ifstream filetriangles(Path + File + ".1.ele");
	//std::ifstream filetriangles ("D:/AM/Circulo/Mapas/SCMapas/TriangleOutput/Departamento-5.1.ele");
    
	if (filetriangles.is_open()) {
        int temp;
        int ntriangles;
        filetriangles >> ntriangles;
        filetriangles >> temp;
        filetriangles >> temp;

        UE_LOG(LogClass, Log, TEXT("numero de triangulos: %d"), ntriangles);
        for (int i = 0; i < ntriangles; i++) {
            filetriangles >> temp;
            int id1;
            int id2;
            int id3;
            filetriangles >> id1;
            filetriangles >> id2;
            filetriangles >> id3;
            Triangles.Add(id3-1);
            Triangles.Add(id2-1);
            Triangles.Add(id1-1);
        }
		filetriangles.close();
	}

	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		Normals.Add(FVector(0, 0, 1));
	}
 
	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		//UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY));
		UV0.Add(FVector2D(Vertexs[i].X/200, Vertexs[i].Y/200));
	}
 
 
	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		Tangents.Add(FProcMeshTangent(0, 1, 0));
	}
 
	for (int i = 0; i < Vertexs.Num(); i++) {// o < N*M
		VertexColors.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));
	}

    //agregando los laterales del prisma
    int VerticesIniciales = Vertexs.Num();

    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    //AVisualizationMap * VisMap = GeoGameMode->VisualizatinMapReference;
    AVisualizationMap * VisMap = Cast<AVisualizationMap>(GetOwner());
    //if (GeoGameMode) {
        //DepartamentoData * Data = &GeoGameMode->VisualizationMapReference->Departamentos[Id];
        //modificaion aqui
        //DepartamentoData * Data = VisMap->Departamentos[Id];
    if (GeoGameMode && VisMap) {
        DepartamentoData * Data = &VisMap->Departamentos[Id];
        //en lugar de usar la referencia, voy a usar el owner
        //for (int i = 0; i < GeoGameMode->VisualizationMapReference->PuntosSCMapa.Num(); i++) {
        NumeroVerticesBorde = Data->Puntos.Num();
        for (int i = 0; i < Data->Puntos.Num(); i++) {
            FVector punto = VisMap->LatitudLongitudToXY(VisMap->PuntosSCMapa[Data->Puntos[i]].X, VisMap->PuntosSCMapa[Data->Puntos[i]].Y) - VisMap->LatitudLongitudToXY(VisMap->PuntosSCMapa[Data->Puntos[0]].X, VisMap->PuntosSCMapa[Data->Puntos[0]].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
            Vertexs.Add(punto);
            Tangents.Add(FProcMeshTangent(0, 0, 0));
            Normals.Add(FVector(0, 0, 0));
            UV0.Add(FVector2D(i*100.f, 1.0f));
            VertexColors.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));

            /*estos vertices tambien se usan en el borde*/
            VertexsBorde.Add(punto);
            TangentsBorde.Add(FProcMeshTangent(0, 1, 0));
            NormalsBorde.Add(FVector(0, 0, 1));
            //UV0Borde.Add(FVector2D(i*100.f, 1.0f));
            UV0Borde.Add(FVector2D(punto.X/200, punto.Y/200));
            VertexColorsBorde.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));
        }
        for (int i = 0; i < Data->Puntos.Num(); i++) {
            FVector punto = Vertexs[i + VerticesIniciales];
            punto.Z = AlturaBase;
            Vertexs.Add(punto);
            Tangents.Add(FProcMeshTangent(0, 0, 0));
            Normals.Add(FVector(0, 0, 0));
            UV0.Add(FVector2D(i*100.f, 0.0f));
            VertexColors.Add(FLinearColor(1.0, 1.0, 1.0, 1.0));
        }
        for (int i = 0; i < Data->Puntos.Num(); i++) {
            int IdV1 = (i + 1) % Data->Puntos.Num();
            int IdV2 = Data->Puntos.Num() + (i + 1) % Data->Puntos.Num();
            int IdV3 = i;
            int IdV4 = Data->Puntos.Num() + i;
                //(i+1)%PrecisionAristas,
                //PrecisionAristas + (i + 1)%PrecisionAristas,
                //i));
            Triangles.Add(IdV1 + VerticesIniciales);
            Triangles.Add(IdV2 + VerticesIniciales);
            Triangles.Add(IdV3 + VerticesIniciales);

            FVector V1 = Vertexs[IdV2+VerticesIniciales] - Vertexs[IdV1+VerticesIniciales];
            FVector V2 = Vertexs[IdV3+VerticesIniciales] - Vertexs[IdV1+VerticesIniciales];
            //FVector NormalFace = FVector::CrossProduct(V1, V2);
            FVector NormalFace = FVector::CrossProduct(V2, V1);
            NormalFace = NormalFace.GetSafeNormal();

            Normals[IdV1 + VerticesIniciales] += NormalFace;
            Normals[IdV2 + VerticesIniciales] += NormalFace;
            Normals[IdV3 + VerticesIniciales] += NormalFace;
            Normals[IdV4 + VerticesIniciales] += NormalFace;


                //PrecisionAristas + i, i, PrecisionAristas + (i + 1) % PrecisionAristas));
            Triangles.Add(IdV4 + VerticesIniciales);
            Triangles.Add(IdV3 + VerticesIniciales);
            Triangles.Add(IdV2 + VerticesIniciales);

            //en teoria es la misma normal para los 4 vertices

            /*V1 = Vertexs[IdV2+VerticesIniciales] - Vertexs[IdV4+VerticesIniciales];
            V2 = Vertexs[IdV3+VerticesIniciales] - Vertexs[IdV4+VerticesIniciales];
            NormalFace = FVector::CrossProduct(V1, V2);
            NormalFace = NormalFace.GetSafeNormal();*/

        }
        for (int i = VerticesIniciales; i < Normals.Num(); i++) {
            Normals[i] = Normals[i].GetSafeNormal();
        }

        /*calculos para los bordes*/
        VertexsBorde.SetNum(Data->Puntos.Num() * 2);
        NormalsBorde.SetNum(Data->Puntos.Num() * 2);
        TangentsBorde.SetNum(Data->Puntos.Num() * 2);
        UV0Borde.SetNum(Data->Puntos.Num() * 2);
        VertexColorsBorde.SetNum(Data->Puntos.Num() * 2);

        for (int i = 0; i < Data->Puntos.Num(); i++) {
            //sumar vertices iniciales a los indices para acceder a los vertices que necesito
            //con el i 0, yo obtengo el vertice I1
            FVector V0 = VertexsBorde[i];
            FVector V1 = VertexsBorde[(i+1) % Data->Puntos.Num()];
            FVector V2 = VertexsBorde[(i+2) % Data->Puntos.Num()];
            FVector Direccion01 = (V1 - V0).GetSafeNormal();
            FVector Direccion12 = (V2 - V1).GetSafeNormal();
            FVector Direccion01P(Direccion01.Y, -Direccion01.X, Direccion01.Z);
            FVector Direccion12P(Direccion12.Y, -Direccion12.X, Direccion12.Z);

            //dos formas de calcular los puntos
            FVector DireccionR = (Direccion01 + Direccion12).GetSafeNormal();
            FVector DireccionRP(DireccionR.Y, -DireccionR.X, DireccionR.Z);

            float angle = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(Direccion12, -1*Direccion01)));
            float sing = FVector::CrossProduct(Direccion12, -1*Direccion01).Z;//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
            if (sing >= 0) {
                angle = 360-angle;
            }
            //cout << angle << endl;

            float d = GrosorBorde / (FMath::Cos(FMath::DegreesToRadians(FMath::Abs((angle - 180) / 2))));

            FVector I1 = V1 + -d * DireccionRP;
            int IdI1 = (i + 1) % Data->Puntos.Num() + Data->Puntos.Num();
            VertexsBorde[IdI1] = I1;
            TangentsBorde[IdI1] = FProcMeshTangent(0, 1, 0);
            NormalsBorde[IdI1] = FVector(0, 0, 1);
            //UV0Borde.Add(FVector2D(i*100.f, 1.0f));
            UV0Borde[IdI1] = FVector2D(I1.X/300, I1.Y/300);
            VertexColorsBorde[IdI1] = FLinearColor(1.0, 1.0, 1.0, 1.0);

            //Rotator SpawnRotation(angle, 0.0f, 0.0f);
            //SetActorRelativeRotation(Rotator(angle, 0.0f, 0.0f));

        }

        for (int i = 0; i < Data->Puntos.Num(); i++) {
            int IdV1 = (i + 1) % Data->Puntos.Num();
            int IdV2 = Data->Puntos.Num() + (i + 1) % Data->Puntos.Num();
            int IdV3 = i;
            int IdV4 = Data->Puntos.Num() + i;
                //(i+1)%PrecisionAristas,
                //PrecisionAristas + (i + 1)%PrecisionAristas,
                //i));
            TrianglesBorde.Add(IdV3);
            TrianglesBorde.Add(IdV2);
            TrianglesBorde.Add(IdV1);
                //PrecisionAristas + i, i, PrecisionAristas + (i + 1) % PrecisionAristas));
            TrianglesBorde.Add(IdV2);
            TrianglesBorde.Add(IdV3);
            TrianglesBorde.Add(IdV4);

            //en teoria es la misma normal para los 4 vertices

            /*V1 = Vertexs[IdV2+VerticesIniciales] - Vertexs[IdV4+VerticesIniciales];
            V2 = Vertexs[IdV3+VerticesIniciales] - Vertexs[IdV4+VerticesIniciales];
            NormalFace = FVector::CrossProduct(V1, V2);
            NormalFace = NormalFace.GetSafeNormal();*/

        }
        /*for (int i = VerticesIniciales; i < Tangents.Num(); i++) {
            Tangents[i] = Tangents[i].GetSafeNormal();
        }*/
    }
}

/*
    for(int i = 0; i < TriangulosTerreno.size(); i++){
        Vector V1 = VerticesTerreno[TriangulosTerreno[i].IdB] - VerticesTerreno[TriangulosTerreno[i].IdA];
        Vector V2 = VerticesTerreno[TriangulosTerreno[i].IdC] - VerticesTerreno[TriangulosTerreno[i].IdA];
        Vector NormalFace = V1.CrossProduct(V2);
        NormalFace = NormalFace.Normalize();
        NormalsTerreno[TriangulosTerreno[i].IdA] = NormalsTerreno[TriangulosTerreno[i].IdA] + NormalFace;
        NormalsTerreno[TriangulosTerreno[i].IdB] = NormalsTerreno[TriangulosTerreno[i].IdB] + NormalFace;
        NormalsTerreno[TriangulosTerreno[i].IdC] = NormalsTerreno[TriangulosTerreno[i].IdC] + NormalFace;
    }
    for(int i = 0; i < NormalsTerreno.size(); i++){
        NormalsTerreno[i] = NormalsTerreno[i].Normalize();
    }
*/

void ADepartamento::CambiarColor(FLinearColor NewColor) {
    if (MallaMaterialDynamic != nullptr) {
        MallaMaterialDynamic->SetVectorParameterValue(TEXT("Color"), NewColor);// a que pareametro del material se le asiganara el nuevo color
    }
}

void ADepartamento::ActualizarColor() {
    FLinearColor Color = UKismetMathLibrary::HSVToRGB(ColorComponentH, ColorComponentS, ColorComponentV, 1.0f);
    CambiarColor(Color);
}


void ADepartamento::CambiarOpacidad(float NewOpacity) {
    if (MallaMaterialDynamic != nullptr) {
        MallaMaterialDynamic->SetScalarParameterValue(TEXT("Opacity"), NewOpacity);// a que pareametro del material se le asiganara el nuevo color
    }
}

void ADepartamento::ActualizarOpacidad() {
    CambiarOpacidad(Opacity);
}


void ADepartamento::Click_Implementation() {
}

void ADepartamento::DoubleClick_Implementation() {
    if (bSelected) {
        IBotonInterface::Execute_Unselect(this);
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->VisualizationHUDReference->HideStationsOfDepartament(Name);
        }
    }
    else {
        IBotonInterface::Execute_Select(this);
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->VisualizationHUDReference->ShowStationsOfDepartament(Name);
        }
    }
}

void ADepartamento::Press_Implementation() {
}

void ADepartamento::Release_Implementation() {
}

void ADepartamento::Hover_Implementation() {
    /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Hover Departamento."));*/

    /*ColorComponentV = 1.0f;
    //ColorComponentS = 0.5f;
    ColorComponentS = ColorComponentSBase;
    ActualizarColor();*/
    LabelDepartamento->SetVisibility(true);
}

void ADepartamento::Unhover_Implementation() {
    /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Unhover Departamento."));*/

    /*ColorComponentV = 0.70f;
    //ColorComponentS = 1.0f;
    ColorComponentS = ColorComponentSBase + 0.45f;
    ActualizarColor();*/
    LabelDepartamento->SetVisibility(false);
}

void ADepartamento::Select_Implementation() {
    bSelected = true;
    /*ColorComponentSBase = 0.5f;
    //Opacity = 1.0f;
    ActualizarColor();*/
    //ActualizarOpacidad();
    for (int i = 0; i < Vertexs.Num() - NumeroVerticesBorde; i++) {
        Vertexs[i].Z = AlturaSeleccionado;
    }
    UpdateMesh();
    FVector PosBorde = Borde->GetRelativeTransform().GetLocation();
    PosBorde.Z = AlturaSeleccionado + 0.01f;
    Borde->SetRelativeLocation(PosBorde);
    FVector PosLabel = LabelDepartamento->GetRelativeTransform().GetLocation();
    PosLabel.Z = AlturaSeleccionado + 0.02f;
    LabelDepartamento->SetRelativeLocation(PosLabel);
}

void ADepartamento::Unselect_Implementation() {
    bSelected = false;
    /*ColorComponentSBase = 0.0f;
    //Opacity = 0.8f;
    ActualizarColor();*/
    //ActualizarOpacidad();
    for (int i = 0; i < Vertexs.Num() - NumeroVerticesBorde; i++) {
        Vertexs[i].Z = AlturaNoSeleccionado;
    }
    UpdateMesh();
    FVector PosBorde = Borde->GetRelativeTransform().GetLocation();
    PosBorde.Z = AlturaNoSeleccionado + 0.01f;
    Borde->SetRelativeLocation(PosBorde);
    FVector PosLabel = LabelDepartamento->GetRelativeTransform().GetLocation();
    PosLabel.Z = AlturaNoSeleccionado + 0.02f;
    LabelDepartamento->SetRelativeLocation(PosLabel);
} 

void ADepartamento::ActualizarInformacion() {
    UDepartamentoWidget * Widget = Cast<UDepartamentoWidget>(LabelDepartamento->GetUserWidgetObject());
    if (Widget) {
        Widget->SetInformation(Name);
    }
}