// Fill out your copyright notice in the Description page of Project Settings.

#include "ClimateServerConnection.h"
#include "Kismet/GameplayStatics.h"
#include "../MapaTemp.h"
#include "Engine/Engine.h"
#include "../GeoVisualizationGameModeBase.h"


// Sets default values
AClimateServerConnection::AClimateServerConnection()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

    ApiBaseUrl = "http://127.0.0.1:8000/climate_data/";

    NumeroConsultasSimultaneas = 10;
    NumeroConsultasDisponibles = 10;
}

// Called when the game starts or when spawned
void AClimateServerConnection::BeginPlay()
{
	Super::BeginPlay();
	Http = &FHttpModule::Get();

    //GetStation(2);
    //GetStations();
}

// Called every frame
void AClimateServerConnection::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    while (QueueRequests.Num() && NumeroConsultasDisponibles) {
        TSharedRef<IHttpRequest> Request = QueueRequests[0];
        Send(Request);
        QueueRequests.RemoveAt(0);
    }

}

TSharedRef<IHttpRequest> AClimateServerConnection::RequestWithRoute(FString Subroute) {
    TSharedRef<IHttpRequest> Request = Http->CreateRequest();
    Request->SetURL(ApiBaseUrl + Subroute);
    SetRequestHeaders(Request);
    return Request;
}

void AClimateServerConnection::SetRequestHeaders(TSharedRef<IHttpRequest>& Request) {
    Request->SetHeader(TEXT("User-Agent"), TEXT("X-UnrealEngine-Agent"));//no se si esta linea es necesario o no
    Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
    Request->SetHeader(TEXT("Accepts"), TEXT("application/json"));
    //Request->SetHeader(AuthorizationHeader, AuthorizationHash);
}

TSharedRef<IHttpRequest> AClimateServerConnection::GetRequest(FString Subroute) {
    TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
    Request->SetVerb("GET");
    return Request;
}

TSharedRef<IHttpRequest> AClimateServerConnection::PostRequest(FString Subroute, FString ContentJsonString) {
    TSharedRef<IHttpRequest> Request = RequestWithRoute(Subroute);
    Request->SetVerb("POST");
    Request->SetContentAsString(ContentJsonString);
    return Request;
}

void AClimateServerConnection::Send(TSharedRef<IHttpRequest>& Request) {
    NumeroConsultasDisponibles--;
    Request->ProcessRequest();
}

bool AClimateServerConnection::ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful) {
    if (!bWasSuccessful || !Response.IsValid()) {
        return false;
    }
    if (EHttpResponseCodes::IsOk(Response->GetResponseCode())) {
        return true;
    }
    else {
        UE_LOG(LogTemp, Warning, TEXT("Http Response returned error code: %d"), Response->GetResponseCode());
        return false;
    }
}

//serializar
template <typename StructType>
void AClimateServerConnection::GetJsonStringFromStruct(StructType FilledStruct, FString& StringOutput) {
	FJsonObjectConverter::UStructToJsonObjectString(StructType::StaticStruct(), &FilledStruct, StringOutput, 0, 0);
}

template<typename StructType>
void AClimateServerConnection::GetStructFromJsonString(FString JsonString, StructType & StructOutput) {
	FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(JsonString, &StructOutput, 0, 0);
}

template<typename StructType>
void AClimateServerConnection::GetStructArrayFromJsonStringArray(FString JsonString, TArray<StructType> & ArrayStructOutput) {
	FJsonObjectConverter::JsonArrayStringToUStruct<StructType>(JsonString, &ArrayStructOutput, 0, 0);
}

//deserializar
template <typename StructType>
void AClimateServerConnection::GetStructFromResponse(FHttpResponsePtr Response, StructType& StructOutput) {//por que por referencia la salida? no se puede retornar?
	StructType StructData;
	FString JsonString = Response->GetContentAsString();
	FJsonObjectConverter::JsonObjectStringToUStruct<StructType>(JsonString, &StructOutput, 0, 0);
}

void AClimateServerConnection::GetStation(int id) {
	TSharedRef<IHttpRequest> Request = GetRequest("stations/" + FString::FromInt(id) + "/");
	Request->OnProcessRequestComplete().BindUObject(this, &AClimateServerConnection::GetStationResponse);
	Send(Request);
}

void AClimateServerConnection::GetStationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
    if (!ResponseIsValid(Response, bWasSuccessful)) {
        return;
    }

	FStationStruct StationResponse;

	FString JsonString = Response->GetContentAsString();
	GetStructFromJsonString<FStationStruct>(JsonString, StationResponse);
    
    UE_LOG(LogTemp, Warning, TEXT("Id is: %d"), StationResponse.id);
    UE_LOG(LogTemp, Warning, TEXT("Id_ghcn is: %s"), *StationResponse.id_ghcn);

}

void AClimateServerConnection::GetStations() {
	TSharedRef<IHttpRequest> Request = GetRequest("stations/");
	Request->OnProcessRequestComplete().BindUObject(this, &AClimateServerConnection::GetStationsResponse);
	Send(Request);
}

void AClimateServerConnection::GetStationsResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
    if (!ResponseIsValid(Response, bWasSuccessful)) {
        return;
    }

	FString JsonString = Response->GetContentAsString();

    TArray<FStationStruct> StationsResponse;
    GetStructArrayFromJsonStringArray(JsonString, StationsResponse);
    for (int i = 0; i < StationsResponse.Num(); i++) {
        //UE_LOG(LogTemp, Warning, TEXT("id_ghcn is: %s"), *StationsResponse[i].id_ghcn);
    }
    bConsultaCorrecta = true;
    Output = StationsResponse;
    UE_LOG(LogTemp, Warning, TEXT("Consulta terminada y correcta"));
    //aqui deberia llamar a mapa y darle el rsultado de la consulta


	/*TArray<AActor *> MapasEncontrados;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMapaTemp::StaticClass(), MapasEncontrados);
	UE_LOG(LogClass, Log, TEXT("Numero de Mapas Encontradas: %d"), MapasEncontrados.Num());
	for (int i = 0; i < MapasEncontrados.Num(); i++) {
		//if(GEngine)//no hacer esta verificación provocaba error al iniciar el editor
			//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Blue, TEXT("PARTES ENCONTRADAS"));
		UE_LOG(LogClass, Log, TEXT("Mapas Encontradas %d"), i);
		AMapaTemp * const MapaTemp = Cast<AMapaTemp>(MapasEncontrados[i]);
        if (MapaTemp) {
            UE_LOG(LogClass, Log, TEXT("Mandando respuesta %d"), i);
            MapaTemp->CreateStations(StationsResponse);
        }
	}*/

    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->RecibirStationsData(StationsResponse);
    }

}

void AClimateServerConnection::GetTemperatureAvergeYearMonthForStation(int idstation, int initialyear, int finalyear, TArray<int> months) {
	//TSharedRef<IHttpRequest> Request = GetRequest("temperature_average/?id_station=" + FString::FromInt(id) + "&year=" + FString::FromInt(year) + "&month=" + FString::FromInt(month));
    FString Consulta;
    Consulta += "temperature_average/filter_query/?id_station=" + FString::FromInt(idstation) + "&initialyear=" + FString::FromInt(initialyear) + "&finalyear=" + FString::FromInt(finalyear);
    if (months.Num() && months.Num() != 12) {
        for (int i = 0; i < months.Num(); i++) {
            Consulta += "&months=" + FString::FromInt(months[i]+1);//este mas uno, es por que los indices en la base de datos de los meses, empiezan en 1
            //aqui manejo esa responsabilidad
        }
    }
	TSharedRef<IHttpRequest> Request = GetRequest(Consulta);
	Request->OnProcessRequestComplete().BindUObject(this, &AClimateServerConnection::GetTemperatureAverageYearMonthForStationResponse);
	Send(Request);
}

void AClimateServerConnection::GetTemperatureAverageYearMonthForStationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
    if (!ResponseIsValid(Response, bWasSuccessful)) {
        return;
    }
	FString JsonString = Response->GetContentAsString();

    TArray<FDailyDataStruct> DailyDataResponse;
    GetStructArrayFromJsonStringArray(JsonString, DailyDataResponse);
    /*for (int i = 0; i < DailyDataResponse.Num(); i++) {
        UE_LOG(LogTemp, Warning, TEXT("day: %d, value: %d"), DailyDataResponse[i].day, DailyDataResponse[i].value);
    }*/
    bConsultaCorrecta = true;
    OutputData = DailyDataResponse;//linea innecesaria

    UE_LOG(LogTemp, Warning, TEXT("Consulta terminada y correcta"));

    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->RecibirTemperatureData(DailyDataResponse);
    }
}

void AClimateServerConnection::GetTemperatureAverageInterval() {
	TSharedRef<IHttpRequest> Request = GetRequest("temperature_average/intervalo_registros/");
	Request->OnProcessRequestComplete().BindUObject(this, &AClimateServerConnection::GetTemperatureAverageIntervalResponse);
	Send(Request);
}

void AClimateServerConnection::GetTemperatureAverageIntervalResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
    if (!ResponseIsValid(Response, bWasSuccessful)) {
        return;
    }

	FString JsonString = Response->GetContentAsString();

    TArray<FDailyDataStruct> DailyDataResponse;
    GetStructArrayFromJsonStringArray(JsonString, DailyDataResponse);
    /*for (int i = 0; i < DailyDataResponse.Num(); i++) {
        UE_LOG(LogTemp, Warning, TEXT("year: %d"), DailyDataResponse[i].year);
    }*/
    bConsultaCorrecta = true;
    OutputData = DailyDataResponse;//linea innecesaria

    UE_LOG(LogTemp, Warning, TEXT("Consulta terminada y correcta"));

    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->RecibirIntervaloTemperatureData(DailyDataResponse);
    }
}

void AClimateServerConnection::GetVariableDataYearsMonthsForStation(int idstation, int initialyear, int finalyear, TArray<int> months, FString variable) {
    FString Consulta;
    Consulta += variable + "/filter_query/?id_station=" + FString::FromInt(idstation) + "&initialyear=" + FString::FromInt(initialyear) + "&finalyear=" + FString::FromInt(finalyear);
    if (months.Num() && months.Num() != 12) {
        for (int i = 0; i < months.Num(); i++) {
            Consulta += "&months=" + FString::FromInt(months[i]+1);//este mas uno, es por que los indices en la base de datos de los meses, empiezan en 1
            //aqui manejo esa responsabilidad
        }
    }
    UE_LOG(LogTemp, Warning, TEXT("Pidiendo consulta consulta: %s"), *Consulta);
	TSharedRef<IHttpRequest> Request = GetRequest(Consulta);
	Request->OnProcessRequestComplete().BindUObject(this, &AClimateServerConnection::GetVariableDataYearsMonthsForStationResponse);
    QueueRequests.Add(Request);
	//Send(Request);
}

void AClimateServerConnection::GetVariableDataYearsMonthsForStationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {

    FString URL = Request.Get()->GetURL();
    int idslash = URL.Find("/", ESearchCase::IgnoreCase, ESearchDir::FromStart, ApiBaseUrl.Len());
    FString variable = URL.Mid(ApiBaseUrl.Len(), idslash - ApiBaseUrl.Len());
    UE_LOG(LogTemp, Warning, TEXT("Recibiendo consulta consulta: %s"), *URL);
    //UE_LOG(LogTemp, Warning, TEXT("consulta de tipo: %s"), *FString(URL.Mid(ApiBaseUrl.Len(), idslash - ApiBaseUrl.Len())));
    TArray<FDailyDataStruct> DailyDataResponse;
    NumeroConsultasDisponibles++;
    if (!ResponseIsValid(Response, bWasSuccessful)) {//si no se pudo conectar deberia mandar vacion coo respuesta

        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
        AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoGameMode) {
            GeoGameMode->RecibirVariableData(DailyDataResponse, variable);//recibira la variable
        }
        return;
    }

	FString JsonString = Response->GetContentAsString();

    GetStructArrayFromJsonStringArray(JsonString, DailyDataResponse);
    /*for (int i = 0; i < DailyDataResponse.Num(); i++) {
        UE_LOG(LogTemp, Warning, TEXT("day: %d, value: %d"), DailyDataResponse[i].day, DailyDataResponse[i].value);
    }*/
    bConsultaCorrecta = true;
    OutputData = DailyDataResponse;//linea innecesaria

    UE_LOG(LogTemp, Warning, TEXT("Consulta terminada y correcta"));

    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        GeoGameMode->RecibirVariableData(DailyDataResponse, variable);//recibira la variable
    }
}

/*void AClimateServerConnection::GetStationsResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful) {
    if (!ResponseIsValid(Response, bWasSuccessful)) {
        return;
    }


	FString JsonString = Response->GetContentAsString();

    TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
    TSharedPtr<FJsonValue> JsonValue;
    TSharedRef< TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(JsonString);

    //if (FJsonSerializer::Deserialize(JsonReader, JsonObject) && JsonObject.IsValid()) {
    if (FJsonSerializer::Deserialize(JsonReader, JsonValue) && JsonValue.IsValid()) {
        FString prueba;
        TArray<TSharedPtr<FJsonValue> > JsonArray = JsonValue->AsArray();
        //if (JsonValue->TryGetArray(& JsonArray)){
        UE_LOG(LogTemp, Warning, TEXT("prueba %d"), JsonArray.Num());
        //ya los tengo como array de balores json
        //aqui puedo convertir cada jason value de forma individual,
        //op puedo mandar a convertir el array de json values
        //todo esto gracias a las guniondes de FJsonObjectConverter::
    }

	//FStationStruct StationResponse;
	//GetStructFromJsonString<FStationStruct>(JsonString, StationResponse);
}*/
