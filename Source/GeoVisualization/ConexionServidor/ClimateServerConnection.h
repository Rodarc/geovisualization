// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "Json.h"
#include "JsonUtilities.h"
#include "../Estructuras/StationStruct.h"
#include "../Estructuras/DailyDataStruct.h"
#include "ClimateServerConnection.generated.h"

/*USTRUCT(BlueprintType)
struct FStationStruct {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString id_ghcn;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float latitud;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float longitud;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float elevation;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString wmo_id;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString departament;
};*/

/*USTRUCT(BlueprintType)
struct FDailyDataStruct {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int id_station;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString id_station_ghcn;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int year;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int month;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int day;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float value;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString mflag;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString qflag;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString sflag;
};*/

UCLASS()
class GEOVISUALIZATION_API AClimateServerConnection : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AClimateServerConnection();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    FHttpModule * Http;

    //url de la api
    FString ApiBaseUrl;

    TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);

    TArray<TSharedRef<IHttpRequest>> QueueRequests;

    int NumeroConsultasSimultaneas;

    int NumeroConsultasDisponibles;

    //le pone headesrs a los request
    void SetRequestHeaders(TSharedRef<IHttpRequest>& Request);

    TSharedRef<IHttpRequest> GetRequest(FString Subroute);

    TSharedRef<IHttpRequest> PostRequest(FString Subroute, FString ContentJsonString);

    void Send(TSharedRef<IHttpRequest>& Request);

    //verifica si el response es valdo
    bool ResponseIsValid(FHttpResponsePtr Response, bool bWasSuccessful);

    //serializador
    template<typename StructType>
    void GetJsonStringFromStruct(StructType FilledStruct, FString & StringOutput);

    template<typename StructType>
    void GetStructFromResponse(FHttpResponsePtr Response, StructType & StructOutput);

    //desserializador
    template<typename StructType>
    void GetStructFromJsonString(FString JsonString, StructType & StructOutput);

    //deserializador de array
    template<typename StructType>
    void GetStructArrayFromJsonStringArray(FString JsonString, TArray<StructType>& ArrayStructOutput);


    //peticion de una estaciones
    void GetStation(int id);

    //recepcion de respuesta apra una sola estacion
    void GetStationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

    //peticion de una estaciones
    void GetStations();

    //recepcion de respuesta varias estaciones 
    void GetStationsResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

    TArray<FStationStruct> Output;

    TArray<FDailyDataStruct> OutputData;

    bool bConsultaCorrecta;

    //peticion de temperatura promedio de una estaciones por a�o y mes
    void GetTemperatureAvergeYearMonthForStation(int idstation, int initialyear, int finalyear, TArray<int> months);

    //recepcion de respuesta 
    void GetTemperatureAverageYearMonthForStationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

    //peticion de temperatura promedio de una estaciones por a�o y mes
    void GetTemperatureAverageInterval();

    //recepcion de respuesta 
    void GetTemperatureAverageIntervalResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

    //peticion de temperatura promedio de una estaciones por a�o y mes
    void GetVariableDataYearsMonthsForStation(int idstation, int initialyear, int finalyear, TArray<int> months, FString variable);

    //recepcion de respuesta 
    void GetVariableDataYearsMonthsForStationResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
};
