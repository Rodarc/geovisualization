// Fill out your copyright notice in the Description page of Project Settings.

#include "MapaDebug.h"
#include "Misc/FileHelper.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Kismet/KismetSystemLibrary.h"
#include "DrawDebugHelpers.h"
#include "Math/Color.h"
#include "Triangulacion/Delaunay.h"


// Sets default values
AMapaDebug::AMapaDebug() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = CreateDefaultSubobject<USceneComponent>("Raiz");

}

// Called when the game starts or when spawned
void AMapaDebug::BeginPlay() {
	Super::BeginPlay();

    LeerSCMapa();
	
	//LeerPuntos();//puede ser el ue
	//ConvertirStringstoVectors();//convierte los strings leidos en puntos, vectores
    //EscribirPuntosT();
    //crea puntos para el mapa, el mapa va bien

    //EscribirPuntos();

	/*LeerPrueba();//este carga el conjunto de prueba para el delaunay
	ConvertirStringstoVectorsPrueba();

	Delaunay d;
	d.Triangulate(Triangulos, Vertices);*/
}

// Called every frame
void AMapaDebug::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	DrawPoints();
	//DrawVertices();

	//DrawTriangles();
}

void AMapaDebug::LeerPuntos() {
	std::string line;
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/DatosPeru1.txt");
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/DatosPeru110.txt");
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/DatosPeru50.txt");
	std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/DatosPeru10.txt");
	if (myfile.is_open()) {
		while ( getline (myfile,line) ) {
			//cout << line << '\n';
			PuntosSMapa.Add(line.c_str());
		}
		myfile.close();
	}
	UE_LOG(LogClass, Log, TEXT("Puntos leidos: %d"), PuntosSMapa.Num());

	std::ifstream mydep ("D:/AM/Circulo/Mapas/Peru/DatosDepartamentos10.txt");
	if (mydep.is_open()) {
		while ( getline (mydep,line) ) {
			//cout << line << '\n';
			PuntosSDepartamento.Add(line.c_str());
		}
		mydep.close();
	}
}

void AMapaDebug::EscribirPuntos() {
    std::ofstream myfile("D:/AM/Circulo/Mapas/Peru/Conversion/ConversionDatosPeru110.txt");
    //std::ofstream myfile("D:/AM/Circulo/Mapas/Peru/Conversion/ConversionDatosPeru50.txt");
    //std::ofstream myfile("D:/AM/Circulo/Mapas/Peru/Conversion/ConversionDatosPeru10.txt");
    if (myfile.is_open()) {
        for (int i = 0; i < PuntosMapa.Num(); i++) {
            FVector punto = LatitudLongitudToXY(PuntosMapa[i].X, PuntosMapa[i].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
            myfile << punto.X << "," << punto.Y << endl;
        }
        myfile.close();
    }
}

void AMapaDebug::EscribirPuntosT() {
    //std::ofstream myfile("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeru110.poly");
    //std::ofstream myfile("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeru50.poly");
    //std::ofstream myfile("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeru10.poly");
    std::ofstream myfile("D:/AM/Circulo/Mapas/Peru/FinalVersion/TriangleInput/ConversionDatosPeruDep10.poly");
    if (myfile.is_open()) {
        //impresion de los puntos
        //myfile << PuntosMapa.Num() << " 2 1 0" << endl;
        myfile << PuntosMapa.Num() + PuntosDepartamento.Num() << " 2 1 0" << endl;
        for (int i = 0; i < PuntosMapa.Num(); i++) {
            FVector punto = LatitudLongitudToXY(PuntosMapa[i].X, PuntosMapa[i].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
            myfile << i+1 << " " << punto.X << " " << punto.Y << " 1" << endl;
        }

        for (int i = 0; i < PuntosDepartamento.Num(); i++) {
            FVector punto = LatitudLongitudToXY(PuntosDepartamento[i].X, PuntosDepartamento[i].Y);//esto deberia hacerse al leer y recibir entradas, y algmacenarlo asi
            myfile << PuntosMapa.Num() + i+1 << " " << punto.X << " " << punto.Y << " 1" << endl;
        }
        //impresion de las aristas
        //el numero de aristas depende, pero para el hull de una figura es igual al numero de vertices de ese hull
        myfile << PuntosMapa.Num() << " 0" << endl;
        for (int i = 0; i < PuntosMapa.Num() - 1; i++) {
            myfile << i + 1 << " " << i + 1 << " " << i + 2 << endl;
        }
        myfile << PuntosMapa.Num() << " " << PuntosMapa.Num() << " " << 1 << endl;
        myfile << "0 0 0" << endl;
        myfile.close();
    }
}

void AMapaDebug::LeerPrueba() {//llee puntos normales en el espacio, para probar la rtiangulacion
	std::string line;
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/Prueba.txt");
	std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/Prueba2.txt");//este archivo esta invertido para que el eje y sea el x, y se corresponda con la prueba en geogebra
	//archivos de prueba, esot no requiere la covnersion posterior a 
	if (myfile.is_open()) {
		while ( getline (myfile,line) ) {
			//cout << line << '\n';
			PuntosSMapa.Add(line.c_str());
		}
		myfile.close();
	}
	UE_LOG(LogClass, Log, TEXT("Puntos leidos: %d"), PuntosSMapa.Num());
}

void AMapaDebug::ConvertirStringstoVectorsPrueba() {
	for (int i = 0; i < PuntosSMapa.Num(); i++) {
		FVector punto;
		TArray<FString> puntoss;
		FString hola = ",";
		PuntosSMapa[i].ParseIntoArray(puntoss, *hola);
		if (puntoss.Num() == 2) {
			//punto.X = FCString::Atof(*puntoss[0]);
			//punto.Y = FCString::Atof(*puntoss[1]);
			punto.Y = FCString::Atof(*puntoss[0]);
			punto.X = FCString::Atof(*puntoss[1]);
			Vertices.push_back(punto);
			//PuntosMapa.Add(punto);
		}
	}
	//PuntosMapa.Add(FVector(0.0f));
	UE_LOG(LogClass, Log, TEXT("Puntos convertidos: %d"), PuntosMapa.Num());
}

void AMapaDebug::LeerPuntosUE() {//funcion para leer puntos pero con funciones de unreal en lugar de funciones del std
	//FString FilePath = FString("D:/AM/Circulo/Mapas/Peru/DatosPeru1.txt");
	FString FilePath = FString("D:/AM/Circulo/Mapas/Peru/DatosPeru110.txt");
	//estos dos mapas tiene los puntos invertidos, con la longitud primero luego la latitud
	//tener cuidado cuando lea los datos, de saber quien es la longitud y la laitud!!!!!!!!!!!!!!!
	FFileHelper::LoadFileToStringArray(PuntosSMapa, *FilePath);
	UE_LOG(LogClass, Log, TEXT("Puntos leidos: %d"), PuntosSMapa.Num());
}

void AMapaDebug::ConvertirStringstoVectors() {//esto se usa para convertir lo leido de los archivos a vectores
	for (int i = 0; i < PuntosSMapa.Num(); i++) {
		FVector punto;
		TArray<FString> puntoss;
		FString hola = ",";
		PuntosSMapa[i].ParseIntoArray(puntoss, *hola);
		if (puntoss.Num() == 2) {
			//punto.X = FCString::Atof(*puntoss[0]);
			//punto.Y = FCString::Atof(*puntoss[1]);
			punto.Y = FCString::Atof(*puntoss[0]);
			punto.X = FCString::Atof(*puntoss[1]);
			PuntosMapa.Add(punto);
		}
	}
	//PuntosMapa.Add(FVector(0.0f));
	UE_LOG(LogClass, Log, TEXT("Puntos convertidos: %d"), PuntosMapa.Num());

	for (int i = 0; i < PuntosSDepartamento.Num(); i++) {
		FVector punto;
		TArray<FString> puntoss;
		FString hola = ",";
		PuntosSDepartamento[i].ParseIntoArray(puntoss, *hola);
		if (puntoss.Num() == 2) {
			//punto.X = FCString::Atof(*puntoss[0]);
			//punto.Y = FCString::Atof(*puntoss[1]);
			punto.Y = FCString::Atof(*puntoss[0]);
			punto.X = FCString::Atof(*puntoss[1]);
			PuntosDepartamento.Add(punto);
		}
	}
}


void AMapaDebug::DrawPoints() {//dibuja los puntos obtenidos del mapa
	for (int i = 0; i < PuntosMapa.Num(); i++) {
		FVector punto = LatitudLongitudToXY(PuntosMapa[i].X, PuntosMapa[i].Y);
		DrawDebugSphere(GetWorld(), punto, 1.0f, 8, FColor::Black, false, -1.0f, 0, 0.5f);
        UE_LOG(LogClass, Log, TEXT("Punto = C(%f,%f,%f)"), punto.X, punto.Y, punto.Z);
		//DrawDebugSphere(GetWorld(), PuntosMapa[i]*10, 1.0f, 8, FColor::Black, false, -1.0f, 0, 0.5f);
        //UE_LOG(LogClass, Log, TEXT("Punto = C(%f,%f,%f)"), PuntosMapa[i].X, PuntosMapa[i].Y, PuntosMapa[i].Z);
	}
}

void AMapaDebug::DrawVertices() {//dibuja los vertices de la triangulacion
	for (int i = 0; i < Vertices.size(); i++) {
		DrawDebugSphere(GetWorld(), Vertices[i], 1.0f, 8, FColor::Black, false, -1.0f, 0, 0.5f);

	}
}

void AMapaDebug::DrawTriangles() {//dibuja los triangulos
	for (int i = 0; i < Triangulos.size(); i++) {
		if (!Triangulos[i].bGhost) {
			DrawDebugLine(GetWorld(), *Triangulos[i].Vertices[0], *Triangulos[i].Vertices[1], FColor::Blue, false, -1.0f, 0, 0.5f);
			DrawDebugLine(GetWorld(), *Triangulos[i].Vertices[1], *Triangulos[i].Vertices[2], FColor::Blue, false, -1.0f, 0, 0.5f);
			DrawDebugLine(GetWorld(), *Triangulos[i].Vertices[2], *Triangulos[i].Vertices[0], FColor::Blue, false, -1.0f, 0, 0.5f);
			//UE_LOG(LogClass, Log, TEXT("Punto = C(%f,%f,%f)"), punto.X, punto.Y, punto.Z);
			//DrawDebugSphere(GetWorld(), PuntosMapa[i]*10, 1.0f, 8, FColor::Black, false, -1.0f, 0, 0.5f);
			//UE_LOG(LogClass, Log, TEXT("Punto = C(%f,%f,%f)"), PuntosMapa[i].X, PuntosMapa[i].Y, PuntosMapa[i].Z);
		}
	}
}

FVector AMapaDebug::LatitudLongitudToXY(float Latitud, float Longitud) {//yo usare la y para la latitud, conversion
	//referncia original
	// x = longitud del meridiano central de proyeccion * cos (latitud paralelo estandar);
	//y = latitud;
	//ohi1 toma valores de 0 a 1, depende del tipo de proyeccin, para la que uso toma el valor de 1
	//float DiametroEsfera = 6378137.0f;
	Latitud = FMath::DegreesToRadians(Latitud);
	Longitud = FMath::DegreesToRadians(Longitud);
	float DiametroEsfera = 1500.0f;
	/*float m0 = FMath::Cos(0.0f)/FMath::Sqrt(1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(0.0f), 2));//cual sera el valor de esto?
	m0 = 1;
	float qpart1 = FMath::Loge((1 - FMath::Exp(1)*FMath::Sin(Latitud))/(1 + FMath::Exp(1)*FMath::Sin(Latitud))) / (2*FMath::Exp(1));
	float qpart = (FMath::Sin(Latitud) / (1 - FMath::Exp(2)*FMath::Pow(FMath::Sin(Latitud), 2))) - qpart1;
	float q = (1 - FMath::Exp(2))*qpart;
	float Longitud0 = FMath::DegreesToRadians(102);
	float Y = DiametroEsfera * m0 * (Longitud0 - Longitud);
	float X = DiametroEsfera * q / (2 * m0);*/

	float Y = DiametroEsfera * (Longitud - FMath::DegreesToRadians(-75.0f));//el 0 es el punto en el que es tangnte el cilindro
	float X = DiametroEsfera * FMath::Loge(FMath::Tan(PI/4 + Latitud/2));


	return FVector(X, Y, 0.0f);
	
}

void AMapaDebug::LeerSCMapa() {
	std::string line;
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/DatosPeru1.txt");
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/DatosPeru110.txt");
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/DatosPeru50.txt");
	std::ifstream myfile ("D:/AM/Circulo/Mapas/SCMapas/Peru.scmapa");
	if (myfile.is_open()) {
        string nombrepais;
        myfile >> nombrepais;
        int numerodep;
        myfile >> numerodep;
        int numerolim;
        myfile >> numerolim;
        int numeropuntos;
        myfile >> numeropuntos;
        PuntosSCMapa.SetNum(numeropuntos);
        for (int i = 0; i < numeropuntos; i++) {
            int id;
            myfile >> id;
            myfile >> PuntosSCMapa[id].X;
            myfile >> PuntosSCMapa[id].Y;
            UE_LOG(LogClass, Log, TEXT("Puntos %d: (%f, %f)"), id, PuntosSCMapa[id].X, PuntosSCMapa[id].Y);
        }
        UE_LOG(LogClass, Log, TEXT("Puntos SC Mapa leidos: %d"), numeropuntos);
		myfile.close();
	}
}

void AMapaDebug::ConvertirSCMapa() {
}
