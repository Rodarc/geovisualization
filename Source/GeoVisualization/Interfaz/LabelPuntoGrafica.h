// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "LabelPuntoGrafica.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API ULabelPuntoGrafica : public UUserWidget
{
	GENERATED_BODY()
	
public:
    ULabelPuntoGrafica(const FObjectInitializer & ObjectInitializer);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorFondoLabel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USlateBrushAsset * BrushFondoLabel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString Valor;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString Fecha;

	
	
};
