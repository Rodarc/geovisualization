// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/SizeBox.h"
#include "GraficaWidget.h"
#include "../Visualizaciones/VisualizacionWidget.h"
#include "StationWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UStationWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UStationWidget(const FObjectInitializer & ObjectInitializer);

    //virtual void NativeConstruct() override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdArray;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USizeBox * GraficaSizeBox;
	
    //creo que necesitare el tipo de grafica
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaWidget> TypeGrafica;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UGraficaWidget * Grafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UVisualizacionWidget> TypeVisualizacion;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UVisualizacionWidget * VisualizacionWidget;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString StationName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString GeoPosition;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bMostrandoGrafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bVisible;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D SizeWithGrafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D SizeWithoutGrafica;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void MostrarGrafica();
    virtual void MostrarGrafica_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void OcultarGrafica();
    virtual void OcultarGrafica_Implementation();

    void SetInformation(FString InStationName, float Latitud, float Longitud);//o deberia recibir la estructura?

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void CrearDetalle();
    virtual void CrearDetalle_Implementation();

    void CreateVisualizacionOfType(TSubclassOf<UVisualizacionWidget> TypeVis);//no se si esto sea necesario pasarle el parametro

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ReplaceGrafica();
    virtual void ReplaceGrafica_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void RestoreGrafica();
    virtual void RestoreGrafica_Implementation();

    UVisualizacionWidget * HistogramaActual;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void MostrarHistograma(UVisualizacionWidget * Histograma);
    virtual void MostrarHistograma_Implementation(UVisualizacionWidget * Visualizacion);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void OcultarHistograma();
    virtual void OcultarHistograma_Implementation();
};