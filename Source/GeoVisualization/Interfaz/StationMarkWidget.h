// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Materials/Material.h"
#include "Components/Image.h"
#include "StationMarkWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UStationMarkWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UStationMarkWidget(const FObjectInitializer & ObjectInitializer);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int Id;//probabelmente necesite su referencia para poder ubicarme donde corresponga

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdStation;//probabelmente necesite su referencia para poder ubicarme donde corresponga
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float SizeRadio;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D Size;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UMaterial * MarcaMaterial;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UMaterial * MarcaFijaMaterial;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UImage * Imagen;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bFijado;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bVisible;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void Fijar();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void Liberar();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void Mostrar();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void Ocultar();
	
	
};

//el station mark guiara si debo mostrar el label o no, es decir si la estaiocion esta visible, se muestra su label, al igual que su estation mark.