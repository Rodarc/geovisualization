// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "GraficaWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UGraficaWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<bool> VisibilitySeries;
	
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<FVector2D> Puntos;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<FVector2D> PuntosDibujar;

    //UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<TArray<FVector2D>> Series;

    //UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<TArray<FVector2D>> SeriesDibujar;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    TArray<FVector2D> GetPoints();
	
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetPoints(TArray<FVector2D> Points);

    //UFUNCTION(BlueprintCallable, Category = "Grafica")
    TArray<TArray<FVector2D>> GetSeries();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    TArray<FVector2D> GetSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void SetSerie(int IdSerie, TArray<FVector2D> Points);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void SetColorSerie(int IdSerie, FLinearColor ColorSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual int AddSerie(TArray<FVector2D> Points);//retorna el id de la serie en la grafica

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void HideSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ShowSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ClearSeries();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetIntervalX(float a, float b);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetIntervalY(float a, float b);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetIntervalXForSerie(int IdSerie, float a, float b);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetIntervalYForSerie(int IdSerie, float a, float b);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void CalcularCorners();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void CalcularPuntosDibujar();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetMargin(FMargin NuevoMargen);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FMargin Margen;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D Origin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D LUCorner;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D RBCorner;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float YMax;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float YMin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float XMax;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float XMin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> YMaxs;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> YMins;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> XMaxs;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> XMins;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorFondoGrafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<FLinearColor> ColorSeries;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USlateBrushAsset * BrushFondoGrafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bTransformando;
	
	
};
