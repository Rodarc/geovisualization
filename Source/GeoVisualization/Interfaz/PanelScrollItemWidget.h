// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/SizeBox.h"
#include "../Visualizaciones/VisualizacionWidget.h"
#include "PanelScrollItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UPanelScrollItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UPanelScrollItemWidget(const FObjectInitializer & ObjectInitializer);

    //virtual void NativeConstruct() override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USizeBox * VisualizacionSizeBox;

    //neceisto una referencia del station widget del que vengo, o de la grafica de la que vengo
    //esto par saber si algun satation wiget me mando aqui, y no volver a mandarlo, mentras estoy cmparando otras y se me olvide que ya habia mandado esta

    //creo que necesitare el tipo de grafica
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UVisualizacionWidget> TypeVisualizacion;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UVisualizacionWidget * VisualizacionWidget;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    class UStationWidget * StationWidget;
    //si esto no funciona podria usar el indice de la estacion
    //probablemente necesite tamben referencias a las ventanas
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString StationName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString GeoPosition;

    void SetInformation(FString InStationName, FString InGeoPosition);//o deberia recibir la estructura?
    //esta informacion despues sera un struct, de informacino de visualizacion ya que no siempre sera por estacion, depnedera, esta estructura solo contendra informacion para mostrar proporcionada por la visualizacion contenida

    void CreateVisualizacionOfType(TSubclassOf<UVisualizacionWidget> TypeVis);//no se si esto sea necesario pasarle el parametro

    //crea una ventana con la grafica circular
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void CrearVentana();
    virtual void CrearVentana_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void CrearVentanaGraficaCircular();
    virtual void CrearVentanaGraficaCircular_Implementation();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Panel Detalle")
    FText Title;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Panel Detalle")
    FText Subtitle;

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Panel Detalle")
    void UpdateTitle();
    virtual void UpdateTitle_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Panel Detalle")
    void UpdateSubtitle();
    virtual void UpdateSubtitle_Implementation();

};
