// Fill out your copyright notice in the Description page of Project Settings.

#include "VisualizationHUD.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/PanelWidget.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/ScrollBoxSlot.h"
#include "Components/SizeBox.h"
#include "Engine/GameEngine.h"
#include "../GeoVisualizationGameModeBase.h"
#include "Kismet/KismetMathLibrary.h"

UVisualizationHUD::UVisualizationHUD(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    static ConstructorHelpers::FClassFinder<UGraficaWidget> GraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/GraficaWidgetBP.GraficaWidgetBP_C'"));
    if (GraficaWidgetClass.Succeeded()) {
        TypeGrafica = GraficaWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UGraficaDetalleScrollItemWidget> DetalleGraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/GraficaDetalleScrollItemWidgetBP.GraficaDetalleScrollItemWidgetBP_C'"));
    if (DetalleGraficaWidgetClass.Succeeded()) {
        TypeGraficaDetalle = DetalleGraficaWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UPanelScrollItemWidget> PanelScrollItemWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/PanelScrollItemWidgetBP.PanelScrollItemWidgetBP_C'"));
    if (PanelScrollItemWidgetClass.Succeeded()) {
        TypePanelScrollItem = PanelScrollItemWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UVentanaWidget> VentanaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/SistemaVentanas/VentanaWidgetBP.VentanaWidgetBP_C'"));
    if (VentanaWidgetClass.Succeeded()) {
        TypeVentanaWidget = VentanaWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UStationWidget> StationWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/StationWidgetBP.StationWidgetBP_C'"));
    if (StationWidgetClass.Succeeded()) {
        TypeStationWidget = StationWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UStationMarkWidget> StationMarkWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/StationMarkWidgetBP.StationMarkWidgetBP_C'"));
    if (StationMarkWidgetClass.Succeeded()) {
        TypeStationMarkWidget = StationMarkWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UGraficaLineaPanelWidget> GraficaLineaPanelWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/GraficaLineaPanelWidgetBP.GraficaLineaPanelWidgetBP_C'"));
    if (GraficaLineaPanelWidgetClass.Succeeded()) {
        TypeGraficaLineaPanel = GraficaLineaPanelWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UGraficaLineaLabelWidget> GraficaLineaLabelWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/GraficaLineaLabelWidgetBP.GraficaLineaLabelWidgetBP_C'"));
    if (GraficaLineaLabelWidgetClass.Succeeded()) {
        TypeGraficaLineaLabel = GraficaLineaLabelWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UGraficaLineaVentanaWidget> GraficaLineaVentanaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/GraficaLineaVentanaWidgetBP.GraficaLineaVentanaWidgetBP_C'"));
    if (GraficaLineaVentanaWidgetClass.Succeeded()) {
        TypeGraficaLineaVentana = GraficaLineaVentanaWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UGraficaCircularVentanaWidget> GraficaCircularVentanaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/GraficaCircularVentanaWidgetBP.GraficaCircularVentanaWidgetBP_C'"));
    if (GraficaCircularVentanaWidgetClass.Succeeded()) {
        TypeGraficaCircularVentana = GraficaCircularVentanaWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UVisualizacionNJVentanaWidget> VisualizacionNJVentanaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/VisualizacionNJVentanaWidgetBP.VisualizacionNJVentanaWidgetBP_C'"));
    if (VisualizacionNJVentanaWidgetClass.Succeeded()) {
        TypeVisualizacionNJVentana = VisualizacionNJVentanaWidgetClass.Class;
    }

    Months.Add("Enero");
    AbrevMonths.Add("ENE");
    CantidadDiasMes.Add(31);
    Months.Add("Febrero");
    AbrevMonths.Add("FEB");
    CantidadDiasMes.Add(29);
    Months.Add("Marzo");
    AbrevMonths.Add("MAR");
    CantidadDiasMes.Add(31);
    Months.Add("Abril");
    AbrevMonths.Add("ABR");
    CantidadDiasMes.Add(30);
    Months.Add("Mayo");
    AbrevMonths.Add("MAY");
    CantidadDiasMes.Add(31);
    Months.Add("Junio");
    AbrevMonths.Add("JUN");
    CantidadDiasMes.Add(30);
    Months.Add("Julio");
    AbrevMonths.Add("JUL");
    CantidadDiasMes.Add(31);
    Months.Add("Agosto");
    AbrevMonths.Add("AGO");
    CantidadDiasMes.Add(31);
    Months.Add("Septiembre");
    AbrevMonths.Add("SEP");
    CantidadDiasMes.Add(30);
    Months.Add("Octubre");
    AbrevMonths.Add("OCT");
    CantidadDiasMes.Add(31);
    Months.Add("Noviembre");
    AbrevMonths.Add("NOV");
    CantidadDiasMes.Add(30);
    Months.Add("Diciembre");
    AbrevMonths.Add("DIC");
    CantidadDiasMes.Add(31);

    Variables.Add("Temperatura Promedio");
    Variables.Add("Temperatura Maxima");
    Variables.Add("Temperatura Minima");
    Variables.Add("Precipitacion");
    Variables.Add("Profundidad Nieve");

    VariablesURL.Add("temperature_average");
    VariablesURL.Add("temperature_maximum");
    VariablesURL.Add("temperature_minimum");
    VariablesURL.Add("precipitation");
    VariablesURL.Add("snow_depth");

    VariablesAbrev.Add("TAVG");
    VariablesAbrev.Add("TMAX");
    VariablesAbrev.Add("TMIN");
    VariablesAbrev.Add("PRCP");
    VariablesAbrev.Add("SNWD");

    ColorsSeries.Add(FLinearColor::Blue);
    ColorsSeries.Add(FLinearColor::Red);
    ColorsSeries.Add(FLinearColor::Yellow);
    ColorsSeries.Add(FLinearColor::Green);
    ColorsSeries.Add(FLinearColor::Gray);

    MesesFiltrados.SetNum(12);
    for (int i = 0; i < MesesFiltrados.Num(); i++) {
        MesesFiltrados[i] = true;
    }

    NumeroRangosHistograma = 4;

    DistanciaTrace = 2000.0f;
    bBuscarStation = false;
    bBuscarBoton = false;
}

void UVisualizationHUD::NativeConstruct() {
    Super::NativeConstruct();
    /*if (GetWorld()) {
        Label1 = NewObject<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
        //Label1 = <UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
        //Label1 = CreateWidget<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
        Label1->SetText(FText::FromString("hola"));
        UPanelWidget* RootWidget = Cast<UPanelWidget>(GetRootWidget());
        if (RootWidget) {
            UPanelSlot * Slot = RootWidget->AddChild(Label1);
            UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
            CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
            CanvasSlot->SetPosition(FVector2D(50.0f, 50.0f));
            CanvasSlot->SetSize(FVector2D(100.0f, 100.0f));
        }
    }*/
    /*Label1 = NewObject<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
    Label1->SetText(FText::FromString("Visualizando"));
    UPanelWidget* RootWidget = Cast<UPanelWidget>(GetRootWidget());
    UPanelSlot * Slot = RootWidget->AddChild(Label1);
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetPosition(FVector2D(100.0f, 100.0f));
    CanvasSlot->SetSize(FVector2D(100.0f, 100.0f));

    EditBoxX = NewObject<UEditableTextBox>(UGameplayStatics::GetGameInstance(GetWorld()), UEditableTextBox::StaticClass());
    Slot = RootWidget->AddChild(EditBoxX);
    CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetPosition(FVector2D(300.0f, 100.0f));
    CanvasSlot->SetSize(FVector2D(300.0f, 100.0f));

    CambiarLabelButton = NewObject<UButton>(UGameplayStatics::GetGameInstance(GetWorld()), UButton::StaticClass());
    Slot = RootWidget->AddChild(CambiarLabelButton);
    CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetPosition(FVector2D(500.0f, 100.0f));
    CanvasSlot->SetSize(FVector2D(200.0f, 50.0f));
    */
}

void UVisualizationHUD::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
    UWorld * const World = GetWorld();
    if (World) {
        /*if (HitStation) {
            FVector2D PositionScreen;
            bool Proyected = UGameplayStatics::GetPlayerController(World, 0)->ProjectWorldLocationToScreen(HitStation->GetActorLocation() + FVector(0.0f, 0.0f, 2.5f), PositionScreen);
            if (Proyected) {
                UPanelSlot * Slot = StationWidgets[HitStation->Id]->Slot;
                UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
                if (CanvasSlot) {
                    CanvasSlot->SetPosition(PositionScreen - CanvasSlot->GetSize()/2 - FVector2D (0.0f, 150.0f));//esta ultima diferencia deberia ser aplicada de otra forma
                }
                Slot = StationMarkWidgets[HitStation->Id]->Slot;
                CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
                if (CanvasSlot) {
                    CanvasSlot->SetPosition(PositionScreen - CanvasSlot->GetSize()/2);//esta ultima diferencia deberia ser aplicada de otra forma
                }
            }
        }*/
        for (int i = 0; i < StationWidgets.Num(); i++) {
            if (StationMarkWidgets[i]->bVisible) {
                FVector2D PositionScreen;
                bool Proyected = UGameplayStatics::GetPlayerController(World, 0)->ProjectWorldLocationToScreen(VisualizationMapa->StationActors[i]->GetActorLocation() + FVector(0.0f, 0.0f, 2.5f), PositionScreen);
                if (Proyected) {
                    UPanelSlot * SlotStationWidget = StationWidgets[i]->Slot;
                    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotStationWidget);
                    if (CanvasSlot) {
                        CanvasSlot->SetPosition(PositionScreen - CanvasSlot->GetSize()/2 - FVector2D (0.0f, 100.0f));//esta ultima diferencia deberia ser aplicada de otra forma
                    }
                    SlotStationWidget = StationMarkWidgets[i]->Slot;
                    CanvasSlot = Cast<UCanvasPanelSlot>(SlotStationWidget);
                    if (CanvasSlot) {
                        CanvasSlot->SetPosition(PositionScreen - CanvasSlot->GetSize()/2);//esta ultima diferencia deberia ser aplicada de otra forma
                    }
                }
            }
        }
    }
}

FReply UVisualizationHUD::NativeOnKeyDown(const FGeometry & InGeometry, const FKeyEvent & InKeyEvent) {//pressed

    /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Evento."));*/
    if (InKeyEvent.GetKey() == EKeys::LeftMouseButton) {
        SelectPressed();
        return FReply::Handled();
    }
    return FReply::Unhandled();
}

UGraficaDetalleScrollItemWidget * UVisualizationHUD::CreateGraficaDetalle(UStationWidget * StationWidget) {
    //tal vez deberia retornar el puntero de la grafica creada
    //debo crear un size box, don de pondre la grafica, y esta ponerla en el scroll box
    //UGraficaWidget * grafica = CreateWidget<UGraficaWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeGrafica);
    UGraficaDetalleScrollItemWidget * grafica = CreateWidget<UGraficaDetalleScrollItemWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeGraficaDetalle);
    //Graficas.Add(grafica);
    grafica->AddToViewport(1);
    grafica->StationWidget = StationWidget;
    if (PanelSB) {
        UPanelSlot * SlotGrafica = PanelSB->AddChild(grafica);
        UScrollBoxSlot * ScrollBoxSlot = Cast<UScrollBoxSlot>(SlotGrafica);
        ScrollBoxSlot->SetPadding(FMargin(0.0f, 2.5f));
    }
    return grafica;
}

UPanelScrollItemWidget * UVisualizationHUD::CreateVisualizacionInPanelFromLabel(UStationWidget * StationWidget) {
    //UGraficaWidget * grafica = CreateWidget<UGraficaWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeGrafica);
    UPanelScrollItemWidget * ScrollItem = CreateWidget<UPanelScrollItemWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypePanelScrollItem);
    ScrollItem->AddToViewport(1);

    //aqui deberia haber un if para crear la grafica en funcion del tipo de grafica del label, o el que este activado a nivel general
    ScrollItem->CreateVisualizacionOfType(TypeGraficaLineaPanel);

    ScrollItem->StationWidget = StationWidget;//es necesario darle esa referencia siempre? dependera de algo? se que seria util solo para La de linas y para la circular, para saber de que estacion es
    if (PanelSB) {
        UPanelSlot * SlotScrollItem = PanelSB->AddChild(ScrollItem);
        UScrollBoxSlot * ScrollBoxSlot = Cast<UScrollBoxSlot>(SlotScrollItem);
        ScrollBoxSlot->SetPadding(FMargin(0.0f, 2.5f));
    }
    return ScrollItem;
}

UVentanaWidget * UVisualizationHUD::CreateVisualizacionInWorkSpaceFromPanelItem(UPanelScrollItemWidget * PanelItemWidget, EVisualizationType TypeVis) {
    UVentanaWidget * Ventana = CreateWidget<UVentanaWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeVentanaWidget);
    Ventana->AddToViewport(1);

    //aqui deberia haber un if para crear la grafica en funcion del tipo de grafica del label, o el que este activado a nivel general
    
    switch (TypeVis) {
		case EVisualizationType::EGraficaLineas: {
			Ventana->CreateVisualizacionOfType(TypeGraficaLineaVentana);
		}
		break;
		case EVisualizationType::EGraficaCircular: {
			Ventana->CreateVisualizacionOfType(TypeGraficaCircularVentana);
		}
		break;
    }
    //Ventana->CreateVisualizacionOfType(TypeVis);
    //Ventana->CreateVisualizacionOfType(TypeGraficaCircularVentana);

    //Ventana->StationWidget = StationWidget;//es necesario darle esa referencia siempre? dependera de algo? se que seria util solo para La de linas y para la circular, para saber de que estacion es
    if (WorkSpace) {
        UPanelSlot * SlotVentana = WorkSpace->AddChild(Ventana);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotVentana);
        CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
        CanvasSlot->SetPosition(FVector2D(200.0f, 200.0f));
        CanvasSlot->SetSize(FVector2D(500.0f, 400.0f));
        USizeBox * SizeBoxWidget = Cast<USizeBox>(Ventana->GetRootWidget());
        if (SizeBoxWidget) {
            //CanvasSlot->SetSize(FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
            CanvasSlot->SetSize(FVector2D(SizeBoxWidget->WidthOverride, SizeBoxWidget->HeightOverride));
        }

    }
    return Ventana;
}

UVentanaWidget * UVisualizationHUD::CreateVisualizacionInWorkSpaceFromHUD() {
    UVentanaWidget * Ventana = CreateWidget<UVentanaWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeVentanaWidget);
    Ventana->AddToViewport(1);

    //aqui deberia haber un if para crear la grafica en funcion del tipo de grafica del label, o el que este activado a nivel general
    Ventana->CreateVisualizacionOfType(TypeVisualizacionNJVentana);
    Ventana->VisualizacionWidget->Consulta = ConsultaActual;
    Ventana->VisualizacionWidget->PrepararYearsFiltrados();
    //deberia pasar mas informacion, por ahora solo esto

    //Ventana->StationWidget = StationWidget;//es necesario darle esa referencia siempre? dependera de algo? se que seria util solo para La de linas y para la circular, para saber de que estacion es
    if (WorkSpace) {
        UPanelSlot * SlotVentana = WorkSpace->AddChild(Ventana);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotVentana);
        CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
        CanvasSlot->SetPosition(FVector2D(200.0f, 200.0f));
        CanvasSlot->SetSize(FVector2D(500.0f, 400.0f));
        USizeBox * SizeBoxWidget = Cast<USizeBox>(Ventana->GetRootWidget());
        if (SizeBoxWidget) {
            //CanvasSlot->SetSize(FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
            CanvasSlot->SetSize(FVector2D(SizeBoxWidget->WidthOverride, SizeBoxWidget->HeightOverride));
        }

    }
    Ventana->UpdateFilters();
    Ventana->UpdateTitleVentana();
    Ventana->CollapseFilters();
    return Ventana;
}

void UVisualizationHUD::SetIntervalYears(int YearInicio, int YearFin) {
    for (int year = YearInicio; year <= YearFin; year++) {
        //YearsConsultados.AddUnique(year);//agrego en el array
        Years.AddUnique(FString::FromInt(year));
        //UE_LOG(LogClass, Log, TEXT("Add year: %d"), year);
    }
}

void UVisualizationHUD::Actualizar_Implementation() {
}

void UVisualizationHUD::ActualizarOpciones_Implementation() {
}


/*TArray<FVector2D> UVisualizationHUD::ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct>& Datos) {
    TArray<FVector2D> res;
    for (int i = 0; i < Datos.Num(); i++) {
        if (Datos[i].value != -9999) {
            int idMonthConsultado = MonthsConsultados.Find(Datos[i].month-1);
            int idYearConsultado = Datos[i].year - YearsConsultados[0];//este se podria calcular mas, rapido en base a una resta con el primer elemento
            //int idYearConsultado = YearsConsultados.Find(Datos[i].year);
            int ValX = idYearConsultado * CantidadDiasYearConsulta + Datos[i].day - 1;
            if (idMonthConsultado > 0) {
                ValX += AcumCantidadDiasMesConsultados[idMonthConsultado - 1];
            }
            res.Add(FVector2D(ValX, Datos[i].value));
        }
    }
    return res;
}*/

void UVisualizationHUD::CreateStations(TArray<FStationStruct> NewStations) {
    //esta funcion llamara a CrearStations del VisualizationMap
    //y llamara a la funcion CrearStationsWidget de esta clase, donde creare los station widget, y los referenciara ya sea por puntero o por id a quienes corresponda

    ConsultaActual.StationConsultas.SetNum(NewStations.Num());
    for (int i = 0; i < NewStations.Num(); i++) {
        ConsultaActual.StationConsultas[i].StationData = NewStations[i];
    }

    Stations = NewStations;
    if (VisualizationMapa) {
        VisualizationMapa->CreateStations(Stations);
    }
    CreateStationsWidget();
    CreateStationMarkWidgets();
    HabilitarStations();//aqui llamo a habilitar estaciones, y luego las selecciono con doble click
    for (int i = 0; i < Stations.Num(); i++) {//esto deberia estar en otro lado, despues de crearse, antes de iniciar la primera consulta, podria ser despues de llamarse esta funcion
        IBotonInterface::Execute_DoubleClick(VisualizationMapa->StationActors[i]);//para que tambien mande el mensaje de seleccion y el resaltado en las opciones
        //SelectStation(i);//en realidad deberia ser por el aarya de estaciones o sus datos
        //en teoria deberia seleccionar a traves del hud
        //SelectedStations.Add(i);//o seria mejor tener boolanos para cada estacion, y activar o descativar segun sea el caso, y dejar este array solo para las consultas
    }
}

void UVisualizationHUD::CreateStationsWidget() {
    for (int i = 0; i < Stations.Num(); i++) {
        UStationWidget * StationWidget = CreateWidget<UStationWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeStationWidget);
        StationWidget->IdArray = i;
        StationWidget->SetInformation(Stations[i].name, Stations[i].latitud, Stations[i].longitud);
        StationWidgets.Add(StationWidget);
        StationWidget->AddToViewport(0);

        StationWidget->CreateVisualizacionOfType(TypeGraficaLineaLabel);

        //grafica->SetAlignmentInViewport(FVector2D(0.5, 0.5));//este no es necesario
        //grafica->SetAnchorsInViewport();
        //grafica->SetPositionInViewport(FVector2D(0.0f, 0.0f));

        UPanelWidget* RootWidget = Cast<UPanelWidget>(GetRootWidget());
        UPanelSlot * SlotStationWidget = RootWidget->AddChild(StationWidget);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotStationWidget);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        CanvasSlot->SetZOrder(-1);

        USizeBox * SizeBoxWidget = Cast<USizeBox>(StationWidget->GetRootWidget());
        if (SizeBoxWidget) {
            CanvasSlot->SetSize(FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
        }

        CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
        CanvasSlot->SetPosition(FVector2D(350.0f, 700.0f));
        //CanvasSlot->SetSize(FVector2D(600.0f, 400.0f));
        //pasarle la informacion pertinente

        StationWidget->SetVisibility(ESlateVisibility::Hidden);//deben iniciar ocultos, visible segun lo necesite

        StationWidget->OcultarGrafica();
    }
}

void UVisualizationHUD::CreateStationMarkWidgets() {
    for (int i = 0; i < Stations.Num(); i++) {
        UStationMarkWidget * StationMarkWidget = CreateWidget<UStationMarkWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeStationMarkWidget);
        StationMarkWidget->Id = i;
        StationMarkWidgets.Add(StationMarkWidget);
        StationMarkWidget->AddToViewport(0);
        //grafica->SetAlignmentInViewport(FVector2D(0.5, 0.5));//este no es necesario
        //grafica->SetAnchorsInViewport();
        //grafica->SetPositionInViewport(FVector2D(0.0f, 0.0f));

        UPanelWidget* RootWidget = Cast<UPanelWidget>(GetRootWidget());
        UPanelSlot * SlotStationMark = RootWidget->AddChild(StationMarkWidget);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotStationMark);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot

        CanvasSlot->SetZOrder(-2);
        USizeBox * SizeBoxWidget = Cast<USizeBox>(StationMarkWidget->GetRootWidget());
        if (SizeBoxWidget) {
            CanvasSlot->SetSize(FVector2D(SizeBoxWidget->WidthOverride, SizeBoxWidget->HeightOverride));
        }

        CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
        CanvasSlot->SetPosition(FVector2D(350.0f, 700.0f));
        //CanvasSlot->SetSize(FVector2D(600.0f, 400.0f));
        //pasarle la informacion pertinente

        StationMarkWidget->SetVisibility(ESlateVisibility::Hidden);//deben iniciar ocultos, visible segun lo necesite
    }
}

void UVisualizationHUD::HabilitarStations_Implementation() {
}

AStation * UVisualizationHUD::TraceStation_Implementation() {
    //UGameplayStatics::GetGameInstance()->GetGameViewportClient()->GetMousePosition
    FVector2D MousePosition;
    FVector WorldLocation;
    FVector WorldDirection;
    bool MouseInScreen;
    UWorld * const World = GetWorld();
    if (World) {
        MouseInScreen = UGameplayStatics::GetGameInstance(World)->GetGameViewportClient()->GetMousePosition(MousePosition);
    }
    else {
        MouseInScreen = false;
    }
    if (MouseInScreen && World) {
        APlayerController * PlayerController = UGameplayStatics::GetPlayerController(World, 0);
        if (PlayerController) {
            bool Projected = PlayerController->DeprojectScreenPositionToWorld(MousePosition.X, MousePosition.Y, WorldLocation, WorldDirection);
            if (Projected) {//puedo hacer un line trace
                //FCollisionQueryParams NodoTraceParams = FCollisionQueryParams(FName(TEXT("TraceNodo")), true, this);
                FVector PuntoInicial = WorldLocation;
                FVector PuntoFinal = WorldLocation + WorldDirection * DistanciaTrace;
                //PuntoInical = PuntoInicial + Vec * 10;//para que no se choque con lo que quiero, aun que no deberia importar
                TArray<TEnumAsByte<EObjectTypeQuery> > TiposObjetos;
                TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery7);//Nodo
                //TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery2);//World dynamic, separado esta funcionando bien, supongo que tendre que hacer oto trace par saber si me estoy chocando con la interfaz, y no tener encuenta esta busqueda
                //podria agregar los world static y dynamic, para asi avitar siempre encontrar algun nodo que este destrar de algun menu, y que por seleccionar en el menu tambien le de click a el
                TArray<AActor*> ActoresIgnorados;
                FHitResult Hit;
                bool trace = UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), PuntoInicial, PuntoFinal, TiposObjetos, false, ActoresIgnorados, EDrawDebugTrace::None, Hit, true, FLinearColor::Blue);//el none es para que no se dibuje nada
                //hit se supone que devovera al actor y el punto de impacto si encontró algo, castearlo a nodo, y listo
                AStation * StationEncontrado = nullptr;
                if (trace) {//de pronto no necesito, esto, el caste me denbolvera null
                    //solo que al agregar el worldynamic ,tengo que castear y verificar
                    StationEncontrado = Cast<AStation>(Hit.Actor.Get());
                    //UE_LOG(LogClass, Log, TEXT("Station encontrado"));
                    if (StationEncontrado) {//no estaba
                        //en que momento debo incluir la seccion del label? despues de todo esto, en otra funcion, o en este mismo codigo?

                        /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
                            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Station traced."));*/

                    }
                    //DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 1.5f, 6, FColor::Black, false, 0.0f, 0, 0.25f);
                    
                    //return Hit.ImpactPoint;//si quito toto el if anterior debo activar esta linea, y liesto
                }
                //y si esta funcion es solo para esto, y luego ya verifico si es tal o cual cosa, en otra parte del codigo?

                //DrawDebugLine(GetWorld(), SourceNodo->GetActorLocation(), TargetNodo->GetActorLocation(), FColor::Black, false, -1.0f, 0, Radio*Escala);
                //StationEncontrado = nullptr;
                return StationEncontrado;// los casos manejarlos afuera
            }
        }
    }
    return nullptr;
}

void UVisualizationHUD::TracingStation_Implementation() {
    AStation * StationEncontrado = TraceStation();
    if (StationEncontrado && bBuscarStation) {//comprobamos la interaccion para que no se detecte lo que este detras del menu
        if (HitStation && HitStation != StationEncontrado) {//quiza se pueda hacer con el boleano, debo ocultar si es que es diferente al de ahora,
            //demo mostrar el stationwidget que corresponde
            //ocultar el del anteioe
            //HitStation->OcultarNombre();
            //StationEncontrado->MostrarNombre();
            if (!HitStation->bFijado) {
                HideStationWidget(HitStation->Id);
                HitStation->DesactivarResaltado();
                DesactivarResaltadoStation(HitStation);
            }
            if (!StationEncontrado->bFijado) {
                ShowStationWidget(StationEncontrado->Id);
                StationEncontrado->ActivarResaltado();
                ActivarResaltadoStation(StationEncontrado);
            }
        }
        else {//este el se creo que esta demas
            if (!StationEncontrado->bFijado) {
                ShowStationWidget(StationEncontrado->Id);
                //StationEncontrado->MostrarNombre();
                //if(!StationEncontrado->EfectoResaltado->IsActive())
                StationEncontrado->ActivarResaltado();
                ActivarResaltadoStation(StationEncontrado);
            }
        }
        HitStation = StationEncontrado;//podria dejar ests 3 lineas, y borrar las de adentro
        bHitStation = true;
    }
    else {//si no estoy golpeando algun nodo
        if (HitStation) {
            if (!HitStation->bFijado) {//borrar este nivel e if
                HideStationWidget(HitStation->Id);
                //HitStation->OcultarNombre();
                HitStation->DesactivarResaltado();
                DesactivarResaltadoStation(HitStation);
            }
        }
        HitStation = nullptr;
        bHitStation = false;
        //el caso contrario, seria encontrar como lo deje con el if anterior, asi que no se hace nada
    }
}

void UVisualizationHUD::ShowStationWidget_Implementation(int Id) {
    StationWidgets[Id]->SetVisibility(ESlateVisibility::Visible);//deben iniciar ocultos, visible segun lo necesite
}

void UVisualizationHUD::HideStationWidget_Implementation(int Id) {
    StationWidgets[Id]->SetVisibility(ESlateVisibility::Hidden);//deben iniciar ocultos, visible segun lo necesite
}

void UVisualizationHUD::ActivarResaltadoStation(AStation * Station) {
    StationMarkWidgets[Station->Id]->Mostrar();
    ShowStationWidget(Station->Id);
    /*TemporalStationMark = CreateWidget<UStationMarkWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeStationMarkWidget);
    TemporalStationMark->AddToViewport(1);
    TemporalStationMark->IdStation = Station->Id;*/
    //TemporalStationMark->Fijar();
}

void UVisualizationHUD::FijarResaltadoStation(AStation * Station) {
    StationMarkWidgets[Station->Id]->Fijar();
}

void UVisualizationHUD::LiberarResaltadoStation(AStation * Station) {
    StationMarkWidgets[Station->Id]->Liberar();
}

void UVisualizationHUD::DesactivarResaltadoStation(AStation * Station) {
    StationMarkWidgets[Station->Id]->Ocultar();
    HideStationWidget(Station->Id);
    //TemporalStationMark->RemoveFromParent();
    //TemporalStationMark->Destruct();
    //TemporalStationMark->Fijar();
}

void UVisualizationHUD::SelectPressed() {
    /*if (HitStation) {
        FijarResaltadoStation(HitStation);
    }*/
}

void UVisualizationHUD::SelectReleased() {
}

void UVisualizationHUD::Consultar(TArray<int> StationsConsulta, int InitialYearConsulta, int FinalYearConsulta, TArray<FString> MesesConsulta, TArray<FString> VariablesConsulta) {
    //podria tener un bool o un contador, para saber cuando he recibido todos los resultados, aqui inclusto podria lanzar varias consultas, en lugar de solo una, y entrar en una especia de pila de consultas.
    //debo mandar una consulta por cada estacion seleccionada
    //debo convertir el array de strings a array de ints
    //
    //TArray<int> IdMesesConsulta;
    LimpiarGraficas();

    //debo convertir las variables
    for(int i = 0; i < VariablesConsulta.Num(); i++){
        VariablesConsulta[i] = VariablesURL[Variables.Find(VariablesConsulta[i])];
    }
    SelectedVariables = VariablesConsulta;

    //guardando las variables consultadas;
    ConsultaActual.VariablesConsultadas = VariablesConsulta;
    ConsultaActual.InitialYear = InitialYearConsulta;
    ConsultaActual.FinalYear = FinalYearConsulta;

    MonthsConsultados.Empty();
    AcumCantidadDiasMesConsultados.Empty();
    int acum = 0;
    for (int i = 0; i < MesesConsulta.Num(); i++) {
        int idMesConsultado = Months.Find(MesesConsulta[i]);
        MonthsConsultados.AddUnique(idMesConsultado);
        acum += CantidadDiasMes[idMesConsultado];
        AcumCantidadDiasMesConsultados.AddUnique(acum);
    }
    if (AcumCantidadDiasMesConsultados.Num()) {
        CantidadDiasYearConsulta = AcumCantidadDiasMesConsultados[AcumCantidadDiasMesConsultados.Num() - 1];
    }
    else {
        CantidadDiasYearConsulta = 0;
    }
    YearsConsultados.Empty();
    for (int i = InitialYearConsulta; i <= FinalYearConsulta; i++) {
        YearsConsultados.AddUnique(i);
    }

    ConsultaActual.MesesConsultados = MonthsConsultados;//para copiar los meses como ids

    FeedbackConsulta();
    UE_LOG(LogTemp, Warning, TEXT("Iniciando: %d"), StationsConsulta.Num() * VariablesConsulta.Num());
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        NumeroSubConsultas = StationsConsulta.Num() * VariablesConsulta.Num();
        ConsultaVariableStations.Empty();
        ConsultaVariableStations.SetNum(StationsConsulta.Num());//tener cuidado con esto, por si no consulto todas las estaciones

        ConsultaActual.StationConsultas.SetNum(StationsConsulta.Num());//en realidad deberia ser todas las estaciones, por ahora dejar asi
        //no deberia hacer esto, toda las staciones seran consultadas
        for (int i = 0; i < ConsultaActual.StationConsultas.Num(); i++) {
            ConsultaActual.StationConsultas[i].ConsultasPorVariable.SetNum(VariablesConsulta.Num());
        }

        for (int i = 0; i < ConsultaVariableStations.Num(); i++) {
            ConsultaVariableStations[i].SetNum(VariablesConsulta.Num());//solo deberia reservar espacio para las variables que he consultado, o seleccionado no con todo, al igual que las esaciones
            for (int j = 0; j < VariablesConsulta.Num(); j++) {
                GeoGameMode->GetVariableDataYearsMonthsForStation(StationsConsulta[i]+1, InitialYearConsulta, FinalYearConsulta, MonthsConsultados, VariablesConsulta[j]);//los meses aqui estan con el indicie en 0
            }
        }
    }

}

void UVisualizationHUD::ConsultarGeneral(int InitialYearConsulta, int FinalYearConsulta, TArray<FString> MesesConsulta, TArray<FString> VariablesConsulta) {
    //podria tener un bool o un contador, para saber cuando he recibido todos los resultados, aqui inclusto podria lanzar varias consultas, en lugar de solo una, y entrar en una especia de pila de consultas.
    //debo mandar una consulta por cada estacion seleccionada
    //debo convertir el array de strings a array de ints
    //
    //TArray<int> IdMesesConsulta;
    LimpiarGraficas();//esto deberia estar afuera, no forma parte de esto, si bien se deben dar ambas, esto no forma parte

    //guardando las variables consultadas originales, sin transformar
    ConsultaActual.VariablesConsultadas = VariablesConsulta;
    ConsultaActual.InitialYear = InitialYearConsulta;
    ConsultaActual.FinalYear = FinalYearConsulta;

    

    //debo convertir las variables, cambiando a url para,
    for(int i = 0; i < VariablesConsulta.Num(); i++){
        VariablesConsulta[i] = VariablesURL[Variables.Find(VariablesConsulta[i])];
    }
    SelectedVariables = VariablesConsulta;

    MonthsConsultados.Empty();
    AcumCantidadDiasMesConsultados.Empty();
    int acum = 0;
    for (int i = 0; i < MesesConsulta.Num(); i++) {
        int idMesConsultado = Months.Find(MesesConsulta[i]);
        MonthsConsultados.AddUnique(idMesConsultado);
        acum += CantidadDiasMes[idMesConsultado];
        AcumCantidadDiasMesConsultados.AddUnique(acum);
    }
    if (AcumCantidadDiasMesConsultados.Num()) {
        CantidadDiasYearConsulta = AcumCantidadDiasMesConsultados[AcumCantidadDiasMesConsultados.Num() - 1];
    }
    else {
        CantidadDiasYearConsulta = 0;
    }
    YearsConsultados.Empty();
    for (int i = InitialYearConsulta; i <= FinalYearConsulta; i++) {
        YearsConsultados.AddUnique(i);
    }

    ConsultaActual.MesesConsultados = MonthsConsultados;

    FeedbackConsulta();
    UE_LOG(LogTemp, Warning, TEXT("Iniciando: %d"), Stations.Num() * VariablesConsulta.Num());
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        NumeroSubConsultas = Stations.Num() * VariablesConsulta.Num();

        ConsultaActual.StationConsultas.SetNum(Stations.Num());//en realidad deberia ser todas las estaciones, por ahora dejar asi
        //no deberia hacer esto, toda las staciones seran consultadas
        for (int i = 0; i < ConsultaActual.StationConsultas.Num(); i++) {
            ConsultaActual.StationConsultas[i].ConsultasPorVariable.SetNum(VariablesConsulta.Num());
            for (int j = 0; j < VariablesConsulta.Num(); j++) {
                //en lugar de station.id, podria ir i+1
                GeoGameMode->GetVariableDataYearsMonthsForStation(Stations[i].id, InitialYearConsulta, FinalYearConsulta, MonthsConsultados, VariablesConsulta[j]);//los meses aqui estan con el indicie en 0
            }
        }
    }

}

void UVisualizationHUD::RecibirConsulta(TArray<FDailyDataStruct> & Datos, FString varible) {//digamos que esto es de una sola station
    //el problema es que aqui no se que estaciones han sido consultadas, podria consultar solo 5 estaciones, pero no se quienes son esas 5 estaciones
    //esta funcion recibira consultas por cada estacion.
    //debo verifcar el primer item del resultado para saber de que estacion es, pero que pasa si en algun momento recibo 0?, pues el arraya al que deberia copiar se quedaria en 0 no necesito saber cual es,
    //asignar resutalado, esto recibe de una sationc, no se de cual, las lanzara o devolvera en orden el servidor?
    //agregar este resultado, al array de arrays de datos
    if (Datos.Num()) {
        Datos[0].id_station;//por ahora idstation corresponde al ide de estaciones en el array -1, pero evitar ello
        UE_LOG(LogTemp, Warning, TEXT("Consulta de id_station: %d"), Datos[0].id_station);
        //si hay datos , debo buscar este id seeleccionado -1 1 en el array de seleccionados para obtener su indice y saber donde poenrlo en la consulta
        //int idlocalstation;
        int idlocalvariable;
        if (Datos[0].id_station - 1 < Stations.Num() && SelectedVariables.Find(varible, idlocalvariable)) {
            //si fue encontrado le asigno

            ConsultaActual.StationConsultas[Datos[0].id_station - 1].ConsultasPorVariable[idlocalvariable].Datos = Datos;
            ConsultaActual.StationConsultas[Datos[0].id_station - 1].ConsultasPorVariable[idlocalvariable].Variable = varible;
        }
    }
    //ConsultaStations.Add(Datos);
    NumeroSubConsultas--;

    UE_LOG(LogTemp, Warning, TEXT("Consultas faltantes: %d"), NumeroSubConsultas);
    if (!NumeroSubConsultas) {
        UE_LOG(LogTemp, Warning, TEXT("Consultas completas"));
        PrepararYearsFiltrados();
        UpdateFilters();
        ActualizarGraficas();
        FeedbackRecibirConsulta();
		//VisualizationMapa->PaintDepartamentosBlanco();
        //debo poner los fiiltros en true
        //deberia tener otra funcion que actualice los filtros
        //ocultar el widget de cargando consulta
        //actualizar o apsar la informacion a los station widgets


        //calcular promedios, quiza ese calculo lo hago en en actualizar graficas
        //en otra funcion debo aplicar elevaciones en funion de una de las variables
    }
}

void UVisualizationHUD::FeedbackConsulta_Implementation() {
}

void UVisualizationHUD::FeedbackRecibirConsulta_Implementation() {
}

void UVisualizationHUD::ActualizarGraficas() {//en realidad es actualizar labesl

    XMins.SetNum(SelectedVariables.Num());
    XMaxs.SetNum(SelectedVariables.Num());
    YMins.SetNum(SelectedVariables.Num());
    YMaxs.SetNum(SelectedVariables.Num());
    for (int i = 0; i < SelectedVariables.Num(); i++) {
        XMins[i] = 0;
        XMaxs[i] = YearsConsultados.Num() * CantidadDiasYearConsulta;
        YMins[i] = 0;
        YMaxs[i] = 1;
    }

    for (int i = 0; i < ConsultaActual.StationConsultas.Num(); i++) {//estacoines
        for (int j = 0; j < ConsultaActual.StationConsultas[i].ConsultasPorVariable.Num(); j++) {//variables
            for (int k = 0; k < ConsultaActual.StationConsultas[i].ConsultasPorVariable[j].Datos.Num(); k++) {//datos
                float TempValue = ConsultaActual.StationConsultas[i].ConsultasPorVariable[j].Datos[k].value;
                if (TempValue != -9999) {
                    /*if (ConsultaStations[i][j].day < XMin) {
                        XMin = ConsultaStations[i][j].day;
                    }
                    if (ConsultaStations[i][j].day > XMax) {
                        XMax = ConsultaStations[i][j].day;
                    }*/
                    if (TempValue < YMins[j]) {
                        YMins[j] = TempValue;
                    }
                    if (TempValue > YMaxs[j]) {
                        YMaxs[j] = TempValue;
                    }
                }
            }
        }
    }

    //la pregunta es como asignarle correctamente los datos si lleghan desordenados!!
    for (int i = 0; i < Stations.Num(); i++) {
        UGraficaLineaWidget * GraficaLinea = Cast<UGraficaLineaWidget>(StationWidgets[i]->VisualizacionWidget);
        if (GraficaLinea) {
            GraficaLinea->Consulta.FinalYear = ConsultaActual.FinalYear;
            GraficaLinea->Consulta.InitialYear = ConsultaActual.InitialYear;
            GraficaLinea->Consulta.MesesConsultados = ConsultaActual.MesesConsultados;
            GraficaLinea->Consulta.VariablesConsultadas = ConsultaActual.VariablesConsultadas;
            GraficaLinea->Consulta.StationConsultas.Empty();
            GraficaLinea->Consulta.StationConsultas.Add(ConsultaActual.StationConsultas[i]);//copiand los datos que le corresponeden a esta grafica
            //informacion pasada, ahora debo actualizar la visualizacion en si misma, lo deberia hacer la propia visualizacion, o lo deberia hacer esta cosa?, por ahora lo hare aqui, luego lo encapsulo de forma interna
            GraficaLinea->GeneralColorSeries = ColorsSeries;//Le copio los colores ya que estos son fijos para el numero de variables, aun no es automatico, en aquel caso lo calculara de forma interna la grafica.
            GraficaLinea->AcumCantidadDiasMesConsultados = AcumCantidadDiasMesConsultados;
            GraficaLinea->CantidadDiasYearConsulta = CantidadDiasYearConsulta;
            GraficaLinea->CantidadDiasMes = CantidadDiasMes;//este deberia estar ya por defecto en las visualizaciones, ya que es independiente de los calculos

            for (int j = 0; j < GraficaLinea->Consulta.StationConsultas[0].ConsultasPorVariable.Num(); j++) {
                //Los intervalos de y son necearios para que todas las grafica esten uniformizadas en la misma escala de valores
                //los intervalos de x no son necessarios, ya que estos seran modificados por la grafica ventana, cuando haya filtros, por lo tanto deberian calcularse solos en todas las graficas, por ahora los copio
                GraficaLinea->Consulta.StationConsultas[0].ConsultasPorVariable[j].YMin = YMins[j];
                GraficaLinea->Consulta.StationConsultas[0].ConsultasPorVariable[j].YMax = YMaxs[j];
                GraficaLinea->Consulta.StationConsultas[0].ConsultasPorVariable[j].XMin = XMins[j];
                GraficaLinea->Consulta.StationConsultas[0].ConsultasPorVariable[j].XMax = XMaxs[j];

                //lo siguientes es llamaar a la grafica, y decirle que se actualice solo
                //eso significa que solo convierta los datos a cun Vector@d y comience a crear las series para dibujarse
            }
            //no llamo a calcular puntos dibujar?
            GraficaLinea->PrepararYearsFiltrados();
            GraficaLinea->VisualizarConsulta();
            StationWidgets[i]->MostrarGrafica();
        }
    }
    //calcular promedio apra las estaciones solo de la primera variable
    ActualizarPromedios();
    /*if (SelectedVariables.Num()) {
        VisualizationMapa->ValorMinimo = YMins[0];
        VisualizationMapa->ValorMaximo = YMaxs[0];
        //deberia nivelar otra vez a 0 las elevaciones
        for (int i = 0; i < ConsultaActual.StationConsultas.Num(); i++) {
            for (int k = 0; k < ConsultaActual.StationConsultas[i].ConsultasPorVariable.Num(); k++) {
                float VariablePromedio = 0.0f;
                int VariableCount = 0;
                for (int j = 0; j < ConsultaActual.StationConsultas[i].ConsultasPorVariable[k].Datos.Num(); j++) {//pero deberi ser solo los validos, si es invalido no deberia sumar
                    float DataValue = ConsultaActual.StationConsultas[i].ConsultasPorVariable[k].Datos[j].value;
                    if (DataValue != -9999) {
                        VariableCount++;
                        VariablePromedio += DataValue;
                    }
                }
                if (VariableCount) {
                    VariablePromedio /= VariableCount;
                    
                    float AlturaLimitada = VariablePromedio / (YMaxs[k] - YMins[k]);
                    //aqui creo barras, pero los calculos son similares a actualizar promedios, deberia tener alguna forma de juntar ambas funciones
                    int IdBarra = VisualizationMapa->StationActors[i]->AddBarra();//el id de la barra deberia ser igual a k
                    VisualizationMapa->StationActors[i]->SetValueBarra(IdBarra, AlturaLimitada);
                    VisualizationMapa->StationActors[i]->SetColorBarra(IdBarra, ColorsSeries[k]);
                    //FVector TempPunto = VisualizationMapa->StationActors[i]->GetRootComponent()->GetRelativeTransform().GetLocation();
                    //TempPunto.Z = 0.0f;
                    //float AlturaLimitada = VisualizationMapa->CalcularAlturaLimitada(TempPromedio);
                    //VisualizationMapa->ElevarEn(TempPunto, AlturaLimitada);
                    //VisualizationMapa->StationActors[i]->MoveToAltura(AlturaLimitada);
                }
            }
            VisualizationMapa->StationActors[i]->DistribuirBarras();
        }
    }*/
    //el promedio esta solo en funcion de la primera variable
    //VisualizationMapa->Elevar();
    //las estaiciones no selecionadas deberian bajar al nivel 0.
    //las setaciones deberian tener referencia a los datos para poder usarse
    //promedio para cada una de las variables para cada estacion?
}

void UVisualizationHUD::LimpiarGraficas() {
    for (int i = 0; i < StationWidgets.Num(); i++) {
        //en realidad esta serie debe ser en funion de la variable
        //StationWidgets[i]->Grafica->ClearSeries();
        UGraficaLineaWidget * GraficaLinea = Cast<UGraficaLineaWidget>(StationWidgets[i]->VisualizacionWidget);
        if (GraficaLinea) {
            GraficaLinea->ClearSeries();
        }
        //esto deberia ser solo limpiar, pero en caso de que haga cambios entre linea y circular, con otra funcion debo eliminar los widgets y crear nuevos, por ahora puedo seguir usando esto
        StationWidgets[i]->OcultarGrafica();
    }
    for (int i = 0; i < VisualizationMapa->StationActors.Num(); i++) {
        VisualizationMapa->StationActors[i]->ClearBarras();
    }
}

void UVisualizationHUD::SelectStation_Implementation(int id) {
    SelectedStations.AddUnique(id);
    SelectedStations.StableSort();//me serive de algo tener esto ordenado?
}

void UVisualizationHUD::UnselectStation_Implementation(int id) {
    SelectedStations.Remove(id);
}

void UVisualizationHUD::ShowStationsOfDepartament_Implementation(const FString & NameDepartamento) {
    //le digo a los actores station que corresponda que se muestren
    //for (int i = 0; i < VisualizationMapa->StationActors.Num(); i++) {//quiza esto deberia estar en un mapa o algo asi
    for (int i = 0; i < VisualizationMapa->StationActors.Num(); i++) {//quiza esto deberia estar en un mapa o algo asi
        if (Stations[i].departament == NameDepartamento) {
            IBotonInterface::Execute_Show(VisualizationMapa->StationActors[i]);
        }
    }
}

void UVisualizationHUD::HideStationsOfDepartament_Implementation(const FString & NameDepartamento) {
    for (int i = 0; i < VisualizationMapa->StationActors.Num(); i++) {//quiza esto deberia estar en un mapa o algo asi
        if (Stations[i].departament == NameDepartamento) {
            IBotonInterface::Execute_Unselect(VisualizationMapa->StationActors[i]);
            IBotonInterface::Execute_Hide(VisualizationMapa->StationActors[i]);
            LiberarResaltadoStation(VisualizationMapa->StationActors[i]);
            UnselectStation(VisualizationMapa->StationActors[i]->Id);
        }
    }
}

void UVisualizationHUD::ClickEvent() {
    /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Click Event."));*/
    if (HitBoton) {
        IBotonInterface::Execute_Click(Cast<AActor>(HitBoton));
    }
    if (HitStation) {
        if (HitStation->bFijado) {
            HitStation->bFijado = false;
            LiberarResaltadoStation(HitStation);//esto es solo para cambiar a un ascpeto fijado
        }
        else {
            HitStation->bFijado = true;
            FijarResaltadoStation(HitStation);//para cambiar a un aspecto fijado
        }
    }
}

void UVisualizationHUD::DoubleClickEvent() {
    /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
        GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Double Click Event."));*/
    if (HitBoton) {
        IBotonInterface::Execute_DoubleClick(Cast<AActor>(HitBoton));
    }
    if (HitStation) {
        if (SelectedStations.Contains(HitStation->Id)) {
            UnselectStation(HitStation->Id);
            //SelectedStations.Remove(HitStation->Id);
        }
        else {
            SelectStation(HitStation->Id);
            //SelectedStations.AddUnique(HitStation->Id);
            //me convendria tener esto ordenado?
            //SelectedStations.StableSort();
        }
    }
}


//IBotonInterface * UVisualizationHUD::TraceBoton_Implementation() {
IBotonInterface * UVisualizationHUD::TraceBoton() {
    //UGameplayStatics::GetGameInstance()->GetGameViewportClient()->GetMousePosition
    FVector2D MousePosition;
    FVector WorldLocation;
    FVector WorldDirection;
    bool MouseInScreen;
    UWorld * const World = GetWorld();
    if (World) {
        MouseInScreen = UGameplayStatics::GetGameInstance(World)->GetGameViewportClient()->GetMousePosition(MousePosition);
    }
    else {
        MouseInScreen = false;
    }
    if (MouseInScreen && World) {
        APlayerController * PlayerController = UGameplayStatics::GetPlayerController(World, 0);
        if (PlayerController) {
            bool Projected = PlayerController->DeprojectScreenPositionToWorld(MousePosition.X, MousePosition.Y, WorldLocation, WorldDirection);
            if (Projected) {//puedo hacer un line trace
                //FCollisionQueryParams NodoTraceParams = FCollisionQueryParams(FName(TEXT("TraceNodo")), true, this);
                FVector PuntoInicial = WorldLocation;
                FVector PuntoFinal = WorldLocation + WorldDirection * DistanciaTrace;
                //PuntoInical = PuntoInicial + Vec * 10;//para que no se choque con lo que quiero, aun que no deberia importar
                TArray<TEnumAsByte<EObjectTypeQuery> > TiposObjetos;
                TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery7);//Station
                TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery8);//Departamento
                //TiposObjetos.Add(EObjectTypeQuery::ObjectTypeQuery2);//World dynamic, separado esta funcionando bien, supongo que tendre que hacer oto trace par saber si me estoy chocando con la interfaz, y no tener encuenta esta busqueda
                //podria agregar los world static y dynamic, para asi avitar siempre encontrar algun nodo que este destrar de algun menu, y que por seleccionar en el menu tambien le de click a el
                TArray<AActor*> ActoresIgnorados;
                FHitResult Hit;
                bool trace = UKismetSystemLibrary::LineTraceSingleForObjects(GetWorld(), PuntoInicial, PuntoFinal, TiposObjetos, false, ActoresIgnorados, EDrawDebugTrace::None, Hit, true, FLinearColor::Blue);//el none es para que no se dibuje nada
                //hit se supone que devovera al actor y el punto de impacto si encontró algo, castearlo a nodo, y listo
                IBotonInterface * BotonEncontrado = nullptr;
                if (trace) {//de pronto no necesito, esto, el caste me denbolvera null
                    //solo que al agregar el worldynamic ,tengo que castear y verificar
                    BotonEncontrado = Cast<IBotonInterface>(Hit.Actor.Get());
                    //UE_LOG(LogClass, Log, TEXT("Station encontrado"));
                    if (BotonEncontrado) {//no estaba
                        //en que momento debo incluir la seccion del label? despues de todo esto, en otra funcion, o en este mismo codigo?

                        /*if (GEngine)//no hacer esta verificación provocaba error al iniciar el editor
                            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Station traced."));*/

                    }
                    //DrawDebugSphere(GetWorld(), Hit.ImpactPoint, 1.5f, 6, FColor::Black, false, 0.0f, 0, 0.25f);
                    
                    //return Hit.ImpactPoint;//si quito toto el if anterior debo activar esta linea, y liesto
                }
                //y si esta funcion es solo para esto, y luego ya verifico si es tal o cual cosa, en otra parte del codigo?

                //DrawDebugLine(GetWorld(), SourceNodo->GetActorLocation(), TargetNodo->GetActorLocation(), FColor::Black, false, -1.0f, 0, Radio*Escala);
                //StationEncontrado = nullptr;
                return BotonEncontrado;// los casos manejarlos afuera
            }
        }
    }
    return nullptr;
}

void UVisualizationHUD::TracingBoton_Implementation() {
    IBotonInterface * BotonEncontrado = TraceBoton();
    if (BotonEncontrado && bBuscarBoton) {//comprobamos la interaccion para que no se detecte lo que este detras del menu
        if (HitBoton){ 
            if (HitBoton != BotonEncontrado) {//quiza se pueda hacer con el boleano, debo ocultar si es que es diferente al de ahora,
                IBotonInterface::Execute_Unhover(Cast<AActor>(HitBoton));


                IBotonInterface::Execute_Hover(Cast<AActor>(BotonEncontrado));
            }
            else {//si es el mismo, no hago nada
            }
        }
        else {//este el se creo que esta demas
            IBotonInterface::Execute_Hover(Cast<AActor>(BotonEncontrado));

        }
        HitBoton = BotonEncontrado;//podria dejar ests 3 lineas, y borrar las de adentro
        bHitBoton= true;
    }
    else {//si no estoy golpeando algun nodo
        if (HitBoton) {
            IBotonInterface::Execute_Unhover(Cast<AActor>(HitBoton));
        }
        HitBoton = nullptr;
        bHitBoton = false;
        //el caso contrario, seria encontrar como lo deje con el if anterior, asi que no se hace nada
    }
}

void UVisualizationHUD::ActualizarOrdenVentanas_Implementation() {
    for (int i = 0; i < VisualizacionesInWorkSpace.Num(); i++) {
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(VisualizacionesInWorkSpace[i]->Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        if (CanvasSlot) {
            CanvasSlot->SetZOrder(i);
        }
    }
}

void UVisualizationHUD::FocusVentana_Implementation(UVentanaWidget * VentanaReference) {
    VisualizacionesInWorkSpace.Remove(VentanaReference);
    VisualizacionesInWorkSpace.Add(VentanaReference);
    ActualizarOrdenVentanas();
}

void UVisualizationHUD::MinimizarVentana_Implementation(UVentanaWidget * VentanaReference) {
}

void UVisualizationHUD::DesminimizarVentana_Implementation(UVentanaWidget * VentanaReference) {
}

void UVisualizationHUD::ShowLabelVisualizacion_Implementation() {
}

void UVisualizationHUD::HideLabelVisualizacion_Implementation() {
}

void UVisualizationHUD::SetInformationLabelVisualizacion_Implementation(const FText & NewInformation) {
}

void UVisualizationHUD::SetPositionLabelVisualizacion_Implementation(const FVector2D & NewPosition) {
}

void UVisualizationHUD::IncluirMes_Implementation(int IdMes) {
    MesesFiltrados[IdMes] = true;
    for (int i = 0; i < StationWidgets.Num(); i++) {
        StationWidgets[i]->VisualizacionWidget->IncluirMes(IdMes);
        StationWidgets[i]->VisualizacionWidget->VisualizarConsulta();
    }
    ActualizarPromedios();
}

void UVisualizationHUD::ExcluirMes_Implementation(int IdMes) {
    MesesFiltrados[IdMes] = false;
    for (int i = 0; i < StationWidgets.Num(); i++) {
        StationWidgets[i]->VisualizacionWidget->ExcluirMes(IdMes);
        StationWidgets[i]->VisualizacionWidget->VisualizarConsulta();
    }
    ActualizarPromedios();
}

void UVisualizationHUD::IncluirYear_Implementation(int IdYear) {
    YearsFiltrados[IdYear] = true;
    for (int i = 0; i < StationWidgets.Num(); i++) {
        StationWidgets[i]->VisualizacionWidget->IncluirYear(IdYear);
        StationWidgets[i]->VisualizacionWidget->VisualizarConsulta();
    }
    ActualizarPromedios();
}

void UVisualizationHUD::ExcluirYear_Implementation(int IdYear) {
    YearsFiltrados[IdYear] = false;
    for (int i = 0; i < StationWidgets.Num(); i++) {
        StationWidgets[i]->VisualizacionWidget->ExcluirYear(IdYear);
        StationWidgets[i]->VisualizacionWidget->VisualizarConsulta();
    }
    ActualizarPromedios();
}

void UVisualizationHUD::OcultarVariable_Implementation(int Id) {
    for (int i = 0; i < StationWidgets.Num(); i++) {
        UGraficaLineaWidget * Grafica = Cast<UGraficaLineaWidget>(StationWidgets[i]->VisualizacionWidget);
        if (Grafica) {
            Grafica->HideSerie(Id);
        }
    }
    for (int i = 0; i < VisualizationMapa->StationActors.Num(); i++) {
        VisualizationMapa->StationActors[i]->HideBarra(Id);
    }
}

void UVisualizationHUD::MostrarVariable_Implementation(int Id) {
    for (int i = 0; i < StationWidgets.Num(); i++) {
        UGraficaLineaWidget * Grafica = Cast<UGraficaLineaWidget>(StationWidgets[i]->VisualizacionWidget);
        if (Grafica) {
            Grafica->ShowSerie(Id);
        }
    }
    for (int i = 0; i < VisualizationMapa->StationActors.Num(); i++) {
        VisualizationMapa->StationActors[i]->ShowBarra(Id);
    }
}

void UVisualizationHUD::UpdateFilters_Implementation() {
}

void UVisualizationHUD::ActualizarPromedios_Implementation() {//en realidad actualiza las barras
    if (SelectedVariables.Num()) {
        VisualizationMapa->ValorMinimo = YMins[0];
        VisualizationMapa->ValorMaximo = YMaxs[0];
        //deberia nivelar otra vez a 0 las elevaciones
        for (int i = 0; i < ConsultaActual.StationConsultas.Num(); i++) {
            for (int k = 0; k < ConsultaActual.StationConsultas[i].ConsultasPorVariable.Num(); k++) {
                float VariablePromedio = 0.0f;
                int VariableCount = 0;
                TArray<int> CountRangos;
                for (int r = 0; r < NumeroRangosHistograma; r++) {
                    CountRangos.Add(0);
                }
                //CountRangos.SetNum(NumeroRangosHistograma);
                float DeltaRango = (YMaxs[k] - YMins[k])/NumeroRangosHistograma;
                for (int j = 0; j < ConsultaActual.StationConsultas[i].ConsultasPorVariable[k].Datos.Num(); j++) {//pero deberi ser solo los validos, si es invalido no deberia sumar
                    FDailyDataStruct DataDaily = ConsultaActual.StationConsultas[i].ConsultasPorVariable[k].Datos[j];
                    //float DataValue = ConsultaActual.StationConsultas[i].ConsultasPorVariable[k].Datos[j].value;
                    float DataValue = DataDaily.value;
                    if (DataValue != -9999 && MesesFiltrados[DataDaily.month-1] && YearsFiltrados[DataDaily.year - ConsultaActual.InitialYear]) {
                    //if (DataValue != -9999) {
                        VariableCount++;
                        VariablePromedio += DataValue;
                        int IdRango = -1;
                        for (int r = 0; r < NumeroRangosHistograma && IdRango == -1; r++) {
                            if (DataValue >= DeltaRango * r && DataValue < DeltaRango * (r + 1)) {
                                IdRango = r;
                            }
                        }
                        if (IdRango != -1) {
                            CountRangos[IdRango]++;
                        }
                    }
                }
                if (!VisualizationMapa->StationActors[i]->ExistBarra(k)) {
                    int IdBarra = VisualizationMapa->StationActors[i]->AddBarra();
                    VisualizationMapa->StationActors[i]->SetColorBarra(k, ColorsSeries[k]);
                    //debo crear los items del histograma
                    for (int r = 0; r < NumeroRangosHistograma; r++) {
                        VisualizationMapa->StationActors[i]->Barras[IdBarra]->HistogramaWidget->AddItem();
                    }
                }
                if (VariableCount) {
                    VariablePromedio /= VariableCount;
                    
                    float AlturaLimitada = VariablePromedio / (YMaxs[k] - YMins[k]);
                    //int IdBarra = VisualizationMapa->StationActors[i]->AddBarra();
                    //VisualizationMapa->StationActors[i]->SetValueBarra(IdBarra, AlturaLimitada);
                    //VisualizationMapa->StationActors[i]->SetColorBarra(IdBarra, ColorsSeries[k]);

                    VisualizationMapa->StationActors[i]->SetValueBarra(k, AlturaLimitada);
                    VisualizationMapa->StationActors[i]->ShowBarra(k);
                    //debo establecerlos valores para el histograma
                    for (int r = 0; r < NumeroRangosHistograma; r++) {
                        float H;
                        float S;
                        float V;
                        float A;
                        UKismetMathLibrary::RGBToHSV(ColorsSeries[k], H, S, V, A);
                        float Porcentaje = float(CountRangos[r])/VariableCount;
                        FLinearColor ColorItem = UKismetMathLibrary::HSVToRGB(H, Porcentaje, 1.0f);// o podria ser  siempre
                        VisualizationMapa->StationActors[i]->Barras[k]->HistogramaWidget->SetItem(r, FString::SanitizeFloat(DeltaRango*r) + " - " + FString::SanitizeFloat(DeltaRango*(r+1)), FString::SanitizeFloat(Porcentaje*100) + "%", ColorItem);
                    }
                }
                else {
                    VisualizationMapa->StationActors[i]->SetValueBarra(k, 0.0f);//no tiene valor, incluso podria ser que este oculta, no tiene data, deberia estar oculta
                    VisualizationMapa->StationActors[i]->HideBarra(k);
                    //VisualizationMapa->StationActors[i]->SetColorBarra(k, ColorsSeries[k]);
                }
            }
            VisualizationMapa->StationActors[i]->DistribuirBarras();//deberia usar se, si ocultar barra quiero que provoque esa distribucion
        }
    }
}
void UVisualizationHUD::PrepararYearsFiltrados() {
    YearsFiltrados.SetNum(ConsultaActual.FinalYear - ConsultaActual.InitialYear + 1);
    for (int i = 0; i < YearsFiltrados.Num(); i++) {
        YearsFiltrados[i] = true;//todo estara seleccionado inicialmente
    }

    /*for(int i = 0; i < ConsultaActual.StationConsultas.Num(); i++){
        StationsFiltradas.AddUnique(i);
    }*/
}
