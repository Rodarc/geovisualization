// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/ScrollBox.h"
#include "Components/CanvasPanel.h"
#include "GraficaWidget.h"
#include "GraficaDetalleScrollItemWidget.h"
#include "../Visualizaciones/GraficaLineaLabelWidget.h"
#include "../Visualizaciones/GraficaLineaPanelWidget.h"
#include "../Visualizaciones/GraficaLineaVentanaWidget.h"
#include "../Visualizaciones/GraficaCircularVentanaWidget.h"
#include "../Visualizaciones/VisualizacionNJVentanaWidget.h"
#include "PanelScrollItemWidget.h"
#include "../SistemaVentanas/VentanaWidget.h"
#include "StationMarkWidget.h"
#include "StationWidget.h"
#include "../Estructuras/StationStruct.h"
#include "../Estructuras/DailyDataStruct.h"
#include "../Estructuras/ConsultaStruct.h"
#include "../Mapa/VisualizationMap.h"
#include "BotonInterface.h"
#include "VisualizationHUD.generated.h"


UENUM(BlueprintType)
enum class EVisualizationType: uint8 {
    EGraficaLineas UMETA(DisplayName = "Grafica Lineas"),
    EGraficaCircular UMETA(DisplayName = "Grafica circular"),
    ENJ UMETA(DisplayName = "NJ"),
};

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UVisualizationHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UVisualizationHUD(const FObjectInitializer & ObjectInitializer);

    virtual void NativeConstruct() override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UScrollBox * PanelSB;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UCanvasPanel * WorkSpace;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<UGraficaWidget *> Graficas;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<UGraficaDetalleScrollItemWidget *> DetalleGraficas;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<UPanelScrollItemWidget *> VisualizacionesInPanel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<UVentanaWidget *> VisualizacionesInWorkSpace;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    UGraficaDetalleScrollItemWidget * CreateGraficaDetalle(UStationWidget * StationWidget);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    UPanelScrollItemWidget * CreateVisualizacionInPanelFromLabel(UStationWidget * StationWidget);//no se si pasar el estation widget, o la visualizacion que contiene, por ahora asi

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    UVentanaWidget * CreateVisualizacionInWorkSpaceFromPanelItem(UPanelScrollItemWidget * PanelItemWidget, EVisualizationType TypeVis);//no se si pasar el estation widget, o la visualizacion que contiene, por ahora asi

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    UVentanaWidget * CreateVisualizacionInWorkSpaceFromHUD();//no se si pasar el estation widget, o la visualizacion que contiene, por ahora asi
	
    //creo que necesitare el tipo de grafica
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaWidget> TypeGrafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaDetalleScrollItemWidget> TypeGraficaDetalle;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UPanelScrollItemWidget> TypePanelScrollItem;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UVentanaWidget> TypeVentanaWidget;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaLineaLabelWidget> TypeGraficaLineaLabel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaLineaPanelWidget> TypeGraficaLineaPanel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaLineaVentanaWidget> TypeGraficaLineaVentana;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaLineaLabelWidget> TypeGraficaCircularLabel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaLineaPanelWidget> TypeGraficaCircularPanel;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaCircularVentanaWidget> TypeGraficaCircularVentana;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UVisualizacionNJVentanaWidget> TypeVisualizacionNJVentana;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UStationWidget> TypeStationWidget;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UStationMarkWidget> TypeStationMarkWidget;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<UStationWidget *> StationWidgets;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<UStationMarkWidget *> StationMarkWidgets;//deberia haber la misma cantidad que las staiones, o las staiton widgets, por que necesitare sus referencias, para descarivar o ivatr que salga uno nuevo, si ya tengo el de una estacion activado
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float YMax;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float YMin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float XMax;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float XMin;
    //es posible que tenga distitnos Xmin y Y Min si estoy trabajando con varias variables, no seran los mismos valores para todas las variables

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> YMaxs;//del tama�o de las variables

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> YMins;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> XMaxs;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> XMins;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<int> CantidadDiasMes;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> AcumCantidadDiasMesConsultados;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int CantidadDiasYearConsulta;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> MonthsConsultados;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> YearsConsultados;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<FString> Years;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FString> Months;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FString> AbrevMonths;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FString> Variables;// para mostrar en la interfaz

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FLinearColor> ColorsSeries;// para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FString> VariablesURL;// para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FString> VariablesAbrev;// para mostrar en la interfaz

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    FConsultaStruct ConsultaActual;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetIntervalYears(int YearInicio, int YearFin);
	
    UFUNCTION(BlueprintNativeEvent, Category = "Grafica")
    void Actualizar();//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void Actualizar_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    UFUNCTION(BlueprintNativeEvent, Category = "Grafica")
    void ActualizarOpciones();//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void ActualizarOpciones_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra


    //TArray<FVector2D> ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct>& Datos);


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    AVisualizationMap * VisualizationMapa;

    //Array de las estructruas de la estaciones
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<FStationStruct> Stations;//esto no se si debe etar aqui o no

    //UFUNCTION(BlueprintCallable, Category = "Grafica")
    void CreateStations(TArray<FStationStruct> NewStations);

    void CreateStationsWidget();

    void CreateStationMarkWidgets();

    UFUNCTION(BlueprintNativeEvent, Category = "Grafica")
    void HabilitarStations();//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void HabilitarStations_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bBuscarStation;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float DistanciaTrace;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bHitStation;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    AStation * HitStation;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdHitStation;//en el array de estaciones

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    AStation * TraceStation();//retorna el puntero si encuentra una estacion
    virtual AStation * TraceStation_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra
    //por alguna razon, no me aparece la opcion de implimentar esta funcio nene l bluprint, debe ser debido a que estoy retornando algo,
    //si lo quiero en blue print, debere hacer con parametro de referencia en lugar de retorno, por ahora esta funciona

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void TracingStation();//busca estaciones mientras el mouse esta en el area de visualizacion, solo en el caso de que un elemento de la UI no se interponga
    virtual void TracingStation_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ShowStationWidget(int Id);
    void ShowStationWidget_Implementation(int Id);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void HideStationWidget(int Id);
    void HideStationWidget_Implementation(int Id);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UStationMarkWidget * TemporalStationMark;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void ActivarResaltadoStation(AStation * Station);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void FijarResaltadoStation(AStation * Station);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void LiberarResaltadoStation(AStation * Station);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void DesactivarResaltadoStation(AStation * Station);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SelectPressed();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SelectReleased();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void Consultar(TArray<int> StationsConsulta, int InitialYearConsulta, int FinalYearConsulta, TArray<FString> MesesConsulta, TArray<FString> VariablesConsulta);//este deberia sera una funcion generica donde le passe todos los elementos del menu, y que por dentro deicda
    //probablemente deberia tener arrays o contendores, donde pondre los resultados de la consulta, asi com olos parametros de los mismos, digamos meses seleccionados de esa consutla,
    //para asi facilitar la comparacon, digamos, todos los arrays de los datos por estacion, tiene un numero multiplo de 3 por que son 3 meses los seleccionados

    //Esta funcion, consulta todas las estaciones, no solo las seleccionadas
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void ConsultarGeneral(int InitialYearConsulta, int FinalYearConsulta, TArray<FString> MesesConsulta, TArray<FString> VariablesConsulta);//este deberia sera una funcion generica donde le passe todos los elementos del menu, y que por dentro deicda

    void RecibirConsulta(TArray<FDailyDataStruct> & Datos, FString varible);

    UFUNCTION(BlueprintNativeEvent, Category = "Grafica")
    void FeedbackConsulta();//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void FeedbackConsulta_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    UFUNCTION(BlueprintNativeEvent, Category = "Grafica")
    void FeedbackRecibirConsulta();//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void FeedbackRecibirConsulta_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    int NumeroSubConsultas;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> SelectedStations;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<FString> SelectedVariables;//igual a las variables recibidas e la consulta, se guardan aqui, para poder conservar el indice solo de las seleccionadas

    TArray<TArray<FDailyDataStruct> > ConsultaStations;//deberia, ser solo las consultadas?
    //o deberia tener otra funcion que le mande una graficas o dattos a los estation widget, si no hay grafica que madar, solo devirle ocultar a los station widget

    TArray<TArray<TArray<FDailyDataStruct>>> ConsultaVariableStations;//deberia, ser solo las consultadas?

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void ActualizarGraficas();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void LimpiarGraficas();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void SelectStation(int id);//o que reciba el punto de un AStation??
    void SelectStation_Implementation(int id);//o que reciba el punto de un AStation??

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void UnselectStation(int id);
    void UnselectStation_Implementation(int id);//o que reciba el punto de un AStation??

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ShowStationsOfDepartament(const FString & NameDepartamento);//podria recibir el name
    void ShowStationsOfDepartament_Implementation(const FString & NameDepartamento);//

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void HideStationsOfDepartament(const FString & NameDepartamento);
    void HideStationsOfDepartament_Implementation(const FString & NameDepartamento);//

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void ClickEvent();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void DoubleClickEvent();
    //estos solo cuando esten el mouse en la visualizacion

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bBuscarBoton;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bHitBoton;

    /*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TScriptInterface<IBotonInterface> HitBoton;*/

    IBotonInterface * HitBoton;
    //si quiero poner esto para que se reciba en bluprint, deberia el boton tratarlo como actores, solo dentro del tracin hacer casteo a botoninterface para comprobaro
    //igual cuando tenga que llamar, creo que no seria necesario tener esto como interface


    //UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    IBotonInterface * TraceBoton();//retorna el puntero si encuentra una estacion
    //virtual IBotonInterface * TraceBoton_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra
    //por alguna razon, no me aparece la opcion de implimentar esta funcio nene l bluprint, debe ser debido a que estoy retornando algo,
    //si lo quiero en blue print, debere hacer con parametro de referencia en lugar de retorno, por ahora esta funciona

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void TracingBoton();//busca estaciones mientras el mouse esta en el area de visualizacion, solo en el caso de que un elemento de la UI no se interponga
    virtual void TracingBoton_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void ActualizarOrdenVentanas();//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void ActualizarOrdenVentanas_Implementation();//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void FocusVentana(UVentanaWidget * VentanaReference);//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void FocusVentana_Implementation(UVentanaWidget * VentanaReference);//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void MinimizarVentana(UVentanaWidget * VentanaReference);//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void MinimizarVentana_Implementation(UVentanaWidget * VentanaReference);//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void DesminimizarVentana(UVentanaWidget * VentanaReference);//analizzar esle parametro, ya lo use el el FPSTutorial
    virtual void DesminimizarVentana_Implementation(UVentanaWidget * VentanaReference);//esta es la implmenetacion de la clase, en el bluprint se podra usar otra

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void ShowLabelVisualizacion();
    virtual void ShowLabelVisualizacion_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void HideLabelVisualizacion();
    virtual void HideLabelVisualizacion_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void SetInformationLabelVisualizacion(const FText & NewInformation);
    virtual void SetInformationLabelVisualizacion_Implementation(const FText & NewInformation);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventanas")
    void SetPositionLabelVisualizacion(const FVector2D & NewPosition);
    virtual void SetPositionLabelVisualizacion_Implementation(const FVector2D & NewPosition);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<bool> MesesFiltrados;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<bool> YearsFiltrados;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void PrepararYearsFiltrados();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void IncluirMes(int IdMes);
    virtual void IncluirMes_Implementation(int IdMes);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ExcluirMes(int IdMes);
    virtual void ExcluirMes_Implementation(int IdMes);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void IncluirYear(int IdYear);
    virtual void IncluirYear_Implementation(int IdYear);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ExcluirYear(int IdYear);
    virtual void ExcluirYear_Implementation(int IdYear);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void OcultarVariable(int Id);
    void OcultarVariable_Implementation(int Id);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void MostrarVariable(int Id);
    void MostrarVariable_Implementation(int Id);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventana")
    void UpdateFilters();
    virtual void UpdateFilters_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventana")
    void ActualizarPromedios();
    virtual void ActualizarPromedios_Implementation();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int NumeroRangosHistograma;
};