// Fill out your copyright notice in the Description page of Project Settings.

#include "PanelScrollItemWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "../GeoVisualizationGameModeBase.h"
#include "../SistemaVentanas/VentanaWidget.h"
#include "../Visualizaciones/GraficaLineaVentanaWidget.h"
#include "../Visualizaciones/GraficaCircularVentanaWidget.h"

UPanelScrollItemWidget::UPanelScrollItemWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer){
    //static ConstructorHelpers::FClassFinder<UGraficaWidget> GraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/GraficaWidgetBP.GraficaWidgetBP_C'"));
}

/*void UGraficaDetalleScrollItemWidget::NativeConstruct() {
}*/

void UPanelScrollItemWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
}

void UPanelScrollItemWidget::SetInformation(FString InStationName, FString InGeoPosition) {
    StationName = InStationName;
    GeoPosition = InGeoPosition;
}

void UPanelScrollItemWidget::CreateVisualizacionOfType(TSubclassOf<UVisualizacionWidget> TypeVis) {
    TypeVisualizacion = TypeVis;
    VisualizacionWidget = CreateWidget<UVisualizacionWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeVisualizacion);
    //debo agregar esta visualizacion al contenedor
    VisualizacionWidget->AddToViewport(1);
    VisualizacionSizeBox->AddChild(VisualizacionWidget);


}

void UPanelScrollItemWidget::CrearVentana_Implementation() {
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        //UGraficaDetalleScrollItemWidget * DetalleScrollItem = GeoGameMode->VisualizationHUDReference->CreateGraficaDetalle(this);
        UVentanaWidget * Ventana = GeoGameMode->VisualizationHUDReference->CreateVisualizacionInWorkSpaceFromPanelItem(this, EVisualizationType::EGraficaLineas);
        //si creo el detalle le paso la infromacion, por ahora no solo paso las series, pero en realidada haora le pasare una copia de la data original, para que el solo se calcule sus cosas, 
        //aqui debo modificar ello
        //debo decirle que cree una visualizacion de un determinado tipo, en funcion de cual este almacenando este cosa, por ahora es fijo
        UGraficaLineaVentanaWidget * ItemGraficaLinea = Cast<UGraficaLineaVentanaWidget>(Ventana->VisualizacionWidget);
        if (ItemGraficaLinea) {
            //ahora debo pasarle la informacion desde la grafica que tengo
            UGraficaLineaPanelWidget * GraficaLinea = Cast<UGraficaLineaPanelWidget>(VisualizacionWidget);
            if (GraficaLinea) {
                ItemGraficaLinea->Consulta = GraficaLinea->Consulta;//copiando la consulta, es exactamente igual, solo debo copiar los interbaloes cantidad dias aucm etc

                //informacion pasada, ahora debo actualizar la visualizacion en si misma, lo deberia hacer la propia visualizacion, o lo deberia hacer esta cosa?, por ahora lo hare aqui, luego lo encapsulo de forma interna
                ItemGraficaLinea->GeneralColorSeries = GraficaLinea->GeneralColorSeries;//Le copio los colores ya que estos son fijos para el numero de variables, aun no es automatico, en aquel caso lo calculara de forma interna la grafica.
                ItemGraficaLinea->AcumCantidadDiasMesConsultados = GraficaLinea->AcumCantidadDiasMesConsultados;
                ItemGraficaLinea->CantidadDiasYearConsulta = GraficaLinea->CantidadDiasYearConsulta;
                ItemGraficaLinea->CantidadDiasMes = GraficaLinea->CantidadDiasMes;//este deberia estar ya por defecto en las visualizaciones, ya que es independiente de los calculos

                //debo copiar mas datautil para el caculo de las cosas apra dibujar, y de nuevo esta cosa visualizacion se calcule sola
                //los minimos y maximos ya estan copiados en la structura consulta, no es necesario copiarlos
                ItemGraficaLinea->PrepararYearsFiltrados();
                ItemGraficaLinea->CalcularPuntosDibujar();
                ItemGraficaLinea->VisualizarConsulta();
            }
        }
        Ventana->UpdateFilters();
        Ventana->UpdateTitleVentana();
    }
}

void UPanelScrollItemWidget::CrearVentanaGraficaCircular_Implementation() {
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        //UGraficaDetalleScrollItemWidget * DetalleScrollItem = GeoGameMode->VisualizationHUDReference->CreateGraficaDetalle(this);
        UVentanaWidget* Ventana = GeoGameMode->VisualizationHUDReference->CreateVisualizacionInWorkSpaceFromPanelItem(this, EVisualizationType::EGraficaCircular);//debo lamar a la especifica de la grafica circular
        //si creo el detalle le paso la infromacion, por ahora no solo paso las series, pero en realidada haora le pasare una copia de la data original, para que el solo se calcule sus cosas, 
        //aqui debo modificar ello
        //debo decirle que cree una visualizacion de un determinado tipo, en funcion de cual este almacenando este cosa, por ahora es fijo
        UGraficaCircularVentanaWidget* ItemGraficaCircular = Cast<UGraficaCircularVentanaWidget>(Ventana->VisualizacionWidget);
        if (ItemGraficaCircular) {
            //ahora debo pasarle la informacion desde la grafica que tengo
            UGraficaLineaPanelWidget* GraficaLinea = Cast<UGraficaLineaPanelWidget>(VisualizacionWidget);//este tambien deberia ser convertido en circular, por ahora dejarlo asi
            if (GraficaLinea) {
                ItemGraficaCircular->Consulta = GraficaLinea->Consulta;//copiando la consulta, es exactamente igual, solo debo copiar los interbaloes cantidad dias aucm etc

                //informacion pasada, ahora debo actualizar la visualizacion en si misma, lo deberia hacer la propia visualizacion, o lo deberia hacer esta cosa?, por ahora lo hare aqui, luego lo encapsulo de forma interna
                ItemGraficaCircular->GeneralColorSeries = GraficaLinea->GeneralColorSeries;//Le copio los colores ya que estos son fijos para el numero de variables, aun no es automatico, en aquel caso lo calculara de forma interna la grafica.
                ItemGraficaCircular->AcumCantidadDiasMesConsultados = GraficaLinea->AcumCantidadDiasMesConsultados;
                ItemGraficaCircular->CantidadDiasYearConsulta = GraficaLinea->CantidadDiasYearConsulta;
                ItemGraficaCircular->CantidadDiasMes = GraficaLinea->CantidadDiasMes;//este deberia estar ya por defecto en las visualizaciones, ya que es independiente de los calculos

                //debo copiar mas datautil para el caculo de las cosas apra dibujar, y de nuevo esta cosa visualizacion se calcule sola
                //los minimos y maximos ya estan copiados en la structura consulta, no es necesario copiarlos
                ItemGraficaCircular->PrepararYearsFiltrados();
                ItemGraficaCircular->CalcularPuntosDibujar();
                ItemGraficaCircular->VisualizarConsulta();
            }
        }
        Ventana->UpdateFilters();
        Ventana->UpdateTitleVentana();
        //PanelScrollItem->SetInformation(StationName, GeoPosition);
    }
}

void UPanelScrollItemWidget::UpdateTitle_Implementation() {
    if (VisualizacionWidget) {
        Title = VisualizacionWidget->GetTitleDetailPanelInformation();
    }
}

void UPanelScrollItemWidget::UpdateSubtitle_Implementation() {
    if (VisualizacionWidget) {
        Subtitle = VisualizacionWidget->GetSubtitleDetailPanelInformation();
    }
}


