// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/PanelSlot.h"
#include "Components/PanelWidget.h"

UGraficaWidget::UGraficaWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    Puntos.Add(FVector2D(0, 200));
    Puntos.Add(FVector2D(100, 800));
    Puntos.Add(FVector2D(200, 500));
    Puntos.Add(FVector2D(300, 300));
    Puntos.Add(FVector2D(400, 100));
    Puntos.Add(FVector2D(500, 400));
    SetDesiredSizeInViewport(FVector2D(200.0f, 150.0f));

    SetIntervalX(0, 500);
    SetIntervalY(100, 800);//podria poner 0 depebnde de como quiera yo graficar
    CalcularCorners();
    CalcularPuntosDibujar();

    Margen.Bottom = 10;
    Margen.Top = 10;
    Margen.Right = 10;
    Margen.Left = 10;
//(SpecifiedColor=(R=0.000000,G=0.000000,B=0.000000,A=0.775000),ColorUseRule=UseColor_Specified)

    ColorFondoGrafica = FLinearColor(0.005f, 0.005f, 0.005f, 0.9);

    static ConstructorHelpers::FObjectFinder<USlateBrushAsset> FondoGraficaBrushFinder(TEXT("SlateBrushAsset'/Game/GeoVisualization/UMG/FondoGraficaBrush.FondoGraficaBrush'"));
    //deberia buscar una grafica
    if (FondoGraficaBrushFinder.Succeeded()) {
        BrushFondoGrafica = FondoGraficaBrushFinder.Object;
    }
}
//opciones:
//poner un canvas panel como raiz desto, y donde sea que lo uso debo ponerlo dentro de un size box
//poner un sizebox, pero que sea con tama�ao ajustable, y con un minimo peque�o.


void UGraficaWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);

    //UWidgetBlueprintLibrary::DrawBox(InContext, GetCachedGeometry().Position, GetCachedGeometry().GetLocalSize(), BrushFondoGrafica, ColorFondoGrafica);
    UWidgetBlueprintLibrary::DrawLine(InContext, LUCorner, Origin, FLinearColor::White, true);
    UWidgetBlueprintLibrary::DrawLine(InContext, Origin, RBCorner, FLinearColor::White, true);
    //UWidgetBlueprintLibrary::DrawLines(InContext, PuntosDibujar, FLinearColor::Blue, true);//aveces aparece un error visual

    /*for (int i = 0; i < PuntosDibujar.Num()-1; i++) {
        UWidgetBlueprintLibrary::DrawLine(InContext, PuntosDibujar[i], PuntosDibujar[i+1], FLinearColor::Blue, true);
    }*/

    for(int k = 0; k < SeriesDibujar.Num(); k++){
        if (VisibilitySeries[k]) {
            for (int i = 0; i < SeriesDibujar[k].Num() - 1; i++) {
                if (Series[k][i + 1].X - Series[k][i].X <= 1.0f) {
                    UWidgetBlueprintLibrary::DrawLine(InContext, SeriesDibujar[k][i], SeriesDibujar[k][i+1], ColorSeries[k], true);
                }
            }
        }
    }


    /*UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    if (CanvasSlot) {
        UWidgetBlueprintLibrary::DrawBox(InContext, CanvasSlot->GetPosition(), CanvasSlot->GetSize(), BrushFondoGrafica, ColorFondoGrafica);
        UWidgetBlueprintLibrary::DrawLine(InContext, LUCorner, Origin, FLinearColor::White, true);
        UWidgetBlueprintLibrary::DrawLine(InContext, Origin, RBCorner, FLinearColor::White, true);
        UWidgetBlueprintLibrary::DrawLines(InContext, PuntosDibujar, FLinearColor::Blue, true);
    }*/
}

void UGraficaWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
    
    //estos calculos deberian ser solo si esta en modificacion de dimensiones, de alguna forma debo tener un bolloeando
    //UE_LOG(LogTemp, Warning, TEXT("Grafica: %s"), *GetName());
    //UE_LOG(LogTemp, Warning, TEXT("Local Size: (%f, %f)"), MyGeometry.GetAbsolutePosition().X, MyGeometry.GetAbsolutePosition().Y);
    /*UPanelWidget* Padre = Cast<UPanelWidget>(GetParent());
    

    UPanelSlot * PanelSlot = Cast<UPanelSlot>(Padre->Slot);
    PanelSlot->posit
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetPosition(FVector2D(100.0f, 100.0f));
    CanvasSlot->SetSize(FVector2D(100.0f, 100.0f));*/
    //UE_LOG(LogTemp, Warning, TEXT("Local Size: (%f, %f)"), MyGeometry.GetLocalSize().X, MyGeometry.GetLocalSize().Y);
    //UE_LOG(LogTemp, Warning, TEXT("Local Size: (%f, %f)"), MyGeometry.GetLocalSize().X, MyGeometry.GetLocalSize().Y);
    //UE_LOG(LogTemp, Warning, TEXT("Absolute Size: (%f, %f)"), MyGeometry.GetAbsoluteSize().X, MyGeometry.GetAbsoluteSize().Y);
    
    //Estos calculos deberiandarse solo si la grafica esta cambiando de tama�o

    LUCorner = MyGeometry.Position + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
    RBCorner = MyGeometry.Position + MyGeometry.GetLocalSize() - FVector2D(Margen.Right, Margen.Top);
    //LUCorner = MyGeometry.GetAbsolutePosition() + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
    //RBCorner = MyGeometry.GetAbsolutePosition() + MyGeometry.GetLocalSize() - FVector2D(Margen.Right, Margen.Top);
    Origin = FVector2D(LUCorner.X, RBCorner.Y);


    CalcularPuntosDibujar();
    //debo calcular los puntos de dibujar y el corner solo cuando estos se deformen por alguna razon, por ejemplo al llamar a acgrandar, meintras tura la animacion, debo llamar a esto en el tick, quiza deba manejar un booleano para esto
    //el problema es cuando se giro la camara, si bien el widget no esta redimensionando pero se esta moviemiento, esntonces los puntos que se dibujan estan en otra posicion en la pantalla
    //creo que se debe de dibujar
}

TArray<FVector2D> UGraficaWidget::GetPoints() {
    return Puntos;
}

void UGraficaWidget::SetPoints(TArray<FVector2D> Points) {
    Puntos = Points;
    //CalcularPuntosDibujar();
    //deberia calcular los intervalos de Xy Y, el problema radica, cuando yo quiero que todas las graficas tengan el mismo intervalo, independiente de los puntos
    //puesto que es posible que a algunas series les falte algun valor, no esten completas, o los valores maximos no sean los mismo, generando graficas, que pdorian parecer igual pero no lo son.

    //quiza debasi o si establecer intervalos y establecer puntos, al momento de llamar, es decir debo yo llamar a set intervals, y luego set points, en este preciso orden
    //para que sea el la calse que isntancia las graficas la responsable de establecer el intervalo para cada una de ellas, o si todas tendran el mismo
}

TArray<TArray<FVector2D>> UGraficaWidget::GetSeries() {
    return Series;
}

TArray<FVector2D> UGraficaWidget::GetSerie(int IdSerie) {
    if (IdSerie < Series.Num()) {
        return Series[IdSerie];
    }
    return TArray<FVector2D>();
}

void UGraficaWidget::SetSerie(int IdSerie, TArray<FVector2D> Points) {
    if (IdSerie < Series.Num()) {
        Series[IdSerie] = Points;
    }
}

void UGraficaWidget::SetColorSerie(int IdSerie, FLinearColor ColorSerie) {
    if (IdSerie < ColorSeries.Num()) {
        ColorSeries[IdSerie] = ColorSerie;
    }
}

int UGraficaWidget::AddSerie(TArray<FVector2D> Points) {//siempre se debe llamar a esta funcion para agregar una nueva series
    Series.Add(Points);
    SeriesDibujar.Add(TArray<FVector2D>());
    ColorSeries.Add(FLinearColor::White);
    VisibilitySeries.Add(true);
    XMaxs.Add(1);
    XMins.Add(0);
    YMins.Add(0);
    YMaxs.Add(1);
    return Series.Num()-1;
}

void UGraficaWidget::HideSerie(int IdSerie) {
    if (IdSerie < Series.Num()) {
        VisibilitySeries[IdSerie] = false;
    }
}

void UGraficaWidget::ShowSerie(int IdSerie) {
    if (IdSerie < Series.Num()) {
        VisibilitySeries[IdSerie] = true;
    }
}

void UGraficaWidget::ClearSeries() {
    Series.Empty();
    SeriesDibujar.Empty();
    VisibilitySeries.Empty();
    ColorSeries.Empty();
    XMaxs.Empty();
    XMins.Empty();
    YMaxs.Empty();
    YMins.Empty();
}

void UGraficaWidget::SetIntervalX(float a, float b) {
    XMax = b;
    XMin = a;
}

void UGraficaWidget::SetIntervalY(float a, float b) {
    YMax = b;
    YMin = a;
}

void UGraficaWidget::SetIntervalXForSerie(int IdSerie, float a, float b) {
    if (IdSerie < XMaxs.Num()) {
        XMaxs[IdSerie] = b;
        XMins[IdSerie] = a;
    }
}

void UGraficaWidget::SetIntervalYForSerie(int IdSerie, float a, float b) {
    if (IdSerie < YMaxs.Num()) {
        YMaxs[IdSerie] = b;
        YMins[IdSerie] = a;
    }
}

void UGraficaWidget::CalcularCorners() {
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    if (CanvasSlot) {
        LUCorner = CanvasSlot->GetPosition() + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
        RBCorner = CanvasSlot->GetPosition() + CanvasSlot->GetSize() - FVector2D(Margen.Right, Margen.Top);
        Origin = FVector2D(LUCorner.X, RBCorner.Y);
    }
}

void UGraficaWidget::CalcularPuntosDibujar() {
    //PuntosDibujar.Empty();//deberi tener el mismo tama�o , el momento de crear
    FVector2D GraficaSize = RBCorner - LUCorner;
    for (int k = 0; k < Series.Num(); k++) {
        if (VisibilitySeries[k]) {
            SeriesDibujar[k].Empty();//deberia tener el miso tama�o que sus puntos en serio, esto evitaria mas calculos, optimizar esto
            for (int i = 0; i < Series[k].Num(); i++) {//este deberian ser los minimos correspondientes
                float X = GraficaSize.X * (Series[k][i].X - XMins[k]) / (XMaxs[k] - XMins[k]);
                float Y = (1 - (Series[k][i].Y - YMins[k]) / (YMaxs[k] - YMins[k])) * GraficaSize.Y;
                FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
                SeriesDibujar[k].Add(PuntoDibujo);
                //PuntosDibujar.Add(PuntoDibujo);
            }
        }
    }
    /*for (int i = 0; i < Puntos.Num(); i++) {
        float X = GraficaSize.X * (Puntos[i].X - XMin) / (XMax - XMin);
        float Y = (1 - (Puntos[i].Y - YMin) / (YMax - YMin)) * GraficaSize.Y;
        FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
        PuntosDibujar.Add(PuntoDibujo);
    }*/
}

void UGraficaWidget::SetMargin(FMargin NuevoMargen) {
    Margen = NuevoMargen;
}




