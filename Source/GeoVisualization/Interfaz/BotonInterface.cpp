// Fill out your copyright notice in the Description page of Project Settings.

#include "BotonInterface.h"


// Add default functionality here for any IBotonInterface functions that are not pure virtual.

void IBotonInterface::Click_Implementation() {
}

void IBotonInterface::DoubleClick_Implementation() {
}

void IBotonInterface::Press_Implementation() {
}

void IBotonInterface::Release_Implementation() {
}

void IBotonInterface::Hover_Implementation() {
}

void IBotonInterface::Unhover_Implementation() {
}

void IBotonInterface::Select_Implementation() {
}

void IBotonInterface::Unselect_Implementation() {
} 

void IBotonInterface::Hide_Implementation() {
}

void IBotonInterface::Show_Implementation() {
}

void IBotonInterface::Collapse_Implementation() {
}

void IBotonInterface::Tint_Implementation(float NewColorComponentH, float NewColorComponentS) {
}
