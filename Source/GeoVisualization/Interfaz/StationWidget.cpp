// Fill out your copyright notice in the Description page of Project Settings.

#include "StationWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "../GeoVisualizationGameModeBase.h"
#include "GraficaDetalleScrollItemWidget.h"
#include "PanelScrollItemWidget.h"
#include "../Visualizaciones/GraficaLineaPanelWidget.h"

UStationWidget::UStationWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer){
    static ConstructorHelpers::FClassFinder<UGraficaWidget> GraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/GraficaWidgetBP.GraficaWidgetBP_C'"));
    if (GraficaWidgetClass.Succeeded()) {
        TypeGrafica = GraficaWidgetClass.Class;
    }

    SizeWithGrafica = FVector2D(300.0f, 130.0f);
    SizeWithoutGrafica = FVector2D(300.0f, 55.0f);
}

/*void UStationWidget::NativeConstruct() {
}*/

void UStationWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
}

void UStationWidget::MostrarGrafica_Implementation() {
    bMostrandoGrafica = true;
}

void UStationWidget::OcultarGrafica_Implementation() {
    bMostrandoGrafica = false;
}

void UStationWidget::SetInformation(FString InStationName, float Latitud, float Longitud) {
    StationName = InStationName;
    GeoPosition = "Coordenadas: (Lat: " + FString::SanitizeFloat(Latitud) + ", Lon: " + FString::SanitizeFloat(Longitud) + ")";
}

void UStationWidget::CrearDetalle_Implementation() {
    AGameModeBase * GameMode = UGameplayStatics::GetGameMode(GetWorld());
    AGeoVisualizationGameModeBase * GeoGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
    if (GeoGameMode) {
        //UGraficaDetalleScrollItemWidget * DetalleScrollItem = GeoGameMode->VisualizationHUDReference->CreateGraficaDetalle(this);
        UPanelScrollItemWidget * PanelScrollItem = GeoGameMode->VisualizationHUDReference->CreateVisualizacionInPanelFromLabel(this);
        //si creo el detalle le paso la infromacion, por ahora no solo paso las series, pero en realidada haora le pasare una copia de la data original, para que el solo se calcule sus cosas, 
        //aqui debo modificar ello
        //debo decirle que cree una visualizacion de un determinado tipo, en funcion de cual este almacenando este cosa, por ahora es fijo
        UGraficaLineaPanelWidget * ItemGrafica = Cast<UGraficaLineaPanelWidget>(PanelScrollItem->VisualizacionWidget);
        if (ItemGrafica) {
            //ahora debo pasarle la informacion desde la grafica que tengo
            UGraficaLineaLabelWidget * GraficaLinea = Cast<UGraficaLineaLabelWidget>(VisualizacionWidget);
            if (GraficaLinea) {
                ItemGrafica->Consulta = GraficaLinea->Consulta;//copiando la consulta, es exactamente igual, solo debo copiar los interbaloes cantidad dias aucm etc

                //informacion pasada, ahora debo actualizar la visualizacion en si misma, lo deberia hacer la propia visualizacion, o lo deberia hacer esta cosa?, por ahora lo hare aqui, luego lo encapsulo de forma interna
                ItemGrafica->GeneralColorSeries = GraficaLinea->GeneralColorSeries;//Le copio los colores ya que estos son fijos para el numero de variables, aun no es automatico, en aquel caso lo calculara de forma interna la grafica.
                ItemGrafica->AcumCantidadDiasMesConsultados = GraficaLinea->AcumCantidadDiasMesConsultados;
                ItemGrafica->CantidadDiasYearConsulta = GraficaLinea->CantidadDiasYearConsulta;
                ItemGrafica->CantidadDiasMes = GraficaLinea->CantidadDiasMes;//este deberia estar ya por defecto en las visualizaciones, ya que es independiente de los calculos

                //debo copiar mas datautil para el caculo de las cosas apra dibujar, y de nuevo esta cosa visualizacion se calcule sola
                //los minimos y maximos ya estan copiados en la structura consulta, no es necesario copiarlos
                ItemGrafica->PrepararYearsFiltrados();
                //deberia pasarle los filtros tambien
                ItemGrafica->CalcularPuntosDibujar();
                ItemGrafica->VisualizarConsulta();
            }
        }
		PanelScrollItem->UpdateTitle();
		PanelScrollItem->UpdateSubtitle();
        //PanelScrollItem->SetInformation(StationName, GeoPosition);
    }
}

void UStationWidget::CreateVisualizacionOfType(TSubclassOf<UVisualizacionWidget> TypeVis) {
    TypeVisualizacion = TypeVis;
    VisualizacionWidget = CreateWidget<UVisualizacionWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeVisualizacion);
    //debo agregar esta visualizacion al contenedor
    VisualizacionWidget->AddToViewport(1);
    GraficaSizeBox->AddChild(VisualizacionWidget);
}

void UStationWidget::ReplaceGrafica_Implementation() {
    if (VisualizacionWidget) {
        //VisualizacionWidget->RemoveFromParent();
        VisualizacionWidget->SetVisibility(ESlateVisibility::Hidden);
        GraficaSizeBox->RemoveChild(VisualizacionWidget);
    }
}

void UStationWidget::RestoreGrafica_Implementation() {
    if (VisualizacionWidget) {
        GraficaSizeBox->AddChild(VisualizacionWidget);
        VisualizacionWidget->SetVisibility(ESlateVisibility::Visible);
    }
}

void UStationWidget::MostrarHistograma_Implementation(UVisualizacionWidget * Visualizacion) {
    ReplaceGrafica();
    if (HistogramaActual) {
        OcultarHistograma();
    }
    HistogramaActual = Visualizacion;
    GraficaSizeBox->AddChild(HistogramaActual);
}

void UStationWidget::OcultarHistograma_Implementation() {
    if (HistogramaActual) {
        GraficaSizeBox->RemoveChild(HistogramaActual);
        HistogramaActual = nullptr;
    }
    RestoreGrafica();
}
