// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaDetalleWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/PanelWidget.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/ScrollBoxSlot.h"
#include "Components/SizeBox.h"

UGraficaDetalleWidget::UGraficaDetalleWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    static ConstructorHelpers::FClassFinder<UPuntoGrafica> PuntoGraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/PuntoGraficaBP.PuntoGraficaBP_C'"));
    if (PuntoGraficaWidgetClass.Succeeded()) {
        TypePunto = PuntoGraficaWidgetClass.Class;
    }
    /*Puntos.Add(FVector2D(0, 200));
    Puntos.Add(FVector2D(100, 800));
    Puntos.Add(FVector2D(200, 500));
    Puntos.Add(FVector2D(300, 300));
    Puntos.Add(FVector2D(400, 100));
    Puntos.Add(FVector2D(500, 400));*/
    /*SetDesiredSizeInViewport(FVector2D(200.0f, 150.0f));

    SetIntervalX(0, 500);
    SetIntervalY(100, 800);//podria poner 0 depebnde de como quiera yo graficar
    CalcularCorners();
    CalcularPuntosDibujar();

    Margen.Bottom = 20;
    Margen.Top = 20;
    Margen.Right = 20;
    Margen.Left = 20;*/
//(SpecifiedColor=(R=0.000000,G=0.000000,B=0.000000,A=0.775000),ColorUseRule=UseColor_Specified)

    //ColorFondoGrafica = FLinearColor(0.005f, 0.005f, 0.005f, 0.9);

    SizePoints = FVector2D(6.0f, 6.0f);

    /*static ConstructorHelpers::FObjectFinder<USlateBrushAsset> FondoGraficaBrushFinder(TEXT("SlateBrushAsset'/Game/GeoVisualization/UMG/FondoGraficaBrush.FondoGraficaBrush'"));
    //deberia buscar una grafica
    if (FondoGraficaBrushFinder.Succeeded()) {
        BrushFondoGrafica = FondoGraficaBrushFinder.Object;
    }*/
}
//opciones:
//poner un canvas panel como raiz desto, y donde sea que lo uso debo ponerlo dentro de un size box
//poner un sizebox, pero que sea con tama�ao ajustable, y con un minimo peque�o.


void UGraficaDetalleWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);

    //UE_LOG(LogTemp, Warning, TEXT("Pintando detalle grafica"));
    /*for (int i = 0; i < PuntosDibujar.Num(); i++) {
        UWidgetBlueprintLibrary::DrawBox(InContext, PuntosDibujar[i] - SizePoints/2, SizePoints, BrushFondoGrafica, FLinearColor::Blue);
    }*/

    /*for(int k = 0; k < SeriesDibujar.Num(); k++){
        if (VisibilitySeries[k]) {
            for (int i = 0; i < SeriesDibujar[k].Num(); i++) {
                UWidgetBlueprintLibrary::DrawBox(InContext, SeriesDibujar[k][i] - SizePoints/2, SizePoints, BrushFondoGrafica, ColorSeries[k]);
            }
        }
    }*/
    //esto sera reemplazado por los puntos con los que se puede interactuar, estos puntos se moveran en el tick, no el paint o tal vez aqui
    // pero estos al si estar dentor de la grafica, pues solo deberian moverse si se esta redimensionando,

}

void UGraficaDetalleWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
    
    /*UE_LOG(LogTemp, Warning, TEXT("Grafica: %s"), *GetName());
    UE_LOG(LogTemp, Warning, TEXT("Local Size: (%f, %f)"), MyGeometry.GetAbsolutePosition().X, MyGeometry.GetAbsolutePosition().Y);
    /*UPanelWidget* Padre = Cast<UPanelWidget>(GetParent());
    

    UPanelSlot * PanelSlot = Cast<UPanelSlot>(Padre->Slot);
    PanelSlot->posit
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetPosition(FVector2D(100.0f, 100.0f));
    CanvasSlot->SetSize(FVector2D(100.0f, 100.0f));* /
    //UE_LOG(LogTemp, Warning, TEXT("Local Size: (%f, %f)"), MyGeometry.GetLocalSize().X, MyGeometry.GetLocalSize().Y);
    UE_LOG(LogTemp, Warning, TEXT("Local Size: (%f, %f)"), MyGeometry.GetLocalSize().X, MyGeometry.GetLocalSize().Y);
    UE_LOG(LogTemp, Warning, TEXT("Absolute Size: (%f, %f)"), MyGeometry.GetAbsoluteSize().X, MyGeometry.GetAbsoluteSize().Y);

    LUCorner = MyGeometry.Position + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
    RBCorner = MyGeometry.Position + MyGeometry.GetLocalSize() - FVector2D(Margen.Right, Margen.Top);
    //LUCorner = MyGeometry.GetAbsolutePosition() + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
    //RBCorner = MyGeometry.GetAbsolutePosition() + MyGeometry.GetLocalSize() - FVector2D(Margen.Right, Margen.Top);
    Origin = FVector2D(LUCorner.X, RBCorner.Y);


    CalcularPuntosDibujar();*/
    //debo calcular los puntos de dibujar y el corner solo cuando estos se deformen por alguna razon, por ejemplo al llamar a acgrandar, meintras tura la animacion, debo llamar a esto en el tick, quiza deba manejar un booleano para esto
    //UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    //if (CanvasSlot) {
    for (int i = 0; i < SeriesDibujar.Num(); i++) {
        for (int j = 0; j < SeriesDibujar[i].Num(); j++) {
            UCanvasPanelSlot * PuntoCanvasSlot = Cast<UCanvasPanelSlot>(SeriesPuntoWidgets[i][j]->Slot);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot

            //CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
            PuntoCanvasSlot->SetPosition(SeriesDibujar[i][j] - MyGeometry.Position - SizePoints/2);// com la funcion es const, este calculo no lo puedo hacer aqui
        }
    }
    //}


}



void UGraficaDetalleWidget::SetSerie(int IdSerie, TArray<FVector2D> Points) {
    Super::SetSerie(IdSerie, Points);
}

void UGraficaDetalleWidget::SetColorSerie(int IdSerie, FLinearColor ColorSerie) {
    Super::SetColorSerie(IdSerie, ColorSerie);
    for (int i = 0; i < SeriesPuntoWidgets[IdSerie].Num(); i++) {
        SeriesPuntoWidgets[IdSerie][i]->ColorFondoPunto = ColorSerie;
    }
}

int UGraficaDetalleWidget::AddSerie(TArray<FVector2D> Points) {
    int IdSerie = Super::AddSerie(Points);

    TArray<UPuntoGrafica *> PuntosGraficaSerie;
    for (int i = 0; i < Points.Num(); i++) {
        UPuntoGrafica *  PuntoGrafica = CreateWidget<UPuntoGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypePunto);
        PuntosGraficaSerie.Add(PuntoGrafica);
        PuntoGrafica->AddToViewport(0);
        //grafica->SetAlignmentInViewport(FVector2D(0.5, 0.5));//este no es necesario
        //grafica->SetAnchorsInViewport();
        //grafica->SetPositionInViewport(FVector2D(0.0f, 0.0f));

        UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
        UPanelWidget * CanvasWidget = nullptr;
        if (RootWidget->GetChildrenCount()){
            CanvasWidget = Cast<UPanelWidget>(RootWidget->GetChildAt(0));
        }
        //UPanelSlot * Slot = RootWidget->AddChild(PuntoGrafica);
        UPanelSlot * SlotPuntoGrafica = CanvasWidget->AddChild(PuntoGrafica);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotPuntoGrafica);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        CanvasSlot->SetZOrder(0);

        USizeBox * SizeBoxWidget = Cast<USizeBox>(PuntoGrafica->GetRootWidget());
        if (SizeBoxWidget) {
            CanvasSlot->SetSize(SizePoints);// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
        }

        CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
        CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
        //CanvasSlot->SetSize(FVector2D(600.0f, 400.0f));
        //pasarle la informacion pertinente

        //PuntoGrafica->SetVisibility(ESlateVisibility::Hidden);//deben iniciar ocultos, visible segun lo necesite

    }
    SeriesPuntoWidgets.Add(PuntosGraficaSerie);
    return IdSerie;
}

void UGraficaDetalleWidget::HideSerie(int IdSerie) {
    Super::HideSerie(IdSerie);
}

void UGraficaDetalleWidget::ShowSerie(int IdSerie) {
    Super::ShowSerie(IdSerie);
}

void UGraficaDetalleWidget::ClearSeries() {
    Super::ClearSeries();
}
