// Fill out your copyright notice in the Description page of Project Settings.

#include "StationMarkWidget.h"
#include "UObject/ConstructorHelpers.h"

UStationMarkWidget::UStationMarkWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
	static ConstructorHelpers::FObjectFinder<UMaterial> MarcaMaterialAsset(TEXT("Material'/Game/GeoVisualization/UMG/Materials/StationMarkMaterial.StationMarkMaterial'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (MarcaMaterialAsset.Succeeded()) {
        MarcaMaterial = MarcaMaterialAsset.Object;
    }

	static ConstructorHelpers::FObjectFinder<UMaterial> MarcaFijaMaterialAsset(TEXT("Material'/Game/GeoVisualization/UMG/Materials/StationMark2Material.StationMark2Material'"));//de usar este creo que debo crear un obtener un  material y ponerselo, este tiene el pivot en el centro de la esfera
    if (MarcaFijaMaterialAsset.Succeeded()) {
        MarcaFijaMaterial = MarcaFijaMaterialAsset.Object;
    }

    bFijado = false;
}

void UStationMarkWidget::Fijar() {
    bFijado = true;
    if (Imagen) {
        Imagen->SetBrushFromMaterial(MarcaFijaMaterial);
    }
}

void UStationMarkWidget::Liberar() {
    bFijado = false;
    if (Imagen) {
        Imagen->SetBrushFromMaterial(MarcaMaterial);
    }
}

void UStationMarkWidget::Mostrar() {
    bVisible = true;
    SetVisibility(ESlateVisibility::HitTestInvisible);//deben iniciar ocultos, visible segun lo necesite
}

void UStationMarkWidget::Ocultar() {
    bVisible = false;
    SetVisibility(ESlateVisibility::Hidden);//deben iniciar ocultos, visible segun lo necesite
}
