// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "BotonInterface.generated.h"

// This class does not need to be modified.
//UINTERFACE(MinimalAPI)
UINTERFACE(Blueprintable)
class UBotonInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GEOVISUALIZATION_API IBotonInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Click();
    virtual void Click_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void DoubleClick();
    virtual void DoubleClick_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Press();
    virtual void Press_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Release();
    virtual void Release_Implementation();
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Hover();
    virtual void Hover_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Unhover();
    virtual void Unhover_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Select();
    virtual void Select_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Unselect();
    virtual void Unselect_Implementation();
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Hide();
    virtual void Hide_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Show();
    virtual void Show_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Collapse();
    virtual void Collapse_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Boton")
    void Tint(float NewColorComponentH, float NewColorComponentS);
    virtual void Tint_Implementation(float NewColorComponentH, float NewColorComponentS);
}; 