// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraficaWidget.h"
#include "Blueprint/UserWidget.h"
#include "../Visualizaciones/PuntoGrafica.h"
#include "GraficaDetalleWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaDetalleWidget : public UGraficaWidget
{
	GENERATED_BODY()
	
public:
    UGraficaDetalleWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    FVector2D SizePoints;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UPuntoGrafica> TypePunto;

    TArray<TArray<UPuntoGrafica *>> SeriesPuntoWidgets;
	
    virtual void SetSerie(int IdSerie, TArray<FVector2D> Points) override;

    virtual void SetColorSerie(int IdSerie, FLinearColor ColorSerie) override;

    virtual int AddSerie(TArray<FVector2D> Points) override;//retorna el id de la serie en la grafica

    virtual void HideSerie(int IdSerie) override;

    virtual void ShowSerie(int IdSerie) override;

    virtual void ClearSeries() override;
	
};

//esta clase puede ervir para diferencia al gunas cosas, que esta sea como un station widget, donde habra tenxtos y otros elementos, y tambine habra un sizebox que contendra una grafica.

//debo tener dos graficas, una con detalle y otra sin detalle