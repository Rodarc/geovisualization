// Fill out your copyright notice in the Description page of Project Settings.

#include "LabelPuntoGrafica.h"
#include "UObject/ConstructorHelpers.h"

ULabelPuntoGrafica::ULabelPuntoGrafica(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {

    ColorFondoLabel = FLinearColor(0.005f, 0.005f, 0.005f, 0.9);

    static ConstructorHelpers::FObjectFinder<USlateBrushAsset> FondoLabelBrushFinder(TEXT("SlateBrushAsset'/Game/GeoVisualization/UMG/FondoGraficaBrush.FondoGraficaBrush'"));
    //deberia buscar una grafica
    if (FondoLabelBrushFinder.Succeeded()) {
        BrushFondoLabel = FondoLabelBrushFinder.Object;
    }
}



