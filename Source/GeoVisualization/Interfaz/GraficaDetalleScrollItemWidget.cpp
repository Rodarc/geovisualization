// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaDetalleScrollItemWidget.h"
#include "UObject/ConstructorHelpers.h"

UGraficaDetalleScrollItemWidget::UGraficaDetalleScrollItemWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer){
    //static ConstructorHelpers::FClassFinder<UGraficaWidget> GraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/GraficaWidgetBP.GraficaWidgetBP_C'"));
    static ConstructorHelpers::FClassFinder<UGraficaWidget> GraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/GraficaDetalleWidgetBP.GraficaDetalleWidgetBP_C'"));
    if (GraficaWidgetClass.Succeeded()) {
        TypeGrafica = GraficaWidgetClass.Class;
    }
}

/*void UGraficaDetalleScrollItemWidget::NativeConstruct() {
}*/

void UGraficaDetalleScrollItemWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
}

void UGraficaDetalleScrollItemWidget::SetInformation(FString InStationName, FString InGeoPosition) {
    StationName = InStationName;
    GeoPosition = InGeoPosition;
}




