// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/SizeBox.h"
#include "GraficaWidget.h"
#include "GraficaDetalleScrollItemWidget.generated.h"

/**
 *  quiza deberia incluir detalle widget de una vez, por ahora como prueba lo dejare con la grafica generica
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaDetalleScrollItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UGraficaDetalleScrollItemWidget(const FObjectInitializer & ObjectInitializer);

    //virtual void NativeConstruct() override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USizeBox * GraficaSizeBox;

    //neceisto una referencia del station widget del que vengo, o de la grafica de la que vengo
    //esto par saber si algun satation wiget me mando aqui, y no volver a mandarlo, mentras estoy cmparando otras y se me olvide que ya habia mandado esta

    //creo que necesitare el tipo de grafica
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UGraficaWidget> TypeGrafica;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UGraficaWidget * Grafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    class UStationWidget * StationWidget;
    //si esto no funciona podria usar el indice de la estacion
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString StationName;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString GeoPosition;

    void SetInformation(FString InStationName, FString InGeoPosition);//o deberia recibir la estructura?
	
	
	
};

//esta clase solo debete tener bonoes para quitar, y de prono alogo de informacion sobre la esctacion perodo y de mas,
//el resto de informacino por ejemplo numero puntitos, de pronto lables, los tiene que dibujar la graficadetalle