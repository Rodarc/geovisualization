// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Interfaz/VisualizationHUD.h"
#include "ConexionServidor/ClimateServerConnection.h"
#include "Estructuras/StationStruct.h"
#include "Estructuras/DailyDataStruct.h"
#include "MapaDepartamentos.h"
#include "MapaTemp.h"
#include "Mapa/VisualizationMap.h"
#include "GeoVisualizationGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API AGeoVisualizationGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
    AGeoVisualizationGameModeBase();
	
    virtual void BeginPlay() override;

    virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "GeoVisualization", Meta = (BlueprintProtected = "true"))//significa que solo se puede cambiar en el constructor
    TSubclassOf<UVisualizationHUD> TypeHUDWidget;  
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GeoVisualization")
    UVisualizationHUD * VisualizationHUDReference;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GeoVisualization")
    AClimateServerConnection * Conexion;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GeoVisualization")
    AMapaTemp * MapaTemp;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GeoVisualization")
    AMapaDepartamentos * MapaDepartamentos;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GeoVisualization")
    AVisualizationMap * VisualizationMapReference;

    UFUNCTION(BlueprintCallable, Category = "GeoVisualization")
    void GetStationsData();

    //UFUNCTION(BlueprintCallable, Category = "GeoVisualization")
    //no se puede llamar desde bluepirnt, esta comunicacion debe ser interna en el codigo c++
    void RecibirStationsData(TArray<FStationStruct> Stations);

    UFUNCTION(BlueprintCallable, Category = "GeoVisualization")
    void GetTemperatureAverageDataYearMonthForStation(int idStation, int initialyear, int finalyear, TArray<int> months);

    void RecibirTemperatureData(TArray<FDailyDataStruct> Datos);

    UFUNCTION(BlueprintCallable, Category = "GeoVisualization")
    void GetVariableDataYearsMonthsForStation(int idStation, int initialyear, int finalyear, TArray<int> months, FString variable);

    void RecibirVariableData(TArray<FDailyDataStruct> Datos, FString variable);


    void RecibirIntervaloTemperatureData(TArray<FDailyDataStruct> Datos);

    UFUNCTION(BlueprintCallable, Category = "GeoVisualization")
    void SetDataOptions();//en teoria deberia recibir que base de datos usare
	
	
};