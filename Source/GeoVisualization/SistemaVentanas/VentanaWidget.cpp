// Fill out your copyright notice in the Description page of Project Settings.

#include "VentanaWidget.h"
#include "Kismet/GameplayStatics.h"

UVentanaWidget::UVentanaWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer){
    //static ConstructorHelpers::FClassFinder<UGraficaWidget> GraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/GraficaWidgetBP.GraficaWidgetBP_C'"));
    MinSize = FVector2D(200.0f, 45.0f);

    Months.Add("Enero");
    AbrevMonths.Add("ENE");
    Months.Add("Febrero");
    AbrevMonths.Add("FEB");
    Months.Add("Marzo");
    AbrevMonths.Add("MAR");
    Months.Add("Abril");
    AbrevMonths.Add("ABR");
    Months.Add("Mayo");
    AbrevMonths.Add("MAY");
    Months.Add("Junio");
    AbrevMonths.Add("JUN");
    Months.Add("Julio");
    AbrevMonths.Add("JUL");
    Months.Add("Agosto");
    AbrevMonths.Add("AGO");
    Months.Add("Septiembre");
    AbrevMonths.Add("SEP");
    Months.Add("Octubre");
    AbrevMonths.Add("OCT");
    Months.Add("Noviembre");
    AbrevMonths.Add("NOV");
    Months.Add("Diciembre");
    AbrevMonths.Add("DIC");
}

/*void UGraficaDetalleScrollItemWidget::NativeConstruct() {
}*/

void UVentanaWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
}

void UVentanaWidget::CreateVisualizacionOfType(TSubclassOf<UVisualizacionWidget> TypeVis) {
    TypeVisualizacion = TypeVis;
    VisualizacionWidget = CreateWidget<UVisualizacionWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeVisualizacion);
    //debo agregar esta visualizacion al contenedor
    VisualizacionWidget->AddToViewport(1);
    VisualizacionSizeBox->AddChild(VisualizacionWidget);
    UpdateIconoVentana();
    //UpdateTitleVentana();


}

void UVentanaWidget::UpdateFilters_Implementation() {
}

void UVentanaWidget::CollapseFilters_Implementation() {
}

void UVentanaWidget::UpdateIconoVentana_Implementation() {
}

void UVentanaWidget::UpdateTitleVentana_Implementation() {
    if (VisualizacionWidget) {
        Title = VisualizacionWidget->GetTitleWindowInformation();
    }
}



