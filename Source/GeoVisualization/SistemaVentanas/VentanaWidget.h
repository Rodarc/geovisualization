// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/SizeBox.h"
#include "../Visualizaciones/VisualizacionWidget.h"
#include "VentanaWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UVentanaWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UVentanaWidget(const FObjectInitializer & ObjectInitializer);

    //virtual void NativeConstruct() override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ventana")
    USizeBox * VisualizacionSizeBox;

    //neceisto una referencia del station widget del que vengo, o de la grafica de la que vengo
    //esto par saber si algun satation wiget me mando aqui, y no volver a mandarlo, mentras estoy cmparando otras y se me olvide que ya habia mandado esta

    //creo que necesitare el tipo de grafica
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UVisualizacionWidget> TypeVisualizacion;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UVisualizacionWidget * VisualizacionWidget;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    class UStationWidget * StationWidget;
    //si esto no funciona podria usar el indice de la estacion
    //probablemente necesite tamben referencias a las ventanas
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ventana")
    FText Title;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ventana")
    FVector2D MinSize;

    void CreateVisualizacionOfType(TSubclassOf<UVisualizacionWidget> TypeVis);//no se si esto sea necesario pasarle el parametro
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventana")
    void UpdateFilters();
    virtual void UpdateFilters_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventana")
    void CollapseFilters();
    virtual void CollapseFilters_Implementation();
	
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ventana - Filtros")
    TArray<FString> Months;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ventana - Filtros")
    TArray<FString> AbrevMonths;//para mostrar en la interfaz

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventana")
    void UpdateIconoVentana();
    virtual void UpdateIconoVentana_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Ventana")
    void UpdateTitleVentana();
    virtual void UpdateTitleVentana_Implementation();
	
};
