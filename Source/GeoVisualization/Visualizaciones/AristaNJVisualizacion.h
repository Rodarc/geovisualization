// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "NodoNJVisualizacion.h"
#include "AristaNJVisualizacion.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UAristaNJVisualizacion : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UAristaNJVisualizacion(const FObjectInitializer & ObjectInitializer);

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTime) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorArista;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float Ancho;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdArista;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UNodoNJVisualizacion * SourceNodo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UNodoNJVisualizacion * TargetNodo;
	
    void Actualizar();
};
