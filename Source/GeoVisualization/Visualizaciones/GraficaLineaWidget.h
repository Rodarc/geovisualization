// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisualizacionWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "GraficaLineaWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaLineaWidget : public UVisualizacionWidget
{
	GENERATED_BODY()
	
public:
    UGraficaLineaWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual int32 NativePaint(const FPaintArgs & Args, const FGeometry & AllottedGeometry, const FSlateRect & MyCullingRect, FSlateWindowElementList & OutDrawElements, int32 LayerId, const FWidgetStyle & InWidgetStyle, bool bParentEnabled) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<bool> VisibilitySeries;
	
    //UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<TArray<FVector2D>> Series;

    //UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<TArray<FVector2D>> SeriesDibujar;

    //UFUNCTION(BlueprintCallable, Category = "Grafica")
    TArray<TArray<FVector2D>> GetSeries();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    TArray<FVector2D> GetSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void SetSerie(int IdSerie, TArray<FVector2D> Points);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void SetColorSerie(int IdSerie, FLinearColor ColorSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual int AddSerie(TArray<FVector2D> Points);//retorna el id de la serie en la grafica

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void HideSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ShowSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ClearSeries();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetIntervalXForSerie(int IdSerie, float a, float b);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetIntervalYForSerie(int IdSerie, float a, float b);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void CalcularCorners();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void CalcularPuntosDibujar();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetMargin(FMargin NuevoMargen);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FMargin Margen;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D Origin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D LUCorner;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D RBCorner;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> YMaxs;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> YMins;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> XMaxs;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> XMins;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorFondoGrafica;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<FLinearColor> ColorSeries;//interno para las graficas dibujadas

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FLinearColor> GeneralColorSeries;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USlateBrushAsset * BrushFondoGrafica;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Filtros")
    TArray<FString> Months;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Filtros")
    TArray<FString> AbrevMonths;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<int> CantidadDiasMes;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> AcumCantidadDiasMesConsultados;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int CantidadDiasYearConsulta;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> AcumCantidadDiasYearConsultados;

    TArray<FVector2D> ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct>& Datos);

    void CalcularCantidadDiasConsulta();

	
}; 