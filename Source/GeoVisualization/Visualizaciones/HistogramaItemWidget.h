// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HistogramaItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UHistogramaItemWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UHistogramaItemWidget(const FObjectInitializer & ObjectInitializer);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ItemColor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString RangoText;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FString PorcentajeText;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetColor(FLinearColor NewColor);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetRangoText(FString NewRangeText);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetPorcentajeText(FString NewPorcentajeText);
	
	
	
};
