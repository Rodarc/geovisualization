// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HistogramaWidget.h"
#include "Components/VerticalBox.h"
#include "HistogramaLabelWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UHistogramaLabelWidget : public UHistogramaWidget
{
	GENERATED_BODY()
	
public:
    UHistogramaLabelWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativeConstruct() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Histograma")
    TSubclassOf<UHistogramaItemWidget> TypeItem;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Histograma")
    TArray<UHistogramaItemWidget *> Items;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Histograma")
    UVerticalBox * ItemsVB;

    virtual int AddItem_Implementation() override;
	
    virtual int AddItem(FString RangoText, FString PorcentajeText, FLinearColor Color) override;

    virtual void SetItem(int IdItem, FString RangoText, FString PorcentajeText, FLinearColor Color) override;

    virtual void ClearItems() override;
	
	
	
};
