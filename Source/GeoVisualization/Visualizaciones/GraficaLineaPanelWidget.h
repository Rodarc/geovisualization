// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraficaLineaWidget.h"
#include "PuntoGrafica.h"
#include "GraficaLineaPanelWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaLineaPanelWidget : public UGraficaLineaWidget
{
	GENERATED_BODY()
	
public:
    UGraficaLineaPanelWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual int32 NativePaint(const FPaintArgs & Args, const FGeometry & AllottedGeometry, const FSlateRect & MyCullingRect, FSlateWindowElementList & OutDrawElements, int32 LayerId, const FWidgetStyle & InWidgetStyle, bool bParentEnabled) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
    virtual void CalcularCorners() override;

    virtual void CalcularPuntosDibujar() override;
	
    FVector2D SizePoints;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UPuntoGrafica> TypePunto;

    TArray<TArray<UPuntoGrafica *>> SeriesPuntoWidgets;
	
    virtual void SetSerie(int IdSerie, TArray<FVector2D> Points) override;

    virtual void SetColorSerie(int IdSerie, FLinearColor ColorSerie) override;

    virtual int AddSerie(TArray<FVector2D> Points) override;//retorna el id de la serie en la grafica

    virtual void HideSerie(int IdSerie) override;

    virtual void ShowSerie(int IdSerie) override;

    virtual void ClearSeries() override;
	
    virtual void VisualizarConsulta_Implementation() override;
};
