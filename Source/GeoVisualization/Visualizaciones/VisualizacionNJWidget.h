// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisualizacionWidget.h"
#include "../NJ/NJ.h"
#include "VisualizacionNJWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UVisualizacionNJWidget : public UVisualizacionWidget
{
	GENERATED_BODY()

public:
    UVisualizacionNJWidget(const FObjectInitializer & ObjectInitializer);

    //No estoy haciendo nada en algunas de estas funciones, quiza no deba sobrescribirlas
    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual int32 NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
    virtual void CreateNodos();

    virtual void CreateAristas();
	
    virtual void AplicarTraslacion(FVector Traslacion);
	
    virtual void AplicarRotacionRelativaANodo(Nodo* NodoReferencia, FVector PuntoReferencia);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visualization - Parametros")
    float DistanciaArista;

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    void Layout();

    UFUNCTION(BlueprintCallable, Category = "Visualization - Layouts")
    virtual void ActualizarLayout();
	
    void Calculos(Nodo * V);

    void Calculos2();

    void Calc();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void RotacionRama();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual FVector InterseccionLineaSuperficie();

    UFUNCTION(BlueprintCallable, Category = "Visualization")
    virtual void TraslacionConNodoGuia();

    void ImprimirNodos(Nodo ** nodos, int num);

    void LeerDatosDePrueba();

    //datos leisod de los archivos, estas variables son las matrices de distancias
    float ** m; //matriz temporal

    string * d; //nombres temporal

    int n; //numero de nodos temporal

    void CalcularNJ();

    NJ * AlgoritmoNJ;

    Nodo ** ArbolGenerado;
    
    int TamArbolGenerado;

    //Nodo * Root;
	
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float RadioNodos;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float AnchoAristas;
	
    UFUNCTION(BlueprintCallable, Category = "Visualization")
    void ConstruirMatrizDeDistanciasDeConsulta();

    TArray<TArray<bool>> SelectedDimensions;//variable, dato individual

    void SelectVectorDimensions();

    float DistanciaEntreStations(int IdStationA, int IdStationB);
};
