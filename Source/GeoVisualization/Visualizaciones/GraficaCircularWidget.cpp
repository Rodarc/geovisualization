// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaCircularWidget.h"

UGraficaCircularWidget::UGraficaCircularWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    Months.Add("Enero");
    AbrevMonths.Add("ENE");
    CantidadDiasMes.Add(31);
    Months.Add("Febrero");
    AbrevMonths.Add("FEB");
    CantidadDiasMes.Add(29);
    Months.Add("Marzo");
    AbrevMonths.Add("MAR");
    CantidadDiasMes.Add(31);
    Months.Add("Abril");
    AbrevMonths.Add("ABR");
    CantidadDiasMes.Add(30);
    Months.Add("Mayo");
    AbrevMonths.Add("MAY");
    CantidadDiasMes.Add(31);
    Months.Add("Junio");
    AbrevMonths.Add("JUN");
    CantidadDiasMes.Add(30);
    Months.Add("Julio");
    AbrevMonths.Add("JUL");
    CantidadDiasMes.Add(31);
    Months.Add("Agosto");
    AbrevMonths.Add("AGO");
    CantidadDiasMes.Add(31);
    Months.Add("Septiembre");
    AbrevMonths.Add("SEP");
    CantidadDiasMes.Add(30);
    Months.Add("Octubre");
    AbrevMonths.Add("OCT");
    CantidadDiasMes.Add(31);
    Months.Add("Noviembre");
    AbrevMonths.Add("NOV");
    CantidadDiasMes.Add(30);
    Months.Add("Diciembre");
    AbrevMonths.Add("DIC");
    CantidadDiasMes.Add(31);

    AnguloInterVar = 2 * PI / 64;//el offset es la mita de esto
    AnguloInterYear = 2 * PI / 128;//el offset es la mita de esto
    AnguloInterMonth = 2 * PI / 256;//el offset es la mita de esto
}

void UGraficaCircularWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);
}

void UGraficaCircularWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
}

void UGraficaCircularWidget::CalcularPuntosDibujar() {
}

TArray<FVector2D> UGraficaCircularWidget::ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct>& Datos) {
    TArray<FVector2D> res;
    for (int i = 0; i < Datos.Num(); i++) {
        //if (Datos[i].value != -9999) {
        if (Datos[i].value != -9999 && MesesFiltrados[Datos[i].month-1] && YearsFiltrados[Datos[i].year - Consulta.InitialYear]) {
            int idMonthConsultado = Consulta.MesesConsultados.Find(Datos[i].month-1);
            int idYearConsultado = Datos[i].year - Consulta.InitialYear;//este se podria calcular mas, rapido en base a una resta con el primer elemento
            //int ValX = idYearConsultado * CantidadDiasYearConsulta + Datos[i].day - 1;
            int ValX = AcumCantidadDiasYearConsultados[idYearConsultado] + Datos[i].day - 1;
            if (idMonthConsultado > 0) {
                ValX += AcumCantidadDiasMesConsultados[idMonthConsultado - 1];
            }
            //convertir tambien el Value de los datos, para evitar cosas, tambien en funcion del intervalor
            //aun no tengo los maximos
            //float ValY = Datos[i].value / (Consulta.StationConsultas[0].ConsultasPorVariable[i].YMax - Consulta.StationConsultas[0].ConsultasPorVariable[i].YMin);
            //res.Add(FVector2D(float(ValX)/Consulta.StationConsultas[0].ConsultasPorVariable[i].XMax, ValY));//en ese xmax, esta el acumulador

            res.Add(FVector2D(ValX, Datos[i].value));
        }
    }
    return res;
}

TArray<FVector2D> UGraficaCircularWidget::ConvertirArrayDailyDataStructToFVector2DComplete(TArray<FDailyDataStruct>& Datos) {
    TArray<FVector2D> res;
    for (int i = 0; i < Datos.Num(); i++) {
        //if (Datos[i].value != -9999 && MesesFiltrados[Datos[i].month-1] && YearsFiltrados[Datos[i].year - Consulta.InitialYear]) {
        int idMonthConsultado = Consulta.MesesConsultados.Find(Datos[i].month-1);
        int idYearConsultado = Datos[i].year - Consulta.InitialYear;//este se podria calcular mas, rapido en base a una resta con el primer elemento
        //int ValX = idYearConsultado * CantidadDiasYearConsulta + Datos[i].day - 1;
        int ValX = AcumCantidadDiasYearConsultados[idYearConsultado] + Datos[i].day - 1;
        if (idMonthConsultado > 0) {
            ValX += AcumCantidadDiasMesConsultados[idMonthConsultado - 1];
        }
        //convertir tambien el Value de los datos, para evitar cosas, tambien en funcion del intervalor
        //aun no tengo los maximos
        //float ValY = Datos[i].value / (Consulta.StationConsultas[0].ConsultasPorVariable[i].YMax - Consulta.StationConsultas[0].ConsultasPorVariable[i].YMin);
        //res.Add(FVector2D(float(ValX)/Consulta.StationConsultas[0].ConsultasPorVariable[i].XMax, ValY));//en ese xmax, esta el acumulador

        res.Add(FVector2D(ValX, Datos[i].value));
        //}
    }
    return res;
}

TArray<TArray<TArray<FVector2D>>> UGraficaCircularWidget::ConvertirArrayDailyDataStructToFVector2DDivido(TArray<FDailyDataStruct>& Datos) {
	TArray<TArray<TArray<FVector2D>>> res;
	res.SetNum(Consulta.FinalYear - Consulta.InitialYear + 1);
	for (int i = 0; i < res.Num(); i++) {
		res[i].SetNum(Consulta.MesesConsultados.Num());
	}
	for (int i = 0; i < Datos.Num(); i++) {
        int idMonthConsultado = Consulta.MesesConsultados.Find(Datos[i].month-1);
        int idYearConsultado = Datos[i].year - Consulta.InitialYear;//este se podria calcular mas, rapido en base a una resta con el primer elemento
        res[idYearConsultado][idMonthConsultado].Add(FVector2D(Datos[i].day - 1, Datos[i].value));
	}
	return res;
}

void UGraficaCircularWidget::CalcularCantidadDiasConsulta() {
    int acum = 0;

    AcumCantidadDiasMesConsultados.Empty();
    //AcumCantidadDiasMesConsultados.SetNum(12);
    //for (int i = 0; i < MesesFiltrados.Num(); i++) {//si un mes esta en la consulta entonces acumulo, caso contrario es 0
    for (int i = 0; i < Consulta.MesesConsultados.Num(); i++) {//si un mes esta en la consulta entonces acumulo, caso contrario es 0
        //los meses filtrados es un array de 12
        //int idMesConsultado = Months.Find(MesesConsulta[i]);
        if (MesesFiltrados[Consulta.MesesConsultados[i]]) {
            acum += CantidadDiasMes[Consulta.MesesConsultados[i]];
        }
        AcumCantidadDiasMesConsultados.Add(acum);
    }
    if (AcumCantidadDiasMesConsultados.Num()) {
        CantidadDiasYearConsulta = AcumCantidadDiasMesConsultados[AcumCantidadDiasMesConsultados.Num() - 1];
    }
    else {
        CantidadDiasYearConsulta = 0;
    }
    acum = 0;
    AcumCantidadDiasYearConsultados.Empty();
    for (int i = 0; i < Consulta.FinalYear - Consulta.InitialYear + 1; i++) {
        AcumCantidadDiasYearConsultados.Add(acum);
        if (YearsFiltrados[i]) {//le aumento depues a acum por que el primer a�� tiene 0 dias acumulados anteriore, eso para el calulo de la formula
            acum += CantidadDiasYearConsulta;
        }
    }

    for (int i = 0; i < Consulta.StationConsultas[0].ConsultasPorVariable.Num(); i++) {
        //Consulta.StationConsultas[0].ConsultasPorVariable[i].XMax = (Consulta.FinalYear - Consulta.InitialYear + 1) * CantidadDiasYearConsulta;//deberia cambiar en funcion de los a��s filtrados, 
        Consulta.StationConsultas[0].ConsultasPorVariable[i].XMax = acum;//deberia cambiar en funcion de los a��s filtrados, 
    }
}

void UGraficaCircularWidget::SetSerie(int IdSerie, TArray<FVector2D> Points) {
}

void UGraficaCircularWidget::SetColorSerie(int IdSerie, FLinearColor ColorSerie) {
    if (IdSerie < ColorSeries.Num()) {
        ColorSeries[IdSerie] = ColorSerie;
    }
}

int UGraficaCircularWidget::AddSerie(TArray<FDailyDataStruct> Datos) {//no encesita ser copiada
    TArray<FVector2D> Points = ConvertirArrayDailyDataStructToFVector2DComplete(Datos);
    //ahora los id de los pountos coincide con el de los datos, para poder enlazar, solo que al momento de dibujar debo ignorar los q -9999
	TArray<TArray<TArray<FVector2D>>> PointsDivididos = ConvertirArrayDailyDataStructToFVector2DDivido(Datos);
    Series.Add(Points);
	SeriesDividida.Add(PointsDivididos);
    SeriesDibujar.Add(TArray<FVector2D>());//esto para poder convertir
    SeriesDibujarYears.Add(TArray<FVector2D>());//esto para poder convertir
	SeriesDibujarMonths.Add(TArray<TArray<FVector2D>>());
	SeriesDibujarDaily.Add(TArray<TArray<TArray<FVector2D>>>());
    ColorSeries.Add(FLinearColor::White);//agregando el clor
    VisibilitySeries.Add(true);
    //Calcular los WIni y WTam para las series diarias
    WTamVar = 2 * PI / Series.Num();
	WTamCentro = 2 * PI / Series.Num();
    WInisVar.SetNum(Series.Num());
	WInisCentro.SetNum(Series.Num());
    for (int i = 0; i < WInisVar.Num(); i++) {
        WInisVar[i] = i * WTamVar + AnguloInterVar/2 ;
		WInisCentro[i] = i * WTamCentro;
    }

    WTamVar -= AnguloInterVar;
    //aqui inicia el calculo del promedio
    //a los arreglos se les agrea uno
    //esto va en funcion de los a�os y meses consultadosa//usar el arroglo de a�os y meses filtrados, como tama�o
    //WTamYear = (WTamVar - AnguloInterYear*YearsFiltrados.Num())  / YearsFiltrados.Num();//menos el interespacio de los a�os en la misma proporcion
    WTamYear = WTamVar / YearsFiltrados.Num();//menos el interespacio de los a�os en la misma proporcion
    TArray<float> PromediosYear;
    TArray<int> CountsPromediosYear;
    PromediosYear.SetNum(YearsFiltrados.Num());
    CountsPromediosYear.SetNum(YearsFiltrados.Num());
    WInisYears.SetNum(YearsFiltrados.Num());
    TArray<TArray<float>> PromediosMonth;
    TArray<TArray<int>> CountsPromediosMonth;
    PromediosMonth.SetNum(YearsFiltrados.Num());
    CountsPromediosMonth.SetNum(YearsFiltrados.Num());

    for (int i = 0; i < YearsFiltrados.Num(); i++) {
        PromediosYear[i] = 0.0f;
        CountsPromediosYear[i] = 0;
        WInisYears[i] = i * WTamYear + AnguloInterYear/2; //no son los ww si no la proporcion, deberia guardarlo asi?
        PromediosMonth[i].SetNum(MesesFiltrados.Num());
        CountsPromediosMonth[i].SetNum(MesesFiltrados.Num());
        for (int j = 0; j < MesesFiltrados.Num(); j++) {
            PromediosMonth[i][j] = 0.0f;
            CountsPromediosMonth[i][j] = 0;
        }
    }
    WTamYear = (WTamVar - AnguloInterYear*YearsFiltrados.Num()) / YearsFiltrados.Num();//menos el interespacio de los a�os en la misma proporcion
    //el calculo delos promedios deberia hacer antes? despues?
    //necesito la data correcta

    WInisMonths.SetNum(Consulta.MesesConsultados.Num());
    WTamsMonths.SetNum(Consulta.MesesConsultados.Num());
    if (Consulta.MesesConsultados.Num()) {
        //WInisMonths[0] = 0.0f;//el primer inis, deberia ser, Angleintervar/2 + year/2 + mont/2
        WInisMonths[0] = AnguloInterMonth/2;//el primer inis, deberia ser, Angleintervar/2 + year/2 + mont/2
    }
    for (int i = 0; i < Consulta.MesesConsultados.Num(); i++) {
        //WTamsMonths[i] = WTamYear * CantidadDiasMes[Consulta.MesesConsultados[i]] / CantidadDiasYearConsulta;//como seria con los meses filtrados?
        WTamsMonths[i] = (WTamYear - AnguloInterMonth*Consulta.MesesConsultados.Num()) * CantidadDiasMes[Consulta.MesesConsultados[i]] / CantidadDiasYearConsulta;//como seria con los meses filtrados?
        if(i > 0){
            WInisMonths[i] = WInisMonths[i - 1] + WTamsMonths[i - 1] + AnguloInterMonth;
        }
    }

    PromediosVar.Add(0.0f);
    CountPromediosVar.Add(0);
    PromediosMonths.Add(PromediosMonth);
    CountsPromediosMonths.Add(CountsPromediosMonth);
    PromediosYears.Add(PromediosYear);
    CountsPromediosYears.Add(CountsPromediosYear);
    //ahora procedo a calcular los promedios?
    //ahora debo poner o crear lo necesario para las series de los meses y a�os
	CalcularPromedios(Series.Num()-1, Datos);
    return Series.Num()-1;
}

void UGraficaCircularWidget::HideSerie(int IdSerie) {
}

void UGraficaCircularWidget::ShowSerie(int IdSerie) {
}

void UGraficaCircularWidget::ClearSeries() {
    Series.Empty();
    SeriesDibujar.Empty();
    VisibilitySeries.Empty();
    ColorSeries.Empty();
    WInis.Empty();
    WTams.Empty();
}

void UGraficaCircularWidget::CalcularPromedios(int IdSerie, TArray<FDailyDataStruct>& Datos) {// no necesitaria ser virtual
    for (int i = 0; i < Datos.Num(); i++) {
        //if (Datos[i].value != -9999) {
        if (Datos[i].value != -9999 && MesesFiltrados[Datos[i].month-1] && YearsFiltrados[Datos[i].year - Consulta.InitialYear]) {
            int idMonth = Datos[i].month-1;
            int idYear = Datos[i].year - Consulta.InitialYear;//este se podria calcular mas, rapido en base a una resta con el primer elemento
            PromediosVar[IdSerie] += Datos[i].value;
            CountPromediosVar[IdSerie]++;
            PromediosYears[IdSerie][idYear] += Datos[i].value;
            CountsPromediosYears[IdSerie][idYear]++;
            PromediosMonths[IdSerie][idYear][idMonth] += Datos[i].value;
            CountsPromediosMonths[IdSerie][idYear][idMonth]++;
        }
    }
    for (int i = 0; i < YearsFiltrados.Num(); i++) {
        if (CountsPromediosYears[IdSerie][i]) {
            PromediosYears[IdSerie][i] = PromediosYears[IdSerie][i] / CountsPromediosYears[IdSerie][i];
        }
        for (int j = 0; j < MesesFiltrados.Num(); j++) {
            if (CountsPromediosMonths[IdSerie][i][j]) {
                PromediosMonths[IdSerie][i][j] = PromediosMonths[IdSerie][i][j] / CountsPromediosMonths[IdSerie][i][j];
            }
        }
    }
	if (CountPromediosVar[IdSerie]) {//no deberia mostrar promedios si es que no se ha contado nada
		PromediosVar[IdSerie] = PromediosVar[IdSerie] / CountPromediosVar[IdSerie];
	}
    //promedios calculados
}

void UGraficaCircularWidget::ActualizarGrafica() {
}

void UGraficaCircularWidget::ExplotarPunto(int IdVar, int IdYear, int IdMonth) {
}

void UGraficaCircularWidget::ContraerPunto(int IdVar, int IdYear, int IdMonth) {
}
