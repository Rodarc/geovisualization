// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "../Estructuras/ConsultaStruct.h"
#include "VisualizacionWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UVisualizacionWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UVisualizacionWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativeConstruct() override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bTransformando;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FConsultaStruct Consulta;
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void VisualizarConsulta();
    virtual void VisualizarConsulta_Implementation();
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<bool> MesesFiltrados;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<bool> YearsFiltrados;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> StationsFiltradas;
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void IncluirMes(int IdMes);
    virtual void IncluirMes_Implementation(int IdMes);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ExcluirMes(int IdMes);
    virtual void ExcluirMes_Implementation(int IdMes);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void IncluirYear(int IdYear);
    virtual void IncluirYear_Implementation(int IdYear);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ExcluirYear(int IdYear);
    virtual void ExcluirYear_Implementation(int IdYear);
    
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void IncluirVariable(int IdYear);
    virtual void IncluirVariable_Implementation(int IdVariable);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ExcluirVariable(int IdVariable);
    virtual void ExcluirVariable_Implementation(int IdVariable);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void IncluirStation(int IdYear);
    virtual void IncluirStation_Implementation(int IdStation);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ExcluirStation(int IdStation);
    virtual void ExcluirStation_Implementation(int IdStation);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void PrepararYearsFiltrados();
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    FText GetTitleWindowInformation();
    virtual FText GetTitleWindowInformation_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    FText GetTitleDetailPanelInformation();
    virtual FText GetTitleDetailPanelInformation_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    FText GetSubtitleDetailPanelInformation();
    virtual FText GetSubtitleDetailPanelInformation_Implementation();
};