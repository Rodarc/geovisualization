// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaLineaWidget.h"


UGraficaLineaWidget::UGraficaLineaWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    Months.Add("Enero");
    AbrevMonths.Add("ENE");
    CantidadDiasMes.Add(31);
    Months.Add("Febrero");
    AbrevMonths.Add("FEB");
    CantidadDiasMes.Add(29);
    Months.Add("Marzo");
    AbrevMonths.Add("MAR");
    CantidadDiasMes.Add(31);
    Months.Add("Abril");
    AbrevMonths.Add("ABR");
    CantidadDiasMes.Add(30);
    Months.Add("Mayo");
    AbrevMonths.Add("MAY");
    CantidadDiasMes.Add(31);
    Months.Add("Junio");
    AbrevMonths.Add("JUN");
    CantidadDiasMes.Add(30);
    Months.Add("Julio");
    AbrevMonths.Add("JUL");
    CantidadDiasMes.Add(31);
    Months.Add("Agosto");
    AbrevMonths.Add("AGO");
    CantidadDiasMes.Add(31);
    Months.Add("Septiembre");
    AbrevMonths.Add("SEP");
    CantidadDiasMes.Add(30);
    Months.Add("Octubre");
    AbrevMonths.Add("OCT");
    CantidadDiasMes.Add(31);
    Months.Add("Noviembre");
    AbrevMonths.Add("NOV");
    CantidadDiasMes.Add(30);
    Months.Add("Diciembre");
    AbrevMonths.Add("DIC");
    CantidadDiasMes.Add(31);

}


void UGraficaLineaWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);
}

int32 UGraficaLineaWidget::NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const {
    int32 res = Super::NativePaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
    return res;
}


void UGraficaLineaWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
}

TArray<TArray<FVector2D>> UGraficaLineaWidget::GetSeries() {
    return Series;
}

TArray<FVector2D> UGraficaLineaWidget::GetSerie(int IdSerie) {
    if (IdSerie < Series.Num()) {
        return Series[IdSerie];
    }
    return TArray<FVector2D>();
}

void UGraficaLineaWidget::SetSerie(int IdSerie, TArray<FVector2D> Points) {
    if (IdSerie < Series.Num()) {
        Series[IdSerie] = Points;
    }
}

void UGraficaLineaWidget::SetColorSerie(int IdSerie, FLinearColor ColorSerie) {
    if (IdSerie < ColorSeries.Num()) {
        ColorSeries[IdSerie] = ColorSerie;
    }
}

int UGraficaLineaWidget::AddSerie(TArray<FVector2D> Points) {//siempre se debe llamar a esta funcion para agregar una nueva series
    Series.Add(Points);
    SeriesDibujar.Add(TArray<FVector2D>());
    ColorSeries.Add(FLinearColor::White);
    VisibilitySeries.Add(true);
    XMaxs.Add(1);
    XMins.Add(0);
    YMins.Add(0);
    YMaxs.Add(1);
    return Series.Num()-1;
}

void UGraficaLineaWidget::HideSerie(int IdSerie) {
    if (IdSerie < Series.Num()) {
        VisibilitySeries[IdSerie] = false;
    }
}

void UGraficaLineaWidget::ShowSerie(int IdSerie) {
    if (IdSerie < Series.Num()) {
        VisibilitySeries[IdSerie] = true;
    }
}

void UGraficaLineaWidget::ClearSeries() {
    Series.Empty();
    SeriesDibujar.Empty();
    VisibilitySeries.Empty();
    ColorSeries.Empty();
    XMaxs.Empty();
    XMins.Empty();
    YMaxs.Empty();
    YMins.Empty();
}

void UGraficaLineaWidget::SetIntervalXForSerie(int IdSerie, float a, float b) {
    if (IdSerie < XMaxs.Num()) {
        XMaxs[IdSerie] = b;
        XMins[IdSerie] = a;
    }
}

void UGraficaLineaWidget::SetIntervalYForSerie(int IdSerie, float a, float b) {
    if (IdSerie < YMaxs.Num()) {
        YMaxs[IdSerie] = b;
        YMins[IdSerie] = a;
    }
}

void UGraficaLineaWidget::CalcularCorners() {
}

void UGraficaLineaWidget::CalcularPuntosDibujar() {
}

void UGraficaLineaWidget::SetMargin(FMargin NuevoMargen) {
    Margen = NuevoMargen;
}

TArray<FVector2D> UGraficaLineaWidget::ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct>& Datos) {
    TArray<FVector2D> res;
    for (int i = 0; i < Datos.Num(); i++) {
        //if (Datos[i].value != -9999) {
        if (Datos[i].value != -9999 && MesesFiltrados[Datos[i].month-1] && YearsFiltrados[Datos[i].year - Consulta.InitialYear]) {
            int idMonthConsultado = Consulta.MesesConsultados.Find(Datos[i].month-1);
            int idYearConsultado = Datos[i].year - Consulta.InitialYear;//este se podria calcular mas, rapido en base a una resta con el primer elemento
            //int ValX = idYearConsultado * CantidadDiasYearConsulta + Datos[i].day - 1;
            int ValX = AcumCantidadDiasYearConsultados[idYearConsultado] + Datos[i].day - 1;
            if (idMonthConsultado > 0) {
                ValX += AcumCantidadDiasMesConsultados[idMonthConsultado - 1];
            }
            res.Add(FVector2D(ValX, Datos[i].value));
        }
    }
    return res;
}

void UGraficaLineaWidget::CalcularCantidadDiasConsulta() {
    int acum = 0;

    AcumCantidadDiasMesConsultados.Empty();
    //AcumCantidadDiasMesConsultados.SetNum(12);
    //for (int i = 0; i < MesesFiltrados.Num(); i++) {//si un mes esta en la consulta entonces acumulo, caso contrario es 0
    for (int i = 0; i < Consulta.MesesConsultados.Num(); i++) {//si un mes esta en la consulta entonces acumulo, caso contrario es 0
        //los meses filtrados es un array de 12
        //int idMesConsultado = Months.Find(MesesConsulta[i]);
        if (MesesFiltrados[Consulta.MesesConsultados[i]]) {
            acum += CantidadDiasMes[Consulta.MesesConsultados[i]];
        }
        AcumCantidadDiasMesConsultados.Add(acum);
    }
    if (AcumCantidadDiasMesConsultados.Num()) {
        CantidadDiasYearConsulta = AcumCantidadDiasMesConsultados[AcumCantidadDiasMesConsultados.Num() - 1];
    }
    else {
        CantidadDiasYearConsulta = 0;
    }
    acum = 0;
    AcumCantidadDiasYearConsultados.Empty();
    for (int i = 0; i < Consulta.FinalYear - Consulta.InitialYear + 1; i++) {
        AcumCantidadDiasYearConsultados.Add(acum);
        if (YearsFiltrados[i]) {//le aumento depues a acum por que el primer a�� tiene 0 dias acumulados anteriore, eso para el calulo de la formula
            acum += CantidadDiasYearConsulta;
        }
    }

    for (int i = 0; i < Consulta.StationConsultas[0].ConsultasPorVariable.Num(); i++) {
        //Consulta.StationConsultas[0].ConsultasPorVariable[i].XMax = (Consulta.FinalYear - Consulta.InitialYear + 1) * CantidadDiasYearConsulta;//deberia cambiar en funcion de los a��s filtrados, 
        Consulta.StationConsultas[0].ConsultasPorVariable[i].XMax = acum;//deberia cambiar en funcion de los a��s filtrados, 
    }
}
