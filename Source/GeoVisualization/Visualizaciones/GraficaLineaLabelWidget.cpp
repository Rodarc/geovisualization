// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaLineaLabelWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/PanelSlot.h"
#include "Components/PanelWidget.h"

UGraficaLineaLabelWidget::UGraficaLineaLabelWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    SetDesiredSizeInViewport(FVector2D(200.0f, 150.0f));

    CalcularCorners();
    CalcularPuntosDibujar();

    Margen.Bottom = 10;
    Margen.Top = 10;
    Margen.Right = 10;
    Margen.Left = 10;
//(SpecifiedColor=(R=0.000000,G=0.000000,B=0.000000,A=0.775000),ColorUseRule=UseColor_Specified)

    ColorFondoGrafica = FLinearColor(0.005f, 0.005f, 0.005f, 0.9);

    static ConstructorHelpers::FObjectFinder<USlateBrushAsset> FondoGraficaBrushFinder(TEXT("SlateBrushAsset'/Game/GeoVisualization/UMG/FondoGraficaBrush.FondoGraficaBrush'"));
    //deberia buscar una grafica
    if (FondoGraficaBrushFinder.Succeeded()) {
        BrushFondoGrafica = FondoGraficaBrushFinder.Object;
    }
}

void UGraficaLineaLabelWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);

    //UWidgetBlueprintLibrary::DrawBox(InContext, GetCachedGeometry().Position, GetCachedGeometry().GetLocalSize(), BrushFondoGrafica, ColorFondoGrafica);
    //UWidgetBlueprintLibrary::DrawLine(InContext, LUCorner, Origin, FLinearColor::White, true);
    //UWidgetBlueprintLibrary::DrawLine(InContext, Origin, RBCorner, FLinearColor::White, true);
    //UWidgetBlueprintLibrary::DrawLines(InContext, PuntosDibujar, FLinearColor::Blue, true);//aveces aparece un error visual

    //for(int k = 0; k < SeriesDibujar.Num(); k++){
    //    if (VisibilitySeries[k]) {
    //        for (int i = 0; i < SeriesDibujar[k].Num() - 1; i++) {
    //            if (Series[k][i + 1].X - Series[k][i].X <= 1.0f) {
    //                UWidgetBlueprintLibrary::DrawLine(InContext, SeriesDibujar[k][i], SeriesDibujar[k][i+1], ColorSeries[k], true);
    //            }
    //        }
    //    }
    //}
}

int32 UGraficaLineaLabelWidget::NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const {
    int32 res = Super::NativePaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);

    FPaintContext InContext(AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
    //UWidgetBlueprintLibrary::DrawBox(InContext, GetCachedGeometry().Position, GetCachedGeometry().GetLocalSize(), BrushFondoGrafica, ColorFondoGrafica);
    UWidgetBlueprintLibrary::DrawLine(InContext, LUCorner, Origin, FLinearColor::White, true);
    UWidgetBlueprintLibrary::DrawLine(InContext, Origin, RBCorner, FLinearColor::White, true);
    //UWidgetBlueprintLibrary::DrawLines(InContext, PuntosDibujar, FLinearColor::Blue, true);//aveces aparece un error visual

    for(int k = 0; k < SeriesDibujar.Num(); k++){
        if (VisibilitySeries[k]) {
            for (int i = 0; i < SeriesDibujar[k].Num() - 1; i++) {
                if (Series[k][i + 1].X - Series[k][i].X <= 1.0f) {
                    UWidgetBlueprintLibrary::DrawLine(InContext, SeriesDibujar[k][i], SeriesDibujar[k][i+1], ColorSeries[k], true);
                }
            }
        }
    }
    return res;
}

void UGraficaLineaLabelWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
    

    LUCorner = MyGeometry.Position + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
    RBCorner = MyGeometry.Position + MyGeometry.GetLocalSize() - FVector2D(Margen.Right, Margen.Top);
    //LUCorner = MyGeometry.GetAbsolutePosition() + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
    //RBCorner = MyGeometry.GetAbsolutePosition() + MyGeometry.GetLocalSize() - FVector2D(Margen.Right, Margen.Top);
    Origin = FVector2D(LUCorner.X, RBCorner.Y);


    CalcularPuntosDibujar();
    //debo calcular los puntos de dibujar y el corner solo cuando estos se deformen por alguna razon, por ejemplo al llamar a acgrandar, meintras tura la animacion, debo llamar a esto en el tick, quiza deba manejar un booleano para esto
    //el problema es cuando se giro la camara, si bien el widget no esta redimensionando pero se esta moviemiento, esntonces los puntos que se dibujan estan en otra posicion en la pantalla
    //creo que se debe de dibujar
}

void UGraficaLineaLabelWidget::CalcularCorners() {
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    if (CanvasSlot) {
        LUCorner = CanvasSlot->GetPosition() + FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
        RBCorner = CanvasSlot->GetPosition() + CanvasSlot->GetSize() - FVector2D(Margen.Right, Margen.Top);
        Origin = FVector2D(LUCorner.X, RBCorner.Y);
    }
}

void UGraficaLineaLabelWidget::CalcularPuntosDibujar() {
    FVector2D GraficaSize = RBCorner - LUCorner;
    for (int k = 0; k < Series.Num(); k++) {
        if (VisibilitySeries[k]) {
            SeriesDibujar[k].Empty();//deberia tener el miso tama�o que sus puntos en serio, esto evitaria mas calculos, optimizar esto
            for (int i = 0; i < Series[k].Num(); i++) {//este deberian ser los minimos correspondientes
                float X = GraficaSize.X * (Series[k][i].X - XMins[k]) / (XMaxs[k] - XMins[k]);
                float Y = (1 - (Series[k][i].Y - YMins[k]) / (YMaxs[k] - YMins[k])) * GraficaSize.Y;
                FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
                SeriesDibujar[k].Add(PuntoDibujo);
                //PuntosDibujar.Add(PuntoDibujo);
            }
        }
    }
}

void UGraficaLineaLabelWidget::VisualizarConsulta_Implementation() {
    CalcularCantidadDiasConsulta();
    ClearSeries();//visualizar implica crear nuevas series, por lo tanto debo eliminar las anteriores si es que habia
    for (int j = 0; j < Consulta.StationConsultas[0].ConsultasPorVariable.Num(); j++) {
        int IdSerie = AddSerie(ConvertirArrayDailyDataStructToFVector2D(Consulta.StationConsultas[0].ConsultasPorVariable[j].Datos));//idserie deberia ser igual que j
        SetIntervalXForSerie(IdSerie, Consulta.StationConsultas[0].ConsultasPorVariable[j].XMin, Consulta.StationConsultas[0].ConsultasPorVariable[j].XMax);
        SetIntervalYForSerie(IdSerie, Consulta.StationConsultas[0].ConsultasPorVariable[j].YMin, Consulta.StationConsultas[0].ConsultasPorVariable[j].YMax);
        SetColorSerie(IdSerie, GeneralColorSeries[j]);//necesito un array de colores
        CalcularPuntosDibujar();
    }
}
