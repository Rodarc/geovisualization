// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisualizacionNJWidget.h"
#include "NodoNJVisualizacion.h"
#include "AristaNJVisualizacion.h"
#include "Components/Image.h"
#include "Engine/Texture2D.h"
#include "VisualizacionVentanaInterface.h"
#include "VisualizacionNJVentanaWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UVisualizacionNJVentanaWidget : public UVisualizacionNJWidget, public IVisualizacionVentanaInterface
{
	GENERATED_BODY()
	
public:
    UVisualizacionNJVentanaWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D TraslacionOrigen;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float Escala;
	
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void CreateNodos() override;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void CreateAristas() override;

    virtual void ActualizarLayout() override;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UNodoNJVisualizacion> TypeNodo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UAristaNJVisualizacion> TypeArista;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UTexture2D * TextureLinea;

    TArray<UNodoNJVisualizacion *> NodosWidgets;

    TArray<UAristaNJVisualizacion *> AristasWidgets;

    TArray<UImage *> AristasImages;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float DeltaZoom;
	
    virtual void VisualizarConsulta_Implementation() override;

    virtual void ZoomIn_Implementation() override;

    virtual void ZoomOut_Implementation() override;

    virtual void Reestablecer_Implementation() override;

    virtual FText GetTitleWindowInformation_Implementation() override;
	
};
