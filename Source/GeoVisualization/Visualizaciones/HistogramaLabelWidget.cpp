// Fill out your copyright notice in the Description page of Project Settings.

#include "HistogramaLabelWidget.h"
#include "Kismet/GameplayStatics.h"

UHistogramaLabelWidget::UHistogramaLabelWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
}

void UHistogramaLabelWidget::NativeConstruct() {
    Super::NativeConstruct();
}

int UHistogramaLabelWidget::AddItem_Implementation() {
    UHistogramaItemWidget * Item = CreateWidget<UHistogramaItemWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeItem);
    if (Item) {
        Items.Add(Item);
        Item->AddToViewport(0);
        if (ItemsVB) {
            ItemsVB->AddChildToVerticalBox(Item);
        }
    }
    return Items.Num()-1;
}

int UHistogramaLabelWidget::AddItem(FString RangoText, FString PorcentajeText, FLinearColor Color) {
    UHistogramaItemWidget * Item = CreateWidget<UHistogramaItemWidget>(UGameplayStatics::GetGameInstance(GetWorld()), TypeItem);
    if (Item) {
        Items.Add(Item);
        Item->AddToViewport(0);
        ItemsVB->AddChildToVerticalBox(Item);
        Item->SetColor(Color);
        Item->SetRangoText(RangoText);
        Item->SetPorcentajeText(PorcentajeText);
    }
    return Items.Num()-1;
}

void UHistogramaLabelWidget::SetItem(int IdItem, FString RangoText, FString PorcentajeText, FLinearColor Color) {
    if (IdItem < Items.Num()) {
        Items[IdItem]->SetColor(Color);
        Items[IdItem]->SetRangoText(RangoText);
        Items[IdItem]->SetPorcentajeText(PorcentajeText);
    }
}

void UHistogramaLabelWidget::ClearItems() {
    for (int i = 0; i < Items.Num(); i++) {
        Items[i]->RemoveFromParent();
        Items[i]->Destruct();
    }
    Items.Empty();
}



