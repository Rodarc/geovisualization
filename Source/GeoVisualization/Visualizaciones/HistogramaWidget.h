// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisualizacionWidget.h"
#include "HistogramaItemWidget.h"
#include "HistogramaWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UHistogramaWidget : public UVisualizacionWidget
{
	GENERATED_BODY()
	
public:
    UHistogramaWidget(const FObjectInitializer & ObjectInitializer);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    int AddItem();
    virtual int AddItem_Implementation();//estoy usando esta forma debido a un error, ya que no se llenaba el ItemVB
	
    virtual int AddItem(FString RangoText, FString PorcentajeText, FLinearColor Color);

    virtual void SetItem(int IdItem, FString RangoText, FString PorcentajeText, FLinearColor Color);

    virtual void ClearItems();
	
	
};
