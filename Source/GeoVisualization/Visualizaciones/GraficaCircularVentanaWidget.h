// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraficaCircularWidget.h"
#include "PuntoGrafica.h"
#include "LineaGrafica.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Engine/Texture2D.h"
#include "VisualizacionVentanaInterface.h"
#include "GraficaCircularVentanaWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaCircularVentanaWidget : public UGraficaCircularWidget, public IVisualizacionVentanaInterface
{
	GENERATED_BODY()
public:
    UGraficaCircularVentanaWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D TraslacionOrigen;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float Escala;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float DeltaZoom;
	
    virtual void CalcularPuntosDibujar() override;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D SizePoints;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float SizeLines;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UPuntoGrafica> TypePunto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<ULineaGrafica> TypeLinea;

    TArray<TArray<UPuntoGrafica *>> SeriesPuntoDailyWidgets;

    TArray<TArray<ULineaGrafica *>> SeriesLineaDailyWidgets;

    TArray<TArray<TArray<TArray<UPuntoGrafica *>>>> SeriesPuntoDayWidgets;

    TArray<TArray<TArray<TArray<ULineaGrafica *>>>> SeriesLineaDayWidgets;

    TArray<TArray<TArray<UPuntoGrafica *>>> SeriesPuntoMonthWidgets;

    TArray<TArray<TArray<ULineaGrafica *>>> SeriesLineaMonthWidgets;

    TArray<TArray<UPuntoGrafica *>> SeriesPuntoYearWidgets;

    TArray<TArray<ULineaGrafica *>> SeriesLineaYearWidgets;

    TArray<UPuntoGrafica *> SeriesPuntoVarWidgets;


    TArray<TArray<TArray<TArray<UTextBlock*>>>> LabelsDay;//son solo dos labels para las series de dias, el primero y el ultimo

    TArray<TArray<TArray<UTextBlock *>>> LabelsMonths;

    TArray<TArray<UTextBlock *>> LabelsYears;

    TArray<UTextBlock *> LabelsVar;

    TArray<TArray<TArray<TArray<UTextBlock*>>>> LabelsRangosDay;//son solo dos labels para las series de dias, el primero y el ultimo

    TArray<TArray<TArray<UTextBlock *>>> LabelsRangosMonths;

    TArray<TArray<UTextBlock *>> LabelsRangosYears;

    TArray<TArray<UTextBlock *>> LabelsRangosVar;

	
    virtual void SetSerie(int IdSerie, TArray<FVector2D> Points) override;

    virtual void SetColorSerie(int IdSerie, FLinearColor ColorSerie) override;

    //UFUNCTION(BlueprintCallable, Category = "Grafica")
    //virtual int AddSerie(TArray<FVector2D> Points) override;//retorna el id de la serie en la grafica

    virtual int AddSerie(TArray<FDailyDataStruct> Datos) override;//retorna el id de la serie en la grafica

    virtual void HideSerie(int IdSerie) override;

    virtual void ShowSerie(int IdSerie) override;

    virtual void ClearSeries() override;
	
    virtual void VisualizarConsulta_Implementation() override;

    void ActualizarGrafica() override;//deberia ser actualzar graficas
	
    virtual void ZoomIn_Implementation() override;

    virtual void ZoomOut_Implementation() override;

    virtual void Reestablecer_Implementation() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorLineasReferencias;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UTexture2D * TextureLinea;

    TArray<FVector2D> CirculoInteriorPuntos;//este lo usare por si quiero crear una especie de grilla de fondo

    TArray<UImage *> CirculoInteriorImages;//este lo usare por si quiero crear una especie de grilla de fondo

    //el utlimo array es el array de elementos que conforman la seccion circular, los araray anteriores, se corresponden con los indices por ejemplo de promedio, o deseries dibujar

    TArray<TArray<FVector2D>> PuntosInteriorCentro;//donde van los promedios generales de las variables de las variables

    TArray<TArray<UImage *>> ImagesInteriorCentro;

    TArray<TArray<FVector2D>> PuntosExteriorCentro;

    TArray<TArray<UImage *>> ImagesExteriorCentro;

    TArray<TArray<FVector2D>> PuntosVerticalesCentro;

    TArray<TArray<UImage *>> ImagesVerticalesCentro;

	//Para las secciones que engloban los promedios de a�os
    TArray<TArray<FVector2D>> PuntosInteriorVariables;

    TArray<TArray<UImage *>> ImagesInteriorVariables;

    TArray<TArray<FVector2D>> PuntosExteriorVariables;

    TArray<TArray<UImage *>> ImagesExteriorVariables;

    TArray<TArray<FVector2D>> PuntosVerticalesVariables;

    TArray<TArray<UImage *>> ImagesVerticalesVariables;

	//para las secciones que engloban los promedios de los meses de cada a�o
    TArray<TArray<TArray<FVector2D>>> PuntosInteriorYears;

    TArray<TArray<TArray<UImage *>>> ImagesInteriorYears;

    TArray<TArray<TArray<FVector2D>>> PuntosExteriorYears;

    TArray<TArray<TArray<UImage *>>> ImagesExteriorYears;

    TArray<TArray<TArray<FVector2D>>> PuntosVerticalesYears;

    TArray<TArray<TArray<UImage *>>> ImagesVerticalesYears;

	//para las secciones que engloban la series de cada mes, los datos diarios
    TArray<TArray<TArray<TArray<FVector2D>>>> PuntosInteriorMonths;

    TArray<TArray<TArray<TArray<UImage *>>>> ImagesInteriorMonths;

    TArray<TArray<TArray<TArray<FVector2D>>>> PuntosExteriorMonths;

    TArray<TArray<TArray<TArray<UImage *>>>> ImagesExteriorMonths;

    TArray<TArray<TArray<TArray<FVector2D>>>> PuntosVerticalesMonths;

    TArray<TArray<TArray<TArray<UImage *>>>> ImagesVerticalesMonths;

    int Precision;

    float AnchoLineasReferencias;

    float EspacioEntreNiveles;

    void CreateCirculoInterior();

    void ActualizarCirculoInterior();

    void DeleteCirculoInterior();

    void CreateSeccionCirculo(float WInicio, float WTamano, float Radio, float RadioTam, TArray<FVector2D> & Puntos, TArray<UImage *> & Images, TArray<FVector2D> & PuntosVerticales, TArray<UImage *> & ImagesVerticales, TArray<FVector2D> & PuntosEx, TArray<UImage *> & ImagesEx);

    void UpdateSeccionCirculo(float Radio, float RadioTam, TArray<FVector2D> & Puntos, TArray<UImage *> & Images, TArray<FVector2D> & PuntosVerticales, TArray<UImage *> & ImagesVerticales, TArray<FVector2D> & PuntosEx, TArray<UImage *> & ImagesEx);

    void DeleteSeccionCirculo(TArray<FVector2D> & Puntos, TArray<UImage *> & Images, TArray<FVector2D> & PuntosVerticales, TArray<UImage *> & ImagesVerticales, TArray<FVector2D> & PuntosEx, TArray<UImage *> & ImagesEx);

    void HideSeccionCirculo(TArray<UImage *> & Images, TArray<UImage *> & ImagesVerticales, TArray<UImage *> & ImagesEx);

    void ShowSeccionCirculo(TArray<UImage *> & Images, TArray<UImage *> & ImagesVerticales, TArray<UImage *> & ImagesEx);

    virtual void ExplotarPunto(int IdVar, int IdYear, int IdMonth) override;

    virtual void ContraerPunto(int IdVar, int IdYear, int IdMonth) override;

    void CreateTextos();

    void ActualizarTextos();

    void DeleteTextos();

	UTextBlock * CreateTexto(float WInicio, float WTamano, float Radio, int Size);

	void UpdateTexto(float WInicio, float Radio, int Size, UTextBlock * Texto);

    void DeleteTexto(UTextBlock * Texto);

    void HideTexto(UTextBlock * Texto);

	void ShowTexto(UTextBlock * Texto);

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FString> Variables;// para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FString> VariablesAbrev;// para mostrar en la interfaz

    void CreateRangos();

    void ActualizarRangos();

    void DeleteRangos();
}; 
