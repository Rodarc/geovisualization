// Fill out your copyright notice in the Description page of Project Settings.

#include "PuntoGrafica.h"
#include "VisualizacionWidget.h"
#include "GraficaCircularWidget.h"
#include "Kismet/KismetMathLibrary.h"

UPuntoGrafica::UPuntoGrafica(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
}


/*void UPuntoGrafica::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);
}*/

//esta funcion no necesitare
/*void UPuntoGrafica::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
}*/

FDailyDataStruct UPuntoGrafica::GetDataStruct() {
    if (Visualizacion) {
        return Visualizacion->Consulta.StationConsultas[0].ConsultasPorVariable[IdSerie].Datos[IdData];
    }
    return FDailyDataStruct();
}
void UPuntoGrafica::SetColor(FLinearColor NewColor) {
    float H;
    float S;
    float V;
    float A;
    UKismetMathLibrary::RGBToHSV(NewColor, H, S, V, A);
    ColorFondoPunto = NewColor;
    ColorLineaPunto = UKismetMathLibrary::HSVToRGB(H, S, V - 0.7f);// o podria ser  siempre
}

float UPuntoGrafica::GetPromedioVar() {
    if (Visualizacion) {
		UGraficaCircularWidget * Grafica = Cast<UGraficaCircularWidget>(Visualizacion);
		if (Grafica) {
            return Grafica->PromediosVar[IdSerie];
		}
		return 0.0f;
    }
	return 0.0f;
}

float UPuntoGrafica::GetPromedioYear() {
    if (Visualizacion) {
		UGraficaCircularWidget * Grafica = Cast<UGraficaCircularWidget>(Visualizacion);
		if (Grafica) {
			return Grafica->PromediosYears[IdSerie][IdYear];
		}
		return 0.0f;
    }
	return 0.0f;
}

float UPuntoGrafica::GetPromedioMonth() {
    if (Visualizacion) {
		UGraficaCircularWidget * Grafica = Cast<UGraficaCircularWidget>(Visualizacion);
		if (Grafica) {
			return Grafica->PromediosMonths[IdSerie][IdYear][IdMonth];
		}
		return 0.0f;
    }
	return 0.0f;
}

float UPuntoGrafica::GetValueDay() {
    if (Visualizacion) {
		UGraficaCircularWidget * Grafica = Cast<UGraficaCircularWidget>(Visualizacion);
		if (Grafica) {
			return Grafica->SeriesDividida[IdSerie][IdYear][IdMonth][IdDay].Y;
		}
		return 0.0f;
    }
	return 0.0f;
}
