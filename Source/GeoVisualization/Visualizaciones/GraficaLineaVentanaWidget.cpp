// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaLineaVentanaWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/PanelWidget.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/ScrollBoxSlot.h"
#include "Components/SizeBox.h"
#include "Components/CanvasPanel.h"
#include "Engine/GameEngine.h"


UGraficaLineaVentanaWidget::UGraficaLineaVentanaWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    SetDesiredSizeInViewport(FVector2D(200.0f, 150.0f));

    CalcularCorners();
    CalcularPuntosDibujar();

    Margen.Bottom = 70;
    Margen.Top = 30;
    Margen.Right = 30;
    Margen.Left = 60;
//(SpecifiedColor=(R=0.000000,G=0.000000,B=0.000000,A=0.775000),ColorUseRule=UseColor_Specified)

    ColorFondoGrafica = FLinearColor(0.005f, 0.005f, 0.005f, 0.9);

    static ConstructorHelpers::FObjectFinder<USlateBrushAsset> FondoGraficaBrushFinder(TEXT("SlateBrushAsset'/Game/GeoVisualization/UMG/FondoGraficaBrush.FondoGraficaBrush'"));
    //deberia buscar una grafica
    if (FondoGraficaBrushFinder.Succeeded()) {
        BrushFondoGrafica = FondoGraficaBrushFinder.Object;
    }

    static ConstructorHelpers::FClassFinder<UPuntoGrafica> PuntoGraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/PuntoGraficaBP.PuntoGraficaBP_C'"));
    if (PuntoGraficaWidgetClass.Succeeded()) {
        TypePunto = PuntoGraficaWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<ULineaGrafica> LineaGraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/LineaGraficaBP.LineaGraficaBP_C'"));
    if (LineaGraficaWidgetClass.Succeeded()) {
        TypeLinea = LineaGraficaWidgetClass.Class;
    }

    SizePoints = FVector2D(10.0f, 10.0f);
    SizeLines = 5.0f;
}

void UGraficaLineaVentanaWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);
}

int32 UGraficaLineaVentanaWidget::NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const {
    return Super::NativePaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
}


void UGraficaLineaVentanaWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
    
    if (bTransformando) {
        ActualizarGrafica();
    }
}

void UGraficaLineaVentanaWidget::CalcularCorners() {
    UPanelWidget * Padre = GetParent();//se supone que un sizebox
    UCanvasPanelSlot * CanvasSlot = nullptr;
    if (Padre) {
        CanvasSlot = Cast<UCanvasPanelSlot>(Padre->Slot);
        /*if (GEngine)//no hacer esta verificaci�n provocaba error al iniciar el editor
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Intentando calcular corners."));*/
    }
    //UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    if (CanvasSlot) {
        /*if (GEngine)//no hacer esta verificaci�n provocaba error al iniciar el editor
            GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("Calculando corners."));*/

        LUCorner = FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
        //RBCorner = CanvasSlot->GetSize() - FVector2D(Margen.Right, Margen.Top);
        RBCorner = Padre->GetCachedGeometry().GetLocalSize() - FVector2D(Margen.Right, Margen.Bottom);
        //no puedo acceder al size, ya que la forma que he puesto hace que quese expanda, no tien un ancho, por eso recurro al gemotry
        //sin mebargo la crearse la ventana no hay un geometry por eso la grafica inical sal mal,
        //en el siguiente tick ya tedra un geomtry ahi deberia llamarse a visualizar consulta, ver como hacer esto
        Origin = FVector2D(LUCorner.X, RBCorner.Y);
    }
}

void UGraficaLineaVentanaWidget::CalcularPuntosDibujar() {
    FVector2D GraficaSize = RBCorner - LUCorner;
    for (int k = 0; k < Series.Num(); k++) {
        if (VisibilitySeries[k]) {
            SeriesDibujar[k].Empty();//deberia tener el miso tama�o que sus puntos en serio, esto evitaria mas calculos, optimizar esto
            for (int i = 0; i < Series[k].Num(); i++) {//este deberian ser los minimos correspondientes
                float X = GraficaSize.X * (Series[k][i].X - XMins[k]) / (XMaxs[k] - XMins[k]);
                float Y = (1 - (Series[k][i].Y - YMins[k]) / (YMaxs[k] - YMins[k])) * GraficaSize.Y;
                FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
                SeriesDibujar[k].Add(PuntoDibujo);
                //PuntosDibujar.Add(PuntoDibujo);
            }
        }
    }
}

void UGraficaLineaVentanaWidget::SetSerie(int IdSerie, TArray<FVector2D> Points) {//no necesito sobreescribir esta
    Super::SetSerie(IdSerie, Points);
}

void UGraficaLineaVentanaWidget::SetColorSerie(int IdSerie, FLinearColor ColorSerie) {
    Super::SetColorSerie(IdSerie, ColorSerie);
    for (int i = 0; i < SeriesPuntoWidgets[IdSerie].Num(); i++) {
        //SeriesPuntoWidgets[IdSerie][i]->ColorFondoPunto = ColorSerie;
        //SeriesPuntoWidgets[IdSerie][i]->ColorLineaPunto = ColorSerie;
        SeriesPuntoWidgets[IdSerie][i]->SetColor(ColorSerie);
    }
    for (int i = 0; i < SeriesLineaWidgets[IdSerie].Num(); i++) {
        SeriesLineaWidgets[IdSerie][i]->ColorLinea = ColorSerie;
    }
    /*for (int i = 0; i < SeriesLineasImage[IdSerie].Num(); i++) {
        SeriesLineasImage[IdSerie][i]->SetColorAndOpacity(ColorSerie);
    }*/
}

int UGraficaLineaVentanaWidget::AddSerie(TArray<FVector2D> Points) {
    int IdSerie = Super::AddSerie(Points);

    TArray<UPuntoGrafica *> PuntosGraficaSerie;
    for (int i = 0; i < Points.Num(); i++) {
        UPuntoGrafica *  PuntoGrafica = CreateWidget<UPuntoGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypePunto);
        PuntoGrafica->IdPunto = i;//no se coincide con los id de las consultas, aqui hay un problema
        PuntoGrafica->IdSerie = IdSerie;
        PuntoGrafica->Visualizacion = this;
        PuntosGraficaSerie.Add(PuntoGrafica);
        PuntoGrafica->AddToViewport(0);
        //grafica->SetAlignmentInViewport(FVector2D(0.5, 0.5));//este no es necesario
        //grafica->SetAnchorsInViewport();
        //grafica->SetPositionInViewport(FVector2D(0.0f, 0.0f));

        UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
        UPanelSlot * SlotPuntoGrafica = RootWidget->AddChild(PuntoGrafica);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotPuntoGrafica);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        CanvasSlot->SetZOrder(0);

        USizeBox * SizeBoxWidget = Cast<USizeBox>(PuntoGrafica->GetRootWidget());
        if (SizeBoxWidget) {
            CanvasSlot->SetSize(SizePoints);// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
        }

        CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
        CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
        //CanvasSlot->SetSize(FVector2D(600.0f, 400.0f));
        //pasarle la informacion pertinente

        //PuntoGrafica->SetVisibility(ESlateVisibility::Hidden);//deben iniciar ocultos, visible segun lo necesite

    }
    SeriesPuntoWidgets.Add(PuntosGraficaSerie);

    //TArray<UImage *> LineasImagenSerie;
    TArray<ULineaGrafica *> LineasGraficaSerie;
    for (int i = 0; i < Points.Num() - 1; i++) {
        if (Points[i + 1].X - Points[i].X <= 1.0f) {
            ULineaGrafica *  Linea = CreateWidget<ULineaGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypeLinea);
            //Linea->IdArista = count;
            Linea->PuntoInicio = PuntosGraficaSerie[i];
            Linea->PuntoFinal = PuntosGraficaSerie[i + 1];
            LineasGraficaSerie.Add(Linea);
            Linea->AddToViewport(0);
            Linea->Ancho = SizeLines;

            UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
            UPanelSlot * SlotLinea = RootWidget->AddChild(Linea);
            UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
            CanvasSlot->SetZOrder(-1);

            USizeBox * SizeBoxWidget = Cast<USizeBox>(Linea->GetRootWidget());
            if (SizeBoxWidget) {
                CanvasSlot->SetSize(FVector2D(20.0f, SizeLines));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
            }

            CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
            CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
        }

    }

    //SeriesLineasImage.Add(LineasImagenSerie);
    SeriesLineaWidgets.Add(LineasGraficaSerie);

    return IdSerie;
}

//las sigueintes funciones no se han terminado de implementar creo

void UGraficaLineaVentanaWidget::HideSerie(int IdSerie) {
    Super::HideSerie(IdSerie);
    if (IdSerie >= 0 && IdSerie < SeriesPuntoWidgets.Num()) {
        for (int i = 0; i < SeriesPuntoWidgets[IdSerie].Num(); i++) {
            SeriesPuntoWidgets[IdSerie][i]->SetVisibility(ESlateVisibility::Hidden);
        }
        for (int i = 0; i < SeriesLineaWidgets[IdSerie].Num(); i++) {
            SeriesLineaWidgets[IdSerie][i]->SetVisibility(ESlateVisibility::Hidden);
        }
        /*for (int i = 0; i < SeriesLineasImage[IdSerie].Num(); i++) {
            SeriesLineasImage[IdSerie][i]->SetVisibility(ESlateVisibility::Hidden);
        }*/
    }
}

void UGraficaLineaVentanaWidget::ShowSerie(int IdSerie) {
    Super::ShowSerie(IdSerie);
    if (IdSerie >= 0 && IdSerie < SeriesPuntoWidgets.Num()) {
        for (int i = 0; i < SeriesPuntoWidgets[IdSerie].Num(); i++) {
            SeriesPuntoWidgets[IdSerie][i]->SetVisibility(ESlateVisibility::Visible);
        }
        for (int i = 0; i < SeriesLineaWidgets[IdSerie].Num(); i++) {
            SeriesLineaWidgets[IdSerie][i]->SetVisibility(ESlateVisibility::Visible);
        }
        /*for (int i = 0; i < SeriesLineasImage[IdSerie].Num(); i++) {
            SeriesLineasImage[IdSerie][i]->SetVisibility(ESlateVisibility::Visible);
        }*/
    }
}

void UGraficaLineaVentanaWidget::ClearSeries() {
    Super::ClearSeries();
    for (int i = 0; i < SeriesPuntoWidgets.Num(); i++) {
        for (int j = 0; j < SeriesPuntoWidgets[i].Num(); j++) {
            SeriesPuntoWidgets[i][j]->RemoveFromParent();
            SeriesPuntoWidgets[i][j]->Destruct();
        }
        SeriesPuntoWidgets[i].Empty();
    }
    SeriesPuntoWidgets.Empty();

    for (int i = 0; i < SeriesLineaWidgets.Num(); i++) {
        for (int j = 0; j < SeriesLineaWidgets[i].Num(); j++) {
            SeriesLineaWidgets[i][j]->RemoveFromParent();
            SeriesLineaWidgets[i][j]->Destruct();
        }
        SeriesLineaWidgets[i].Empty();
    }
    SeriesLineaWidgets.Empty();

    /*for (int i = 0; i < SeriesLineasImage.Num(); i++) {
        for (int j = 0; j < SeriesLineasImage[i].Num(); j++) {
            SeriesLineasImage[i][j]->RemoveFromParent();
        }
        SeriesLineasImage[i].Empty();
    }
    SeriesLineasImage.Empty();*/
}

void UGraficaLineaVentanaWidget::VisualizarConsulta_Implementation() {
    CalcularCantidadDiasConsulta();
    ClearSeries();//visualizar implica crear nuevas series, por lo tanto debo eliminar las anteriores si es que habia
    for (int j = 0; j < Consulta.StationConsultas[0].ConsultasPorVariable.Num(); j++) {
        int IdSerie = AddSerie(ConvertirArrayDailyDataStructToFVector2D(Consulta.StationConsultas[0].ConsultasPorVariable[j].Datos));//idserie deberia ser igual que j
        SetIntervalXForSerie(IdSerie, Consulta.StationConsultas[0].ConsultasPorVariable[j].XMin, Consulta.StationConsultas[0].ConsultasPorVariable[j].XMax);
        SetIntervalYForSerie(IdSerie, Consulta.StationConsultas[0].ConsultasPorVariable[j].YMin, Consulta.StationConsultas[0].ConsultasPorVariable[j].YMax);
        SetColorSerie(IdSerie, GeneralColorSeries[j]);//necesito un array de colores
        //CalcularPuntosDibujar();
    }
    ClearAxis();
    CreateAxis();//la creo despues por que tambien debo crear los textos que van en funcion de las graficas y consultas
    ClearLeyendaAxis();
    CreateLeyendaAxis();
    ActualizarGrafica();
}

void UGraficaLineaVentanaWidget::ActualizarGrafica() {

    /*UCanvasPanelSlot * GraficaCanvasSlot = Cast<UCanvasPanelSlot>(Slot);
    if (GraficaCanvasSlot) {
        RBCorner = GraficaCanvasSlot->GetSize() - FVector2D(Margen.Right, Margen.Top);
    }

    LUCorner = FVector2D(Margen.Left, Margen.Top);// com la funcion es const, este calculo no lo puedo hacer aqui
    //RBCorner = MyGeometry.GetLocalSize() - FVector2D(Margen.Right, Margen.Top);

    //necesito el size del canvs
    Origin = FVector2D(LUCorner.X, RBCorner.Y);
    */

    CalcularCorners();
    CalcularPuntosDibujar();
    //debo calcular los puntos de dibujar y el corner solo cuando estos se deformen por alguna razon, por ejemplo al llamar a acgrandar, meintras tura la animacion, debo llamar a esto en el tick, quiza deba manejar un booleano para esto
    //el problema es cuando se giro la camara, si bien el widget no esta redimensionando pero se esta moviemiento, esntonces los puntos que se dibujan estan en otra posicion en la pantalla
    //creo que se debe de dibujar

    for (int i = 0; i < SeriesDibujar.Num(); i++) {
        for (int j = 0; j < SeriesDibujar[i].Num(); j++) {
            UCanvasPanelSlot * PuntoCanvasSlot = Cast<UCanvasPanelSlot>(SeriesPuntoWidgets[i][j]->Slot);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot

            //CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
            PuntoCanvasSlot->SetPosition(SeriesDibujar[i][j] - SizePoints / 2);// com la funcion es const, este calculo no lo puedo hacer aqui
        }
        for (int j = 0; j < SeriesLineaWidgets[i].Num(); j++) {
            SeriesLineaWidgets[i][j]->Actualizar();
        }
    }

    //actualizando ejes
    if(LineaEjeY && LineaEjeX){
        UCanvasPanelSlot * LineaCanvasSlot = Cast<UCanvasPanelSlot>(LineaEjeY->Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        //LineaCanvasSlot->
        LineaEjeY->SetRenderTransformAngle(0.0f);
        FVector2D Direction = LUCorner - Origin;
        LineaCanvasSlot->SetPosition(Origin + Direction/2 - FVector2D(Direction.Size() , SizeLines)/2);// necesito un size linea
        LineaCanvasSlot->SetSize(FVector2D(Direction.Size(), SizeLines));
        Direction = Direction.GetSafeNormal();
        
        float angle = FMath::RadiansToDegrees(FMath::Acos(FVector2D::DotProduct(FVector2D(1, 0), Direction)));
        //float angle = FMath::RadiansToDegrees(acos(FVector2D::DotProduct(FVector2D(0, 1), Direction)));
        float sing = FVector2D::CrossProduct(FVector2D(0, 1), Direction);//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
        if (LUCorner.Y < Origin.Y) {
            angle = -angle;
        }
        LineaEjeY->SetRenderTransformAngle(angle);

        LineaCanvasSlot = Cast<UCanvasPanelSlot>(LineaEjeX->Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        //LineaCanvasSlot->
        LineaEjeX->SetRenderTransformAngle(0.0f);
        Direction = RBCorner - Origin;
        LineaCanvasSlot->SetPosition(Origin + Direction/2 - FVector2D(Direction.Size() , SizeLines)/2);// necesito un size linea
        LineaCanvasSlot->SetSize(FVector2D(Direction.Size(), SizeLines));
        Direction = Direction.GetSafeNormal();
        
        angle = FMath::RadiansToDegrees(FMath::Acos(FVector2D::DotProduct(FVector2D(1, 0), Direction)));
        //float angle = FMath::RadiansToDegrees(acos(FVector2D::DotProduct(FVector2D(0, 1), Direction)));
        sing = FVector2D::CrossProduct(FVector2D(0, 1), Direction);//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
        if (RBCorner.Y < Origin.Y) {
            angle = -angle;
        }
        LineaEjeX->SetRenderTransformAngle(angle);
    }

    int k = 0;
    int yf = 0;
    for (int i = 0; i < YearsFiltrados.Num(); i++) {
        if (YearsFiltrados[i]) {
            for (int j = 0; j < Consulta.MesesConsultados.Num(); j++) {
                if (MesesFiltrados[Consulta.MesesConsultados[j]]) {
                    float valX = yf*CantidadDiasYearConsulta;
                    if (Consulta.MesesConsultados[j] > 0) {
                        valX += AcumCantidadDiasMesConsultados[Consulta.MesesConsultados[j] - 1];
                    }
                    //valX += AcumCantidadDiasMesConsultados[Consulta.MesesConsultados[j]];
                    valX /= Consulta.StationConsultas[0].ConsultasPorVariable[0].XMax;
                    valX = Origin.X + (RBCorner.X - Origin.X)*valX;
                    FVector2D PosicionLabel (valX, Origin.Y + 0.0f);//deberia cambiar en funcion de los a��s filtrados, 

                    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(TextosEjeX[k]->Slot);
                    //CanvasSlot->SetPosition(FVector2D(Origin.X + (RBCorner.X - Origin.X)*(i/TextosEjeX.Num()), Origin.Y));
                    CanvasSlot->SetPosition(PosicionLabel);
                    k++;
                }
            }
            yf++;
        }
    }

    /*for (int i = 0; i < TextosEjeX.Num(); i++) {
        float valX= i*CantidadDiasYearConsulta/Consulta.StationConsultas[0].ConsultasPorVariable[0].XMax;
        valX = Origin.X + (RBCorner.X - Origin.X)*valX;
        FVector2D PosicionLabel (valX, Origin.Y + 10.0f);//deberia cambiar en funcion de los a��s filtrados, 

        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(TextosEjeX[i]->Slot);
        //CanvasSlot->SetPosition(FVector2D(Origin.X + (RBCorner.X - Origin.X)*(i/TextosEjeX.Num()), Origin.Y));
        CanvasSlot->SetPosition(PosicionLabel);
        //esto si quisera manteer la equidistancia de los labels, pero si me intenresa ponerlos donde corresponda, digamos los comienzo de cada a��, ya no aplica
        //la equidistancia si funcionaria en el eje vertical
    }*/
    for (int i = 0; i < TextosEjeY.Num(); i++) {
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(TextosEjeY[i]->Slot);
        CanvasSlot->SetPosition(FVector2D(Origin.X - 5.0f, Origin.Y - ((Origin.Y - LUCorner.Y)*i/(TextosEjeY.Num()-1))));
        //esto si quisera manteer la equidistancia de los labels, pero si me intenresa ponerlos donde corresponda, digamos los comienzo de cada a��, ya no aplica
        //la equidistancia si funcionaria en el eje vertical
    }
    /*if (TextosEjeX.Num() >= 2) {//los textos deberian tene una posicion de 0 a 1 asignada, en funcion de la escala que se esta visualizando
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(TextosEjeX[0]->Slot);
        CanvasSlot->SetPosition(Origin);
        FNumberFormattingOptions Formato;
        Formato.UseGrouping = false;
        TextosEjeX[0]->SetText(FText::AsNumber(Consulta.InitialYear, &Formato));
        CanvasSlot = Cast<UCanvasPanelSlot>(TextosEjeX[1]->Slot);
        CanvasSlot->SetPosition(RBCorner);
        TextosEjeX[1]->SetText(FText::AsNumber(Consulta.FinalYear, &Formato));//esta asignacion de texto deberia estar en otro ladio, donde actualice las grarias al filtrar
        //el llenado de datos debe estar en una funcion aparte, y es en funcion del tama�o
        //esto implica creacion y eliminacion de textos si es que el espacio es muy angosto
    }*/
}

void UGraficaLineaVentanaWidget::CreateAxis() {
    LineaEjeX = NewObject<UImage>(UGameplayStatics::GetGameInstance(GetWorld()), UImage::StaticClass());
    //LineasImagenSerie.Add(LineaImagen);
    LineaEjeX->SetBrushFromTexture(TextureLinea);
    UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
    UPanelSlot * SlotLinea = RootWidget->AddChild(LineaEjeX);
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
    //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
    CanvasSlot->SetZOrder(-2);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetPosition(FVector2D(10.0f, 10.0f));
    CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless

    LineaEjeY = NewObject<UImage>(UGameplayStatics::GetGameInstance(GetWorld()), UImage::StaticClass());
    //LineasImagenSerie.Add(LineaImagen);
    LineaEjeY->SetBrushFromTexture(TextureLinea);
    RootWidget = Cast<UPanelWidget>(GetRootWidget());
    SlotLinea = RootWidget->AddChild(LineaEjeY);
    CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
    //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
    CanvasSlot->SetZOrder(-2);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetPosition(FVector2D(10.0f, 20.0f));
    CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless
}

void UGraficaLineaVentanaWidget::ClearAxis() {//tambien limpia los textos
    if (LineaEjeX && LineaEjeY) {
        LineaEjeX->RemoveFromParent();
        LineaEjeY->RemoveFromParent();
    }
}

void UGraficaLineaVentanaWidget::CreateLeyendaAxis() {//necesito leer los minimos y maximos de las sereies ode la consulta
    for (int i = 0; i < YearsFiltrados.Num(); i++) {
        if (YearsFiltrados[i]) {
            for (int j = 0; j < Consulta.MesesConsultados.Num(); j++) {
                if (MesesFiltrados[Consulta.MesesConsultados[j]]) {
                    UTextBlock * Texto = NewObject<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
                    TextosEjeX.Add(Texto);
                    FSlateFontInfo Fuente = Texto->Font;
                    Fuente.Size = 10;
                    Fuente.TypefaceFontName = FName("Regular");
                    Texto->SetFont(Fuente);
                    FString year = FString::FromInt(Consulta.InitialYear + i);
                    FString Etiqueta = AbrevMonths[Consulta.MesesConsultados[j]] + "-" + year;
                    Texto->SetText(FText::FromString(Etiqueta));
                    Texto->SetRenderTransformPivot(FVector2D(0.0f, 0.5f));
                    Texto->SetRenderTransformAngle(90.0f);
                    Texto->SetJustification(ETextJustify::Left);
                    //LineaEjeX->SetBrushFromTexture(TextureLinea);
                    UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
                    UPanelSlot * SlotTexto = RootWidget->AddChild(Texto);
                    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotTexto);
                    //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
                    CanvasSlot->SetZOrder(-1);
                    CanvasSlot->SetAutoSize(true);
                    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
                    CanvasSlot->SetAlignment(FVector2D(0.0f, 0.0f));
                    CanvasSlot->SetPosition(Origin + FVector2D(0.0f, 5.0f));
                    //CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless
                    //FNumberFormattingOptions Formato;
                    //Formato.UseGrouping = false;
                    //Texto->SetText(FText::FromString("Inicio"));
                }
            }
        }
    }
    for (int i = 0; i < 4; i++) {
            UTextBlock * Texto = NewObject<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
            TextosEjeY.Add(Texto);
            //LineaEjeX->SetBrushFromTexture(TextureLinea);
            UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
            UPanelSlot * SlotTexto = RootWidget->AddChild(Texto);
            UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotTexto);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
            CanvasSlot->SetZOrder(-1);
            CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
            CanvasSlot->SetAlignment(FVector2D(1.0f, 0.5f));
            CanvasSlot->SetPosition(Origin + FVector2D(0.0f, 5.0f));
            CanvasSlot->SetAutoSize(true);
            //CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless
            FNumberFormattingOptions Formato;
            Formato.UseGrouping = false;
            float val = (Consulta.StationConsultas[0].ConsultasPorVariable[0].YMax - Consulta.StationConsultas[0].ConsultasPorVariable[0].YMin)*i/4;
            Texto->SetText(FText::AsNumber(val + Consulta.StationConsultas[0].ConsultasPorVariable[0].YMin, &Formato));
            //Texto->SetText(FText::FromString("Inicio"));
            FSlateFontInfo Fuente = Texto->Font;
            Fuente.Size = 10;
            Fuente.TypefaceFontName = FName("Regular");
            Texto->SetFont(Fuente);
            Texto->SetJustification(ETextJustify::Right);

    }
    /*UTextBlock * Texto = NewObject<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
    TextosEjeX.Add(Texto);
    //LineaEjeX->SetBrushFromTexture(TextureLinea);
    UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
    UPanelSlot * SlotTexto = RootWidget->AddChild(Texto);
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotTexto);
    //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
    CanvasSlot->SetZOrder(-1);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetAlignment(FVector2D(0.5f, 0.0f));
    CanvasSlot->SetPosition(Origin + FVector2D(0.0f, 5.0f));
    CanvasSlot->SetAutoSize(true);
    //CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless
    Texto->SetText(FText::FromString("Inicio"));
    FSlateFontInfo Fuente = Texto->Font;
    Fuente.Size = 10;
    Fuente.TypefaceFontName = FName("Regular");
    Texto->SetFont(Fuente);

    Texto = NewObject<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
    TextosEjeX.Add(Texto);
    //LineaEjeX->SetBrushFromTexture(TextureLinea);
    RootWidget = Cast<UPanelWidget>(GetRootWidget());
    SlotTexto = RootWidget->AddChild(Texto);
    CanvasSlot = Cast<UCanvasPanelSlot>(SlotTexto);
    //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
    CanvasSlot->SetZOrder(-1);
    CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
    CanvasSlot->SetAlignment(FVector2D(0.5f, 0.0f));
    CanvasSlot->SetPosition(RBCorner + FVector2D(0.0f, 5.0f));
    CanvasSlot->SetAutoSize(true);
    //CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless
    Texto->SetText(FText::FromString("Fin"));
    Fuente = Texto->Font;
    Fuente.Size = 10;
    Fuente.TypefaceFontName = FName("Regular");
    Texto->SetFont(Fuente);
    //Texto->Font.Size = 10;
    //Texto->Font.TypefaceFontName = FName("Regular");*/
}

void UGraficaLineaVentanaWidget::ClearLeyendaAxis() {
    for (int i = 0; i < TextosEjeX.Num(); i++) {
        TextosEjeX[i]->RemoveFromParent();
    }
    TextosEjeX.Empty();
    for (int i = 0; i < TextosEjeY.Num(); i++) {
        TextosEjeY[i]->RemoveFromParent();
    }
    TextosEjeY.Empty();
}


void UGraficaLineaVentanaWidget::ZoomIn_Implementation() {
}

void UGraficaLineaVentanaWidget::ZoomOut_Implementation() {
}

void UGraficaLineaVentanaWidget::Reestablecer_Implementation() {
    VisualizarConsulta();
}


