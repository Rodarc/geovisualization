// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "VisualizacionVentanaInterface.generated.h"

// This class does not need to be modified.
//UINTERFACE(MinimalAPI)
UINTERFACE(Blueprintable)
class UVisualizacionVentanaInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GEOVISUALIZATION_API IVisualizacionVentanaInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ZoomIn();
    virtual void ZoomIn_Implementation();

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void ZoomOut();
    virtual void ZoomOut_Implementation();
	
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Grafica")
    void Reestablecer();//centrar en todo
    virtual void Reestablecer_Implementation();
};
