// Fill out your copyright notice in the Description page of Project Settings.

#include "HistogramaItemWidget.h"

UHistogramaItemWidget::UHistogramaItemWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
}

void UHistogramaItemWidget::SetColor(FLinearColor NewColor) {
    ItemColor = NewColor;
}

void UHistogramaItemWidget::SetRangoText(FString NewRangeText) {
    RangoText = NewRangeText;
}

void UHistogramaItemWidget::SetPorcentajeText(FString NewPorcentajeText) {
    PorcentajeText = NewPorcentajeText;
}
