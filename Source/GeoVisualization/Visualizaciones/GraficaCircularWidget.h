// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VisualizacionWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "GraficaCircularWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaCircularWidget : public UVisualizacionWidget
{
	GENERATED_BODY()
public:
    UGraficaCircularWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float RadioInterior;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float RadioCentro;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float RadioVars;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float RadioYears;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float RadioMonths;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float RadioDaily;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<bool> VisibilitySeries;

    TArray<float> PromediosVar;

    TArray<float> CountPromediosVar;

    TArray<TArray<float>> PromediosYears;//la primera dimension es la serie a la que pertenece

    TArray<TArray<TArray<float>>> PromediosMonths;

    TArray<TArray<int>> CountsPromediosYears;//la primera dimension es la serie a la que pertenece

    TArray<TArray<TArray<int>>> CountsPromediosMonths;
	
    //UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<TArray<FVector2D>> Series;//series convertidas

    TArray<TArray<TArray<TArray<FVector2D>>>> SeriesDividida;//series converticas y mapeadas, usando Wini y Wtam

    //UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grafica")
    TArray<TArray<FVector2D>> SeriesDibujar;//series converticas y mapeadas, usando Wini y Wtam

    TArray<FVector2D> SeriesDibujarVar;

    TArray<TArray<FVector2D>> SeriesDibujarYears;//series converticas y mapeadas, usando Wini y Wtam

    TArray<TArray<TArray<FVector2D>>> SeriesDibujarMonths;//series converticas y mapeadas, usando Wini y Wtam

    TArray<TArray<TArray<TArray<FVector2D>>>> SeriesDibujarDaily;//series converticas y mapeadas, usando Wini y Wtam

	
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void CalcularPuntosDibujar();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Filtros")
    TArray<FString> Months;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Filtros")
    TArray<FString> AbrevMonths;//para mostrar en la interfaz

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<int> CantidadDiasMes;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> AcumCantidadDiasMesConsultados;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int CantidadDiasYearConsulta;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<int> AcumCantidadDiasYearConsultados;

    TArray<FVector2D> ConvertirArrayDailyDataStructToFVector2D(TArray<FDailyDataStruct>& Datos);

    TArray<FVector2D> ConvertirArrayDailyDataStructToFVector2DComplete(TArray<FDailyDataStruct>& Datos);

    TArray<TArray<TArray<FVector2D>>> ConvertirArrayDailyDataStructToFVector2DDivido(TArray<FDailyDataStruct>& Datos);

    void CalcularCantidadDiasConsulta();
	
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void SetSerie(int IdSerie, TArray<FVector2D> Points);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void SetColorSerie(int IdSerie, FLinearColor ColorSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual int AddSerie(TArray<FDailyDataStruct> Datos);//retorna el id de la serie en la grafica

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void HideSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ShowSerie(int IdSerie);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ClearSeries();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<FLinearColor> ColorSeries;//interno para las graficas dibujadas

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica - Opciones")
    TArray<FLinearColor> GeneralColorSeries;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> WInis;//este si varia para cada variable

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> WTams;//para cada serie, en caso de que sean diferentes pero en realidad son lo mismo para todos

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> WInisVar;//este si varia para cada variable

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float WTamVar;//el mismo tama�o para las variables

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> WInisCentro;//este si varia para cada variable

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float WTamCentro;//el mismo tama�o para las variables

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float WTamYear;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> WInisYears;//este si varia para cada variable

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> WInisMonths;//este si varia para cada variable

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TArray<float> WTamsMonths;//para cada serie, en caso de que sean diferentes pero en realidad son lo mismo para todos

    virtual void CalcularPromedios(int IdSerie, TArray<FDailyDataStruct>& Datos);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ActualizarGrafica();

    float AnguloInterVar;

    float AnguloInterYear;

    float AnguloInterMonth;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ExplotarPunto(int IdVar, int IdYear, int IdMonth);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    virtual void ContraerPunto(int IdVar, int IdYear, int IdMonth);
	
};
