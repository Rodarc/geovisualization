// Fill out your copyright notice in the Description page of Project Settings.

#include "GraficaCircularVentanaWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/PanelWidget.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/ScrollBoxSlot.h"
#include "Components/SizeBox.h"
#include "Components/CanvasPanel.h"
#include "Engine/GameEngine.h"

UGraficaCircularVentanaWidget::UGraficaCircularVentanaWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    static ConstructorHelpers::FClassFinder<UPuntoGrafica> PuntoGraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/PuntoGraficaBP.PuntoGraficaBP_C'"));
    if (PuntoGraficaWidgetClass.Succeeded()) {
        TypePunto = PuntoGraficaWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<ULineaGrafica> LineaGraficaWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/LineaGraficaBP.LineaGraficaBP_C'"));
    if (LineaGraficaWidgetClass.Succeeded()) {
        TypeLinea = LineaGraficaWidgetClass.Class;
    }

	//deberia recibir copias de estas cosas al ser creado
    Variables.Add("Temperatura Promedio");
    Variables.Add("Temperatura Maxima");
    Variables.Add("Temperatura Minima");
    Variables.Add("Precipitacion");
    Variables.Add("Profundidad Nieve");

    VariablesAbrev.Add("TAVG");
    VariablesAbrev.Add("TMAX");
    VariablesAbrev.Add("TMIN");
    VariablesAbrev.Add("PRCP");
    VariablesAbrev.Add("SNWD");


    ColorLineasReferencias = FLinearColor(0.4f, 0.4f, 0.4f, 1.0f);

    TraslacionOrigen = FVector2D::ZeroVector;
    Escala = 1.0f;
    DeltaZoom = 0.1f;

    SizePoints = FVector2D(8.0f, 8.0f);
    SizeLines = 5.0f;
    RadioInterior = 75.0f;// deberia ser 50 tambien
	RadioCentro = 40.0f;
    RadioVars = 60.0f;
	RadioYears = 80.0f;
	RadioMonths = 130.0f;
    RadioDaily = 180.0f;

    Precision = 16;
    AnchoLineasReferencias = 5.0f;
    EspacioEntreNiveles = 50.0f;
}

void UGraficaCircularVentanaWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);
}


void UGraficaCircularVentanaWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
    
    if (bTransformando) {
        ActualizarGrafica();
    }
}


void UGraficaCircularVentanaWidget::SetSerie(int IdSerie, TArray<FVector2D> Points) {
}

void UGraficaCircularVentanaWidget::SetColorSerie(int IdSerie, FLinearColor ColorSerie) {
    Super::SetColorSerie(IdSerie, ColorSerie);

    SeriesPuntoVarWidgets[IdSerie]->SetColor(ColorSerie);

    /*for (int i = 0; i < SeriesPuntoDailyWidgets[IdSerie].Num(); i++) {
        //SeriesPuntoWidgets[IdSerie][i]->ColorFondoPunto = ColorSerie;
        //SeriesPuntoWidgets[IdSerie][i]->ColorLineaPunto = ColorSerie;
        SeriesPuntoDailyWidgets[IdSerie][i]->SetColor(ColorSerie);
    }
    for (int i = 0; i < SeriesLineaDailyWidgets[IdSerie].Num(); i++) {
        SeriesLineaDailyWidgets[IdSerie][i]->ColorLinea = ColorSerie;
    }*/
    for (int i = 0; i < SeriesPuntoYearWidgets[IdSerie].Num(); i++) {
        SeriesPuntoYearWidgets[IdSerie][i]->SetColor(ColorSerie);
    }
    for (int i = 0; i < SeriesLineaYearWidgets[IdSerie].Num(); i++) {
        SeriesLineaYearWidgets[IdSerie][i]->ColorLinea = ColorSerie;
    }
    for (int i = 0; i < SeriesPuntoMonthWidgets[IdSerie].Num(); i++) {
		for (int j = 0; j < SeriesPuntoMonthWidgets[IdSerie][i].Num(); j++) {
			SeriesPuntoMonthWidgets[IdSerie][i][j]->SetColor(ColorSerie);
		}
    }
    for (int i = 0; i < SeriesLineaMonthWidgets[IdSerie].Num(); i++) {
		for (int j = 0; j < SeriesLineaMonthWidgets[IdSerie][i].Num(); j++) {
			SeriesLineaMonthWidgets[IdSerie][i][j]->ColorLinea = ColorSerie;
		}
    }
    for (int i = 0; i < SeriesPuntoDayWidgets[IdSerie].Num(); i++) {
		for (int j = 0; j < SeriesPuntoDayWidgets[IdSerie][i].Num(); j++) {
			for (int k = 0; k < SeriesPuntoDayWidgets[IdSerie][i][j].Num(); k++) {
				SeriesPuntoDayWidgets[IdSerie][i][j][k]->SetColor(ColorSerie);
			}
		}
    }
    for (int i = 0; i < SeriesLineaDayWidgets[IdSerie].Num(); i++) {
		for (int j = 0; j < SeriesLineaDayWidgets[IdSerie][i].Num(); j++) {
			for (int k = 0; k < SeriesLineaDayWidgets[IdSerie][i][j].Num(); k++) {
				SeriesLineaDayWidgets[IdSerie][i][j][k]->ColorLinea = ColorSerie;
			}
		}
    }
}

int UGraficaCircularVentanaWidget::AddSerie(TArray<FDailyDataStruct> Datos) {//no encesita ser copiada
    int IdSerie = Super::AddSerie(Datos);

    UPuntoGrafica *  PuntoGrafica = CreateWidget<UPuntoGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypePunto);
    PuntoGrafica->IdSerie = IdSerie;
    PuntoGrafica->IdYear = -1;
    PuntoGrafica->IdMonth = -1;
    PuntoGrafica->IdDay = -1;
    PuntoGrafica->IdData = -1;
    //no tiene IdData
    PuntoGrafica->Visualizacion = this;
    SeriesPuntoVarWidgets.Add(PuntoGrafica);
    PuntoGrafica->IdPunto = SeriesPuntoVarWidgets.Num()-1;//no se coincide con los id de las consultas, aqui hay un problema
    PuntoGrafica->AddToViewport(0);

    UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
    UPanelSlot * SlotPuntoGrafica = RootWidget->AddChild(PuntoGrafica);
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotPuntoGrafica);
    //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
    CanvasSlot->SetZOrder(0);

    USizeBox * SizeBoxWidget = Cast<USizeBox>(PuntoGrafica->GetRootWidget());
    if (SizeBoxWidget) {
        CanvasSlot->SetSize(SizePoints);// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
    }

    CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
    CanvasSlot->SetPosition(FVector2D(10.0f, 10.0f));


    /*TArray<UPuntoGrafica *> PuntosGraficaSerie;
    for (int i = 0; i < Series[IdSerie].Num(); i++) {
		if (Datos[i].value != -9999) {
			UPuntoGrafica *  PuntoGrafica = CreateWidget<UPuntoGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypePunto);
			PuntoGrafica->IdData = i;
			PuntoGrafica->IdDay = Datos[i].day - 1;
			PuntoGrafica->IdMonth = Datos[i].month - 1;//deberia ser de los meses consultados?
			PuntoGrafica->IdYear = Datos[i].year - Consulta.InitialYear;
			PuntoGrafica->IdSerie = IdSerie;
			PuntoGrafica->Visualizacion = this;
			PuntoGrafica->IdPunto = PuntosGraficaSerie.Add(PuntoGrafica);
			PuntoGrafica->AddToViewport(0);

			UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
			UPanelSlot * Slot = RootWidget->AddChild(PuntoGrafica);
			UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
			//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
			CanvasSlot->SetZOrder(0);

			USizeBox * SizeBoxWidget = Cast<USizeBox>(PuntoGrafica->GetRootWidget());
			if (SizeBoxWidget) {
				CanvasSlot->SetSize(SizePoints);// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
			}

			CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
			CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
		}
    }
    SeriesPuntoDailyWidgets.Add(PuntosGraficaSerie);

    TArray<ULineaGrafica *> LineasGraficaSerie;
	for (int i = 0; i < PuntosGraficaSerie.Num() - 1; i++) {
		if (PuntosGraficaSerie[i + 1]->IdData - PuntosGraficaSerie[i]->IdData == 1) {
            ULineaGrafica *  Linea = CreateWidget<ULineaGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypeLinea);
            Linea->PuntoInicio = PuntosGraficaSerie[i];//no conincide, deberia trabajar en los puntos para comprobar directamente en el array
            Linea->PuntoFinal = PuntosGraficaSerie[i + 1];
            LineasGraficaSerie.Add(Linea);
            Linea->AddToViewport(0);
            Linea->Ancho = SizeLines;

            UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
            UPanelSlot * Slot = RootWidget->AddChild(Linea);
            UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Slot);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
            CanvasSlot->SetZOrder(-1);

            USizeBox * SizeBoxWidget = Cast<USizeBox>(Linea->GetRootWidget());
            if (SizeBoxWidget) {
                CanvasSlot->SetSize(FVector2D(20.0f, SizeLines));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
            }

            CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
            CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
		}
	}
    SeriesLineaDailyWidgets.Add(LineasGraficaSerie);
	*/

	//agregando puntos para los a�os
	TArray<UPuntoGrafica *> PuntosYear;
	for (int i = 0; i < YearsFiltrados.Num(); i++) {
		if (YearsFiltrados[i]) {
			PuntoGrafica = CreateWidget<UPuntoGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypePunto);
			PuntoGrafica->IdSerie = IdSerie;
			PuntoGrafica->IdYear = i;
			PuntoGrafica->IdMonth = -1;
			PuntoGrafica->IdDay = -1;
			PuntoGrafica->IdData = -1;
			//no tiene IdData
			PuntoGrafica->Visualizacion = this;
			PuntosYear.Add(PuntoGrafica);
			PuntoGrafica->IdPunto = PuntosYear.Num()-1;//no se coincide con los id de las consultas, aqui hay un problema
			PuntoGrafica->AddToViewport(0);

			RootWidget = Cast<UPanelWidget>(GetRootWidget());
			SlotPuntoGrafica = RootWidget->AddChild(PuntoGrafica);
			CanvasSlot = Cast<UCanvasPanelSlot>(SlotPuntoGrafica);
			//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
			CanvasSlot->SetZOrder(0);

			SizeBoxWidget = Cast<USizeBox>(PuntoGrafica->GetRootWidget());
			if (SizeBoxWidget) {
				CanvasSlot->SetSize(SizePoints);// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
			}

			CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
			CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
		}
	}
	SeriesPuntoYearWidgets.Add(PuntosYear);
	
    TArray<ULineaGrafica *> LineasYear;
	for (int i = 0; i < PuntosYear.Num() - 1; i++) {
		//if (PuntosYear[i + 1]->IdYear - PuntosYear[i]->IdYear == 1) {//no deberia ser esto, simpremente debo unir
            ULineaGrafica *  Linea = CreateWidget<ULineaGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypeLinea);
            Linea->PuntoInicio = PuntosYear[i];//no conincide, deberia trabajar en los puntos para comprobar directamente en el array
            Linea->PuntoFinal = PuntosYear[i + 1];
            LineasYear.Add(Linea);
            Linea->AddToViewport(0);
            Linea->Ancho = SizeLines;

            RootWidget = Cast<UPanelWidget>(GetRootWidget());
            UPanelSlot * SlotLinea = RootWidget->AddChild(Linea);
            CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
            CanvasSlot->SetZOrder(-1);

            SizeBoxWidget = Cast<USizeBox>(Linea->GetRootWidget());
            if (SizeBoxWidget) {
                CanvasSlot->SetSize(FVector2D(20.0f, SizeLines));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
            }

            CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
            CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
		//}
	}
    SeriesLineaYearWidgets.Add(LineasYear);

	TArray<TArray<UPuntoGrafica *>> PuntosMonths;
	for (int i = 0; i < YearsFiltrados.Num(); i++) {
		TArray<UPuntoGrafica *> PuntosMonth;
		if (YearsFiltrados[i]) {
			//creo punto en la posicion donde corresponda de los puntos; quiza deberia mostrar todos?, confusion
			for (int j = 0; j < Consulta.MesesConsultados.Num(); j++) {//podria recorrer meses filtrados, y tomar los filtrados
				PuntoGrafica = CreateWidget<UPuntoGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypePunto);
				PuntoGrafica->IdSerie = IdSerie;
				PuntoGrafica->IdYear = i;
				PuntoGrafica->IdMonth = Consulta.MesesConsultados[j];
				PuntoGrafica->IdDay = -1;
				PuntoGrafica->IdData = -1;
				PuntoGrafica->Visualizacion = this;
				PuntosMonth.Add(PuntoGrafica);
				PuntoGrafica->IdPunto = PuntosMonth.Num()-1;//o seria mejor el id compuesto i * tammeses + j
				PuntoGrafica->AddToViewport(0);

				RootWidget = Cast<UPanelWidget>(GetRootWidget());
				SlotPuntoGrafica = RootWidget->AddChild(PuntoGrafica);
				CanvasSlot = Cast<UCanvasPanelSlot>(SlotPuntoGrafica);
				//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
				CanvasSlot->SetZOrder(0);

				SizeBoxWidget = Cast<USizeBox>(PuntoGrafica->GetRootWidget());
				if (SizeBoxWidget) {
					CanvasSlot->SetSize(SizePoints);// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
				}

				CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
				CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
			}
		}
		PuntosMonths.Add(PuntosMonth);
	}
	SeriesPuntoMonthWidgets.Add(PuntosMonths);

	TArray<TArray<ULineaGrafica *>> LineasMonths;
	for (int k = 0; k < SeriesPuntoMonthWidgets[IdSerie].Num(); k++) {
		TArray<ULineaGrafica *> LineasMonth;
		for (int i = 0; i < SeriesPuntoMonthWidgets[IdSerie][k].Num() - 1; i++) {
			//if (PuntosYear[i + 1]->IdYear - PuntosYear[i]->IdYear == 1) {//no deberia ser esto, simpremente debo unir
				ULineaGrafica *  Linea = CreateWidget<ULineaGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypeLinea);
				Linea->PuntoInicio = SeriesPuntoMonthWidgets[IdSerie][k][i];//no conincide, deberia trabajar en los puntos para comprobar directamente en el array
				Linea->PuntoFinal = SeriesPuntoMonthWidgets[IdSerie][k][i + 1];
				LineasMonth.Add(Linea);
				Linea->AddToViewport(0);
				Linea->Ancho = SizeLines;

				RootWidget = Cast<UPanelWidget>(GetRootWidget());
				UPanelSlot * SlotLinea = RootWidget->AddChild(Linea);
				CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
				//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
				CanvasSlot->SetZOrder(-1);

				SizeBoxWidget = Cast<USizeBox>(Linea->GetRootWidget());
				if (SizeBoxWidget) {
					CanvasSlot->SetSize(FVector2D(20.0f, SizeLines));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
				}

				CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
				CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));

			//}
		}
		LineasMonths.Add(LineasMonth);
	}
	SeriesLineaMonthWidgets.Add(LineasMonths);


	TArray<TArray<TArray<UPuntoGrafica *>>> PuntosDaily;
	PuntosDaily.SetNum(YearsFiltrados.Num());
	for (int i = 0; i < YearsFiltrados.Num(); i++) {
		//TArray<UPuntoGrafica *> PuntosMonth;
		if (YearsFiltrados[i]) {
			//creo punto en la posicion donde corresponda de los puntos; quiza deberia mostrar todos?, confusion
			PuntosDaily[i].SetNum(Consulta.MesesConsultados.Num());
			for (int j = 0; j < Consulta.MesesConsultados.Num(); j++) {//podria recorrer meses filtrados, y tomar los filtrados

				for (int k = 0; k < SeriesDividida[IdSerie][i][j].Num(); k++) {

					if (SeriesDividida[IdSerie][i][j][k].Y != -9999) {
						PuntoGrafica = CreateWidget<UPuntoGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypePunto);
						PuntoGrafica->IdSerie = IdSerie;
						PuntoGrafica->IdYear = i;
						PuntoGrafica->IdMonth = Consulta.MesesConsultados[j];
						PuntoGrafica->IdDay = SeriesDividida[IdSerie][i][j][k].X;
						PuntoGrafica->IdData = -1;
						PuntoGrafica->Visualizacion = this;
						PuntosDaily[i][j].Add(PuntoGrafica);
						PuntoGrafica->IdPunto = PuntosDaily[i][j].Num() - 1;//o seria mejor el id compuesto i * tammeses + j
						PuntoGrafica->AddToViewport(0);

						RootWidget = Cast<UPanelWidget>(GetRootWidget());
						SlotPuntoGrafica = RootWidget->AddChild(PuntoGrafica);
						CanvasSlot = Cast<UCanvasPanelSlot>(SlotPuntoGrafica);
						//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
						CanvasSlot->SetZOrder(0);

						SizeBoxWidget = Cast<USizeBox>(PuntoGrafica->GetRootWidget());
						if (SizeBoxWidget) {
							CanvasSlot->SetSize(SizePoints);// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
						}

						CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
						CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
					}
				}
			}
		}
		//PuntosMonths.Add(PuntosMonth);
	}
	SeriesPuntoDayWidgets.Add(PuntosDaily);

	TArray<TArray<TArray<ULineaGrafica *>>> LineasDaily;
	LineasDaily.SetNum(YearsFiltrados.Num());
	for (int k = 0; k < SeriesPuntoDayWidgets[IdSerie].Num(); k++) {
		LineasDaily[k].SetNum(Consulta.MesesConsultados.Num());
		for (int j = 0; j < SeriesPuntoDayWidgets[IdSerie][k].Num(); j++) {
			for (int i = 0; i < SeriesPuntoDayWidgets[IdSerie][k][j].Num() - 1; i++) {
				//if (PuntosYear[i + 1]->IdYear - PuntosYear[i]->IdYear == 1) {//no deberia ser esto, simpremente debo unir
				if (SeriesPuntoDayWidgets[IdSerie][k][j][i + 1]->IdDay- SeriesPuntoDayWidgets[IdSerie][k][j][i]->IdDay == 1) {
					ULineaGrafica *  Linea = CreateWidget<ULineaGrafica>(UGameplayStatics::GetGameInstance(GetWorld()), TypeLinea);
					Linea->PuntoInicio = SeriesPuntoDayWidgets[IdSerie][k][j][i];//no conincide, deberia trabajar en los puntos para comprobar directamente en el array
					Linea->PuntoFinal = SeriesPuntoDayWidgets[IdSerie][k][j][i + 1];
					LineasDaily[k][j].Add(Linea);
					Linea->AddToViewport(0);
					Linea->Ancho = SizeLines;

					RootWidget = Cast<UPanelWidget>(GetRootWidget());
					UPanelSlot * SlotLinea = RootWidget->AddChild(Linea);
					CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
					//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
					CanvasSlot->SetZOrder(-1);

					SizeBoxWidget = Cast<USizeBox>(Linea->GetRootWidget());
					if (SizeBoxWidget) {
						CanvasSlot->SetSize(FVector2D(20.0f, SizeLines));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
					}

					CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
					CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));

				}
			}
		}
	}
	SeriesLineaDayWidgets.Add(LineasDaily);

    return IdSerie;
}

void UGraficaCircularVentanaWidget::HideSerie(int IdSerie) {
}

void UGraficaCircularVentanaWidget::ShowSerie(int IdSerie) {
}

void UGraficaCircularVentanaWidget::ClearSeries() {
    Super::ClearSeries();
    //PUNTOS VAR
    for (int i = 0; i < SeriesPuntoVarWidgets.Num(); i++) {
        SeriesPuntoVarWidgets[i]->RemoveFromParent();
        SeriesPuntoVarWidgets[i]->Destruct();
    }
    SeriesPuntoVarWidgets.Empty();

	//PUNTOS
    for (int i = 0; i < SeriesPuntoDailyWidgets.Num(); i++) {
        for (int j = 0; j < SeriesPuntoDailyWidgets[i].Num(); j++) {
            SeriesPuntoDailyWidgets[i][j]->RemoveFromParent();
            SeriesPuntoDailyWidgets[i][j]->Destruct();
        }
        SeriesPuntoDailyWidgets[i].Empty();
    }
    SeriesPuntoDailyWidgets.Empty();

    for (int i = 0; i < SeriesLineaDailyWidgets.Num(); i++) {
        for (int j = 0; j < SeriesLineaDailyWidgets[i].Num(); j++) {
            SeriesLineaDailyWidgets[i][j]->RemoveFromParent();
            SeriesLineaDailyWidgets[i][j]->Destruct();
        }
        SeriesLineaDailyWidgets[i].Empty();
    }
    SeriesLineaDailyWidgets.Empty();

	//PUNTOS YEAR
    for (int i = 0; i < SeriesPuntoYearWidgets.Num(); i++) {
        for (int j = 0; j < SeriesPuntoYearWidgets[i].Num(); j++) {
            SeriesPuntoYearWidgets[i][j]->RemoveFromParent();
            SeriesPuntoYearWidgets[i][j]->Destruct();
        }
        SeriesPuntoYearWidgets[i].Empty();
    }
    SeriesPuntoYearWidgets.Empty();

    for (int i = 0; i < SeriesLineaYearWidgets.Num(); i++) {
        for (int j = 0; j < SeriesLineaYearWidgets[i].Num(); j++) {
            SeriesLineaYearWidgets[i][j]->RemoveFromParent();
            SeriesLineaYearWidgets[i][j]->Destruct();
        }
        SeriesLineaYearWidgets[i].Empty();
    }
    SeriesLineaYearWidgets.Empty();

	//PUNTOS MONTH
    for (int i = 0; i < SeriesPuntoMonthWidgets.Num(); i++) {
        for (int j = 0; j < SeriesPuntoMonthWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesPuntoMonthWidgets[i][j].Num(); k++) {
				SeriesPuntoMonthWidgets[i][j][k]->RemoveFromParent();
				SeriesPuntoMonthWidgets[i][j][k]->Destruct();
			}
			SeriesPuntoMonthWidgets[i][j].Empty();
        }
        SeriesPuntoMonthWidgets[i].Empty();
    }
    SeriesPuntoMonthWidgets.Empty();

    for (int i = 0; i < SeriesLineaMonthWidgets.Num(); i++) {
        for (int j = 0; j < SeriesLineaMonthWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesLineaMonthWidgets[i][j].Num(); k++) {
				SeriesLineaMonthWidgets[i][j][k]->RemoveFromParent();
				SeriesLineaMonthWidgets[i][j][k]->Destruct();
			}
			SeriesLineaMonthWidgets[i][j].Empty();
        }
        SeriesLineaMonthWidgets[i].Empty();
    }
    SeriesLineaMonthWidgets.Empty();

	//PUNTOS Day 
    for (int i = 0; i < SeriesPuntoDayWidgets.Num(); i++) {
        for (int j = 0; j < SeriesPuntoDayWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesPuntoDayWidgets[i][j].Num(); k++) {
				for (int r = 0; r < SeriesPuntoDayWidgets[i][j][k].Num(); r++) {
					SeriesPuntoDayWidgets[i][j][k][r]->RemoveFromParent();
					SeriesPuntoDayWidgets[i][j][k][r]->Destruct();
				}
				SeriesPuntoDayWidgets[i][j][k].Empty();
			}
			SeriesPuntoDayWidgets[i][j].Empty();
        }
        SeriesPuntoDayWidgets[i].Empty();
    }
    SeriesPuntoDayWidgets.Empty();

    for (int i = 0; i < SeriesLineaDayWidgets.Num(); i++) {
        for (int j = 0; j < SeriesLineaDayWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesLineaDayWidgets[i][j].Num(); k++) {
				for (int r = 0; r < SeriesLineaDayWidgets[i][j][k].Num(); r++) {
					SeriesLineaDayWidgets[i][j][k][r]->RemoveFromParent();
					SeriesLineaDayWidgets[i][j][k][r]->Destruct();
				}
				SeriesLineaDayWidgets[i][j][k].Empty();
			}
			SeriesLineaDayWidgets[i][j].Empty();
        }
        SeriesLineaDayWidgets[i].Empty();
    }
    SeriesLineaDayWidgets.Empty();

}

void UGraficaCircularVentanaWidget::VisualizarConsulta_Implementation() {
    CalcularCantidadDiasConsulta();
    ClearSeries();//visualizar implica crear nuevas series, por lo tanto debo eliminar las anteriores si es que habia
    for (int j = 0; j < Consulta.StationConsultas[0].ConsultasPorVariable.Num(); j++) {
        //int IdSerie = AddSerie(ConvertirArrayDailyDataStructToFVector2D(Consulta.StationConsultas[0].ConsultasPorVariable[j].Datos));//idserie deberia ser igual que j
        int IdSerie = AddSerie(Consulta.StationConsultas[0].ConsultasPorVariable[j].Datos);//idserie deberia ser igual que j
        SetColorSerie(IdSerie, GeneralColorSeries[j]);//necesito un array de colores
    }
    DeleteCirculoInterior();
    CreateCirculoInterior();
	DeleteTextos();
	CreateTextos();
	DeleteRangos();
	CreateRangos();
    ActualizarGrafica();
}

void UGraficaCircularVentanaWidget::CalcularPuntosDibujar() {
    //aqui podria hacer los caclulos para los wini y tam, en lugar de hacerlo al agregar la serie, no seria necesario al agregar la serie
    //cuando haga estos calculos, debo guardar, los w que son la base en las variables, y las proporciones, para el resto de elementos (yeasr, months, y dayli)
    //estos niveles se controutyen con la informaicon del anterio, el anterior podria hacer que guarde winti digamos para a�os, pera calcular mas arpido el de meses, y de este guardo par alcalular el de los dayl
    //la ventaja que tiene los a��s y mese al no estar calculando sus wini ni guardandola, es que la misma proporcion se aplicaria en las demas series o variables, por lo tanto solo necesitaria guardar uno apra todos
    //lo unico que necesito es la posicion proporcional y el tama�o proporcional, (es proporcional por que hay mese que tengan mas dias que otros)
    //de hecho estas proporciones se podrian crear antes, cuando se actualizan los filtros, ya que son dindependientes, y seria coincidente el tam�o de los arreglos con a�os fltrados y con meses filtrados o cinsultados
    SeriesDibujarVar.Empty();
    for (int k = 0; k < Series.Num(); k++) {
        if (VisibilitySeries[k]) {//no es solo estoaqui debereia resdistribuir el espacion entre quienes esten visibles, por ahora dejarlo asi
			SeriesDibujarYears[k].Empty();
			for (int i = 0; i < PromediosYears[k].Num(); i++) {//este deberian ser los minimos correspondientes
				float W = WInisVar[k] + WInisYears[i] + WTamYear / 2;
                float R = RadioInterior + RadioCentro + EspacioEntreNiveles + RadioVars * PromediosYears[k][i] / (Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax - Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin);
                float X = R * FMath::Cos(W);//conversion de polares a cartesianas
                float Y = R * FMath::Sin(W);
                //FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
                SeriesDibujarYears[k].Add(FVector2D(X,Y));
			}
			SeriesDibujarMonths[k].Empty();
			for (int i = 0; i < SeriesPuntoMonthWidgets[k].Num(); i++) {//este deberian ser los minimos correspondientes
				SeriesDibujarMonths[k].Add(TArray<FVector2D>());
				for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {
					float W = WInisVar[k] + WInisYears[i] + WInisMonths[j] + WTamsMonths[j] / 2;
					float R = RadioInterior + RadioCentro + RadioVars + 2*EspacioEntreNiveles + RadioYears * PromediosMonths[k][i][Consulta.MesesConsultados[j]] / (Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax - Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin);
					float X = R * FMath::Cos(W);//conversion de polares a cartesianas
					float Y = R * FMath::Sin(W);
					//FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
					SeriesDibujarMonths[k][i].Add(FVector2D(X,Y));
				}
			}
			SeriesDibujarDaily[k].Empty();
			SeriesDibujarDaily[k].SetNum(SeriesPuntoDayWidgets[k].Num());
			for (int i = 0; i < SeriesPuntoDayWidgets[k].Num(); i++) {//este deberian ser los minimos correspondientes
				SeriesDibujarDaily[k][i].SetNum(SeriesPuntoDayWidgets[k][i].Num());
				for (int j = 0; j < SeriesPuntoDayWidgets[k][i].Num(); j++) {
					//SeriesDibujarDaily[k][i][j].SetNum(SeriesPuntoDayWidgets[k][i][j].Num());
					for (int r = 0; r < SeriesPuntoDayWidgets[k][i][j].Num(); r++) {
						//float W = WInisVar[k] + WInisYears[i] + WInisMonths[j] + WTamsMonths[j] * SeriesDividida[k][i][j][SeriesPuntoDayWidgets[k][i][j][r]->IdDay].X / CantidadDiasMes[Consulta.MesesConsultados[j]];
						float W = WInisVar[k] + WInisYears[i] + WInisMonths[j] + WTamsMonths[j] * SeriesPuntoDayWidgets[k][i][j][r]->IdDay / CantidadDiasMes[Consulta.MesesConsultados[j]];
						float R = RadioInterior + RadioCentro + RadioVars + 3 * EspacioEntreNiveles + RadioYears + RadioMonths * SeriesDividida[k][i][j][SeriesPuntoDayWidgets[k][i][j][r]->IdDay].Y/*SeriesDividida[k][i][j][r].Y*/ / (Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax - Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin);
						float X = R * FMath::Cos(W);//conversion de polares a cartesianas
						float Y = R * FMath::Sin(W);
						//FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
						SeriesDibujarDaily[k][i][j].Add(FVector2D(X,Y));
					}
				}
			}

            float W = WInisVar[k] + WTamVar/2;//es X o Y elq ue represente la proporcio, asumo que x
            float R = RadioInterior + RadioCentro * PromediosVar[k] / (Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax - Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin);
            float X = R * FMath::Cos(W);//conversion de polares a cartesianas
            float Y = R * FMath::Sin(W);
            //FVector2D PuntoDibujo = LUCorner + FVector2D(X, Y);
            SeriesDibujarVar.Add(FVector2D(X,Y));
        }
    }
    //aqui debo calcular lo spuntos para los promedios tambien
}

void UGraficaCircularVentanaWidget::ActualizarGrafica() {
    CalcularPuntosDibujar();
    //debo calcular los puntos de dibujar y el corner solo cuando estos se deformen por alguna razon, por ejemplo al llamar a acgrandar, meintras tura la animacion, debo llamar a esto en el tick, quiza deba manejar un booleano para esto
    //el problema es cuando se giro la camara, si bien el widget no esta redimensionando pero se esta moviemiento, esntonces los puntos que se dibujan estan en otra posicion en la pantalla
    //creo que se debe de dibujar

    for (int i = 0; i < SeriesPuntoDayWidgets.Num(); i++) {
        /*for (int j = 0; j < SeriesPuntoDailyWidgets[i].Num(); j++) {
			UPuntoGrafica * PG = SeriesPuntoDailyWidgets[i][j];
            UCanvasPanelSlot * PuntoCanvasSlot = Cast<UCanvasPanelSlot>(PG->Slot);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot

            //CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
            PuntoCanvasSlot->SetPosition(SeriesDibujar[i][PG->IdData]*Escala - SizePoints / 2 + TraslacionOrigen*Escala);// com la funcion es const, este calculo no lo puedo hacer aqui
        }
        for (int j = 0; j < SeriesLineaDailyWidgets[i].Num(); j++) {
            SeriesLineaDailyWidgets[i][j]->Actualizar();
        }*/
		for (int j = 0; j < SeriesPuntoYearWidgets[i].Num(); j++) {
			UPuntoGrafica * PG = SeriesPuntoYearWidgets[i][j];
            UCanvasPanelSlot * PuntoCanvasSlot = Cast<UCanvasPanelSlot>(PG->Slot);
            PuntoCanvasSlot->SetPosition(SeriesDibujarYears[i][j]*Escala - SizePoints / 2 + TraslacionOrigen*Escala);// com la funcion es const, este calculo no lo puedo hacer aqui
		}
        for (int j = 0; j < SeriesLineaYearWidgets[i].Num(); j++) {
            SeriesLineaYearWidgets[i][j]->Actualizar();
        }
		for (int j = 0; j < SeriesPuntoMonthWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesPuntoMonthWidgets[i][j].Num(); k++) {
				UPuntoGrafica * PG = SeriesPuntoMonthWidgets[i][j][k];
				UCanvasPanelSlot * PuntoCanvasSlot = Cast<UCanvasPanelSlot>(PG->Slot);
				PuntoCanvasSlot->SetPosition(SeriesDibujarMonths[i][j][k]*Escala - SizePoints / 2 + TraslacionOrigen*Escala);// com la funcion es const, este calculo no lo puedo hacer aqui
			}
		}
		for (int j = 0; j < SeriesLineaMonthWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesLineaMonthWidgets[i][j].Num(); k++) {
				SeriesLineaMonthWidgets[i][j][k]->Actualizar();
			}
		}
		for (int j = 0; j < SeriesPuntoDayWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesPuntoDayWidgets[i][j].Num(); k++) {
				for (int r = 0; r < SeriesPuntoDayWidgets[i][j][k].Num(); r++) {
					UPuntoGrafica * PG = SeriesPuntoDayWidgets[i][j][k][r];
					UCanvasPanelSlot * PuntoCanvasSlot = Cast<UCanvasPanelSlot>(PG->Slot);
					PuntoCanvasSlot->SetPosition(SeriesDibujarDaily[i][j][k][r]*Escala - SizePoints / 2 + TraslacionOrigen*Escala);// com la funcion es const, este calculo no lo puedo hacer aqui
				}
			}
		}
		for (int j = 0; j < SeriesLineaDayWidgets[i].Num(); j++) {
			for (int k = 0; k < SeriesLineaDayWidgets[i][j].Num(); k++) {
				for (int r = 0; r < SeriesLineaDayWidgets[i][j][k].Num(); r++) {
					SeriesLineaDayWidgets[i][j][k][r]->Actualizar();
				}
			}
		}
        UPuntoGrafica * PG = SeriesPuntoVarWidgets[i];
        UCanvasPanelSlot * PuntoCanvasSlot = Cast<UCanvasPanelSlot>(PG->Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot

        //CanvasSlot->SetAnchors(FAnchors(0.0f, 0.0f));
        PuntoCanvasSlot->SetPosition(SeriesDibujarVar[i]*Escala - SizePoints / 2 + TraslacionOrigen*Escala);// com la funcion es const, este calculo no lo puedo hacer aqui
    }

    ActualizarCirculoInterior();
	ActualizarTextos();
	ActualizarRangos();
}

void UGraficaCircularVentanaWidget::ZoomIn_Implementation() {
    Escala += 0.2f;
    ActualizarGrafica();
}

void UGraficaCircularVentanaWidget::ZoomOut_Implementation() {
    Escala -= 0.2f;
    if (Escala < 0.1f) {
        Escala = 0.1f;
    }
    ActualizarGrafica();
}

void UGraficaCircularVentanaWidget::Reestablecer_Implementation() {
    TraslacionOrigen = FVector2D::ZeroVector;
    Escala = 1.0f;//en realidad deberia ser enfocar todo, sea con el que sea que deba ser el zoom, y reestableciendo la posicion a 0
    ActualizarGrafica();
}

void UGraficaCircularVentanaWidget::CreateCirculoInterior() {
    for (int k = 0; k < SeriesPuntoVarWidgets.Num(); k++) {
        ImagesInteriorYears.Add(TArray<TArray<UImage *>>());
        PuntosInteriorYears.Add(TArray<TArray<FVector2D>>());
        ImagesExteriorYears.Add(TArray<TArray<UImage *>>());
        PuntosExteriorYears.Add(TArray<TArray<FVector2D>>());
        ImagesVerticalesYears.Add(TArray<TArray<UImage *>>());
        PuntosVerticalesYears.Add(TArray<TArray<FVector2D>>());
        ImagesInteriorMonths.Add(TArray<TArray<TArray<UImage *>>>());
        PuntosInteriorMonths.Add(TArray<TArray<TArray<FVector2D>>>());
        ImagesExteriorMonths.Add(TArray<TArray<TArray<UImage *>>>());
        PuntosExteriorMonths.Add(TArray<TArray<TArray<FVector2D>>>());
        ImagesVerticalesMonths.Add(TArray<TArray<TArray<UImage *>>>());
        PuntosVerticalesMonths.Add(TArray<TArray<TArray<FVector2D>>>());
        for (int i = 0; i < SeriesPuntoYearWidgets[k].Num(); i++) {
            ImagesInteriorMonths[k].Add(TArray<TArray<UImage *>>());
            PuntosInteriorMonths[k].Add(TArray<TArray<FVector2D>>());
            ImagesExteriorMonths[k].Add(TArray<TArray<UImage *>>());
            PuntosExteriorMonths[k].Add(TArray<TArray<FVector2D>>());
            ImagesVerticalesMonths[k].Add(TArray<TArray<UImage *>>());
            PuntosVerticalesMonths[k].Add(TArray<TArray<FVector2D>>());
            for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {//espero funcione
                TArray<FVector2D> Puntos;
                TArray<UImage *> Images;
                TArray<FVector2D> PuntosEx;
                TArray<UImage *> ImagesEx;
                TArray<FVector2D> PuntosVerticales;
                TArray<UImage *> ImagesVerticales;
                CreateSeccionCirculo(WInisVar[k] + WInisYears[i] + WInisMonths[j], WTamsMonths[j], RadioInterior + RadioCentro + RadioVars + RadioYears + 3*EspacioEntreNiveles, RadioMonths, Puntos, Images, PuntosVerticales, ImagesVerticales, PuntosEx, ImagesEx);
                PuntosInteriorMonths[k][i].Add(Puntos);
                ImagesInteriorMonths[k][i].Add(Images);
                PuntosExteriorMonths[k][i].Add(PuntosEx);
                ImagesExteriorMonths[k][i].Add(ImagesEx);
                PuntosVerticalesMonths[k][i].Add(PuntosVerticales);
                ImagesVerticalesMonths[k][i].Add(ImagesVerticales);
            }
            
            TArray<FVector2D> Puntos;
            TArray<UImage *> Images;
            TArray<FVector2D> PuntosEx;
            TArray<UImage *> ImagesEx;
            TArray<FVector2D> PuntosVerticales;
            TArray<UImage *> ImagesVerticales;
            CreateSeccionCirculo(WInisVar[k] + WInisYears[i], WTamYear, RadioInterior + RadioCentro + RadioVars + 2 * EspacioEntreNiveles, RadioYears, Puntos, Images, PuntosVerticales, ImagesVerticales, PuntosEx, ImagesEx);
            PuntosInteriorYears[k].Add(Puntos);
            ImagesInteriorYears[k].Add(Images);
            PuntosExteriorYears[k].Add(PuntosEx);
            ImagesExteriorYears[k].Add(ImagesEx);
            PuntosVerticalesYears[k].Add(PuntosVerticales);
            ImagesVerticalesYears[k].Add(ImagesVerticales);
        }
        //para el de las variables
        TArray<FVector2D> Puntos;
        TArray<UImage *> Images;
        TArray<FVector2D> PuntosEx;
        TArray<UImage *> ImagesEx;
        TArray<FVector2D> PuntosVerticales;
        TArray<UImage *> ImagesVerticales;
        CreateSeccionCirculo(WInisVar[k], WTamVar, RadioInterior + RadioCentro + EspacioEntreNiveles, RadioVars, Puntos, Images, PuntosVerticales, ImagesVerticales, PuntosEx, ImagesEx);
        PuntosInteriorVariables.Add(Puntos);
        ImagesInteriorVariables.Add(Images);
        PuntosExteriorVariables.Add(PuntosEx);
        ImagesExteriorVariables.Add(ImagesEx);
        PuntosVerticalesVariables.Add(PuntosVerticales);
        ImagesVerticalesVariables.Add(ImagesVerticales);

        Puntos.Empty();
        Images.Empty();
        PuntosEx.Empty();
        ImagesEx.Empty();
        PuntosVerticales.Empty();
        ImagesVerticales.Empty();
        CreateSeccionCirculo(WInisCentro[k], WTamCentro, RadioInterior, RadioCentro, Puntos, Images, PuntosVerticales, ImagesVerticales, PuntosEx, ImagesEx);
        PuntosInteriorCentro.Add(Puntos);
        ImagesInteriorCentro.Add(Images);
        PuntosExteriorCentro.Add(PuntosEx);
        ImagesExteriorCentro.Add(ImagesEx);
        PuntosVerticalesCentro.Add(PuntosVerticales);
        ImagesVerticalesCentro.Add(ImagesVerticales);
    }
}

void UGraficaCircularVentanaWidget::ActualizarCirculoInterior() {
    for (int k = 0; k < ImagesInteriorVariables.Num(); k++) {
        for (int i = 0; i < SeriesPuntoYearWidgets[k].Num(); i++) {
            UpdateSeccionCirculo(RadioInterior + RadioCentro + RadioVars + 2 * EspacioEntreNiveles, RadioYears, PuntosInteriorYears[k][i], ImagesInteriorYears[k][i], PuntosVerticalesYears[k][i], ImagesVerticalesYears[k][i], PuntosExteriorYears[k][i], ImagesExteriorYears[k][i]);
            for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {
                UpdateSeccionCirculo(RadioInterior + RadioCentro + RadioVars + RadioYears + 3*EspacioEntreNiveles, RadioMonths, PuntosInteriorMonths[k][i][j], ImagesInteriorMonths[k][i][j], PuntosVerticalesMonths[k][i][j], ImagesVerticalesMonths[k][i][j], PuntosExteriorMonths[k][i][j], ImagesExteriorMonths[k][i][j]);
            }
        }
		//tener cuidado el ubicar que radio es cual
        UpdateSeccionCirculo(RadioInterior + RadioCentro + EspacioEntreNiveles, RadioVars, PuntosInteriorVariables[k], ImagesInteriorVariables[k], PuntosVerticalesVariables[k], ImagesVerticalesVariables[k], PuntosExteriorVariables[k], ImagesExteriorVariables[k]);
        UpdateSeccionCirculo(RadioInterior, RadioCentro, PuntosInteriorCentro[k], ImagesInteriorCentro[k], PuntosVerticalesCentro[k], ImagesVerticalesCentro[k], PuntosExteriorCentro[k], ImagesExteriorCentro[k]);
    }
}

void UGraficaCircularVentanaWidget::DeleteCirculoInterior() {
    for (int k = 0; k < ImagesInteriorVariables.Num(); k++) {
        DeleteSeccionCirculo(PuntosInteriorVariables[k], ImagesInteriorVariables[k], PuntosVerticalesVariables[k], ImagesVerticalesVariables[k],PuntosExteriorVariables[k], ImagesExteriorVariables[k]);
        DeleteSeccionCirculo(PuntosInteriorCentro[k], ImagesInteriorCentro[k], PuntosVerticalesCentro[k], ImagesVerticalesCentro[k],PuntosExteriorCentro[k], ImagesExteriorCentro[k]);
        for (int i = 0; i < SeriesPuntoYearWidgets[k].Num(); i++) {
            DeleteSeccionCirculo(PuntosInteriorYears[k][i], ImagesInteriorYears[k][i], PuntosVerticalesYears[k][i], ImagesVerticalesYears[k][i], PuntosExteriorYears[k][i], ImagesExteriorYears[k][i]);
            for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {
                DeleteSeccionCirculo(PuntosInteriorMonths[k][i][j], ImagesInteriorMonths[k][i][j], PuntosVerticalesMonths[k][i][j], ImagesVerticalesMonths[k][i][j], PuntosExteriorMonths[k][i][j], ImagesExteriorMonths[k][i][j]);
            }
            PuntosInteriorMonths[k][i].Empty();//quiza estos no sean necesarios
            ImagesInteriorMonths[k][i].Empty();
            PuntosExteriorMonths[k][i].Empty();//quiza estos no sean necesarios
            ImagesExteriorMonths[k][i].Empty();
            PuntosVerticalesMonths[k][i].Empty();//quiza estos no sean necesarios
            ImagesVerticalesMonths[k][i].Empty();
        }
        PuntosInteriorYears[k].Empty();//quiza estos no sean necesarios
        ImagesInteriorYears[k].Empty();
        PuntosExteriorYears[k].Empty();//quiza estos no sean necesarios
        ImagesExteriorYears[k].Empty();
        PuntosVerticalesYears[k].Empty();//quiza estos no sean necesarios
        ImagesVerticalesYears[k].Empty();
        PuntosInteriorMonths[k].Empty();//quiza estos no sean necesarios
        ImagesInteriorMonths[k].Empty();
        PuntosExteriorMonths[k].Empty();//quiza estos no sean necesarios
        ImagesExteriorMonths[k].Empty();
        PuntosVerticalesMonths[k].Empty();//quiza estos no sean necesarios
        ImagesVerticalesMonths[k].Empty();
    }
    ImagesInteriorCentro.Empty();
    PuntosInteriorCentro.Empty();
    ImagesExteriorCentro.Empty();
    PuntosExteriorCentro.Empty();
    ImagesVerticalesCentro.Empty();
    PuntosVerticalesCentro.Empty();
    ImagesInteriorVariables.Empty();
    PuntosInteriorVariables.Empty();
    ImagesExteriorVariables.Empty();
    PuntosExteriorVariables.Empty();
    ImagesVerticalesVariables.Empty();
    PuntosVerticalesVariables.Empty();
    PuntosInteriorYears.Empty();
    ImagesInteriorYears.Empty();
    PuntosExteriorYears.Empty();
    ImagesExteriorYears.Empty();
    PuntosVerticalesYears.Empty();
    ImagesVerticalesYears.Empty();
    PuntosInteriorMonths.Empty();//quiza estos no sean necesarios
    ImagesInteriorMonths.Empty();
    PuntosExteriorMonths.Empty();//quiza estos no sean necesarios
    ImagesExteriorMonths.Empty();
    PuntosVerticalesMonths.Empty();//quiza estos no sean necesarios
    ImagesVerticalesMonths.Empty();
}

void UGraficaCircularVentanaWidget::CreateSeccionCirculo(float WInicio, float WTamano, float Radio, float RadioTam, TArray<FVector2D> & Puntos, TArray<UImage *> & Images, TArray<FVector2D> & PuntosVerticales, TArray<UImage *> & ImagesVerticales, TArray<FVector2D> & PuntosEx, TArray<UImage *> & ImagesEx) {
    float Delta = WTamano / (Precision-1);
    //TArray<FVector2D> Puntos;
    //TArray<UImage *> Images;
    for (int i = 0; i < Precision; i++) {//estos puntos estaran en coordenadas polare
        Puntos.Add(FVector2D(WInicio + i * Delta, Radio));//no necesito el dario interior, si es igual para todos
        PuntosEx.Add(FVector2D(WInicio + i * Delta, Radio + RadioTam));

        if (i != Precision - 1) {
            UImage * Seccion = NewObject<UImage>(UGameplayStatics::GetGameInstance(GetWorld()), UImage::StaticClass());
            //LineasImagenSerie.Add(LineaImagen);
            //Seccion->SetColorAndOpacity(ColorLineasReferencias);
            Seccion->SetBrushFromTexture(TextureLinea);
            Seccion->ColorAndOpacity = ColorLineasReferencias;
            UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
            UPanelSlot * SlotLinea = RootWidget->AddChild(Seccion);
            UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
            CanvasSlot->SetZOrder(-2);
            CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
            CanvasSlot->SetAlignment(FVector2D(0.5f, 0.25f));
            Seccion->SetRenderTransformPivot(FVector2D(0.5f, 0.25f));
            CanvasSlot->SetPosition(FVector2D(i * 100.0f, 0.0f));
            CanvasSlot->SetSize(FVector2D(100.0f, AnchoLineasReferencias));//que tama�o deberia ponerless
            Images.Add(Seccion);

            Seccion = NewObject<UImage>(UGameplayStatics::GetGameInstance(GetWorld()), UImage::StaticClass());
            //LineasImagenSerie.Add(LineaImagen);
            Seccion->SetBrushFromTexture(TextureLinea);
            Seccion->ColorAndOpacity = ColorLineasReferencias;
            //Seccion->SetColorAndOpacity(ColorLineasReferencias);
            RootWidget = Cast<UPanelWidget>(GetRootWidget());
            SlotLinea = RootWidget->AddChild(Seccion);
            CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
            //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
            CanvasSlot->SetZOrder(-2);
            CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
            CanvasSlot->SetAlignment(FVector2D(0.5f, 0.25f));
            Seccion->SetRenderTransformPivot(FVector2D(0.5f, 0.25f));
            CanvasSlot->SetPosition(FVector2D(i * 100.0f, 0.0f));
            CanvasSlot->SetSize(FVector2D(100.0f, AnchoLineasReferencias));//que tama�o deberia ponerless
            ImagesEx.Add(Seccion);
        }
    }
    PuntosVerticales.Add(FVector2D(WInicio, Radio));
    PuntosVerticales.Add(FVector2D(WInicio, Radio + RadioTam));
    PuntosVerticales.Add(FVector2D(WInicio + WTamano, Radio));
    PuntosVerticales.Add(FVector2D(WInicio + WTamano, Radio + RadioTam));
    for (int i = 0; i < 2; i++) {
        UImage * Seccion = NewObject<UImage>(UGameplayStatics::GetGameInstance(GetWorld()), UImage::StaticClass());
        //LineasImagenSerie.Add(LineaImagen);
        Seccion->SetBrushFromTexture(TextureLinea);
        Seccion->ColorAndOpacity = ColorLineasReferencias;
        //Seccion->SetColorAndOpacity(ColorLineasReferencias);
        UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
        UPanelSlot * SlotLinea = RootWidget->AddChild(Seccion);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotLinea);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        CanvasSlot->SetZOrder(-2);
        CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
        CanvasSlot->SetAlignment(FVector2D(0.5f, 0.5f));
        Seccion->SetRenderTransformPivot(FVector2D(0.5f, 0.5f));
        CanvasSlot->SetPosition(FVector2D(i * 100.0f, 0.0f));
        CanvasSlot->SetSize(FVector2D(100.0f, AnchoLineasReferencias));//que tama�o deberia ponerless
        ImagesVerticales.Add(Seccion);
    }
}

void UGraficaCircularVentanaWidget::UpdateSeccionCirculo(float Radio, float RadioTam, TArray<FVector2D>& Puntos, TArray<UImage*>& Images, TArray<FVector2D> & PuntosVerticales, TArray<UImage *> & ImagesVerticales, TArray<FVector2D> & PuntosEx, TArray<UImage *> & ImagesEx) {
    for (int i = 0; i < Images.Num(); i++) {
        //tomar las imagenes de forma ciclica en los puntos para actualizar
        UCanvasPanelSlot * SeccionCanvasSlot = Cast<UCanvasPanelSlot>(Images[i]->Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        //LineaCanvasSlot->
        Images[i]->SetRenderTransformAngle(0.0f);

        //los puntos los calculo
        FVector2D P1(Radio * Escala * FMath::Cos(Puntos[i].X), Radio * Escala * FMath::Sin(Puntos[i].X));
        FVector2D P2(Radio * Escala * FMath::Cos(Puntos[(i+1) % Precision].X), Radio * Escala * FMath::Sin(Puntos[(i+1) % Precision].X));

        FVector2D Direction = P2 - P1;
        //SeccionCanvasSlot->SetPosition(P1 + Direction/2 - FVector2D(Direction.Size() , AnchoLineasReferencias)/2 + TraslacionOrigen*Escala);// necesito un size linea
        SeccionCanvasSlot->SetPosition(P1 + Direction/2 + TraslacionOrigen*Escala);// necesito un size linea
        SeccionCanvasSlot->SetSize(FVector2D(Direction.Size(), AnchoLineasReferencias));
        Direction = Direction.GetSafeNormal();
        
        float angle = FMath::RadiansToDegrees(FMath::Acos(FVector2D::DotProduct(FVector2D(1, 0), Direction)));
        //float angle = FMath::RadiansToDegrees(acos(FVector2D::DotProduct(FVector2D(0, 1), Direction)));
        float sing = FVector2D::CrossProduct(FVector2D(0, 1), Direction);//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
        if (P2.Y < P1.Y) {
            angle = -angle;
        }
        Images[i]->SetRenderTransformAngle(angle);
    }

    for (int i = 0; i < ImagesEx.Num(); i++) {
        //tomar las imagenes de forma ciclica en los puntos para actualizar
        UCanvasPanelSlot * SeccionCanvasSlot = Cast<UCanvasPanelSlot>(ImagesEx[i]->Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        //LineaCanvasSlot->
        ImagesEx[i]->SetRenderTransformAngle(0.0f);

        //los puntos los calculo
        FVector2D P1((Radio+RadioTam) * Escala * FMath::Cos(Puntos[i].X), (Radio+RadioTam) * Escala * FMath::Sin(Puntos[i].X));
        FVector2D P2((Radio+RadioTam) * Escala * FMath::Cos(Puntos[(i+1) % Precision].X), (Radio+RadioTam) * Escala * FMath::Sin(Puntos[(i+1) % Precision].X));

        FVector2D Direction = P2 - P1;
        //SeccionCanvasSlot->SetPosition(P1 + Direction/2 - FVector2D(Direction.Size() , AnchoLineasReferencias)/2 + TraslacionOrigen*Escala);// necesito un size linea
        SeccionCanvasSlot->SetPosition(P1 + Direction/2 + TraslacionOrigen*Escala);// necesito un size linea
        SeccionCanvasSlot->SetSize(FVector2D(Direction.Size(), AnchoLineasReferencias));
        Direction = Direction.GetSafeNormal();
        
        float angle = FMath::RadiansToDegrees(FMath::Acos(FVector2D::DotProduct(FVector2D(1, 0), Direction)));
        //float angle = FMath::RadiansToDegrees(acos(FVector2D::DotProduct(FVector2D(0, 1), Direction)));
        float sing = FVector2D::CrossProduct(FVector2D(0, 1), Direction);//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
        if (P2.Y < P1.Y) {
            angle = -angle;
        }
        ImagesEx[i]->SetRenderTransformAngle(angle);
    }

    for (int i = 0; i < ImagesVerticales.Num(); i++) {
        //tomar las imagenes de forma ciclica en los puntos para actualizar
        UCanvasPanelSlot * SeccionCanvasSlot = Cast<UCanvasPanelSlot>(ImagesVerticales[i]->Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        //LineaCanvasSlot->
        ImagesVerticales[i]->SetRenderTransformAngle(0.0f);

        //los puntos los calculo
        FVector2D P1(Radio * Escala * FMath::Cos(PuntosVerticales[2*i].X), Radio * Escala * FMath::Sin(PuntosVerticales[2*i].X));
        FVector2D P2((Radio + RadioTam) * Escala * FMath::Cos(PuntosVerticales[2*i+1].X), (Radio + RadioTam) * Escala * FMath::Sin(PuntosVerticales[2*i+1].X));

        FVector2D Direction = P2 - P1;
        //SeccionCanvasSlot->SetPosition(P1 + Direction/2 - FVector2D(Direction.Size() , AnchoLineasReferencias)/2 + TraslacionOrigen*Escala);// necesito un size linea
        SeccionCanvasSlot->SetPosition(P1 + Direction/2 + TraslacionOrigen*Escala);// necesito un size linea
        SeccionCanvasSlot->SetSize(FVector2D(Direction.Size(), AnchoLineasReferencias));
        Direction = Direction.GetSafeNormal();
        
        float angle = FMath::RadiansToDegrees(FMath::Acos(FVector2D::DotProduct(FVector2D(1, 0), Direction)));
        //float angle = FMath::RadiansToDegrees(acos(FVector2D::DotProduct(FVector2D(0, 1), Direction)));
        float sing = FVector2D::CrossProduct(FVector2D(0, 1), Direction);//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
        if (P2.Y < P1.Y) {
            angle = -angle;
        }
        ImagesVerticales[i]->SetRenderTransformAngle(angle);
    }

}

void UGraficaCircularVentanaWidget::DeleteSeccionCirculo(TArray<FVector2D>& Puntos, TArray<UImage*>& Images, TArray<FVector2D> & PuntosVerticales, TArray<UImage *> & ImagesVerticales, TArray<FVector2D> & PuntosEx, TArray<UImage *> & ImagesEx) {
    for (int i = 0; i < Images.Num(); i++) {//estos puntos estaran en coordenadas polare
        Images[i]->RemoveFromParent();
    }
    Images.Empty();
    Puntos.Empty();
    for (int i = 0; i < ImagesEx.Num(); i++) {//estos puntos estaran en coordenadas polare
        ImagesEx[i]->RemoveFromParent();
    }
    ImagesEx.Empty();
    PuntosEx.Empty();
    for (int i = 0; i < ImagesVerticales.Num(); i++) {//estos puntos estaran en coordenadas polare
        ImagesVerticales[i]->RemoveFromParent();
    }
    ImagesVerticales.Empty();
    PuntosVerticales.Empty();
}

void UGraficaCircularVentanaWidget::HideSeccionCirculo(TArray<UImage*>& Images, TArray<UImage*>& ImagesVerticales, TArray<UImage*>& ImagesEx) {
    for (int i = 0; i < Images.Num(); i++) {//estos puntos estaran en coordenadas polare
        Images[i]->SetVisibility(ESlateVisibility::Hidden);
    }
    for (int i = 0; i < ImagesEx.Num(); i++) {//estos puntos estaran en coordenadas polare
        ImagesEx[i]->SetVisibility(ESlateVisibility::Hidden);
    }
    for (int i = 0; i < ImagesVerticales.Num(); i++) {//estos puntos estaran en coordenadas polare
        ImagesVerticales[i]->SetVisibility(ESlateVisibility::Hidden);
    }
}

void UGraficaCircularVentanaWidget::ShowSeccionCirculo(TArray<UImage*>& Images, TArray<UImage*>& ImagesVerticales, TArray<UImage*>& ImagesEx) {
    for (int i = 0; i < Images.Num(); i++) {//estos puntos estaran en coordenadas polare
        Images[i]->SetVisibility(ESlateVisibility::Visible);
    }
    for (int i = 0; i < ImagesEx.Num(); i++) {//estos puntos estaran en coordenadas polare
        ImagesEx[i]->SetVisibility(ESlateVisibility::Visible);
    }
    for (int i = 0; i < ImagesVerticales.Num(); i++) {//estos puntos estaran en coordenadas polare
        ImagesVerticales[i]->SetVisibility(ESlateVisibility::Visible);
    }
}

void UGraficaCircularVentanaWidget::ExplotarPunto(int IdVar, int IdYear, int IdMonth) {//solo explotar el nivel siguiente
    if (IdMonth == -1) {
        if (IdYear == -1) {//solo hay id var, debo oculta o mostrar todos los idva
            //procedo a coultar a��s y meses, y dias
			ShowSeccionCirculo(ImagesInteriorVariables[IdVar], ImagesVerticalesVariables[IdVar], ImagesExteriorVariables[IdVar]);
			ShowTexto(LabelsVar[IdVar]);
			ShowTexto(LabelsRangosYears[IdVar][0]);
			ShowTexto(LabelsRangosYears[IdVar][1]);
            for (int i = 0; i < SeriesPuntoYearWidgets[IdVar].Num(); i++) {
                SeriesPuntoYearWidgets[IdVar][i]->SetVisibility(ESlateVisibility::Visible);
				ShowTexto(LabelsYears[IdVar][i]);//no deberia estar dentro de este for, solo para aprovechar esta aqui
            }
            for (int i = 0; i < SeriesLineaYearWidgets[IdVar].Num(); i++) {
                SeriesLineaYearWidgets[IdVar][i]->SetVisibility(ESlateVisibility::Visible);
            }
        }
        else {
			ShowSeccionCirculo(ImagesInteriorYears[IdVar][IdYear], ImagesVerticalesYears[IdVar][IdYear], ImagesExteriorYears[IdVar][IdYear]);
			//ShowTexto(LabelsYears[IdVar][IdYear]);
			ShowTexto(LabelsRangosMonths[IdVar][IdYear][0]);
			ShowTexto(LabelsRangosMonths[IdVar][IdYear][1]);
            for (int i = 0; i < SeriesPuntoMonthWidgets[IdVar][IdYear].Num(); i++) {
                SeriesPuntoMonthWidgets[IdVar][IdYear][i]->SetVisibility(ESlateVisibility::Visible);
				ShowTexto(LabelsMonths[IdVar][IdYear][i]);
            }
            for (int j = 0; j < SeriesLineaMonthWidgets[IdVar][IdYear].Num(); j++) {
                SeriesLineaMonthWidgets[IdVar][IdYear][j]->SetVisibility(ESlateVisibility::Visible);
            }
        }
    }
	else {
		//tambien mosrtar las series diarias de ese mes
		ShowSeccionCirculo(ImagesInteriorMonths[IdVar][IdYear][IdMonth], ImagesVerticalesMonths[IdVar][IdYear][IdMonth], ImagesExteriorMonths[IdVar][IdYear][IdMonth]);
		//ShowTexto(LabelsMonths[IdVar][IdYear][IdMonth]);
		ShowTexto(LabelsRangosDay[IdVar][IdYear][IdMonth][0]);
		ShowTexto(LabelsRangosDay[IdVar][IdYear][IdMonth][1]);
		for (int i = 0; i < SeriesPuntoDayWidgets[IdVar][IdYear][IdMonth].Num(); i++) {
			SeriesPuntoDayWidgets[IdVar][IdYear][IdMonth][i]->SetVisibility(ESlateVisibility::Visible);
		}
		for (int i = 0; i < SeriesLineaDayWidgets[IdVar][IdYear][IdMonth].Num(); i++) {
			SeriesLineaDayWidgets[IdVar][IdYear][IdMonth][i]->SetVisibility(ESlateVisibility::Visible);
		}
		for (int i = 0; i < LabelsDay[IdVar][IdYear][IdMonth].Num(); i++) {
			ShowTexto(LabelsDay[IdVar][IdYear][IdMonth][i]);
		}
	}
}

void UGraficaCircularVentanaWidget::ContraerPunto(int IdVar, int IdYear, int IdMonth) {//ocultar todos los niveles siguientes
    if (IdMonth == -1) {
        if (IdYear == -1) {//solo hay id var, debo oculta o mostrar todos los idva
            //procedo a coultar a��s y meses, y dias
			HideSeccionCirculo(ImagesInteriorVariables[IdVar], ImagesVerticalesVariables[IdVar], ImagesExteriorVariables[IdVar]);
			HideTexto(LabelsRangosYears[IdVar][0]);
			HideTexto(LabelsRangosYears[IdVar][1]);
            for (int i = 0; i < SeriesPuntoYearWidgets[IdVar].Num(); i++) {
                SeriesPuntoYearWidgets[IdVar][i]->SetVisibility(ESlateVisibility::Hidden);
				HideSeccionCirculo(ImagesInteriorYears[IdVar][i], ImagesVerticalesYears[IdVar][i], ImagesExteriorYears[IdVar][i]);
				HideTexto(LabelsYears[IdVar][i]);
				HideTexto(LabelsRangosMonths[IdVar][i][0]);
				HideTexto(LabelsRangosMonths[IdVar][i][1]);
                for (int j = 0; j < SeriesPuntoMonthWidgets[IdVar][i].Num(); j++) {
                    SeriesPuntoMonthWidgets[IdVar][i][j]->SetVisibility(ESlateVisibility::Hidden);
					HideSeccionCirculo(ImagesInteriorMonths[IdVar][i][j], ImagesVerticalesMonths[IdVar][i][j], ImagesExteriorMonths[IdVar][i][j]);
					HideTexto(LabelsMonths[IdVar][i][j]);
					HideTexto(LabelsRangosDay[IdVar][i][j][0]);
					HideTexto(LabelsRangosDay[IdVar][i][j][1]);
					for (int k = 0; k < SeriesPuntoDayWidgets[IdVar][i][j].Num(); k++) {
						SeriesPuntoDayWidgets[IdVar][i][j][k]->SetVisibility(ESlateVisibility::Hidden);
					}
					for (int k = 0; k < SeriesLineaDayWidgets[IdVar][i][j].Num(); k++) {
						SeriesLineaDayWidgets[IdVar][i][j][k]->SetVisibility(ESlateVisibility::Hidden);
					}
					for (int k = 0; k < LabelsDay[IdVar][i][j].Num(); k++) {
						HideTexto(LabelsDay[IdVar][i][j][k]);
					}
                }
                for (int j = 0; j < SeriesLineaMonthWidgets[IdVar][i].Num(); j++) {
                    SeriesLineaMonthWidgets[IdVar][i][j]->SetVisibility(ESlateVisibility::Hidden);
                }
            }
            for (int i = 0; i < SeriesLineaYearWidgets[IdVar].Num(); i++) {
                SeriesLineaYearWidgets[IdVar][i]->SetVisibility(ESlateVisibility::Hidden);
            }
        }
        else {
			HideSeccionCirculo(ImagesInteriorYears[IdVar][IdYear], ImagesVerticalesYears[IdVar][IdYear], ImagesExteriorYears[IdVar][IdYear]);
			//HideTexto(LabelsYears[IdVar][IdYear]);
			HideTexto(LabelsRangosMonths[IdVar][IdYear][0]);
			HideTexto(LabelsRangosMonths[IdVar][IdYear][1]);
            for (int i = 0; i < SeriesPuntoMonthWidgets[IdVar][IdYear].Num(); i++) {
                SeriesPuntoMonthWidgets[IdVar][IdYear][i]->SetVisibility(ESlateVisibility::Hidden);
            }
            for (int j = 0; j < SeriesLineaMonthWidgets[IdVar][IdYear].Num(); j++) {
                SeriesLineaMonthWidgets[IdVar][IdYear][j]->SetVisibility(ESlateVisibility::Hidden);
            }
            for (int i = 0; i < ImagesInteriorMonths[IdVar][IdYear].Num(); i++) {
                HideSeccionCirculo(ImagesInteriorMonths[IdVar][IdYear][i], ImagesVerticalesMonths[IdVar][IdYear][i], ImagesExteriorMonths[IdVar][IdYear][i]);
				HideTexto(LabelsMonths[IdVar][IdYear][i]);
				HideTexto(LabelsRangosDay[IdVar][IdYear][i][0]);
				HideTexto(LabelsRangosDay[IdVar][IdYear][i][1]);
				for (int j = 0; j < SeriesPuntoDayWidgets[IdVar][IdYear][i].Num(); j++) {
					SeriesPuntoDayWidgets[IdVar][IdYear][i][j]->SetVisibility(ESlateVisibility::Hidden);
				}
				for (int j = 0; j < SeriesLineaDayWidgets[IdVar][IdYear][i].Num(); j++) {
					SeriesLineaDayWidgets[IdVar][IdYear][i][j]->SetVisibility(ESlateVisibility::Hidden);
				}
				for (int j = 0; j < LabelsDay[IdVar][IdYear][i].Num(); j++) {
					HideTexto(LabelsDay[IdVar][IdYear][i][j]);
				}
            }
        }
    }
	else {
		//tambien mosrtar las series diarias de ese mes
		HideSeccionCirculo(ImagesInteriorMonths[IdVar][IdYear][IdMonth], ImagesVerticalesMonths[IdVar][IdYear][IdMonth], ImagesExteriorMonths[IdVar][IdYear][IdMonth]);
		//HideTexto(LabelsMonths[IdVar][IdYear][IdMonth]);
		HideTexto(LabelsRangosDay[IdVar][IdYear][IdMonth][0]);
		HideTexto(LabelsRangosDay[IdVar][IdYear][IdMonth][1]);
		for (int i = 0; i < SeriesPuntoDayWidgets[IdVar][IdYear][IdMonth].Num(); i++) {
			SeriesPuntoDayWidgets[IdVar][IdYear][IdMonth][i]->SetVisibility(ESlateVisibility::Hidden);
		}
		for (int i = 0; i < SeriesLineaDayWidgets[IdVar][IdYear][IdMonth].Num(); i++) {
			SeriesLineaDayWidgets[IdVar][IdYear][IdMonth][i]->SetVisibility(ESlateVisibility::Hidden);
		}
		for (int i = 0; i < LabelsDay[IdVar][IdYear][IdMonth].Num(); i++) {
			HideTexto(LabelsDay[IdVar][IdYear][IdMonth][i]);
		}
	}
}

void UGraficaCircularVentanaWidget::CreateTextos() {
    for (int k = 0; k < SeriesPuntoVarWidgets.Num(); k++) {
		LabelsYears.Add(TArray<UTextBlock *>());
		LabelsMonths.Add(TArray<TArray<UTextBlock *>>());
		LabelsDay.Add(TArray<TArray<TArray<UTextBlock *>>>());
        for (int i = 0; i < SeriesPuntoYearWidgets[k].Num(); i++) {
			LabelsMonths[k].Add(TArray<UTextBlock *>());
			LabelsDay[k].Add(TArray<TArray<UTextBlock *>>());
            for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {//espero funcione
				LabelsDay[k][i].Add(TArray<UTextBlock *>());
				//iniciando el poner dias
				//primer dia y ultimo dia
				UTextBlock * Texto = CreateTexto(WInisVar[k] + WInisYears[i]  + WInisMonths[j], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + RadioMonths + 3 * EspacioEntreNiveles, 10);
				FNumberFormattingOptions Formato;
				Formato.UseGrouping = false;
				Texto->SetText(FText::AsNumber(1, &Formato));
				LabelsDay[k][i][j].Add(Texto);
				//
				Texto = CreateTexto(WInisVar[k] + WInisYears[i]  + WInisMonths[j] + WTamsMonths[j], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + RadioMonths + 3 * EspacioEntreNiveles, 10);
				Texto->SetText(FText::AsNumber(CantidadDiasMes[Consulta.MesesConsultados[j]], &Formato));
				LabelsDay[k][i][j].Add(Texto);
				//terminando de poner los dias
				//for (int r = 0; r < SeriesPuntoDayWidgets[k][i][j].Num(); r++) {
					//UTextBlock * Texto = CreateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + 3 * EspacioEntreNiveles, 10.0f);
					//LabelsDay[k][i][j].Add(Texto);
				//}
				Texto = CreateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j] + WTamsMonths[j] / 2, 0.0f, RadioInterior + RadioCentro + RadioVars + 2*EspacioEntreNiveles, 10);
				Texto->SetText(FText::FromString(AbrevMonths[Consulta.MesesConsultados[j]]));
				LabelsMonths[k][i].Add(Texto);
            }
			UTextBlock * Texto = CreateTexto(WInisVar[k] + WInisYears[i] + WTamYear / 2, 0.0f, RadioInterior + RadioCentro + EspacioEntreNiveles, 10);
            FNumberFormattingOptions Formato;
            Formato.UseGrouping = false;
            Texto->SetText(FText::AsNumber(i + Consulta.InitialYear, &Formato));
			LabelsYears[k].Add(Texto);
        }
        //para el de las variables
		UTextBlock * Texto = CreateTexto(WInisVar[k] + WTamVar/2, 0.0f, RadioInterior, 10);
		Texto->SetText(FText::FromString(VariablesAbrev[Variables.Find(Consulta.VariablesConsultadas[k])]));
		LabelsVar.Add(Texto);
    }

}

void UGraficaCircularVentanaWidget::ActualizarTextos() {
    for (int k = 0; k < SeriesPuntoVarWidgets.Num(); k++) {
        for (int i = 0; i < SeriesPuntoYearWidgets[k].Num(); i++) {
            for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {//espero funcione
				//primer dia y ultimo dia
				UpdateTexto(WInisVar[k] + WInisYears[i]  + WInisMonths[j], RadioInterior + RadioCentro + RadioVars + RadioYears + 3 * EspacioEntreNiveles, 10, LabelsDay[k][i][j][0]);
				UpdateTexto(WInisVar[k] + WInisYears[i]  + WInisMonths[j] + WTamsMonths[j], RadioInterior + RadioCentro + RadioVars + RadioYears + 3 * EspacioEntreNiveles, 10, LabelsDay[k][i][j][1]);
				//
				//for (int r = 0; r < SeriesPuntoDayWidgets[k][i][j].Num(); r++) {
					//UTextBlock * Texto = CreateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + 3 * EspacioEntreNiveles, 10.0f);
					//LabelsDay[k][i][j].Add(Texto);
				//}
				UpdateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j] + WTamsMonths[j] / 2, RadioInterior + RadioCentro + RadioVars + 2*EspacioEntreNiveles, 10, LabelsMonths[k][i][j]);
				//Texto->SetText(FText::FromString(AbrevMonths[Consulta.MesesConsultados[j]]));
            }
			UpdateTexto(WInisVar[k] + WInisYears[i] + WTamYear / 2, RadioInterior + RadioCentro + EspacioEntreNiveles, 10, LabelsYears[k][i]);
        }
        //para el de las variables
		UpdateTexto(WInisVar[k] + WTamVar/2, RadioInterior, 10, LabelsVar[k]);
    }
}

void UGraficaCircularVentanaWidget::DeleteTextos() {
    for (int k = 0; k < LabelsVar.Num(); k++) {
        for (int i = 0; i < LabelsYears[k].Num(); i++) {
            for (int j = 0; j < LabelsMonths[k][i].Num(); j++) {//espero funcione
				DeleteTexto(LabelsDay[k][i][j][0]);
				DeleteTexto(LabelsDay[k][i][j][1]);
				//for (int r = 0; r < LabelsDay[k][i][j].Num(); r++) {
					//UTextBlock * Texto = CreateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + 3 * EspacioEntreNiveles, 10.0f);
					//LabelsDay[k][i][j].Add(Texto);
				//}
				LabelsDay[k][i][j].Empty();
				DeleteTexto(LabelsMonths[k][i][j]);
				//Texto->SetText(FText::FromString(AbrevMonths[Consulta.MesesConsultados[j]]));
            }
			LabelsMonths[k][i].Empty();
			LabelsDay[k][i].Empty();
			DeleteTexto(LabelsYears[k][i]);
        }
		LabelsMonths[k].Empty();
		LabelsDay[k].Empty();
		LabelsYears[k].Empty();
        //para el de lasvariables
		DeleteTexto(LabelsVar[k]);
    }
	LabelsVar.Empty();
	LabelsYears.Empty();
	LabelsMonths.Empty();
	LabelsDay.Empty();
}

UTextBlock * UGraficaCircularVentanaWidget::CreateTexto(float WInicio, float WTamano, float Radio, int Size) {//falta el array don de lo agregare, o tal vez no
	UTextBlock * Texto = NewObject<UTextBlock>(UGameplayStatics::GetGameInstance(GetWorld()), UTextBlock::StaticClass());
	//TextosEjeY.Add(Texto);
	//LineaEjeX->SetBrushFromTexture(TextureLinea);
	UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
	UPanelSlot * SlotTexto = RootWidget->AddChild(Texto);
	UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotTexto);
	//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
	CanvasSlot->SetZOrder(-2);
	CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
	CanvasSlot->SetAlignment(FVector2D(0.5f, 0.0f));
	//calcular lasposiciones
	float X = Radio * FMath::Cos(WInicio);//conversion de polares a cartesianas
	float Y = Radio * FMath::Sin(WInicio);
	CanvasSlot->SetPosition(FVector2D(X, Y));
	CanvasSlot->SetAutoSize(true);
	//CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless
	FSlateFontInfo Fuente = Texto->Font;
	Fuente.Size = Size;
	Fuente.TypefaceFontName = FName("Regular");
	Texto->SetFont(Fuente);
	//Texto->SetText(FText::FromString(Etiqueta));//que texto?
	Texto->SetRenderTransformPivot(FVector2D(0.5f, 0.0f));
	Texto->SetRenderTransformAngle(FMath::RadiansToDegrees(WInicio + PI/2));
	Texto->SetJustification(ETextJustify::Center);
	return Texto;
}

void UGraficaCircularVentanaWidget::UpdateTexto(float WInicio, float Radio, int Size, UTextBlock * Texto) {
	Texto->SetRenderTransformAngle(0.0f);
	//UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
	//UPanelSlot * SlotTexto = RootWidget->AddChild(Texto);
	UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
	//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
	//CanvasSlot->SetZOrder(-2);
	//CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
	//CanvasSlot->SetAlignment(FVector2D(0.5f, 0.0f));
	//calcular lasposiciones
	float X = (Radio - Size)*Escala * FMath::Cos(WInicio);//conversion de polares a cartesianas
	float Y = (Radio - Size)*Escala * FMath::Sin(WInicio);
	CanvasSlot->SetPosition(FVector2D(X, Y) + TraslacionOrigen*Escala);
	//CanvasSlot->SetAutoSize(true);
	//CanvasSlot->SetSize(FVector2D(100.0f, SizeLines));//que tama�o deberia ponerless
	//FSlateFontInfo Fuente = Texto->Font;
	//Fuente.Size = 10;
	//Fuente.TypefaceFontName = FName("Regular");
	//Texto->SetFont(Fuente);
	//Texto->SetRenderTransformPivot(FVector2D(0.5f, 0.0f));
	Texto->SetRenderTransformAngle(FMath::RadiansToDegrees(WInicio + PI/2));
	//Texto->SetJustification(ETextJustify::Center);
}

void UGraficaCircularVentanaWidget::DeleteTexto(UTextBlock * Texto) {
	Texto->RemoveFromParent();
}

void UGraficaCircularVentanaWidget::HideTexto(UTextBlock * Texto) {
	Texto->SetVisibility(ESlateVisibility::Hidden);
}

void UGraficaCircularVentanaWidget::ShowTexto(UTextBlock * Texto) {
	Texto->SetVisibility(ESlateVisibility::Visible);
}

void UGraficaCircularVentanaWidget::CreateRangos() {
    for (int k = 0; k < SeriesPuntoVarWidgets.Num(); k++) {
		LabelsRangosVar.Add(TArray<UTextBlock *>());
		LabelsRangosYears.Add(TArray<UTextBlock *>());
		LabelsRangosMonths.Add(TArray<TArray<UTextBlock *>>());
		LabelsRangosDay.Add(TArray<TArray<TArray<UTextBlock *>>>());
        for (int i = 0; i < SeriesPuntoYearWidgets[k].Num(); i++) {
			LabelsRangosMonths[k].Add(TArray<UTextBlock *>());
			LabelsRangosDay[k].Add(TArray<TArray<UTextBlock *>>());
            for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {//espero funcione
				LabelsRangosDay[k][i].Add(TArray<UTextBlock *>());

				FNumberFormattingOptions Formato;
				Formato.UseGrouping = false;
				UTextBlock * Texto = CreateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + 3*EspacioEntreNiveles, 8);
				Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin, &Formato));
				UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
				CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
				Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
				LabelsRangosDay[k][i][j].Add(Texto);

				Texto = CreateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + RadioMonths + 3*EspacioEntreNiveles, 8);
				Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax, &Formato));
				CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
				CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
				Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
				LabelsRangosDay[k][i][j].Add(Texto);
            }
			UTextBlock * Texto = CreateTexto(WInisVar[k] + WInisYears[i], 0.0f, RadioInterior + RadioCentro + RadioVars + 2 * EspacioEntreNiveles, 8);
            FNumberFormattingOptions Formato;
            Formato.UseGrouping = false;
			Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin, &Formato));
			UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
			CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
			Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
			LabelsRangosMonths[k][i].Add(Texto);

			Texto = CreateTexto(WInisVar[k] + WInisYears[i], 0.0f, RadioInterior + RadioCentro + RadioVars + RadioYears + 2 * EspacioEntreNiveles, 8);
			Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax, &Formato));
			CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
			CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
			Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
			LabelsRangosMonths[k][i].Add(Texto);
			//a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        }
        //para el de las variables
		FNumberFormattingOptions Formato;
		Formato.UseGrouping = false;
		UTextBlock * Texto = CreateTexto(WInisVar[k], 0.0f, RadioInterior, 8);
		Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin, &Formato));
		UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
		CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
		Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
		LabelsRangosVar[k].Add(Texto);

		Texto = CreateTexto(WInisVar[k], 0.0f, RadioInterior + RadioCentro, 8);
		Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax, &Formato));
		CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
		CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
		Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
		LabelsRangosVar[k].Add(Texto);

		Texto = CreateTexto(WInisVar[k], 0.0f, RadioInterior + RadioCentro + EspacioEntreNiveles, 8);
		Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMin, &Formato));
		CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
		CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
		Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
		LabelsRangosYears[k].Add(Texto);

		Texto = CreateTexto(WInisVar[k], 0.0f, RadioInterior + RadioCentro + RadioVars + EspacioEntreNiveles, 8);
		Texto->SetText(FText::AsNumber(Consulta.StationConsultas[0].ConsultasPorVariable[k].YMax, &Formato));
		CanvasSlot = Cast<UCanvasPanelSlot>(Texto->Slot);
		CanvasSlot->SetAlignment(FVector2D(1.0f, 1.0f));
		Texto->SetRenderTransformPivot(FVector2D(1.0f, 1.0f));
		LabelsRangosYears[k].Add(Texto);
    }
}

void UGraficaCircularVentanaWidget::ActualizarRangos() {
    for (int k = 0; k < SeriesPuntoVarWidgets.Num(); k++) {
        for (int i = 0; i < SeriesPuntoYearWidgets[k].Num(); i++) {
            for (int j = 0; j < SeriesPuntoMonthWidgets[k][i].Num(); j++) {//espero funcione
				UpdateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j], RadioInterior + RadioCentro + RadioVars + RadioYears + 3*EspacioEntreNiveles + 8, 8, LabelsRangosDay[k][i][j][0]);
				UpdateTexto(WInisVar[k] + WInisYears[i] + WInisMonths[j], RadioInterior + RadioCentro + RadioVars + RadioYears + RadioMonths + 3*EspacioEntreNiveles - 8, 8, LabelsRangosDay[k][i][j][1]);
            }
			UpdateTexto(WInisVar[k] + WInisYears[i], RadioInterior + RadioCentro + RadioVars + 2*EspacioEntreNiveles + 8, 8, LabelsRangosMonths[k][i][0]);
			UpdateTexto(WInisVar[k] + WInisYears[i], RadioInterior + RadioCentro + RadioVars + RadioYears + 2*EspacioEntreNiveles - 8, 8, LabelsRangosMonths[k][i][1]);
        }
		UpdateTexto(WInisVar[k], RadioInterior + 8, 8, LabelsRangosVar[k][0]);
		UpdateTexto(WInisVar[k], RadioInterior + RadioCentro - 8, 8, LabelsRangosVar[k][1]);
		UpdateTexto(WInisVar[k], RadioInterior + RadioCentro + EspacioEntreNiveles + 8, 8, LabelsRangosYears[k][0]);
		UpdateTexto(WInisVar[k], RadioInterior + RadioCentro + RadioVars + EspacioEntreNiveles - 8, 8, LabelsRangosYears[k][1]);
    }
}

void UGraficaCircularVentanaWidget::DeleteRangos() {
    for (int k = 0; k < LabelsRangosVar.Num(); k++) {
        for (int i = 0; i < LabelsRangosMonths[k].Num(); i++) {
            for (int j = 0; j < LabelsRangosDay[k][i].Num(); j++) {//espero funcione
				for (int r = 0; r < LabelsRangosDay.Num(); r++) {
					DeleteTexto(LabelsRangosDay[k][i][j][r]);
				}
				LabelsRangosDay[k][i][j].Empty();
            }
			LabelsRangosDay[k][i].Empty();
			for (int r = 0; r < LabelsRangosMonths[k][i].Num(); r++) {
				DeleteTexto(LabelsRangosMonths[k][i][r]);
			}
			LabelsRangosMonths[k][i].Empty();
        }
		LabelsRangosDay[k].Empty();
		LabelsRangosMonths[k].Empty();
		LabelsRangosYears[k].Empty();
        //para el de lasvariables
		for (int r = 0; r < LabelsRangosYears.Num(); r++) {
			DeleteTexto(LabelsRangosYears[k][r]);
		}
		for (int r = 0; r < LabelsRangosVar.Num(); r++) {
			DeleteTexto(LabelsRangosVar[k][r]);
		}
		LabelsRangosVar[k].Empty();
    }
	LabelsRangosDay.Empty();
	LabelsRangosVar.Empty();
	LabelsRangosYears.Empty();
	LabelsRangosMonths.Empty();
}
