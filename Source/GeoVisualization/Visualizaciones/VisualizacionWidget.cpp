// Fill out your copyright notice in the Description page of Project Settings.

#include "VisualizacionWidget.h"

UVisualizacionWidget::UVisualizacionWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    MesesFiltrados.SetNum(12);
    for (int i = 0; i < MesesFiltrados.Num(); i++) {
        MesesFiltrados[i] = true;
    }
}

void UVisualizacionWidget::NativeConstruct() {
    Super::NativeConstruct();
}

void UVisualizacionWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
}


void UVisualizacionWidget::VisualizarConsulta_Implementation() {
}

void UVisualizacionWidget::IncluirMes_Implementation(int IdMes) {
    MesesFiltrados[IdMes] = true;
}

void UVisualizacionWidget::ExcluirMes_Implementation(int IdMes) {
    MesesFiltrados[IdMes] = false;
}

void UVisualizacionWidget::IncluirYear_Implementation(int IdYear) {
    YearsFiltrados[IdYear] = true;
}

void UVisualizacionWidget::ExcluirYear_Implementation(int IdYear) {
    YearsFiltrados[IdYear] = false;
}

void UVisualizacionWidget::IncluirVariable_Implementation(int IdVariable) {
    //deberia tener un array de variables filtradas para tenerlas en cuenta o no al momento de calcular las mtrices de distancias en el arbolito, au que esto podria ser solo de ese visualizacion
}

void UVisualizacionWidget::ExcluirVariable_Implementation(int IdVariable) {
}

void UVisualizacionWidget::IncluirStation_Implementation(int IdStation) {
    //necesito si el filtro de estaciones
    StationsFiltradas.AddUnique(IdStation);
    StationsFiltradas.Sort();
}

void UVisualizacionWidget::ExcluirStation_Implementation(int IdStation) {
    StationsFiltradas.Remove(IdStation);
}

void UVisualizacionWidget::PrepararYearsFiltrados() {
    YearsFiltrados.SetNum(Consulta.FinalYear - Consulta.InitialYear + 1);
    for (int i = 0; i < YearsFiltrados.Num(); i++) {
        YearsFiltrados[i] = true;//todo estara seleccionado inicialmente
    }

    for(int i = 0; i < Consulta.StationConsultas.Num(); i++){
        StationsFiltradas.AddUnique(i);
    }
}

FText UVisualizacionWidget::GetTitleWindowInformation_Implementation() {
    return FText();
}

FText UVisualizacionWidget::GetTitleDetailPanelInformation_Implementation() {
	return FText();
}

FText UVisualizacionWidget::GetSubtitleDetailPanelInformation_Implementation() {
	return FText();
}

