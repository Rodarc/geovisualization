// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PuntoGrafica.h"
#include "LineaGrafica.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API ULineaGrafica : public UUserWidget
{
	GENERATED_BODY()
	
public:
    ULineaGrafica(const FObjectInitializer & ObjectInitializer);

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTime) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorLinea;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float Ancho;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdLinea;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UPuntoGrafica * PuntoInicio;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UPuntoGrafica * PuntoFinal;
	
    void Actualizar();
	
	
	
};
