// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "../Estructuras/DailyDataStruct.h"
#include "PuntoGrafica.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UPuntoGrafica : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UPuntoGrafica(const FObjectInitializer & ObjectInitializer);

    //virtual void NativePaint(FPaintContext & InContext) const override;

    //virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorFondoPunto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorLineaPunto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USlateBrushAsset * BrushFondoPunto;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdSerie;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdPunto;//id en el array de puntos

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdData;//id en el array de datos

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdDay;//id del dia al que pertenece

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdMonth;//ide del mes al que pertenece

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdYear;//id del a�o al que pertenece

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    class UVisualizacionWidget * Visualizacion;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    FDailyDataStruct GetDataStruct();
	
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetColor(FLinearColor NewColor);

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    float GetPromedioVar();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    float GetPromedioYear();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica Circular")
    bool Explotado;

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    float GetPromedioMonth();

    UFUNCTION(BlueprintCallable, Category = "Grafica")
    float GetValueDay();
};

//si tien todo los ID, o si tiene un IdDia, entonces es un punto diario,
//si tiene todos los ID menos de los dias, entonces es un punto de mes,
//Si tiene solo el IdYear, entonces es un punto de year