// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraficaLineaWidget.h"
#include "PuntoGrafica.h"
#include "LineaGrafica.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Engine/Texture2D.h"
#include "VisualizacionVentanaInterface.h"
#include "GraficaLineaVentanaWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaLineaVentanaWidget : public UGraficaLineaWidget, public IVisualizacionVentanaInterface
{
	GENERATED_BODY()
	
public:
    UGraficaLineaVentanaWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual int32 NativePaint(const FPaintArgs & Args, const FGeometry & AllottedGeometry, const FSlateRect & MyCullingRect, FSlateWindowElementList & OutDrawElements, int32 LayerId, const FWidgetStyle & InWidgetStyle, bool bParentEnabled) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
    virtual void CalcularCorners() override;

    virtual void CalcularPuntosDibujar() override;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D SizePoints;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float SizeLines;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<UPuntoGrafica> TypePunto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    TSubclassOf<ULineaGrafica> TypeLinea;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    UTexture2D * TextureLinea;

    TArray<TArray<UPuntoGrafica *>> SeriesPuntoWidgets;

    TArray<TArray<ULineaGrafica *>> SeriesLineaWidgets;

    TArray<TArray<UPuntoGrafica * >> PuntosYears;//la primera dimension es la serie a la que pertenece

    TArray<TArray<ULineaGrafica * >> LineasYears;//la primera dimension es la serie a la que pertenece

    TArray<TArray<TArray<UPuntoGrafica * >>> PuntosMonths;

    TArray<TArray<TArray<ULineaGrafica * >>> LineasMonths;

    TArray<UImage *> SeriesLineasImage;//este lo usare por si quiero crear una especie de grilla de fondo

    UImage * LineaEjeX;

    UImage * LineaEjeY;

    TArray<UTextBlock *> TextosEjeX;

    TArray<UTextBlock *> TextosEjeY;
	
    virtual void SetSerie(int IdSerie, TArray<FVector2D> Points) override;

    virtual void SetColorSerie(int IdSerie, FLinearColor ColorSerie) override;

    virtual int AddSerie(TArray<FVector2D> Points) override;//retorna el id de la serie en la grafica

    virtual void HideSerie(int IdSerie) override;

    virtual void ShowSerie(int IdSerie) override;

    virtual void ClearSeries() override;
	
    virtual void VisualizarConsulta_Implementation() override;

    void ActualizarGrafica();//deberia ser actualzar graficas

    void CreateAxis();

    void ClearAxis();

    void CreateLeyendaAxis();

    void ClearLeyendaAxis();
	
    virtual void ZoomIn_Implementation() override;

    virtual void ZoomOut_Implementation() override;

    virtual void Reestablecer_Implementation() override;
	
};
