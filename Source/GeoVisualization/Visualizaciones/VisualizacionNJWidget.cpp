// Fill out your copyright notice in the Description page of Project Settings.

#include "VisualizacionNJWidget.h"
#include <fstream>
#include <iostream>
#include <stack>


UVisualizacionNJWidget::UVisualizacionNJWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
    DistanciaArista = 25.0f;
}


void UVisualizacionNJWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);
}

int32 UVisualizacionNJWidget::NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect, FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const {
    return Super::NativePaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
}


void UVisualizacionNJWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
}

void UVisualizacionNJWidget::CreateNodos() {
    //esta creacion de nodos es para el generar elementos visualizes
}


void UVisualizacionNJWidget::CreateAristas() {
}

void UVisualizacionNJWidget::Layout() {
    //el layout no corre sobre los elementos visuales, si no sobre los Nodos del NJ, esta clase tendra un miembro NJ

    //parece que solo necesito la cantidad de hojas, pero siempre es mejor tener todos los datos
    TQueue<Nodo *> Cola;
    //la raiz es el ultimo nodo
    Nodo * Root = ArbolGenerado[TamArbolGenerado - 1];
    Calculos2();
    Calc();//no estaba antes
    //agregado para el nuevo radio
    //int NivelDenso, CantidadNodosNivelDenso;
    //NivelMasDenso(NivelDenso, CantidadNodosNivelDenso);
    //NewRadio = EncontrarRadio1(DeltaPhi * NivelDenso, CantidadNodosNivelDenso); 
    //
    Root->Theta = 0;
    Root->Phi = 0;
    Root->WTam = 2*PI;
    Root->WInicio = 0;
    Root->X = 0;
    Root->Y = 0;
    //UE_LOG(LogClass, Log, TEXT("Root id = %d, (%f,%f,%f)"), Root->Id, Root->Xcoordinate, Root->Ycoordinate, Root->Zcoordinate);
    //float DeltaPhi = PI / Root->Altura;
    float WTemp = Root->WInicio;
    //debo tener en cuenta al padre para hacer los calculos, ya que esto esta como arbol sin raiz

    //Root->Parent->Phi = Root->Phi + DeltaPhi;//estaba dividido /2
    Root->Padre->WTam = 2*PI * (float(Root->Padre->Hojas) / Root->Hojas);
    Root->Padre->WInicio = WTemp;
    Root->Padre->Theta = Root->Padre->WInicio + Root->Padre->WTam / 2;
    Root->Padre->X = Root->X + DistanciaArista * FMath::Cos(Root->Padre->Theta);
    Root->Padre->Y = Root->Y + DistanciaArista * FMath::Sin(Root->Padre->Theta);
    WTemp += Root->Padre->WTam;
    Cola.Enqueue(Root->Padre);
    for (int i = 0; i < 2; i++) {//ir por los hijos
        //Root->Sons[i]->Phi = Root->Phi + DeltaPhi;//estaba dividido por 2
        Root->Hijos[i]->WTam = 2*PI * (float(Root->Hijos[i]->Hojas) / Root->Hojas);
        Root->Hijos[i]->WInicio = WTemp;
        Root->Hijos[i]->Theta = Root->Hijos[i]->WInicio + Root->Hijos[i]->WTam / 2;
        Root->Hijos[i]->X = Root->X + DistanciaArista * FMath::Cos(Root->Hijos[i]->Theta);
        Root->Hijos[i]->Y = Root->Y + DistanciaArista * FMath::Sin(Root->Hijos[i]->Theta);
        WTemp += Root->Hijos[i]->WTam;
        Cola.Enqueue(Root->Hijos[i]);
    }

    while (!Cola.IsEmpty()) {
        Nodo * V;
        Cola.Dequeue(V);
        WTemp = V->WInicio;
        if (V->Hijos[0]) {
            for (int i = 0; i < 2; i++) {
                V->Hijos[i]->WTam = 2*PI * (float(V->Hijos[i]->Hojas) / Root->Hojas);
                V->Hijos[i]->WInicio = WTemp;
                V->Hijos[i]->Theta = V->Hijos[i]->WInicio + V->Hijos[i]->WTam / 2;
                V->Hijos[i]->X = V->X + DistanciaArista * FMath::Cos(V->Hijos[i]->Theta);
                V->Hijos[i]->Y = V->Y + DistanciaArista * FMath::Sin(V->Hijos[i]->Theta);
                WTemp += V->Hijos[i]->WTam;
                Cola.Enqueue(V->Hijos[i]);
            }
        }
    }
}

void UVisualizacionNJWidget::ActualizarLayout() {
}

void UVisualizacionNJWidget::Calculos(Nodo * V) {//lo uso dentro de claculos 2, por alguna razon
    //calcular hojas, altura,
    if (!V->Hijos[0]) {//deberia usar si es virtual o no
        V->Hojas = 1;
        V->Altura = 0;
    }
    else {
        V->Hojas = 0;
        V->Altura = 0;
        if (V->Hijos[0]) {
            for (int i = 0; i < 2; i++) {//solo tienen dos hijos
                Calculos(V->Hijos[i]);
                V->Hojas += V->Hijos[i]->Hojas;//tal ve no necesite calcular
                V->Altura = FMath::Max<float>(V->Altura, V->Hijos[i]->Altura);
            }
        }
        V->Altura++;
    }
    //si el arbol tiene 4 niveles, el valor de altura de la raiz es 3
}

void UVisualizacionNJWidget::Calculos2() {//calcula hojas y altura, de otra forma
    Nodo * Root = ArbolGenerado[TamArbolGenerado - 1];
    Calculos(Root->Padre);
    Root->Altura = Root->Padre->Altura;
    Root->Hojas = Root->Padre->Hojas;
    for (int i = 0; i < 2; i++) {
        Calculos(Root->Hijos[i]);
        Root->Hojas += Root->Hijos[i]->Hojas;
        Root->Altura = FMath::Max<float>(Root->Altura, Root->Hijos[i]->Altura);
    }
    Root->Altura++;
}

void UVisualizacionNJWidget::Calc() {//para hallar niveles

    stack<Nodo *> pila;
    //la raiz es el ultimo nodo
    Nodo * Root = ArbolGenerado[TamArbolGenerado - 1];
    Root->Nivel = 0;
    //pila.push(Root);//no deberia dsencolarlo
    Root->Padre->Nivel = 1;
    pila.push(Root->Padre);
    Root->Hijos[1]->Nivel = 1;
    pila.push(Root->Hijos[1]);
    Root->Hijos[0]->Nivel = 1;
    pila.push(Root->Hijos[0]);
    while (!pila.empty()) {
        Nodo * V = pila.top();
        pila.pop();
        if (V->Hijos[0]) {//existen los hijios
            for (int i = 1; i >= 0; i--) {//ya no deberia usar fors
                V->Hijos[i]->Nivel = V->Nivel + 1;
                pila.push(V->Hijos[i]);
            }
        }
    }
}

void UVisualizacionNJWidget::AplicarTraslacion(FVector Traslacion) {
}

void UVisualizacionNJWidget::AplicarRotacionRelativaANodo(Nodo* NodoReferencia, FVector PuntoReferencia) {
}

FVector UVisualizacionNJWidget::InterseccionLineaSuperficie() {//retorna en espacio local, esto pero quiza sea conveniete que retorne en espacio glloba, convertir estarepuesta despues
    return FVector();
}

void UVisualizacionNJWidget::TraslacionConNodoGuia() {
}

void UVisualizacionNJWidget::RotacionRama() {
}

void UVisualizacionNJWidget::ImprimirNodos(Nodo ** nodos, int num){
    for(int i = 0; i < num; i++){
        cout << "Id: " << nodos[i]->Id << endl;
        cout << "Nombre: " << nodos[i]->Nombre << endl;
        cout << "Padre: " << nodos[i]->PadreId << " -- " << nodos[i]->DistanciaPadre << endl;
        cout << "Hijo0: " << nodos[i]->HijosId[0] << " -- " << nodos[i]->DistanciasHijos[0] << endl;
        cout << "Hijo1: " << nodos[i]->HijosId[1] << " -- " << nodos[i]->DistanciasHijos[1] << endl;
    }
}

void UVisualizacionNJWidget::LeerDatosDePrueba() {

	string line;
	ifstream myfile ("D:/AM/Circulo/Algoritmo-NJ/NJ-AM/Input/AMTesis.txt");
	//ifstream myfile ("D:/AM/Circulo/Algoritmo-NJ/NJ-AM/Input/LeafShapes.dmat.txt");
    if (myfile.is_open()) {

        myfile >> n;//en realida es cin del archivo
        m = new float *[n];
        d = new string[n];
        string * clase = new string[n];
        for (int i = 0; i < n; i++) {
            myfile >> d[i];
            m[i] = new float[n];
        }
        for (int i = 0; i < n; i++) {
            myfile >> clase[i];
        }
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                myfile >> m[i][j];
                m[j][i] = m[i][j];//esto se queita si solo quiero almacenar la matriz inferior
            }
        }
        //devuelvo matrices cuadradas, con los datos reflejados
        delete[] clase;
        myfile.close();
    }
}

void UVisualizacionNJWidget::CalcularNJ() {
    //LeerDatosDePrueba();//aqui deberia ir construir matriz de distancias
    ConstruirMatrizDeDistanciasDeConsulta();
    AlgoritmoNJ = new NJ;
    AlgoritmoNJ->DatosIniciales(d, n);
    TamArbolGenerado = AlgoritmoNJ->GenerarArbol(m, n, ArbolGenerado);//los nodos de este arbol se han quedado guardados en ArbolGeneradoa, siendo su ultimo elemento la raiz
    ImprimirNodos(ArbolGenerado, TamArbolGenerado);
    delete AlgoritmoNJ;
    AlgoritmoNJ = nullptr;
    for(int i = 0; i < n; i++){
        delete [] m[i];
    }
    delete [] m;
    delete [] d;
}

void UVisualizacionNJWidget::ConstruirMatrizDeDistanciasDeConsulta() {
    //asgina valores a las variables d n y m, para que san procesados por el arbol
    //debo leer las estaciones filtradas solamente
    n = StationsFiltradas.Num();
    m = new float * [n];
    d = new string [n];
    string * clase = new string [n];
    for (int i = 0; i < n; i++) {
        d[i] = to_string(StationsFiltradas[i]);
        m[i] = new float[n];
    }
    for (int i = 1; i < n; i++) {
        for (int j = 0; j < i; j++) {
            m[i][j] = DistanciaEntreStations(StationsFiltradas[i], StationsFiltradas[j]);
            m[j][i] = m[i][j];//esto se queita si solo quiero almacenar la matriz inferior
        }
    }

}

void UVisualizacionNJWidget::SelectVectorDimensions() {
    //que dimensiones usare para filtrar
    //evaluando estacion por estacion, 
    /*SelectedDimensions.SetNum(Consulta.StationConsultas[0].ConsultasPorVariable.Num());
    for (int i = 0; i < SelectedDimensions.Num(); i++) {
        SelectedDimensions[i].SetNum(Consulta.StationConsultas[0].ConsultasPorVariable[0].Datos.Num());
    }*/
}

float UVisualizacionNJWidget::DistanciaEntreStations(int IdStationA, int IdStationB) {
    float acum = 0;
    for (int i = 0; i < Consulta.StationConsultas[0].ConsultasPorVariable.Num(); i++) {
        for (int j = 0; j < Consulta.StationConsultas[0].ConsultasPorVariable[0].Datos.Num(); j++) {
            float ValA;
            if (Consulta.StationConsultas[IdStationA].ConsultasPorVariable[i].Datos.Num()) {
                ValA = Consulta.StationConsultas[IdStationA].ConsultasPorVariable[i].Datos[j].value;
            }
            else {
                ValA = -9999;
            }
            float ValB;
            if (Consulta.StationConsultas[IdStationB].ConsultasPorVariable[i].Datos.Num()) {
                ValB = Consulta.StationConsultas[IdStationB].ConsultasPorVariable[i].Datos[j].value;
            }
            else {
                ValB = -9999;
            }
            if (ValA == -9999) {
                ValA = 0.0f;
            }
            if (ValB == -9999) {
                ValB = 0.0f;
            }
            acum += FMath::Pow(ValB - ValA, 2);
        }
    }
    return FMath::Sqrt(acum);
}

