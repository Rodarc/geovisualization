// Fill out your copyright notice in the Description page of Project Settings.

#include "NodoNJVisualizacion.h"
#include "Kismet/KismetMathLibrary.h"
#include "VisualizacionWidget.h"


UNodoNJVisualizacion::UNodoNJVisualizacion(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
}

void UNodoNJVisualizacion::SetColor(FLinearColor NewColor) {
    float H;
    float S;
    float V;
    float A;
    UKismetMathLibrary::RGBToHSV(NewColor, H, S, V, A);
    ColorFondoPunto = NewColor;
    ColorLineaPunto = UKismetMathLibrary::HSVToRGB(H, 0.0f, V - 0.2);// o podria ser  siempre
}

FStationStruct UNodoNJVisualizacion::GetStationStruct() {
    if (Visualizacion) {
        if (IdNodo < Visualizacion->Consulta.StationConsultas.Num()) {//no seria necesario
            return Visualizacion->Consulta.StationConsultas[IdNodo].StationData;
        }
        return FStationStruct();
    }
    return FStationStruct();
}
