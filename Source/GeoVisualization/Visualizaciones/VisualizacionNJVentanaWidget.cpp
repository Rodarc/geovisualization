// Fill out your copyright notice in the Description page of Project Settings.

#include "VisualizacionNJVentanaWidget.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Components/PanelWidget.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/ScrollBoxSlot.h"
#include "Components/SizeBox.h"

UVisualizacionNJVentanaWidget::UVisualizacionNJVentanaWidget(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {

    static ConstructorHelpers::FClassFinder<UNodoNJVisualizacion> NodoNJWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/NodoNJVisualizacionBP.NodoNJVisualizacionBP_C'"));
    if (NodoNJWidgetClass.Succeeded()) {
        TypeNodo = NodoNJWidgetClass.Class;
    }

    static ConstructorHelpers::FClassFinder<UAristaNJVisualizacion> AristaNJWidgetClass(TEXT("WidgetBlueprintGeneratedClass'/Game/GeoVisualization/UMG/Visualizaciones/AristaNJVisualizacionBP.AristaNJVisualizacionBP_C'"));
    if (AristaNJWidgetClass.Succeeded()) {
        TypeArista = AristaNJWidgetClass.Class;
    }

    RadioNodos = 6.0f;//el tama�� total sera el doble
    AnchoAristas = 3.5f;
    TraslacionOrigen = FVector2D::ZeroVector;
    Escala = 1.0f;
    DeltaZoom = 0.1f;
}

void UVisualizacionNJVentanaWidget::NativePaint(FPaintContext & InContext) const {
    Super::NativePaint(InContext);
}

void UVisualizacionNJVentanaWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) {
    Super::NativeTick(MyGeometry, InDeltaTIme);
}

void UVisualizacionNJVentanaWidget::CreateNodos() {
    for (int i = 0; i < TamArbolGenerado; i++) {
        UNodoNJVisualizacion *  NodoNJ = CreateWidget<UNodoNJVisualizacion>(UGameplayStatics::GetGameInstance(GetWorld()), TypeNodo);
        NodoNJ->IdNodo = i;//no se coincide con los id de las consultas, aqui hay un problema
        NodoNJ->Visualizacion = this;
        NodosWidgets.Add(NodoNJ);
        NodoNJ->AddToViewport(0);
        NodoNJ->bValido = ArbolGenerado[i]->Valido;
        //grafica->SetAlignmentInViewport(FVector2D(0.5, 0.5));//este no es necesario
        //grafica->SetAnchorsInViewport();
        //grafica->SetPositionInViewport(FVector2D(0.0f, 0.0f));

        UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
        UPanelSlot * SlotNodoNJ = RootWidget->AddChild(NodoNJ);
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotNodoNJ);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        CanvasSlot->SetZOrder(1);

        USizeBox * SizeBoxWidget = Cast<USizeBox>(NodoNJ->GetRootWidget());
        /*if (SizeBoxWidget) {
            CanvasSlot->SetSize(FVector2D(2*RadioNodos, 2*RadioNodos));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
        }*/
        if (ArbolGenerado[i]->Valido) {
            NodoNJ->SetColor(FLinearColor::Red);
            if (SizeBoxWidget) {
                CanvasSlot->SetSize(FVector2D(2*RadioNodos, 2*RadioNodos));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
                CanvasSlot->SetZOrder(2);
            }
        }
        else {
            NodoNJ->SetColor(FLinearColor::White);
            if (SizeBoxWidget) {
                CanvasSlot->SetSize(FVector2D(1.5f*RadioNodos, 1.5f*RadioNodos));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
                CanvasSlot->SetZOrder(1);
            }
        }

        CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
        CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));
        //CanvasSlot->SetSize(FVector2D(600.0f, 400.0f));
        //pasarle la informacion pertinente

        //PuntoGrafica->SetVisibility(ESlateVisibility::Hidden);//deben iniciar ocultos, visible segun lo necesite

    }
}

void UVisualizacionNJVentanaWidget::CreateAristas() {

    int count = 0;
    for (int i = 0; i < NodosWidgets.Num(); i++) {
        //if (ArbolGenerado[i]->Hijos[0]) { 
        if (!ArbolGenerado[i]->Valido) { 
            for (int j = 0; j < 2; j++) {
                UAristaNJVisualizacion *  AristaNJ = CreateWidget<UAristaNJVisualizacion>(UGameplayStatics::GetGameInstance(GetWorld()), TypeArista);
                AristaNJ->IdArista = count;
                AristaNJ->SourceNodo = NodosWidgets[i];
                AristaNJ->TargetNodo = NodosWidgets[ArbolGenerado[i]->HijosId[j]];
                AristasWidgets.Add(AristaNJ);
                AristaNJ->AddToViewport(0);
                AristaNJ->Ancho = AnchoAristas;

                UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
                UPanelSlot * SlotAristaNJ = RootWidget->AddChild(AristaNJ);
                UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotAristaNJ);
                //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
                CanvasSlot->SetZOrder(0);

                USizeBox * SizeBoxWidget = Cast<USizeBox>(AristaNJ->GetRootWidget());
                if (SizeBoxWidget) {
                    CanvasSlot->SetSize(FVector2D(20.0f, AnchoAristas));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
                }

                CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
                CanvasSlot->SetPosition(FVector2D(i * 10.0f, 10.0f));

                count++;
            }
        }
    }

    UAristaNJVisualizacion *  AristaNJ = CreateWidget<UAristaNJVisualizacion>(UGameplayStatics::GetGameInstance(GetWorld()), TypeArista);
    AristaNJ->IdArista = count;
    AristaNJ->SourceNodo = NodosWidgets[TamArbolGenerado - 1];
    AristaNJ->TargetNodo = NodosWidgets[ArbolGenerado[TamArbolGenerado - 1]->PadreId];
    AristasWidgets.Add(AristaNJ);
    AristaNJ->AddToViewport(0);
    AristaNJ->Ancho = AnchoAristas;

    UPanelWidget * RootWidget = Cast<UPanelWidget>(GetRootWidget());
    UPanelSlot * SlotAristaNJ = RootWidget->AddChild(AristaNJ);
    UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(SlotAristaNJ);
    //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
    CanvasSlot->SetZOrder(0);

    USizeBox * SizeBoxWidget = Cast<USizeBox>(AristaNJ->GetRootWidget());
    if (SizeBoxWidget) {
        CanvasSlot->SetSize(FVector2D(20.0f, AnchoAristas));// FVector2D(SizeBoxWidget->MinDesiredWidth, SizeBoxWidget->MinDesiredHeight));
    }

    CanvasSlot->SetAnchors(FAnchors(0.5f, 0.5f));
    CanvasSlot->SetPosition(FVector2D(TamArbolGenerado * 10.0f, 10.0f));
}

void UVisualizacionNJVentanaWidget::ActualizarLayout() {
    for (int i = 0; i < TamArbolGenerado; i++) {
        NodosWidgets[i]->X = ArbolGenerado[i]->X;
        NodosWidgets[i]->Y = ArbolGenerado[i]->Y;
        UCanvasPanelSlot * CanvasSlot = Cast<UCanvasPanelSlot>(NodosWidgets[i]->Slot);

        CanvasSlot->SetPosition(FVector2D(NodosWidgets[i]->X, NodosWidgets[i]->Y)*Escala - CanvasSlot->GetSize()/2 + TraslacionOrigen*Escala);
        //CanvasSlot->SetPosition(FVector2D(NodosWidgets[i]->X, NodosWidgets[i]->Y) + FVector2D(250.0f, 250.0f));
    }
    for (int i = 0; i < AristasWidgets.Num(); i++) {
        AristasWidgets[i]->Actualizar();
    }
}

void UVisualizacionNJVentanaWidget::VisualizarConsulta_Implementation() {
    //eliminar los widgets para el arbol
    for (int i = 0; i < NodosWidgets.Num(); i++) {
        NodosWidgets[i]->RemoveFromParent();
        NodosWidgets[i]->Destruct();
    }
    NodosWidgets.Empty();
    for (int i = 0; i < AristasWidgets.Num(); i++) {
        AristasWidgets[i]->RemoveFromParent();
        AristasWidgets[i]->Destruct();
    }
    AristasWidgets.Empty();
    CalcularNJ();
    Layout();
    CreateNodos();
    CreateAristas();
    ActualizarLayout();
    //visualizar la consulta que implica?
    //aqui genero el arbol
    //y despues lo muestor
    //creo nodos
    //y actualizo el layout

}

void UVisualizacionNJVentanaWidget::ZoomIn_Implementation() {
    Escala += 0.2f;
    ActualizarLayout();
}

void UVisualizacionNJVentanaWidget::ZoomOut_Implementation() {
    Escala -= 0.2f;
    if (Escala < 0.1f) {
        Escala = 0.1f;
    }
    ActualizarLayout();
}

void UVisualizacionNJVentanaWidget::Reestablecer_Implementation() {
    TraslacionOrigen = FVector2D::ZeroVector;
    Escala = 1.0f;//en realidad deberia ser enfocar todo, sea con el que sea que deba ser el zoom, y reestableciendo la posicion a 0
    ActualizarLayout();
}

FText UVisualizacionNJVentanaWidget::GetTitleWindowInformation_Implementation() {
    return FText();
}
