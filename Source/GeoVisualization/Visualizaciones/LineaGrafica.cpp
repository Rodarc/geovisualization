// Fill out your copyright notice in the Description page of Project Settings.

#include "LineaGrafica.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/SizeBox.h"

ULineaGrafica::ULineaGrafica(const FObjectInitializer & ObjectInitializer) : Super(ObjectInitializer) {
}

void ULineaGrafica::NativeTick(const FGeometry & MyGeometry, float InDeltaTime) {
    Super::NativeTick(MyGeometry, InDeltaTime);
}

void ULineaGrafica::Actualizar() {

        UCanvasPanelSlot * AristaCanvasSlot = Cast<UCanvasPanelSlot>(Slot);
        //a este objeto camvas es al que debo darle posiciones, y dimenciones, las cuales luego seran leidas por el la grafica para dibujar en este espacio con la funcion onpain, cuando lee su slot
        //LineaCanvasSlot->
        SetRenderTransformAngle(0.0f);

        UCanvasPanelSlot * InicioCanvasSlot = Cast<UCanvasPanelSlot>(PuntoInicio->Slot);
        UCanvasPanelSlot * FinalCanvasSlot = Cast<UCanvasPanelSlot>(PuntoFinal->Slot);

        FVector2D P1 = InicioCanvasSlot->GetPosition() + InicioCanvasSlot->GetSize()/2;
        FVector2D P2 = FinalCanvasSlot->GetPosition() + FinalCanvasSlot->GetSize()/2;

        FVector2D Direction = P2 - P1;
        AristaCanvasSlot->SetPosition(P1 + Direction/2 - FVector2D(Direction.Size() , Ancho)/2);// necesito un size linea
        AristaCanvasSlot->SetSize(FVector2D(Direction.Size(), Ancho));
        Direction = Direction.GetSafeNormal();
        
        float angle = FMath::RadiansToDegrees(FMath::Acos(FVector2D::DotProduct(FVector2D(1, 0), Direction)));
        //float angle = FMath::RadiansToDegrees(acos(FVector2D::DotProduct(FVector2D(0, 1), Direction)));
        float sing = FVector2D::CrossProduct(FVector2D(0, 1), Direction);//esto es por que el signo es impotante para saber si fue un angulo mayor de 180 o no
        if (P2.Y < P1.Y) {
            angle = -angle;
        }
        /*if (sing >= 0) {
            angle = 360-angle;
        }*/

        SetRenderTransformAngle(angle);
}



