// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraficaLineaWidget.h"
#include "GraficaLineaLabelWidget.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UGraficaLineaLabelWidget : public UGraficaLineaWidget
{
	GENERATED_BODY()
	
public:
    UGraficaLineaLabelWidget(const FObjectInitializer & ObjectInitializer);

    virtual void NativePaint(FPaintContext & InContext) const override;

    virtual int32 NativePaint(const FPaintArgs & Args, const FGeometry & AllottedGeometry, const FSlateRect & MyCullingRect, FSlateWindowElementList & OutDrawElements, int32 LayerId, const FWidgetStyle & InWidgetStyle, bool bParentEnabled) const override;

    virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
    virtual void CalcularCorners() override;

    virtual void CalcularPuntosDibujar() override;

    virtual void VisualizarConsulta_Implementation() override;
	
	
};
