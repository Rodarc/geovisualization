// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "../Estructuras/StationStruct.h"
#include "NodoNJVisualizacion.generated.h"

/**
 * 
 */
UCLASS()
class GEOVISUALIZATION_API UNodoNJVisualizacion : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UNodoNJVisualizacion(const FObjectInitializer & ObjectInitializer);

    //virtual void NativePaint(FPaintContext & InContext) const override;

    //virtual void NativeTick(const FGeometry & MyGeometry, float InDeltaTIme) override;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorFondoPunto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FLinearColor ColorLineaPunto;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    USlateBrushAsset * BrushFondoPunto;
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    int IdNodo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float X;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float Y;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bValido;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    FVector2D PosicionLayout;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    class UVisualizacionWidget * Visualizacion;//este deberjia ser visualizacionNJ
	
    //este deberia tener informacion del la estacion, es decir deberia poder obtener su station struct
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    void SetColor(FLinearColor NewColor);
	
    UFUNCTION(BlueprintCallable, Category = "Grafica")
    FStationStruct GetStationStruct();
	
};
