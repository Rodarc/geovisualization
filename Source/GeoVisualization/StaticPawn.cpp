// Fill out your copyright notice in the Description page of Project Settings.

#include "StaticPawn.h"
#include "GeoVisualizationGameModeBase.h"
#include "Interfaz/VisualizationHUD.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AStaticPawn::AStaticPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    UmbralDoubleClick = 0.25f;
    UmbralActualDoubleClick = 0.0f;
    bHold = false;
    bClick = false;
    bDoubleClick = false;
    DistanciaCamara = 500.0f;

    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

    Camara = CreateDefaultSubobject<UCameraComponent>(TEXT("Camara"));
    Camara->SetupAttachment(RootComponent);
    //Camara->SetRelativeLocation(FVector(-290.0f, 0.0f, 300.0f));
    Camara->SetRelativeRotation(FRotator(-60.0f, 0.0f, 0.0f));

    FVector NewLocationCamara = FVector::ZeroVector;
    NewLocationCamara.X = -DistanciaCamara * FMath::Cos(FMath::DegreesToRadians(-60.0f));
    NewLocationCamara.Z = -DistanciaCamara * FMath::Sin(FMath::DegreesToRadians(-60.0f));
    Camara->SetRelativeLocation(NewLocationCamara);

}


// Called when the game starts or when spawned
void AStaticPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStaticPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    if (bClick) {
        UmbralActualDoubleClick += DeltaTime;
    }
    if (UmbralActualDoubleClick >= UmbralDoubleClick) {
        UWorld * World = GetWorld();
        if (World) {
            AGameModeBase * GameMode = UGameplayStatics::GetGameMode(World);
            AGeoVisualizationGameModeBase * GeoVisGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
            if (GeoVisGameMode) {
                GeoVisGameMode->VisualizationHUDReference->ClickEvent();
            }
        }
        if (bDoubleClick) {
            bClick = true;
            bDoubleClick = false;
            UmbralActualDoubleClick = 0.0f;
        }
        else {
            bClick = false;
            bDoubleClick = false;
            UmbralActualDoubleClick = 0.0f;
        }
    }

}

// Called to bind functionality to input
void AStaticPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

    FInputActionBinding Action = InputComponent->BindAction("Select", IE_Pressed, this, &AStaticPawn::SelectPressed);
    Action.bConsumeInput = false;
    Action = InputComponent->BindAction("Select", IE_Released, this, &AStaticPawn::SelectReleased);
    Action.bConsumeInput = false;
    InputComponent->BindAxis("RotatePitch", this, &AStaticPawn::RotatePitch);
    InputComponent->BindAxis("RotateYaw", this, &AStaticPawn::RotateYaw);

}

void AStaticPawn::SelectPressed() {//reacciona al click izquierdo
    bHold = true;//este true deberia ser evaluado en funcion de si estoy dando click en un area libre
    if (bClick) {
        bDoubleClick = true;
    }
    else {
        bClick = true;
        //UmbralActualDoubleClick = 0.0f;
    }
    /*UWorld * World = GetWorld();
    if (World) {
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(World);
        AGeoVisualizationGameModeBase * GeoVisGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoVisGameMode) {
            GeoVisGameMode->VisualizationHUDReference->SelectPressed();
        }
    }*/
}

void AStaticPawn::SelectReleased() {
    bHold = false;
    if (bDoubleClick && UmbralActualDoubleClick <= UmbralDoubleClick) {
        UWorld * World = GetWorld();
        if (World) {
            AGameModeBase * GameMode = UGameplayStatics::GetGameMode(World);
            AGeoVisualizationGameModeBase * GeoVisGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
            if (GeoVisGameMode) {
                GeoVisGameMode->VisualizationHUDReference->DoubleClickEvent();
            }
        }
        bClick = false;
        bDoubleClick = false;
        UmbralActualDoubleClick = 0.0f;
    }
    else if (UmbralActualDoubleClick > UmbralDoubleClick) {
        UWorld * World = GetWorld();
        if (World) {
            AGameModeBase * GameMode = UGameplayStatics::GetGameMode(World);
            AGeoVisualizationGameModeBase * GeoVisGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
            if (GeoVisGameMode) {
                GeoVisGameMode->VisualizationHUDReference->ClickEvent();
            }
        }
        bClick = false;
        bDoubleClick = false;
        UmbralActualDoubleClick = 0.0f;
    }
    //deberia guardar una referencia en el .h
    /*UWorld * World = GetWorld();
    if (World) {
        AGameModeBase * GameMode = UGameplayStatics::GetGameMode(World);
        AGeoVisualizationGameModeBase * GeoVisGameMode = Cast<AGeoVisualizationGameModeBase>(GameMode);
        if (GeoVisGameMode) {
            GeoVisGameMode->VisualizationHUDReference->SelectReleased();
        }
    }*/
}

void AStaticPawn::RotatePitch(float AxisValue) {
    if (bHold && AxisValue != 0.0f) {// y si super el umbral creo. no usa click, por que se descativa al superar el umbral, no esta coordinandos se bien el release en esas parte. al lanzar el resles, ya que relaes y preses son distintos a click
        UE_LOG(LogClass, Log, TEXT("Pitch axis %f"), AxisValue);
        FRotator NewRotationCamara = Camara->GetRelativeTransform().GetRotation().Rotator();
        //NewRotationCamara.Pitch += CameraPitch;
        NewRotationCamara.Pitch = FMath::Clamp(NewRotationCamara.Pitch + AxisValue, -89.0f, 0.0f);//limitando, error cuadno unso -90
        Camara->SetRelativeRotation(NewRotationCamara);
        FVector NewLocationCamara = FVector::ZeroVector;
        //los rotators trabajan con grados pero las funciones con radianes, lo anterior pero asi ense�o dos formas de obtener lo mismo
        //NewLocationCamara.X = -DistanciaCamara * FMath::Cos(FMath::DegreesToRadians(NewRotationCamara.Pitch));
        //NewLocationCamara.Z = -DistanciaCamara * FMath::Sin(FMath::DegreesToRadians(NewRotationCamara.Pitch));
        NewLocationCamara.X = -DistanciaCamara * FMath::Cos(FMath::DegreesToRadians(Camara->GetComponentRotation().Pitch));
        NewLocationCamara.Z = -DistanciaCamara * FMath::Sin(FMath::DegreesToRadians(Camara->GetComponentRotation().Pitch));
        //NewLocationCamara.X = -DistanciaCamara * FMath::Cos(FMath::DegreesToRadians(NewRotationCamara.Pitch));
        //NewLocationCamara.Z = -DistanciaCamara * FMath::Sin(FMath::DegreesToRadians(NewRotationCamara.Pitch)) + ObjetivoCamara.Z;
        Camara->SetRelativeLocation(NewLocationCamara);

    }

}

void AStaticPawn::RotateYaw(float AxisValue) {
    if (bHold && AxisValue != 0.0f) {// y si super el umbral creo. no usa click, por que se descativa al superar el umbral, no esta coordinandos se bien el release en esas parte. al lanzar el resles, ya que relaes y preses son distintos a click
        FRotator NewRotation = GetActorRotation();
        NewRotation.Yaw += AxisValue;//sumamos, automaticamente se hace modulo
        SetActorRotation(NewRotation);
    }
}
