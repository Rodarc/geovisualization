#pragma once

#include "CoreMinimal.h"
#include "OpcionDataStruct.generated.h"

USTRUCT(BlueprintType)
struct FOpcionDataStruct {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int Id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Name;
    
};
