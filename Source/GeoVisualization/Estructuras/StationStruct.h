#pragma once

#include "CoreMinimal.h"
#include "StationStruct.generated.h"

USTRUCT(BlueprintType)
struct FStationStruct {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString id_ghcn;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float latitud;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float longitud;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float elevation;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString wmo_id;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString departament;
};
