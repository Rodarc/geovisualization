#pragma once

#include "CoreMinimal.h"
#include "DailyDataStruct.generated.h"

USTRUCT(BlueprintType)
struct FDailyDataStruct {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int id_station;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString id_station_ghcn;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int year;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int month;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int day;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float value;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString mflag;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString qflag;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString sflag;
};
