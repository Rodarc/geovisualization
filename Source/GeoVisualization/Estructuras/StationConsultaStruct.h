#pragma once

#include "CoreMinimal.h"
#include "StationStruct.h"
#include "VariableConsultaStruct.h"
#include "StationConsultaStruct.generated.h"

USTRUCT(BlueprintType)
struct FStationConsultaStruct {
    GENERATED_BODY()

    //UPROPERTY(EditAnywhere, BlueprintReadWrite)
    //int Id;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FStationStruct StationData;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FVariableConsultaStruct> ConsultasPorVariable;
};
