#pragma once

#include "StationConsultaStruct.h"
#include "ConsultaStruct.generated.h"

USTRUCT(BlueprintType)
struct FConsultaStruct {
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int InitialYear;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int FinalYear;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<int> MesesConsultados;//solo deberian ser ids, no string, para en otro lado acceder con este id

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FString> VariablesConsultadas;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FString> VariablesEstadisticasConsultadas;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FStationConsultaStruct> StationConsultas;
};
