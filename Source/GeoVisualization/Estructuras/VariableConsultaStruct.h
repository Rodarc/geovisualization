#pragma once

#include "CoreMinimal.h"
#include "DailyDataStruct.h"
#include "VariableConsultaStruct.generated.h"

USTRUCT(BlueprintType)
struct FVariableConsultaStruct {
    GENERATED_BODY()

    //UPROPERTY(EditAnywhere, BlueprintReadWrite)
    //int IdVariable;//no es util en realidad

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FString Variable;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TArray<FDailyDataStruct> Datos;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float YMin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float YMax;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float XMin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float XMax;
};
