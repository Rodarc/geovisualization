// Fill out your copyright notice in the Description page of Project Settings.

#include "PruebaProceduralMesh.h"
#include "Components/SphereComponent.h"


// Sets default values
APruebaProceduralMesh::APruebaProceduralMesh()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));

	Malla = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	Malla->SetupAttachment(RootComponent);
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Components|ProceduralMesh", meta = (AutoCreateRefTerm = "Normals,UV0,VertexColors,Tangents"))
	//	void CreateMeshSection(int32 SectionIndex, const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FVector>& Normals,
	// const TArray<FVector2D>& UV0, const TArray<FColor>& VertexColors, const TArray<FProcMeshTangent>& Tangents, bool bCreateCollision);
 
	// New in UE 4.17, multi-threaded PhysX cooking.
	Malla->bUseAsyncCooking = true;

	MallaArista = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMeshArista"));
	MallaArista->SetupAttachment(RootComponent);
	MallaArista->bUseAsyncCooking = true;
}

// Called when the game starts or when spawned
void APruebaProceduralMesh::BeginPlay()
{
	Super::BeginPlay();
	
    CreateSphereTemplate(2);
    //en niver 3 se ve perfectamente
    //UE_LOG(LogClass, Log, TEXT("VerticesTemplate = %d"), VerticesNodoTemplate.size());

    //AddNodo(FVector::ZeroVector, 50, 1);
    AddArregloNodos(100, 100);
    CreateNodos();
    /*FVector Source(-20, -10, -30);
    FVector Target(30, 25, 43);

    AddNodo(Source, 10, 0);
    AddNodo(Target, 10, 1);

    AddArista(Source, Target, 8, 5, 0);

    FVector Source2(20, 10, -30);

    AddNodo(Source2, 10, 2);

    AddArista(Source2, Target, 8, 5, 1);

    CreateNodos();
    CreateAristas();*/
}

// Called every frame
void APruebaProceduralMesh::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    AddActorLocalRotation(FRotator(1.0f, 0.0f, 0.0f));
}

void APruebaProceduralMesh::PostActorCreated()
{
	Super::PostActorCreated();
	//CreateMallaRegular(1000, 1000);
	//CreateMallaRegular(10, 10);
	//CreateTriangles();
	//CreateTriangle();
}
 
// This is called when actor is already in level and map is opened
void APruebaProceduralMesh::PostLoad()
{
	Super::PostLoad();
	//CreateMallaRegular(1000, 1000);
	//CreateMallaRegular(10, 10);
	//CreateTriangles();
	//CreateTriangle();
}
 
void APruebaProceduralMesh::CreateTriangle()
{
	TArray<FVector> vertices;
	vertices.Add(FVector(0, 0, 0));
	vertices.Add(FVector(0, 50, 0));
	vertices.Add(FVector(0, 0, 50));
 
	TArray<int32> Triangles;
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
 
	TArray<FVector> normals;
	normals.Add(FVector(-1, 0, 0));
	normals.Add(FVector(-1, 0, 0));
	normals.Add(FVector(-1, 0, 0));
 
	TArray<FVector2D> UV0;
	UV0.Add(FVector2D(0, 0));
	UV0.Add(FVector2D(10, 0));
	UV0.Add(FVector2D(0, 10));
 
 
	TArray<FProcMeshTangent> tangents;
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
 
	TArray<FLinearColor> vertexColors;
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
 
	Malla->CreateMeshSection_LinearColor(0, vertices, Triangles, normals, UV0, vertexColors, tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void APruebaProceduralMesh::CreateTriangles()
{
	TArray<FVector> vertices;
	vertices.Add(FVector(0, 0, 0));
	vertices.Add(FVector(0, 50, 0));
	vertices.Add(FVector(0, 0, 50));
	vertices.Add(FVector(0, 50, 50));
 
	TArray<int32> Triangles;
	Triangles.Add(0);
	Triangles.Add(1);
	Triangles.Add(2);
	Triangles.Add(2);
	Triangles.Add(1);
	Triangles.Add(3);
 
	TArray<FVector> normals;
	normals.Add(FVector(-1, 0, 0));
	normals.Add(FVector(-1, 0, 0));
	normals.Add(FVector(-1, 0, 0));
	normals.Add(FVector(-1, 0, 0));
 
	TArray<FVector2D> UV0;
	UV0.Add(FVector2D(0, 0));
	UV0.Add(FVector2D(10, 0));
	UV0.Add(FVector2D(0, 10));
	UV0.Add(FVector2D(10, 10));
 
 
	TArray<FProcMeshTangent> tangents;
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
 
	TArray<FLinearColor> vertexColors;
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
 
	Malla->CreateMeshSection_LinearColor(0, vertices, Triangles, normals, UV0, vertexColors, tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void APruebaProceduralMesh::CreateMallaRegular(int N, int M)
{
	float TamLado = 10.0f;
	float offestX = N * TamLado / 2;
	float offestY = M * TamLado / 2;

	//par convertir el numero de cuadrados, en numero de vertices
	N++;
	M++;
	
	//se resptan esos ofset a los puentos que voy a generar;
	//con el 0.0 al centro de la malla
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			VerticesNodos.Add(FVector(i * TamLado - offestX, j * TamLado - offestY, 0.0f));
		}
	}
 
	for (int i = 0; i < N-1; i++) {
		for (int j = 0; j < M-1; j++) {
			int IdV = i * M + j;
			TrianglesNodos.Add(IdV);
			TrianglesNodos.Add(IdV + 1);
			TrianglesNodos.Add(IdV + M);
			TrianglesNodos.Add(IdV + M);
			TrianglesNodos.Add(IdV + 1);
			TrianglesNodos.Add(IdV + M + 1);
		}
	}
 
	for (int i = 0; i < VerticesNodos.Num(); i++) {// o < N*M
		NormalsNodos.Add(FVector(0, 0, 1));
	}
 
	for (int i = 0; i < VerticesNodos.Num(); i++) {// o < N*M
		//UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY));
		UV0Nodos.Add(FVector2D(VerticesNodos[i].X + offestX, VerticesNodos[i].Y + offestY)/10.0f);
	}
 
 
	for (int i = 0; i < VerticesNodos.Num(); i++) {// o < N*M
		TangentsNodos.Add(FProcMeshTangent(0, 1, 0));
	}
 
	for (int i = 0; i < VerticesNodos.Num(); i++) {// o < N*M
		VertexColorsNodos.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	}
 
	Malla->CreateMeshSection_LinearColor(0, VerticesNodos, TrianglesNodos, NormalsNodos, UV0Nodos, VertexColorsNodos, TangentsNodos, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void APruebaProceduralMesh::AddArregloNodos(int N, int M) {
	float TamLado = 100.0f;
	//float offestX = N * TamLado / 2;
	//float offestY = M * TamLado / 2;

	//par convertir el numero de cuadrados, en numero de vertices
	//N++;
	//M++;
	
	//se resptan esos ofset a los puentos que voy a generar;
	//con el 0.0 al centro de la malla
    /*ColisionesNodos.Add(NewObject<USphereComponent>(this, USphereComponent::StaticClass(), *FString("ColisionNodo")));
    ColisionesNodos[ColisionesNodos.Num()-1]->SetupAttachment(RootComponent);
    ColisionesNodos[ColisionesNodos.Num() - 1]->SetRelativeLocation(FVector::ZeroVector);
    ColisionesNodos[ColisionesNodos.Num()-1]->bHiddenInGame = false;
    ColisionesNodos[ColisionesNodos.Num()-1]->InitSphereRadius(40.0f);//tener cuidado con las unidades, tiene radio 6 quiza sea muy grande
    */
    int count = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
            //AddNodo(FVector(i * TamLado - offestX, j * TamLado - offestY, 0.0f), 50.0f);
            AddNodo(FVector(i * TamLado, j * TamLado, 0.0f), 25.0f, count);

            ColisionesNodos.Add(NewObject<USphereComponent>(this, USphereComponent::StaticClass(), *FString("ColisionNodo" + FString::FromInt(count)), EObjectFlags::RF_DefaultSubObject));
            ColisionesNodos[ColisionesNodos.Num() - 1]->RegisterComponent();
            ColisionesNodos[ColisionesNodos.Num()-1]->SetupAttachment(RootComponent);
            ColisionesNodos[ColisionesNodos.Num() - 1]->SetRelativeLocation(FVector(i * TamLado, j * TamLado, 0.0f));
            ColisionesNodos[ColisionesNodos.Num()-1]->bHiddenInGame = true;
            ColisionesNodos[ColisionesNodos.Num()-1]->InitSphereRadius(25.0f);//tener cuidado con las unidades, tiene radio 6 quiza sea muy grande
            ColisionesNodos[ColisionesNodos.Num() - 1]->SetSphereRadius(25.0f);
            //ColisionesNodos[ColisionesNodos.Num() - 1]->SetCollisionEnabled(ECollisionEnabled::NoCollision);
            ColisionesNodos[ColisionesNodos.Num() - 1]->Deactivate();

            count++;
		}
	}
}

void APruebaProceduralMesh::CreateNodos() {
	Malla->CreateMeshSection_LinearColor(0, VerticesNodos, TrianglesNodos, NormalsNodos, UV0Nodos, VertexColorsNodos, TangentsNodos, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void APruebaProceduralMesh::UpdateMallaRegular(int N, int M)
{
	float TamLado = 10.0f;
	float offestX = N * TamLado / 2;
	float offestY = M * TamLado / 2;

	//par convertir el numero de cuadrados, en numero de vertices
	N++;
	M++;
	
	//se resptan esos ofset a los puentos que voy a generar;
	//con el 0.0 al centro de la malla
	for (int i = 0; i < VerticesNodos.Num(); i++) {
		//Vertices[i].Z = FMath::Lerp<float>(20.0f, 0.0f, (Vertices[0].Size() - Vertices[i].Size())/Vertices[0].Size());
		VerticesNodos[i].Z = FMath::Lerp<float>(20.0f, 0.0f, VerticesNodos[i].Size()/VerticesNodos[0].Size());
	}
 
	Malla->UpdateMeshSection_LinearColor(0, VerticesNodos, NormalsNodos, UV0Nodos, VertexColorsNodos, TangentsNodos);
 
}

FVector APruebaProceduralMesh::PuntoTFromAToB(FVector a, FVector b, float t) {
    FVector direccion = b - a;
    FVector punto = a + direccion*t;
    //normalizar a esfera de radio 1
    punto = punto.GetSafeNormal();
    return punto;
}

FVector APruebaProceduralMesh::PuntoTFromAToBEsferico(FVector a, FVector b, float t) {
    FVector direccion = b - a;
    FVector punto = a + direccion*t;
    //normalizar a esfera de radio 1
    punto = punto.GetSafeNormal();
    float phi = FMath::Acos(punto.Z);
    float theta = FMath::Asin(punto.Y / FMath::Sin(phi));
    FVector puntoesferico (phi, theta, 1.0f);
    return puntoesferico;
}

void APruebaProceduralMesh::DividirTriangulo(Triangulo t) {
    FVector D = PuntoTFromAToB(t.C, t.A, 0.5);
    VerticesNodoTemplate.push_back(D);//0.5 es la mitad
    int IdD = VerticesNodoTemplate.size() - 1;
    FVector DP = PuntoTFromAToBEsferico(t.C, t.A, 0.5);
    VerticesPNodoTemplate.push_back(DP);//0.5 es la mitad
    NormalsNodoTemplate.push_back(D);
    TangentsNodoTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(DP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(DP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    UV0NodoTemplate.push_back(FVector2D(DP.Y/(2*PI), 1 - DP.X/PI));
    VertexColorsNodoTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    FVector E = PuntoTFromAToB(t.B, t.A, 0.5);
    VerticesNodoTemplate.push_back(E);//0.5 es la mitad
    int IdE = VerticesNodoTemplate.size() - 1;
    FVector EP = PuntoTFromAToBEsferico(t.B, t.A, 0.5);
    VerticesPNodoTemplate.push_back(EP);//0.5 es la mitad
    NormalsNodoTemplate.push_back(E);
    TangentsNodoTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(EP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(EP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    UV0NodoTemplate.push_back(FVector2D(EP.Y/(2*PI), 1 - EP.X/PI));
    VertexColorsNodoTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    FVector F = PuntoTFromAToB(t.C, t.B, 0.5);
    VerticesNodoTemplate.push_back(F);//0.5 es la mitad
    int IdF = VerticesNodoTemplate.size() - 1;
    FVector FP = PuntoTFromAToBEsferico(t.C, t.B, 0.5);
    VerticesPNodoTemplate.push_back(FP);//0.5 es la mitad
    NormalsNodoTemplate.push_back(F);
    TangentsNodoTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(FP.Y + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(FP.Y + PI/2), 1.0f * FMath::Cos(PI/2)));
    UV0NodoTemplate.push_back(FVector2D(FP.Y/(2*PI), 1 - FP.X/PI));
    VertexColorsNodoTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    //if (VerticesP[t.IdA].X == VerticesP[t.IdC].X) {//tringulo hacia abajo
    if (t.Orientacion) {//tringulo hacia abajo
        TriangulosNodo.push_back(Triangulo(t.A, E, D, t.IdA, IdE, IdD, t.Nivel*2, 0));
        TriangulosNodo.push_back(Triangulo(D, E, F, IdD, IdE, IdF, t.Nivel*2, 0));
        TriangulosNodo.push_back(Triangulo(D, F, t.C, IdD, IdF, t.IdC, t.Nivel*2, 0));
        TriangulosNodo.push_back(Triangulo(E, t.B, F, IdE, t.IdB, IdF, t.Nivel*2 + 1, 1));
    }
    else {//triangulo hacia arriba, por lo tanto c y b esta a la misma algutar, a arriba
        TriangulosNodo.push_back(Triangulo(t.A, E, D, t.IdA, IdE, IdD, t.Nivel*2, 0));
        TriangulosNodo.push_back(Triangulo(E, t.B, F, IdE, t.IdB, IdF, t.Nivel*2 + 1, 1));
        TriangulosNodo.push_back(Triangulo(E, F, D, IdE, IdF, IdD, t.Nivel*2+1, 1));
        TriangulosNodo.push_back(Triangulo(D, F, t.C, IdD, IdF, t.IdC, t.Nivel*2 + 1, 1));
    }
}

void APruebaProceduralMesh::DividirTriangulos() {
    int n = TriangulosNodo.size();
    for (int i = 0; i < n; i++) {
        DividirTriangulo(TriangulosNodo[0]);
        TriangulosNodo.erase(TriangulosNodo.begin());
    }
    /*for (int i = 0; i < Triangulos.size(); i++) {
        CalcularCentroTriangulo(Triangulos[i]);
    }*/
}

void APruebaProceduralMesh::CreateSphereTemplate(int Precision) {
    UE_LOG(LogClass, Log, TEXT("Creando esfera"));
    float DeltaPhi = 1.10714872;
    float DeltaTheta = PI * 72 / 180;
    float DiffTheta = PI * 36 / 180;
    VerticesNodoTemplate.push_back(FVector(0.0f, 0.0f, 1.0f));//puntos para radio uno
    NormalsNodoTemplate.push_back(FVector(0.0f, 0.0f, 1.0f));//puntos para radio uno
    //Tangents.Add(FProcMeshTangent(1.0f, 0.0f, 0.0f));
    TangentsNodoTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(PI/2), 1.0f * FMath::Cos(PI/2)));
    VerticesPNodoTemplate.push_back(FVector(0.0f, 0.0f, 1.0f));
    UV0NodoTemplate.push_back(FVector2D(0.0f,1.0f));
	VertexColorsNodoTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    for (int i = 0; i < 5; i++) {
        VerticesPNodoTemplate.push_back(FVector(DeltaPhi, i*DeltaTheta, 1.0f));//esto es en esferico, no se para que lo uso desues, creo que para las divisiones para aumentr la suavidad
        FVector posicion;
        posicion.X = 1.0f * FMath::Sin(DeltaPhi) * FMath::Cos(i*DeltaTheta);
        posicion.Y = 1.0f * FMath::Sin(DeltaPhi) * FMath::Sin(i*DeltaTheta);
        posicion.Z = 1.0f * FMath::Cos(DeltaPhi);
        //hasta aqui posicion esta como unitario, puede servir para el verctor de normales
        VerticesNodoTemplate.push_back(posicion);
        NormalsNodoTemplate.push_back(posicion);
        TangentsNodoTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        UV0NodoTemplate.push_back(FVector2D(i*DeltaTheta/(2*PI), 1 - DeltaPhi/PI));
        VertexColorsNodoTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    for (int i = 0; i < 5; i++) {
        VerticesPNodoTemplate.push_back(FVector(PI - DeltaPhi, i*DeltaTheta + DiffTheta, 1.0f));
        FVector posicion;
        posicion.X = 1.0f * FMath::Sin(PI - DeltaPhi) * FMath::Cos(i*DeltaTheta + DiffTheta);
        posicion.Y = 1.0f * FMath::Sin(PI - DeltaPhi) * FMath::Sin(i*DeltaTheta + DiffTheta);
        posicion.Z = 1.0f * FMath::Cos(PI - DeltaPhi);
        //hasta aqui posicion esta como unitario, puede servir para el verctor de normales
        NormalsNodoTemplate.push_back(posicion);
        TangentsNodoTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        VerticesNodoTemplate.push_back(posicion);
        UV0NodoTemplate.push_back(FVector2D((i*DeltaTheta + DiffTheta)/(2*PI), 1 - (PI - DeltaPhi)/PI));
        VertexColorsNodoTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    VerticesNodoTemplate.push_back(FVector(0.0, 0.0f, -1.0f));//puntos para radio uno
    NormalsNodoTemplate.push_back(FVector(0.0, 0.0f, -1.0f));//puntos para radio uno
    //Tangents.Add(FProcMeshTangent(-1.0f, 0.0f, 0.0f));
    TangentsNodoTemplate.push_back(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(PI/2), 1.0f * FMath::Cos(PI/2)));
    VerticesPNodoTemplate.push_back(FVector(PI, 0.0f, 1.0f));
    UV0NodoTemplate.push_back(FVector2D(0.0f,0.0f));
    VertexColorsNodoTemplate.push_back(FLinearColor(0.75, 0.75, 0.75, 1.0));

    //Agregados vertices y normales, faltarian agregar las tangentes

    //Tirangulos superiores
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[0], VerticesNodoTemplate[1], VerticesNodoTemplate[2], 0, 1, 2, 0, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[0], VerticesNodoTemplate[2], VerticesNodoTemplate[3], 0, 2, 3, 0, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[0], VerticesNodoTemplate[3], VerticesNodoTemplate[4], 0, 3, 4, 0, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[0], VerticesNodoTemplate[4], VerticesNodoTemplate[5], 0, 4, 5, 0, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[0], VerticesNodoTemplate[5], VerticesNodoTemplate[1], 0, 5, 1, 0, 0));

    //triangulos medios
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[1], VerticesNodoTemplate[6], VerticesNodoTemplate[2], 1, 6, 2, 1, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[2], VerticesNodoTemplate[6], VerticesNodoTemplate[7], 2, 6, 7, 1, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[2], VerticesNodoTemplate[7], VerticesNodoTemplate[3], 2, 7, 3, 1, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[3], VerticesNodoTemplate[7], VerticesNodoTemplate[8], 3, 7, 8, 1, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[3], VerticesNodoTemplate[8], VerticesNodoTemplate[4], 3, 8, 4, 1, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[4], VerticesNodoTemplate[8], VerticesNodoTemplate[9], 4, 8, 9, 1, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[4], VerticesNodoTemplate[9], VerticesNodoTemplate[5], 4, 9, 5, 1, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[5], VerticesNodoTemplate[9], VerticesNodoTemplate[10], 5, 9, 10, 1, 0));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[5], VerticesNodoTemplate[10], VerticesNodoTemplate[1], 5, 10, 1, 1, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[1], VerticesNodoTemplate[10], VerticesNodoTemplate[6], 1, 10, 6, 1, 0));

    //triangulos inferiores
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[6], VerticesNodoTemplate[11], VerticesNodoTemplate[7], 6, 11, 7, 2, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[7], VerticesNodoTemplate[11], VerticesNodoTemplate[8], 7, 11, 8, 2, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[8], VerticesNodoTemplate[11], VerticesNodoTemplate[9], 8, 11, 9, 2, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[9], VerticesNodoTemplate[11], VerticesNodoTemplate[10], 9, 11, 10, 2, 1));
    TriangulosNodo.push_back(Triangulo(VerticesNodoTemplate[10], VerticesNodoTemplate[11], VerticesNodoTemplate[6], 10, 11, 6, 2, 1));

    while (Precision) {
        DividirTriangulos();
        Precision--;
    }
}

void APruebaProceduralMesh::AddNodo(FVector Posicion, float Radio, int NumNodo) {
    for (int i = 0; i < VerticesNodoTemplate.size(); i++) {
        VerticesNodos.Add(VerticesNodoTemplate[i] * Radio + Posicion);
    }
    for (int i = 0; i < VerticesPNodoTemplate.size(); i++) {
        VerticesPNodos.Add(VerticesPNodoTemplate[i]);
    }
    for (int i = 0; i < TriangulosNodo.size(); i++) {
        TrianglesNodos.Add(TriangulosNodo[i].IdC + NumNodo * VerticesNodoTemplate.size());
        TrianglesNodos.Add(TriangulosNodo[i].IdB + NumNodo * VerticesNodoTemplate.size());
        TrianglesNodos.Add(TriangulosNodo[i].IdA + NumNodo * VerticesNodoTemplate.size());
    }
    for (int i = 0; i < NormalsNodoTemplate.size(); i++) {
        NormalsNodos.Add(NormalsNodoTemplate[i]);
    }
    for (int i = 0; i < UV0NodoTemplate.size(); i++) {
        UV0Nodos.Add(UV0NodoTemplate[i]);
    }
    for (int i = 0; i < TangentsNodoTemplate.size(); i++) {
        TangentsNodos.Add(TangentsNodoTemplate[i]);
    }
    for (int i = 0; i < VertexColorsNodoTemplate.size(); i++) {
        VertexColorsNodos.Add(VertexColorsNodoTemplate[i]);
    }
}

void APruebaProceduralMesh::AddArista(FVector Source, FVector Target, int Precision, int Radio, int NumArista) {
    //no he probado esto con varias aristas, quiza falla
    //precision debe ser mayor a 3, represante el numero de lados del cilindro
    //la precision deberia ser de forma general,asi lo puedo usar para determinar la cantidad de vertices a�adidos
    FVector Direccion = (Target - Source).GetSafeNormal();
    //calculando vertices superioes

    float phi = PI/2;
    float theta = FMath::Asin(Direccion.Y / FMath::Sin(phi)) + PI/2;
    FVector VectorU;
    VectorU.X = 1.0f * FMath::Sin(phi) * FMath::Cos(theta);
    VectorU.Y = 1.0f * FMath::Sin(phi) * FMath::Sin(theta);
    VectorU.Z = 1.0f * FMath::Cos(phi);
    FVector VectorV = FVector::CrossProduct(Direccion, VectorU).GetSafeNormal();
    float DeltaTheta = 2 * PI / Precision;

    for (int i = 0; i < Precision; i++) {
        VerticesAristas.Add(Target + (VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta))*Radio);
        NormalsAristas.Add((VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta)).GetSafeNormal());
        TangentsAristas.Add(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        UV0Aristas.Add(FVector2D(i*(1.0f/Precision), 1));
        VertexColorsAristas.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
    }
    for (int i = 0; i < Precision; i++) {
        VerticesAristas.Add(Source + (VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta))*Radio);
        NormalsAristas.Add((VectorU*FMath::Cos(i*DeltaTheta) + VectorV*FMath::Sin(i*DeltaTheta)).GetSafeNormal());
        TangentsAristas.Add(FProcMeshTangent(1.0f * FMath::Sin(PI/2)*FMath::Cos(i*DeltaTheta + PI/2), 1.0f * FMath::Sin(PI/2)*FMath::Sin(i*DeltaTheta + PI/2), 1.0f * FMath::Cos(PI/2)));
        UV0Aristas.Add(FVector2D(i*(1.0f/Precision), 0));
        VertexColorsAristas.Add(FLinearColor(0.0, 0.75, 0.75, 1.0));
    }

    for (int i = 0; i < Precision; i++) {
        TrianglesAristas.Add((i+1)%Precision + NumArista * Precision*2);
        TrianglesAristas.Add(Precision + (i + 1)%Precision + NumArista * Precision*2);
        TrianglesAristas.Add(i + NumArista * Precision*2);

        TrianglesAristas.Add(Precision + i + NumArista * Precision*2);
        TrianglesAristas.Add(i + NumArista * Precision*2);
        TrianglesAristas.Add(Precision + (i+1)%Precision + NumArista * Precision*2);
    }
}

void APruebaProceduralMesh::CreateAristas() {
	MallaArista->CreateMeshSection_LinearColor(0, VerticesAristas, TrianglesAristas, NormalsAristas, UV0Aristas, VertexColorsAristas, TangentsAristas, true);
 
        // Enable collision data
	MallaArista->ContainsPhysicsTriMeshData(true);
}
