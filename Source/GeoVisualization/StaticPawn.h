// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "StaticPawn.generated.h"

UCLASS()
class GEOVISUALIZATION_API AStaticPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AStaticPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    void SelectPressed();
	
    void SelectReleased();

    void RotatePitch(float AxisValue);

    void RotateYaw(float AxisValue);

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "JuegoRobot")
    UCameraComponent * Camara;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bHold;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    bool bDoubleClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float UmbralActualDoubleClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float UmbralDoubleClick;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grafica")
    float DistanciaCamara;
};