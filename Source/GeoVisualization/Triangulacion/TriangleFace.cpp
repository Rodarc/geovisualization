// Fill out your copyright notice in the Description page of Project Settings.

#include "TriangleFace.h"

TriangleFace::TriangleFace() {
	Vertices.resize(3);
	IdVertices.resize(3);
	TriangulosAdjacentes.resize(3);
	IdTriangulosAdjacentes.resize(3);
	OrientacionesTriangulosAdjacentes.resize(3);
    for(int i = 0; i < Vertices.size(); i++){
        Vertices[i] = nullptr;
        TriangulosAdjacentes[i] = nullptr;
        IdTriangulosAdjacentes[i] = -1;
        IdVertices[i] = -1;
        OrientacionesTriangulosAdjacentes[i] = -1;
    }
}

TriangleFace::~TriangleFace() {
}
