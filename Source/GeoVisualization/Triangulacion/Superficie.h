// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Superficie.generated.h"

UCLASS()
class GEOVISUALIZATION_API ASuperficie : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASuperficie();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<FVector> Vertices;

	TArray<int32> Triangles;

	TArray<FVector> Normals;

	TArray<FVector2D> UV0;

	TArray<FProcMeshTangent> Tangents;

	TArray<FLinearColor> VertexColors;

	virtual void PostActorCreated() override;

	virtual void PostLoad() override;

	void CreateTriangle();

	void CreateTriangles();

	void CreateMallaRegular(int N, int M);

	UFUNCTION(BlueprintCallable)
	void UpdateMallaRegular(int N, int M);

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent * Malla;

	
	
};
