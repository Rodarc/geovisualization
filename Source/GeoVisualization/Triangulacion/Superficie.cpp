// Fill out your copyright notice in the Description page of Project Settings.

#include "Superficie.h"


// Sets default values
ASuperficie::ASuperficie()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Malla = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = Malla;
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Components|ProceduralMesh", meta = (AutoCreateRefTerm = "Normals,UV0,VertexColors,Tangents"))
	//	void CreateMeshSection(int32 SectionIndex, const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FVector>& Normals,
	// const TArray<FVector2D>& UV0, const TArray<FColor>& VertexColors, const TArray<FProcMeshTangent>& Tangents, bool bCreateCollision);
 
	// New in UE 4.17, multi-threaded PhysX cooking.
	Malla->bUseAsyncCooking = true;
}

// Called when the game starts or when spawned
void ASuperficie::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASuperficie::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASuperficie::PostActorCreated()
{
	Super::PostActorCreated();
	//CreateMallaRegular(1000, 1000);
	CreateMallaRegular(10, 10);
	//CreateTriangles();
	//CreateTriangle();
}
 
// This is called when actor is already in level and map is opened
void ASuperficie::PostLoad()
{
	Super::PostLoad();
	//CreateMallaRegular(1000, 1000);
	CreateMallaRegular(10, 10);
	//CreateTriangles();
	//CreateTriangle();
}
 
void ASuperficie::CreateTriangle()
{
	TArray<FVector> vertices;
	vertices.Add(FVector(0, 0, 0));
	vertices.Add(FVector(0, 50, 0));
	vertices.Add(FVector(0, 0, 50));
 
	TArray<int32> triangles;
	triangles.Add(0);
	triangles.Add(1);
	triangles.Add(2);
 
	TArray<FVector> normals;
	normals.Add(FVector(1, 0, 0));
	normals.Add(FVector(1, 0, 0));
	normals.Add(FVector(1, 0, 0));
 
	TArray<FVector2D> uv0;
	uv0.Add(FVector2D(0, 0));
	uv0.Add(FVector2D(10, 0));
	uv0.Add(FVector2D(0, 10));
 
 
	TArray<FProcMeshTangent> tangents;
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
 
	TArray<FLinearColor> vertexColors;
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
 
	Malla->CreateMeshSection_LinearColor(0, vertices, triangles, normals, uv0, vertexColors, tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void ASuperficie::CreateTriangles()
{
	TArray<FVector> vertices;
	vertices.Add(FVector(0, 0, 0));
	vertices.Add(FVector(0, 50, 0));
	vertices.Add(FVector(0, 0, 50));
	vertices.Add(FVector(0, 50, 50));
 
	TArray<int32> triangles;
	triangles.Add(0);
	triangles.Add(1);
	triangles.Add(2);
	triangles.Add(2);
	triangles.Add(1);
	triangles.Add(3);
 
	TArray<FVector> normals;
	normals.Add(FVector(1, 0, 0));
	normals.Add(FVector(1, 0, 0));
	normals.Add(FVector(1, 0, 0));
	normals.Add(FVector(1, 0, 0));
 
	TArray<FVector2D> uv0;
	uv0.Add(FVector2D(0, 0));
	uv0.Add(FVector2D(10, 0));
	uv0.Add(FVector2D(0, 10));
	uv0.Add(FVector2D(10, 10));
 
 
	TArray<FProcMeshTangent> tangents;
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
	tangents.Add(FProcMeshTangent(0, 1, 0));
 
	TArray<FLinearColor> vertexColors;
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	vertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
 
	Malla->CreateMeshSection_LinearColor(0, vertices, triangles, normals, uv0, vertexColors, tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void ASuperficie::CreateMallaRegular(int N, int M)
{
	float TamLado = 10.0f;
	float offestX = N * TamLado / 2;
	float offestY = M * TamLado / 2;

	//par convertir el numero de cuadrados, en numero de vertices
	N++;
	M++;
	
	//se resptan esos ofset a los puentos que voy a generar;
	//con el 0.0 al centro de la malla
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			Vertices.Add(FVector(i * TamLado - offestX, j * TamLado - offestY, 0.0f));
		}
	}
 
	for (int i = 0; i < N-1; i++) {
		for (int j = 0; j < M-1; j++) {
			int IdV = i * M + j;
			Triangles.Add(IdV);
			Triangles.Add(IdV + 1);
			Triangles.Add(IdV + M);
			Triangles.Add(IdV + M);
			Triangles.Add(IdV + 1);
			Triangles.Add(IdV + M + 1);
		}
	}
 
	for (int i = 0; i < Vertices.Num(); i++) {// o < N*M
		Normals.Add(FVector(0, 0, 1));
	}
 
	for (int i = 0; i < Vertices.Num(); i++) {// o < N*M
		//UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY));
		UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY)/10.0f);
	}
 
 
	for (int i = 0; i < Vertices.Num(); i++) {// o < N*M
		Tangents.Add(FProcMeshTangent(0, 0, 1));
	}
 
	for (int i = 0; i < Vertices.Num(); i++) {// o < N*M
		VertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	}
 
	Malla->CreateMeshSection_LinearColor(0, Vertices, Triangles, Normals, UV0, VertexColors, Tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void ASuperficie::UpdateMallaRegular(int N, int M)
{
	float TamLado = 10.0f;
	float offestX = N * TamLado / 2;
	float offestY = M * TamLado / 2;

	//par convertir el numero de cuadrados, en numero de vertices
	N++;
	M++;
	
	//se resptan esos ofset a los puentos que voy a generar;
	//con el 0.0 al centro de la malla
	for (int i = 0; i < Vertices.Num(); i++) {
		//Vertices[i].Z = FMath::Lerp<float>(20.0f, 0.0f, (Vertices[0].Size() - Vertices[i].Size())/Vertices[0].Size());
		Vertices[i].Z = FMath::Lerp<float>(20.0f, 0.0f, Vertices[i].Size()/Vertices[0].Size());
	}
 
	Malla->UpdateMeshSection_LinearColor(0, Vertices, Normals, UV0, VertexColors, Tangents);
 
}
