// Fill out your copyright notice in the Description page of Project Settings.

#include "SuperficieDelaunay.h"
#include <iostream>
#include <fstream>
#include <string>
#include "Delaunay.h"

// Sets default values
ASuperficieDelaunay::ASuperficieDelaunay() {
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Malla = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = Malla;
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	//UFUNCTION(BlueprintCallable, Category = "Components|ProceduralMesh", meta = (AutoCreateRefTerm = "Normals,UV0,VertexColors,Tangents"))
	//	void CreateMeshSection(int32 SectionIndex, const TArray<FVector>& Vertices, const TArray<int32>& Triangles, const TArray<FVector>& Normals,
	// const TArray<FVector2D>& UV0, const TArray<FColor>& VertexColors, const TArray<FProcMeshTangent>& Tangents, bool bCreateCollision);
 
	// New in UE 4.17, multi-threaded PhysX cooking.
	Malla->bUseAsyncCooking = true;
}

// Called when the game starts or when spawned
void ASuperficieDelaunay::BeginPlay() {
	Super::BeginPlay();
	
	LeerPrueba();//este carga el conjunto de prueba para el delaunay
	ConvertirStringstoVectorsPrueba();

	Delaunay d;
	Triangulos = d.Triangulate(Vertices, VerticesP);
    ConvertirDelaunayAProceduralMesh();
    CreateMesh();
}

// Called every frame
void ASuperficieDelaunay::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}

void ASuperficieDelaunay::ConvertirDelaunayAProceduralMesh() {
    for (int i = 0; i < VerticesP.size(); i++) {
        Vertexs.Add(*VerticesP[i]);
    }
    for (int i = 0; i < Triangulos.size(); i++) {
        if (!Triangulos[i]->bGhost) {
            Triangles.Add(Triangulos[i]->IdVertices[0]);
            Triangles.Add(Triangulos[i]->IdVertices[1]);
            Triangles.Add(Triangulos[i]->IdVertices[2]);
        }
    }

	for (int i = 0; i < VerticesP.size(); i++) {// o < N*M
		Normals.Add(FVector(1, 0, 0));
	}
 
	for (int i = 0; i < VerticesP.size(); i++) {// o < N*M
		//UV0.Add(FVector2D(Vertices[i].X + offestX, Vertices[i].Y + offestY));
		UV0.Add(FVector2D(VerticesP[i]->X, VerticesP[i]->Y));
	}
 
 
	for (int i = 0; i < VerticesP.size(); i++) {// o < N*M
		Tangents.Add(FProcMeshTangent(0, 1, 0));
	}
 
	for (int i = 0; i < VerticesP.size(); i++) {// o < N*M
		VertexColors.Add(FLinearColor(0.75, 0.75, 0.75, 1.0));
	}
}

void ASuperficieDelaunay::CreateMesh() {
	Malla->CreateMeshSection_LinearColor(0, Vertexs, Triangles, Normals, UV0, VertexColors, Tangents, true);
 
        // Enable collision data
	Malla->ContainsPhysicsTriMeshData(true);
}

void ASuperficieDelaunay::LeerPrueba() {//llee puntos normales en el espacio, para probar la rtiangulacion
	std::string line;
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/Prueba.txt");
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/Prueba2.txt");//este archivo esta invertido para que el eje y sea el x, y se corresponda con la prueba en geogebra
	std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/ConversionDatosPeru110.txt");//este archivo esta invertido para que el eje y sea el x, y se corresponda con la prueba en geogebra
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/ConversionDatosPeru50.txt");//este archivo esta invertido para que el eje y sea el x, y se corresponda con la prueba en geogebra
	//std::ifstream myfile ("D:/AM/Circulo/Mapas/Peru/ConversionDatosPeru10.txt");//este archivo esta invertido para que el eje y sea el x, y se corresponda con la prueba en geogebra
	//archivos de prueba, esot no requiere la covnersion posterior a 
	if (myfile.is_open()) {
		while ( getline (myfile,line) ) {
			//cout << line << '\n';
			PuntosSMapa.Add(line.c_str());
		}
		myfile.close();
	}
	UE_LOG(LogClass, Log, TEXT("Puntos leidos: %d"), PuntosSMapa.Num());
}

void ASuperficieDelaunay::ConvertirStringstoVectorsPrueba() {
	for (int i = 0; i < PuntosSMapa.Num(); i++) {
		FVector punto;
		TArray<FString> puntoss;
		FString hola = ",";
		PuntosSMapa[i].ParseIntoArray(puntoss, *hola);
		if (puntoss.Num() == 2) {
			//punto.X = FCString::Atof(*puntoss[0]);
			//punto.Y = FCString::Atof(*puntoss[1]);
			punto.Y = FCString::Atof(*puntoss[0]);
			punto.X = FCString::Atof(*puntoss[1]);
			Vertices.push_back(punto);
			//PuntosMapa.Add(punto);
		}
	}
	//PuntosMapa.Add(FVector(0.0f));
	UE_LOG(LogClass, Log, TEXT("Puntos convertidos: %d"), Vertices.size());
}

