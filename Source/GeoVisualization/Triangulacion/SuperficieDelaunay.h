// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "TriangleFace.h"
#include <vector>
#include "SuperficieDelaunay.generated.h"

UCLASS()
class GEOVISUALIZATION_API ASuperficieDelaunay : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASuperficieDelaunay();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	TArray<FVector> Vertexs;//Para el procedural mesh

	TArray<int32> Triangles;//Para el procedural mesh

	TArray<FVector> Normals;//Para el procedural mesh

	TArray<FVector2D> UV0;//Para el procedural mesh

	TArray<FProcMeshTangent> Tangents;//Para el procedural mesh

	TArray<FLinearColor> VertexColors;//Para el procedural mesh
	
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent * Malla;
	
    void ConvertirDelaunayAProceduralMesh();

    void CreateMesh();

    void LeerPrueba();

    void ConvertirStringstoVectorsPrueba();

	TArray<FVector> PuntosMapa;

	TArray<FString> PuntosSMapa;

	std::vector<TriangleFace *> Triangulos;

	std::vector<FVector> Vertices;

	std::vector<FVector *> VerticesP;
};
