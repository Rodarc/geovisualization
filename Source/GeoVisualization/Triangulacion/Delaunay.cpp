// Fill out your copyright notice in the Description page of Project Settings.

#include "Delaunay.h"
#include <limits>
#include <vector>

std::vector<TriangleFace *> Delaunay::Triangulate(std::vector<FVector>& VerticesIn, std::vector<FVector * > & VerticesOut) {
    //deberia recivbir un vector con puntors a vertices, o devolver, por que necesitare 
	//divide and conquer
    //Vertices = VerticesIn;
    Vertices.resize(VerticesIn.size());
    for (int i = 0; i < VerticesIn.size(); i++) {
        Vertices[i] = new FVector(VerticesIn[i]);
    }
	//PrepararParticiones(Vertices, 0, Vertices.size() - 1, 0);
	PrepararParticiones(0, Vertices.size() - 1, 0);
    cout << "Particiones listas" << endl;

	//DivideConquerP(Vertices, 0, Vertices.size() - 1, 0);

    cout << "Triangulacion finalizada" << endl;
	DivideConquerP(0, Vertices.size() - 1, 0);
	//remover ghost;
	//tal vez los ghoste deba conservarlos, o tal vez tenerlos auiq el refinamiento y todo atnes de removerlos

    VerticesOut = Vertices;
    return Triangulacion;
}

void Delaunay::MergeSortA(std::vector<FVector *> & VerticesIn, int ini, int fin, bool eje) {
    //cout << "MergeSort" << endl;
	if (fin - ini == 0) {//osea, es un verctor de tama�o uno, no se hace nada
		return;
	}
	int med = (ini + fin) / 2;
	MergeSortA(VerticesIn, ini, med, eje);
	MergeSortA(VerticesIn, med + 1, fin, eje);
	MergeA(VerticesIn, ini, med, fin, eje);
}

void Delaunay::MergeA(std::vector<FVector *> & VerticesIn, int ini, int med, int fin, bool eje) {
    //cout << "Merge" << endl;
	std::vector<FVector *> acum (fin - ini + 1);
	int i = ini;
	int j = med + 1;
	int k = 0;//indice para el vector acum
	if (eje) {//si toca ordenar por el eje Y en forma decreciente
		while (i <= med && j <= fin) {
			if (VerticesIn[i]->Y > VerticesIn[j]->Y || (VerticesIn[i]->Y == VerticesIn[j]->Y && VerticesIn[i]->X < VerticesIn[j]->X)) {//si i es mayor se pone primero
				acum[k] = VerticesIn[i];
				i++;
			}
			else {//el otro caso si es mayo el de j
				acum[k] = VerticesIn[j];
				j++;
			}
			k++;
		}
	}
	else {//si toca ordenar por el eje X en forma ascendente
		while (i <= med && j <= fin) {
			if (VerticesIn[i]->X < VerticesIn[j]->X || (VerticesIn[i]->X == VerticesIn[j]->X && VerticesIn[i]->Y > VerticesIn[j]->Y)) {//si i es menor se pone primero
				acum[k] = VerticesIn[i];
				i++;
			}
			else {//el otro caso si es menor el de j
				acum[k] = VerticesIn[j];
				j++;
			}
			k++;
		}
	}
	//creo que este ordenamiento faltante deberia ser con el nuevo criterio tambien, ya que esta cambiando todo el tiempo
	while(j <= fin){
        acum[k] = VerticesIn[j];
        j++;
        k++;
    }
    while(i <= med){
        acum[k] = VerticesIn[i];
        i++;
        k++;
    }
	for(int idx = 0; idx < fin-ini+1; idx++){
        VerticesIn[ini+idx] = acum[idx];
    }
	//creo que este sort no va a funcionar
}

//void Delaunay::PrepararParticiones(std::vector<FVector>& Vertices, int ini, int fin, bool eje) {
void Delaunay::PrepararParticiones(int ini, int fin, bool eje) {
    cout << "Preparando Particion" << endl;
    if (fin - ini < 1) {
        cout << "caso 1" << endl;
        return;
    }
	if (fin - ini == 2 || fin - ini == 1) {//es de tama�o 3, o 2
		//caso base
        cout << "Caso base particion [" << ini << ", " << fin << "] eje: 0" << endl;
		eje = 0;
		MergeSortA(Vertices, ini, fin, eje);
        //ImprimirVertices();
	}
	else {
        cout << "Caso particion [" << ini << ", " << fin << "] eje: " << eje << endl;
		int med = (ini + fin) / 2;
		MergeSortA(Vertices, ini, fin, eje);
        //ImprimirVertices();

		//eje 0 se orden con criterio principal X de forma ascendente, para hacer un corte vertical, 
		//eje 1 se ordena con criterio principal Y de frma descendente, para hacer un corte horizontal
		PrepararParticiones(ini, med, !eje);
		PrepararParticiones(med + 1, fin, !eje);
	}
}

void Delaunay::BuscarPuntosTangentesCorteHorizontal(std::vector<TriangleFace *>& Triangulacion1, std::vector<TriangleFace *>& Triangulacion2, TriangleFace * & TLTop, TriangleFace * & TLBottom, TriangleFace * & TLNext, TriangleFace * & TLPrev, TriangleFace * & TRTop, TriangleFace * & TRBottom, TriangleFace * & TRNext, TriangleFace * & TRPrev) {
    //deberia ser vectores de punteros no de ints
    //esta funcion triangulos desde don de estaran las tangentes para el convex hull del merge
    //las variables enteras seran salidas de esta funcion, alli almacenare lo encontrado
    //cuando eje vale 
    //esta funcion triangulos desde don de estaran las tangentes para el convex hull del merge
    //las variables enteras seran salidas de esta funcion, alli almacenare lo encontrado
    //cuando eje vale 
    //buscar los puntos mas cercanos al corte.
    //es decir en el hull izquierdo, buscar el vertice mas a la derecha, y en el hull derecho, buscar el vertice mas a la izquierda

    //tanto en el primer triangulo unido, como en el ultimo se debn crear un triangulo fantasma, esto hacerlo al final, antes de terminar el merge
    //guardar un puntero a ellos o algo, par ponerlo en el lugar apropiado
    //necestio encontrar los vetices arriba y abajo mas a la derecha e izquierda de cada triangulacion

    //una mejora seria solo encontrar el primer triangulo fantasma en cada trianculacion, y usarlo es para navegar solo en los triangulos fantasmas
    float min = std::numeric_limits<float>::max();
    float max = -std::numeric_limits<float>::max();

    TriangleFace * TL = nullptr;
    for (int i = 0; i < Triangulacion2.size(); i++) {//solo examinar uno de los vertices de cada cara del triangulo, de preferencia, el vertice 0
        //este recorrido deberia ser por los triangulos ghost solamente usando sus punteros internos, no con este for sobre todos los triauangulos
        if (Triangulacion2[i]->bGhost) {
            if (Triangulacion2[i]->Vertices[0]->Y > max) {
                TL = Triangulacion2[i];
                max = Triangulacion2[i]->Vertices[0]->Y;
            }
        }
    }
    //ahora toca del lado derecho
    min = std::numeric_limits<float>::max();
    max = -std::numeric_limits<float>::max();

    TriangleFace * TR = nullptr;
    for (int i = 0; i < Triangulacion1.size(); i++) {//solo examinar uno de los vertices de cada cara del triangulo, de preferencia, el vertice 0
        //este recorrido deberia ser por los triangulos ghost solamente usando sus punteros internos, no con este for sobre todos los triauangulos
        if (Triangulacion1[i]->bGhost) {
            if (Triangulacion1[i]->Vertices[1]->Y < min) {
                TR = Triangulacion1[i];
                min = Triangulacion1[i]->Vertices[1]->Y;
            }
        }
    }
    //obtenidos los puntos mas cercanos entre los dos hulls

    //TL = Triangulacion[Triangulacion2[TL]];
    //TR = Triangulacion[Triangulacion1[TR]];
    //ya estan los punteros correctos

    //buscar tangente superior
    TLTop = TL;
    TRTop = TR;
    bool Hecho = false;
    while (!Hecho) {
        Hecho = true;

        //trabajar con incides globales no lo cales
        TLPrev = TLTop->TriangulosAdjacentes[2];
        while (DeteccionDeLadoRelativo(*TLTop->Vertices[0], *TRTop->Vertices[1], *TLPrev->Vertices[0]) < 0) {//neceisto mandarle los vectores
            //debo hacer que el izquierdo avance hacia arriba hasta que el vertice de su triagulo anterior este por debajo de l triangulo izquiero actual
            TLTop = TLPrev;
            TLPrev = TLTop->TriangulosAdjacentes[2];
        }

        TRPrev = TRTop->TriangulosAdjacentes[1];
        while (DeteccionDeLadoRelativo(*TLTop->Vertices[0], *TRTop->Vertices[1], *TRPrev->Vertices[1]) < 0) {//neceisto mandarle los vectores
            //debo hacer que el derecho avance hacia arriba hasta que el vertice de su triagulo anterior en el recorrido este por debajo del triangulo derecho actual
            TRTop = TRPrev;
            TRPrev = TRTop->TriangulosAdjacentes[1];
            Hecho = false;
        }
    }
    // al final del bucle deberia estar ubicado correctamente la tangente

    TL = TL->TriangulosAdjacentes[2];
    TR = TR->TriangulosAdjacentes[1];
    //estas lineas son debido a que en la siguinete parte, para buscar la tangente inferior, utilizo los otros vertices del triangulo, por lo tanto tl y tr correr una poscion
    //para estos vertices que encontre como mas cercanos netre los hulls, sigan siendo los mismos
    //ejemplo si tl tenia el vertice 0 como mas cercano al otro hull, en lo que sigue del algorimo, ya no trabajo con el vertice 0 trabajo con el 1, por lo tanto el triangulo anterior es el que tiene ese vertice como vertice 1

    //buscar tangente inferior 
    //probando una forma de navegar sobre el hull, escogiendo el otro vertice, puede que sea mas dificl de leer
    TLBottom = TL;
    TRBottom = TR;
    Hecho = false;
    while (!Hecho) {
        Hecho = true;
        //trabajar con incides globales no lo cales
        TLNext = TLBottom->TriangulosAdjacentes[1];
        while (DeteccionDeLadoRelativo(*TLBottom->Vertices[1], *TRBottom->Vertices[0], *TLNext->Vertices[1]) > 0) {//neceisto mandarle los vectores
            //debo hacer que el izquierdo avance hacia arriba hasta que el vertice de su triagulo anterior este por debajo de l triangulo izquiero actual
            TLBottom = TLNext;
            TLNext = TLBottom->TriangulosAdjacentes[1];
        }

        TRNext = TRBottom->TriangulosAdjacentes[2];
        while (DeteccionDeLadoRelativo(*TLBottom->Vertices[1], *TRBottom->Vertices[0], *TRNext->Vertices[0]) > 0) {//neceisto mandarle los vectores
            //debo hacer que el derecho avance hacia arriba hasta que el vertice de su triagulo anterior en el recorrido este por debajo del triangulo derecho actual
            TRBottom = TRNext;
            TRNext = TRBottom->TriangulosAdjacentes[2];
            Hecho = false;
        }
    }
    // al final del bucle deberia estar ubicado correctamente la tangente
    // comprobar que todo esto este funcionando bien!!!
    //ya estan los indices correctos, no hago nada mas, fin de la funcion
    //ya estan en funcion de la triangulacion generla, no necesita ninguna conversion

    //los indices estaran en funcion de cada uno de sus convexhull, de sus arreglos 1 y 2
    //estos valores se debern retornar en funcion de la triangulacion general, por eso estas lineas siguientes
}

void Delaunay::BuscarPuntosTangentesCorteVertical(std::vector<TriangleFace *>& Triangulacion1, std::vector<TriangleFace *>& Triangulacion2, TriangleFace * & TLTop, TriangleFace * & TLBottom, TriangleFace * & TLNext, TriangleFace * & TLPrev, TriangleFace * & TRTop, TriangleFace * & TRBottom, TriangleFace * & TRNext, TriangleFace * & TRPrev) {
    //esta funcion triangulos desde don de estaran las tangentes para el convex hull del merge
    //las variables enteras seran salidas de esta funcion, alli almacenare lo encontrado
    //cuando eje vale 
    //buscar los puntos mas cercanos al corte.
    //es decir en el hull izquierdo, buscar el vertice mas a la derecha, y en el hull derecho, buscar el vertice mas a la izquierda

    //tanto en el primer triangulo unido, como en el ultimo se debn crear un triangulo fantasma, esto hacerlo al final, antes de terminar el merge
    //guardar un puntero a ellos o algo, par ponerlo en el lugar apropiado
    //necestio encontrar los vetices arriba y abajo mas a la derecha e izquierda de cada triangulacion

    //una mejora seria solo encontrar el primer triangulo fantasma en cada trianculacion, y usarlo es para navegar solo en los triangulos fantasmas
    float min = std::numeric_limits<float>::max();
    float max = -std::numeric_limits<float>::max();

    TriangleFace * TL = nullptr;
    for (int i = 0; i < Triangulacion1.size(); i++) {//solo examinar uno de los vertices de cada cara del triangulo, de preferencia, el vertice 0
        //este recorrido deberia ser por los triangulos ghost solamente usando sus punteros internos, no con este for sobre todos los triauangulos
        if (Triangulacion1[i]->bGhost) {
            if (Triangulacion1[i]->Vertices[0]->X > max) {
                TL = Triangulacion1[i];
                max = Triangulacion1[i]->Vertices[0]->X;
            }
        }
    }
    //ahora toca del lado derecho
    min = std::numeric_limits<float>::max();
    max = -std::numeric_limits<float>::max();

    TriangleFace * TR = nullptr;
    for (int i = 0; i < Triangulacion2.size(); i++) {//solo examinar uno de los vertices de cada cara del triangulo, de preferencia, el vertice 0
        //este recorrido deberia ser por los triangulos ghost solamente usando sus punteros internos, no con este for sobre todos los triauangulos
        if (Triangulacion2[i]->bGhost) {
            if (Triangulacion2[i]->Vertices[1]->X < min) {
                TR = Triangulacion2[i];
                min = Triangulacion2[i]->Vertices[1]->X;
            }
        }
    }
    //obtenidos los puntos mas cercanos entre los dos hulls

    //TL = Triangulacion1[TL];
    //TR = Triangulacion2[TR];

    //buscar tangente superior
    TLTop = TL;
    TRTop = TR;
    bool Hecho = false;
    while (!Hecho) {
        Hecho = true;
        //trabajar con incides globales no lo cales
        TLPrev = TLTop->TriangulosAdjacentes[2];
        while (DeteccionDeLadoRelativo(*TLTop->Vertices[0], *TRTop->Vertices[1], *TLPrev->Vertices[0]) < 0) {//neceisto mandarle los vectores
            //debo hacer que el izquierdo avance hacia arriba hasta que el vertice de su triagulo anterior este por debajo de l triangulo izquiero actual
            TLTop = TLPrev;
            TLPrev = TLTop->TriangulosAdjacentes[2];
        }

        TRPrev = TRTop->TriangulosAdjacentes[1];
        while (DeteccionDeLadoRelativo(*TLTop->Vertices[0], *TRTop->Vertices[1], *TRPrev->Vertices[1]) < 0) {//neceisto mandarle los vectores
            //debo hacer que el derecho avance hacia arriba hasta que el vertice de su triagulo anterior en el recorrido este por debajo del triangulo derecho actual
            TRTop = TRPrev;
            TRPrev = TRTop->TriangulosAdjacentes[1];
            Hecho = false;
        }
    }
    // al final del bucle deberia estar ubicado correctamente la tangente

    TL = TL->TriangulosAdjacentes[2];
    TR = TR->TriangulosAdjacentes[1];
    //estas lineas son debido a que en la siguinete parte, para buscar la tangente inferior, utilizo los otros vertices del triangulo, por lo tanto tl y tr correr una poscion
    //para estos vertices que encontre como mas cercanos netre los hulls, sigan siendo los mismos
    //ejemplo si tl tenia el vertice 0 como mas cercano al otro hull, en lo que sigue del algorimo, ya no trabajo con el vertice 0 trabajo con el 1, por lo tanto el triangulo anterior es el que tiene ese vertice como vertice 1

    //buscar tangente inferior 
    //probando una forma de navegar sobre el hull, escogiendo el otro vertice, puede que sea mas dificl de leer
    TLBottom = TL;
    TRBottom = TR;
    Hecho = false;
    while (!Hecho) {
        Hecho = true;
        //trabajar con incides globales no lo cales
        TLNext = TLBottom->TriangulosAdjacentes[1];
        while (DeteccionDeLadoRelativo(*TLBottom->Vertices[1], *TRBottom->Vertices[0], *TLNext->Vertices[1]) > 0) {//neceisto mandarle los vectores
            //debo hacer que el izquierdo avance hacia arriba hasta que el vertice de su triagulo anterior este por debajo de l triangulo izquiero actual
            TLBottom = TLNext;
            TLNext = TLBottom->TriangulosAdjacentes[1];
        }

        TRNext = TRBottom->TriangulosAdjacentes[2];
        while (DeteccionDeLadoRelativo(*TLBottom->Vertices[1], *TRBottom->Vertices[0], *TRNext->Vertices[0]) > 0) {//neceisto mandarle los vectores
            //debo hacer que el derecho avance hacia arriba hasta que el vertice de su triagulo anterior en el recorrido este por debajo del triangulo derecho actual
            TRBottom = TRNext;
            TRNext = TRBottom->TriangulosAdjacentes[2];
            Hecho = false;
        }
    }
    // al final del bucle deberia estar ubicado correctamente la tangente
    // comprobar que todo esto este funcionando bien!!!
    //ya estan los indices correctos, no hago nada mas, fin de la funcion
    //ya estan en funcion de la triangulacion generla, no necesita ninguna conversion

    //los indices estaran en funcion de cada uno de sus convexhull, de sus arreglos 1 y 2
    //estos valores se debern retornar en funcion de la triangulacion general, por eso estas lineas siguientes
}

std::vector<TriangleFace *> Delaunay::DivideConquerP(int ini, int fin, bool eje) {
    //los indices son para el vector de vertices!!!
	//desciende como en preprara particiones, epro esta vez para hacer los merge de las partes
	//en los casos base se crean los triangulos
    //por ahora estoy devolviendo un vector de indices sobre el vector de triangulos, pero creo que deberia devolver un vector de punteos a triangulos, que corresponden a esta triangulacion
    //este vector d
    cout << "Dividiendo y conquistando: " << ini << " " << fin << endl;
	if (fin - ini <= 0) {
        //cout << "caso base " << ini << " " << fin << endl;
		return std::vector<TriangleFace *>();
	}
	if (fin - ini == 1) {//es de tama�o 2
		//caso base, ini es un vertice menor
		//crear triangulos, y llenado algunos datos
        cout << "Caso tama�o 2" << endl;
		TriangleFace * Triangulo1 = new TriangleFace;
		TriangleFace * Triangulo2 = new TriangleFace;

		//introduciendolos en el arreglo de triangulos, y haciendo sus asocianes dentro del arreglo de triangluos para que se correspondan correctamente los punteros
		//en este pushback estoy perdiendo tiempo, ya que copia datos crea nuevos triangulos, es un poco deperdicio
		Triangulacion.push_back(Triangulo1);//sera el size()-2
		Triangulacion.push_back(Triangulo2);//sera el size()-1
		//tener cuidado con esto, si fuera paralelo fallaria
		int idTriangulo1 = Triangulacion.size() - 2;
		int idTriangulo2 = Triangulacion.size() - 1;
        //no necesitare usar estos indices aun, dejarlos por si acaso
        //si los uso en los inicide, dentro de cada cara, pero creo que seran prescindibles, dejarlos por ahora que triangleface tnga esos indices

		Triangulo1->bGhost = true;
		Triangulo1->Id = idTriangulo1;
		Triangulo1->Vertices[0] = Vertices[ini];
		Triangulo1->IdVertices[0] = ini;
		Triangulo1->Vertices[1] = Vertices[fin];
		Triangulo1->IdVertices[1] = fin;
		Triangulo1->Vertices[2] = nullptr;
		Triangulo1->IdVertices[2] = -1;

		Triangulo2->bGhost = true;
		Triangulo2->Id = idTriangulo2;
		Triangulo2->Vertices[0] = Vertices[fin];
		Triangulo2->IdVertices[0] = fin;
		Triangulo2->Vertices[1] = Vertices[ini];
		Triangulo2->IdVertices[1] = ini;
		Triangulo2->Vertices[2] = nullptr;
		Triangulo2->IdVertices[2] = -1;


        Triangulo1->TriangulosAdjacentes[0] = Triangulo2;
		Triangulo1->IdTriangulosAdjacentes[0] = idTriangulo2;
		Triangulo1->OrientacionesTriangulosAdjacentes[0] = 0;//esta arista se conecta con la arista 0 del triangulo2
        Triangulo1->TriangulosAdjacentes[1] = Triangulo2;
		Triangulo1->IdTriangulosAdjacentes[1] = idTriangulo2;
		Triangulo1->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
		Triangulo1->TriangulosAdjacentes[2] = Triangulo2;
		Triangulo1->IdTriangulosAdjacentes[2] = idTriangulo2;
		Triangulo1->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 1 del triangulo2

		Triangulo2->TriangulosAdjacentes[0] = Triangulo1;
		Triangulo2->IdTriangulosAdjacentes[0] = idTriangulo1;
		Triangulo2->OrientacionesTriangulosAdjacentes[0] = 0;//esta arista se conecta con la arista 0 del triangulo1
		Triangulo2->TriangulosAdjacentes[1] = Triangulo1;
        Triangulo2->IdTriangulosAdjacentes[1] = idTriangulo1;
		Triangulo2->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 0 del triangulo1
		Triangulo2->TriangulosAdjacentes[2] = Triangulo1;
        Triangulo2->IdTriangulosAdjacentes[2] = idTriangulo1;
        Triangulo2->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 0 del triangulo1

        std::vector<TriangleFace *> respointers;
		respointers.push_back(Triangulo1);
		respointers.push_back(Triangulo2);
        return respointers;

		/*std::vector<int> res;
		res.push_back(idTriangulo1);
		res.push_back(idTriangulo2);
		return res;*/
	}
	else if (fin - ini == 2) {//es de tama�o 3
		//caso base
		//crear triangulos
		//pero como determinar el orden de posicion de los triangulos? el orden de los triangulos ya me dice algo, ya que esta con primer filtro el x menores, y lueog el y mayores?
		//trazar un linea desde el primero al ultimo, y ver si el punto del medio esta por encimao o por abajo de esa linea. asi sabre en que posicion asiganer, para que esten el orden correcto segun reglad de mano derecha
		//para por saber esa informacion, solo debo generalr la ecuacion de esa recta, y reemplazar el punto que quiero verificar, si es mayor que 0 0 1(no recuerdo cual era, esta por encima, si es menor esta por abajo
        //cout << "Caso tama�o 3" << endl;
        //terne cuidado con los valores que esoy suando, creo que no deberia usar x, o si?
        //cout << Vertices[ini].X << ", " << Vertices[ini+1].X << ", " << Vertices[fin].X << endl;
		float Orden = DeteccionDeLadoRelativo(*Vertices[ini], *Vertices[ini+1], *Vertices[fin]);
		TriangleFace * Triangulo0 = new TriangleFace;
		TriangleFace * Triangulo1 = new TriangleFace;
		TriangleFace * Triangulo2 = new TriangleFace;
		TriangleFace * Triangulo3 = new TriangleFace;

		Triangulacion.push_back(Triangulo0);//sera el size()-4
		Triangulacion.push_back(Triangulo1);//sera el size()-3
		Triangulacion.push_back(Triangulo2);//sera el size()-2
		Triangulacion.push_back(Triangulo3);//sera el size()-1

		int idTriangulo0 = Triangulacion.size() - 4;
		int idTriangulo1 = Triangulacion.size() - 3;
		int idTriangulo2 = Triangulacion.size() - 2;
		int idTriangulo3 = Triangulacion.size() - 1;

		if (Orden > 0) {//esta en sentido de las agujas del reloj
            //fin esta debajo de la recta de ini a ini+1
			Triangulo0->bGhost = false;
            Triangulo0->Id = idTriangulo0;
			Triangulo0->Vertices[0] = Vertices[ini];
			Triangulo0->IdVertices[0] = ini;
			Triangulo0->Vertices[1] = Vertices[fin];
			Triangulo0->IdVertices[1] = fin;
			Triangulo0->Vertices[2] = Vertices[ini+1];
			Triangulo0->IdVertices[2] = ini+1;
		}
		else {//esta en sentido contrario a las agujas del reloj
            //fin esta encima de la recta de ini a ini+1
			Triangulo0->bGhost = false;
            Triangulo0->Id = idTriangulo0;
			Triangulo0->Vertices[0] = Vertices[ini];
			Triangulo0->IdVertices[0] = ini;
			Triangulo0->Vertices[1] = Vertices[ini+1];
			Triangulo0->IdVertices[1] = ini+1;
			Triangulo0->Vertices[2] = Vertices[fin];
			Triangulo0->IdVertices[2] = fin;
		}
		//los siguientes triangulos crearlo con los valores que estan en el triangulo 0, no con la entrada,
		Triangulo1->bGhost = true;
        Triangulo1->Id = idTriangulo1;
		Triangulo1->Vertices[0] = Triangulo0->Vertices[1];
		Triangulo1->IdVertices[0] = Triangulo0->IdVertices[1];
		Triangulo1->Vertices[1] = Triangulo0->Vertices[0];
		Triangulo1->IdVertices[1] = Triangulo0->IdVertices[0];
		Triangulo1->Vertices[2] = nullptr;
		Triangulo1->IdVertices[2] = -1;

		Triangulo2->bGhost = true;
        Triangulo2->Id = idTriangulo2;
		Triangulo2->Vertices[0] = Triangulo0->Vertices[2];
		Triangulo2->IdVertices[0] = Triangulo0->IdVertices[2];
		Triangulo2->Vertices[1] = Triangulo0->Vertices[1];
		Triangulo2->IdVertices[1] = Triangulo0->IdVertices[1];
		Triangulo2->Vertices[2] = nullptr;
		Triangulo2->IdVertices[2] = -1;

		Triangulo3->bGhost = true;
        Triangulo3->Id = idTriangulo3;
		Triangulo3->Vertices[0] = Triangulo0->Vertices[0];
		Triangulo3->IdVertices[0] = Triangulo0->IdVertices[0];
		Triangulo3->Vertices[1] = Triangulo0->Vertices[2];
		Triangulo3->IdVertices[1] = Triangulo0->IdVertices[2];
		Triangulo3->Vertices[2] = nullptr;
		Triangulo3->IdVertices[2] = -1;

		//ahora toca agregarlos al array, y asignar los punters, conectarlos


		Triangulo0->TriangulosAdjacentes[0] = Triangulo1;
		Triangulo0->IdTriangulosAdjacentes[0] = idTriangulo1;
		Triangulo0->OrientacionesTriangulosAdjacentes[0] = 0;//esta arista se conecta con la arista 0 del triangulo2
		Triangulo0->TriangulosAdjacentes[1] = Triangulo2;
		Triangulo0->IdTriangulosAdjacentes[1] = idTriangulo2;
		Triangulo0->OrientacionesTriangulosAdjacentes[1] = 0;//esta arista se conecta con la arista 2 del triangulo2
		Triangulo0->TriangulosAdjacentes[2] = Triangulo3;
		Triangulo0->IdTriangulosAdjacentes[2] = idTriangulo3;
		Triangulo0->OrientacionesTriangulosAdjacentes[2] = 0;//esta arista se conecta con la arista 1 del triangulo2

		Triangulo1->TriangulosAdjacentes[0] = Triangulo0;
		Triangulo1->IdTriangulosAdjacentes[0] = idTriangulo0;
		Triangulo1->OrientacionesTriangulosAdjacentes[0] = 0;//esta arista se conecta con la arista 0 del triangulo2
		Triangulo1->TriangulosAdjacentes[1] = Triangulo3;
		Triangulo1->IdTriangulosAdjacentes[1] = idTriangulo3;
		Triangulo1->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
		Triangulo1->TriangulosAdjacentes[2] = Triangulo2;
		Triangulo1->IdTriangulosAdjacentes[2] = idTriangulo2;
		Triangulo1->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 1 del triangulo2

		Triangulo2->TriangulosAdjacentes[0] = Triangulo0;
		Triangulo2->IdTriangulosAdjacentes[0] = idTriangulo0;
		Triangulo2->OrientacionesTriangulosAdjacentes[0] = 0;//esta arista se conecta con la arista 0 del triangulo2
		Triangulo2->TriangulosAdjacentes[1] = Triangulo1;
		Triangulo2->IdTriangulosAdjacentes[1] = idTriangulo1;
		Triangulo2->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
		Triangulo2->TriangulosAdjacentes[2] = Triangulo3;
		Triangulo2->IdTriangulosAdjacentes[2] = idTriangulo3;
		Triangulo2->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 1 del triangulo2

		Triangulo3->TriangulosAdjacentes[0] = Triangulo0;
		Triangulo3->IdTriangulosAdjacentes[0] = idTriangulo0;
		Triangulo3->OrientacionesTriangulosAdjacentes[0] = 0;//esta arista se conecta con la arista 0 del triangulo2
		Triangulo3->TriangulosAdjacentes[1] = Triangulo2;
		Triangulo3->IdTriangulosAdjacentes[1] = idTriangulo2;
		Triangulo3->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
		Triangulo3->TriangulosAdjacentes[2] = Triangulo1;
		Triangulo3->IdTriangulosAdjacentes[2] = idTriangulo1;
		Triangulo3->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 1 del triangulo2

        std::vector<TriangleFace *> respointers;
		respointers.push_back(Triangulo0);
		respointers.push_back(Triangulo1);
		respointers.push_back(Triangulo2);
		respointers.push_back(Triangulo3);
        return respointers;

        //cout << "despues de insertar en el vector de salida" << endl;
        //cout << Triangulacion[res[0]].Vertices[0] << ", " << Triangulacion[res[0]].Vertices[1] << ", " << res[0]->Vertices[2] << endl;
	}
	else {//si es de tama�o 4 o mas
        cout << "Caso normal" << endl;
		int med = (ini + fin) / 2;

		std::vector<TriangleFace *> T1 = DivideConquerP(ini, med, !eje);
		std::vector<TriangleFace *> T2 = DivideConquerP(med + 1, fin, !eje);
		//estas dos cosas deben debolver trigangulaciones en un vector de pounteros, apra saber como estan organizados

        cout << " terminada la division" << endl;

		return MergeTriangulations(T1, T2, ini, med, fin, eje);
		//esta funcion deberia retornar un veector de tpunteros de truangulaciones la cual ebe ser retornado en este para que se ahgan lsa uniones recursivas
		//al final de todo, triangulacioens, y el vector de puntoreso a caras, seran el mismo, con lo cual es deesechable el vector de punteros solo alli es necesario,
		//otro enfoque es que las funciones siempre trabajen uniendo para evtiar eso de punteros, es deir que este merrce, una dos vectores de caras cada un a devuelta por esta funcion de dividr y conquistar
	}
}

std::vector<TriangleFace *> Delaunay::MergeTriangulations(std::vector<TriangleFace *>& Triangulacion1, std::vector<TriangleFace *>& Triangulacion2, int ini, int med, int fin, bool eje) {
    //los indices son para trabajar en el vector de vertices
    cout << "Mezclando: " << eje << endl;
    //estos en lugar de indices seran punteros
    TriangleFace * TLTop;
    TriangleFace * TLBottom;
    TriangleFace * TLNext;
    TriangleFace * TLPrev;
    TriangleFace * TL;
    TriangleFace * TRTop;
    TriangleFace * TRBottom;
    TriangleFace * TRNext;
    TriangleFace * TRPrev;
    TriangleFace * TR;
	if (eje) {//esta cortado horizontalmente
        BuscarPuntosTangentesCorteHorizontal(Triangulacion1, Triangulacion2, TLTop, TLBottom, TLNext, TLPrev, TRTop, TRBottom, TRNext, TRPrev);
	}
	else {//esta cortado verticalmente
        BuscarPuntosTangentesCorteVertical(Triangulacion1, Triangulacion2, TLTop, TLBottom, TLNext, TLPrev, TRTop, TRBottom, TRNext, TRPrev);
	}
    //TRTOP com TRNExte son valores en funcio al tama�o de de las dos triangulaciones
    //deben ser convertidos a la triangulacion general
    //y trabjar alli
    //cout << "Establecidos los triangulos extremos" << endl;
	//ya tengo los triangulos respectivos, los indices
	//ahora se procede la union, debo estabrelcer los recorreidos, el izquierod en un sentido, el derecho en el contrario, ambos desde top hasta bottom
    //tanto rightbottom como left botton no deben ser unidos, solo se une hasta antes de esos triangulos, por lo tanto tl y tr next, son precisamente leftbottom y rightbottom
    //temporalmente almacenare los anterieos a right boton antes

	//guardando lo puntero de los anteriosres a los tops para enalzarlos con el nuevo fantasma
    //son punteros a los triangulos fuera de la region que se unie,para asiluego cerrar el cierculo

	TL = TLTop;//debe inidicar en  las iteracinoes en top
	TR = TRTop;

	//int UltimoUnido = -1;
	TriangleFace * UltimoUnido = nullptr;
    bool LadoUltimoUnido = false;//falso, lado izqueirdo, true, lado derecho
	TriangleFace * PrimerUnido = nullptr;
    bool LadoPrimerUnido = true;//falso, lado izqueirdo, true, lado derecho
    //siempre se necesita el ultimo unido, para poder establecer las conexiones con el el
    //pero no suarlo en la parte de de los triangulos nuevos

    int AristaIntersectada = -1;
    if (eje) {
        //while(!EsDelaunayConElOtroHull(TL->TriangulosAdjacentes[0], Triangulacion1, AristaIntersectada)) {
        while(!EsDelaunayConLaOtraTriangulacion(TL->TriangulosAdjacentes[0], ini, med, AristaIntersectada)) {
            cout << TL->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
            //ConvertirEnFantasmaTrianguloHullLeft(*TL, *TL->TriangulosAdjacentes[0]);
            ConvertirEnFantasmaTrianguloHullLeft(*TL->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TL->TriangulosAdjacentes[0]);
            TLBottom = TLNext->TriangulosAdjacentes[2];//si elimino por casualidad el tl bottom, osea si tl es tl bottom, este tlbotton deberia cambiar, o la mezcla podria terminar con erroes
        }
        //while(!EsDelaunayConElOtroHull(TR->TriangulosAdjacentes[0], Triangulacion2, AristaIntersectada)) {
        while(!EsDelaunayConLaOtraTriangulacion(TR->TriangulosAdjacentes[0], med+1, fin, AristaIntersectada)) {
            cout << TR->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
            //ConvertirEnFantasmaTrianguloHullRight(*TR, *TR->TriangulosAdjacentes[0]);
            ConvertirEnFantasmaTrianguloHullRight(*TR->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TR->TriangulosAdjacentes[0]);
            TRBottom = TRNext->TriangulosAdjacentes[1];
            //esta cosa se arreglaria si hiciera un calculo para elimnar estas cosas en una funcion aparte antes de inicar el merge
            //tal vez tambien deba hacerlo con el top
        }
        float ValEnCirculo = EnCirculo(*TL->Vertices[0], *TL->Vertices[1], *TR->Vertices[1], *TR->Vertices[0]);
        if (ValEnCirculo < 0) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
            //usar otro criterio para ese if, deberia verificar cual de los dos formara un mejor triangulo de delaunay, y escoger ese lado
            TL->bGhost = false;
            TL->Vertices[2] = TR->Vertices[1];
            TL->IdVertices[2] = TR->IdVertices[1];
            //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
            //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
            //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
            //if (UltimoUnido != -1) {
            if (UltimoUnido) {// != nullptr
                if (TL->TriangulosAdjacentes[2] == UltimoUnido) {//el aterio es vecino
                    //esto signifca que el ultimo unido era mi vecino anterior, por lo tanto no tengo que cambir los punteros de las artisas para enlazarlos, solo se hace en el otro cosas,
                    //en el que el ultimo unido no era mi vecino
                    //en realidad no deberia hacer nada
                }
                else {//ultimo unido no es mi adyacente, es del otro hull

                    TL->TriangulosAdjacentes[2] = UltimoUnido;//no deberia tener este orientacion?
                    TL->IdTriangulosAdjacentes[2] = UltimoUnido->Id;//no deberia tener este orientacion?
                    TL->OrientacionesTriangulosAdjacentes[2] = 2;//no deberia tener este orientacion?
                    UltimoUnido->TriangulosAdjacentes[2] = TL;
                    UltimoUnido->IdTriangulosAdjacentes[2] = TL->Id;
                    UltimoUnido->OrientacionesTriangulosAdjacentes[2] = 2;//revisar vien esto
                    //UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia, por lo cual esta linea no debe estar aqui
                }
            }
            PrimerUnido = TL;
            LadoPrimerUnido = false;//lado izqueirdo
            UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia,
            LadoUltimoUnido = false;
            TL = TL->TriangulosAdjacentes[1];
        }
        else {//el derecho es mauor
            TR->bGhost = false;
            TR->Vertices[2] = TL->Vertices[0];
            TR->IdVertices[2] = TL->IdVertices[0];
            //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
            //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
            //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
            //if (UltimoUnido != -1) {
            if (UltimoUnido) {
                if (TR->TriangulosAdjacentes[1] == UltimoUnido) {//el aterio es vecino
                    //en realidad no deberia hacer nada
                }
                else {//ultimo unido no es mi adyacente, es del otro hull
                    //aqui sei debo hacer cambios
                    TR->TriangulosAdjacentes[1] = UltimoUnido;
                    TR->IdTriangulosAdjacentes[1] = UltimoUnido->Id;
                    TR->OrientacionesTriangulosAdjacentes[1] = 1;
                    UltimoUnido->TriangulosAdjacentes[1] = TR;
                    UltimoUnido->IdTriangulosAdjacentes[1] = TR->Id;
                    UltimoUnido->OrientacionesTriangulosAdjacentes[1] = 1;//revisar ben esta linea
                }
            }
            PrimerUnido = TR;
            LadoPrimerUnido = true;//lado derecho
            UltimoUnido = TR;
            LadoUltimoUnido = true;
            TR = TR->TriangulosAdjacentes[2];
        }

        while (TL != TLNext && TR != TRNext) {//los minimos no forma parte de la union, yq ue esos contiene al vertice inferior, como inico, tener cuidado con cruzamientos
        //while (TL != Triangulacion1[TLNext] && TR != Triangulacion2[TRNext]) {//los minimos no forma parte de la union, yq ue esos contiene al vertice inferior, como inico, tener cuidado con cruzamientos
        //while (Triangulacion[TL].IdTriangulosAdjacentes[1] != Triangulacion1[LeftBottom] && Triangulacion[TR].IdTriangulosAdjacentes[2] != Triangulacion2[RightBottom]) {//los minimos no forma parte de la union, yq ue esos contiene al vertice inferior, como inico, tener cuidado con cruzamientos
            //if (Triangulacion[TL].Vertices[0]->X < Triangulacion[TR].Vertices[1]->X) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
            //if (TL->Vertices[1]->X < TR->Vertices[0]->X) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
            //while(!EsDelaunayConElOtroHull(TL->TriangulosAdjacentes[0], Triangulacion1, AristaIntersectada)) {
            while(!EsDelaunayConLaOtraTriangulacion(TL->TriangulosAdjacentes[0], ini, med, AristaIntersectada)) {
                cout << TL->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullLeft(*TL, *TL->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullLeft(*TL->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TL->TriangulosAdjacentes[0]);
                TLBottom = TLNext->TriangulosAdjacentes[2];//si elimino por casualidad el tl bottom, osea si tl es tl bottom, este tlbotton deberia cambiar, o la mezcla podria terminar con erroes
            }
            //while(!EsDelaunayConElOtroHull(TR->TriangulosAdjacentes[0], Triangulacion2, AristaIntersectada)) {
            while(!EsDelaunayConLaOtraTriangulacion(TR->TriangulosAdjacentes[0], med+1, fin, AristaIntersectada)) {
                cout << TR->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullRight(*TR, *TR->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullRight(*TR->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TR->TriangulosAdjacentes[0]);
                TRBottom = TRNext->TriangulosAdjacentes[1];
            }
            ValEnCirculo = EnCirculo(*TL->Vertices[0], *TL->Vertices[1], *TR->Vertices[1], *TR->Vertices[0]);
            if (ValEnCirculo < 0) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
                //usar otro criterio para ese if, deberia verificar cual de los dos formara un mejor triangulo de delaunay, y escoger ese lado
                TL->bGhost = false;
                TL->Vertices[2] = TR->Vertices[1];
                TL->IdVertices[2] = TR->IdVertices[1];
                //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
                //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
                //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
                //if (UltimoUnido != -1) {
                if (UltimoUnido) {// != nullptr
                    if (TL->TriangulosAdjacentes[2] == UltimoUnido) {//el aterio es vecino
                        //esto signifca que el ultimo unido era mi vecino anterior, por lo tanto no tengo que cambir los punteros de las artisas para enlazarlos, solo se hace en el otro cosas,
                        //en el que el ultimo unido no era mi vecino
                        //en realidad no deberia hacer nada
                    }
                    else {//ultimo unido no es mi adyacente, es del otro hull

                        TL->TriangulosAdjacentes[2] = UltimoUnido;//no deberia tener este orientacion?
                        TL->IdTriangulosAdjacentes[2] = UltimoUnido->Id;//no deberia tener este orientacion?
                        TL->OrientacionesTriangulosAdjacentes[2] = 2;//no deberia tener este orientacion?
                        UltimoUnido->TriangulosAdjacentes[2] = TL;
                        UltimoUnido->IdTriangulosAdjacentes[2] = TL->Id;
                        UltimoUnido->OrientacionesTriangulosAdjacentes[2] = 2;//revisar vien esto
                        //UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia, por lo cual esta linea no debe estar aqui
                    }
                }
                UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia,
                LadoUltimoUnido = false;
                TL = TL->TriangulosAdjacentes[1];
            }
            else {//el derecho es mauor
                TR->bGhost = false;
                TR->Vertices[2] = TL->Vertices[0];
                TR->IdVertices[2] = TL->IdVertices[0];
                //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
                //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
                //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
                //if (UltimoUnido != -1) {
                if (UltimoUnido) {
                    if (TR->TriangulosAdjacentes[1] == UltimoUnido) {//el aterio es vecino
                        //en realidad no deberia hacer nada
                    }
                    else {//ultimo unido no es mi adyacente, es del otro hull
                        //aqui sei debo hacer cambios
                        TR->TriangulosAdjacentes[1] = UltimoUnido;
                        TR->IdTriangulosAdjacentes[1] = UltimoUnido->Id;
                        TR->OrientacionesTriangulosAdjacentes[1] = 1;
                        UltimoUnido->TriangulosAdjacentes[1] = TR;
                        UltimoUnido->IdTriangulosAdjacentes[1] = TR->Id;
                        UltimoUnido->OrientacionesTriangulosAdjacentes[1] = 1;//revisar ben esta linea
                    }
                }
                UltimoUnido = TR;
                LadoUltimoUnido = true;
                TR = TR->TriangulosAdjacentes[2];
            }
        }

    }
    else {

        //while(!EsDelaunayConElOtroHull(TL->TriangulosAdjacentes[0], Triangulacion2, AristaIntersectada)) {
        while(!EsDelaunayConLaOtraTriangulacion(TL->TriangulosAdjacentes[0], med + 1, fin, AristaIntersectada)) {
            cout << TL->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
            //ConvertirEnFantasmaTrianguloHullLeft(*TL, *TL->TriangulosAdjacentes[0]);
            ConvertirEnFantasmaTrianguloHullLeft(*TL->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TL->TriangulosAdjacentes[0]);
            TLBottom = TLNext->TriangulosAdjacentes[2];//si elimino por casualidad el tl bottom, osea si tl es tl bottom, este tlbotton deberia cambiar, o la mezcla podria terminar con erroes
        }
        //while(!EsDelaunayConElOtroHull(TR->TriangulosAdjacentes[0], Triangulacion1, AristaIntersectada)) {
        while(!EsDelaunayConLaOtraTriangulacion(TR->TriangulosAdjacentes[0], ini, med, AristaIntersectada)) {
            cout << TR->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
            //ConvertirEnFantasmaTrianguloHullRight(*TR, *TR->TriangulosAdjacentes[0]);
            ConvertirEnFantasmaTrianguloHullRight(*TR->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TR->TriangulosAdjacentes[0]);
            TRBottom = TRNext->TriangulosAdjacentes[1];
        }
        float ValEnCirculo = EnCirculo(*TL->Vertices[0], *TL->Vertices[1], *TR->Vertices[1], *TR->Vertices[0]);//solo para hacer facil el debug
        if (ValEnCirculo < 0) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
            TL->bGhost = false;
            TL->Vertices[2] = TR->Vertices[1];
            TL->IdVertices[2] = TR->IdVertices[1];
            //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
            //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
            //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
            if (UltimoUnido) {
                if (TL->TriangulosAdjacentes[2] == UltimoUnido) {//el aterio es vecino
                    //esto signifca que el ultimo unido era mi vecino anterior, por lo tanto no tengo que cambir los punteros de las artisas para enlazarlos, solo se hace en el otro cosas,
                    //en el que el ultimo unido no era mi vecino
                    //en realidad no deberia hacer nada
                }
                else {//ultimo unido no es mi adyacente, es del otro hull
                    //aqui sei debo hacer cambios
                    TL->TriangulosAdjacentes[2] = UltimoUnido;
                    TL->IdTriangulosAdjacentes[2] = UltimoUnido->Id;
                    TL->OrientacionesTriangulosAdjacentes[2] = 2;
                    UltimoUnido->TriangulosAdjacentes[2] = TL;
                    UltimoUnido->IdTriangulosAdjacentes[2] = TL->Id;
                    UltimoUnido->OrientacionesTriangulosAdjacentes[2] = 2;//revisar vien esto
                    //UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia, por lo cual esta linea no debe estar aqui
                }
            }
            PrimerUnido = TL;
            LadoPrimerUnido = false;//lado izqueirdo
            UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia,
            LadoUltimoUnido = false;
            TL = TL->TriangulosAdjacentes[1];
        }
        else {//el derecho es mauor
            TR->bGhost = false;
            TR->Vertices[2] = TL->Vertices[0];
            TR->IdVertices[2] = TL->IdVertices[0];
            //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
            //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
            //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
            if (UltimoUnido) {
                if (TR->TriangulosAdjacentes[1] == UltimoUnido) {//el aterio es vecino
                    //en realidad no deberia hacer nada
                    //esto quiere decir que el anterior ya era mi vencio en el hull, por lo tanto ultimo nodo, ya apunta a tr, no hay nada que hacer
                }
                else {//ultimo unido no es mi adyacente, es del otro hull
                    //aqui sei debo hacer cambios
                    TR->TriangulosAdjacentes[1] = UltimoUnido;
                    TR->IdTriangulosAdjacentes[1] = UltimoUnido->Id;
                    TR->OrientacionesTriangulosAdjacentes[1] = 1;
                    UltimoUnido->TriangulosAdjacentes[1] = TR;//este 1 deberia ser en funcion de al caso que sea, si el anterio era del lado derecho, pues es otro ya no 1
                    UltimoUnido->IdTriangulosAdjacentes[1] = TR->Id;//este 1 deberia ser en funcion de al caso que sea, si el anterio era del lado derecho, pues es otro ya no 1
                    UltimoUnido->OrientacionesTriangulosAdjacentes[1] = 1;//revisar ben esta linea
                }
            }
            PrimerUnido = TR;
            LadoPrimerUnido = true;//lado izqueirdo
            UltimoUnido = TR;//se uepone que en cada unoi, el ultimo unido cambia,
            LadoUltimoUnido = true;
            TR = TR->TriangulosAdjacentes[2];
        }
        //reallizar siempre una iteracion

        while (TL != TLNext && TR != TRNext) {//los minimos no forma parte de la union, yq ue esos contiene al vertice inferior, como inico, tener cuidado con cruzamientos
        //while (TL != Triangulacion1[TLNext] && TR != Triangulacion2[TRNext]) {//los minimos no forma parte de la union, yq ue esos contiene al vertice inferior, como inico, tener cuidado con cruzamientos
        //while (Triangulacion[TL].IdTriangulosAdjacentes[1] != Triangulacion1[LeftBottom] && Triangulacion[TR].IdTriangulosAdjacentes[2] != Triangulacion2[RightBottom]) {//los minimos no forma parte de la union, yq ue esos contiene al vertice inferior, como inico, tener cuidado con cruzamientos
            ////if (Triangulacion[TL].Vertices[0]->Y > Triangulacion[TR].Vertices[1]->Y) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
            //if (TL->Vertices[1]->Y > TR->Vertices[0]->Y) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
            //while(!EsDelaunayConElOtroHull(TL->TriangulosAdjacentes[0], Triangulacion2, AristaIntersectada)) {
            while(!EsDelaunayConLaOtraTriangulacion(TL->TriangulosAdjacentes[0], med + 1, fin, AristaIntersectada)) {
                cout << TL->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullLeft(*TL, *TL->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullLeft(*TL->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TL->TriangulosAdjacentes[0]);
                TLBottom = TLNext->TriangulosAdjacentes[2];//si elimino por casualidad el tl bottom, osea si tl es tl bottom, este tlbotton deberia cambiar, o la mezcla podria terminar con erroes
            }
            //while(!EsDelaunayConElOtroHull(TR->TriangulosAdjacentes[0], Triangulacion1, AristaIntersectada)) {
            while(!EsDelaunayConLaOtraTriangulacion(TR->TriangulosAdjacentes[0], ini, med, AristaIntersectada)) {
                cout << TR->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullRight(*TR, *TR->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullRight(*TR->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TR->TriangulosAdjacentes[0]);
                TRBottom = TRNext->TriangulosAdjacentes[1];
            }
            ValEnCirculo = EnCirculo(*TL->Vertices[0], *TL->Vertices[1], *TR->Vertices[1], *TR->Vertices[0]);//solo para hacer facil el debug
            if (ValEnCirculo < 0) {//si el del hull izquierdo es mayo, unir con el vertice del derecho, la preguntas a que vertice?
                TL->bGhost = false;
                TL->Vertices[2] = TR->Vertices[1];
                TL->IdVertices[2] = TR->IdVertices[1];
                //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
                //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
                //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
                if (UltimoUnido) {
                    if (TL->TriangulosAdjacentes[2] == UltimoUnido) {//el aterio es vecino
                        //esto signifca que el ultimo unido era mi vecino anterior, por lo tanto no tengo que cambir los punteros de las artisas para enlazarlos, solo se hace en el otro cosas,
                        //en el que el ultimo unido no era mi vecino
                        //en realidad no deberia hacer nada
                    }
                    else {//ultimo unido no es mi adyacente, es del otro hull
                        //aqui sei debo hacer cambios
                        TL->TriangulosAdjacentes[2] = UltimoUnido;
                        TL->IdTriangulosAdjacentes[2] = UltimoUnido->Id;
                        TL->OrientacionesTriangulosAdjacentes[2] = 2;
                        UltimoUnido->TriangulosAdjacentes[2] = TL;
                        UltimoUnido->IdTriangulosAdjacentes[2] = TL->Id;
                        UltimoUnido->OrientacionesTriangulosAdjacentes[2] = 2;//revisar vien esto
                        //UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia, por lo cual esta linea no debe estar aqui
                    }
                }
                UltimoUnido = TL;//se uepone que en cada unoi, el ultimo unido cambia,
                LadoUltimoUnido = false;
                TL = TL->TriangulosAdjacentes[1];
            }
            else {//el derecho es mauor
                TR->bGhost = false;
                TR->Vertices[2] = TL->Vertices[0];
                TR->IdVertices[2] = TL->IdVertices[0];
                //para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
                //esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
                //aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
                if (UltimoUnido) {
                    if (TR->TriangulosAdjacentes[1] == UltimoUnido) {//el aterio es vecino
                        //en realidad no deberia hacer nada
                        //esto quiere decir que el anterior ya era mi vencio en el hull, por lo tanto ultimo nodo, ya apunta a tr, no hay nada que hacer
                    }
                    else {//ultimo unido no es mi adyacente, es del otro hull
                        //aqui sei debo hacer cambios
                        TR->TriangulosAdjacentes[1] = UltimoUnido;
                        TR->IdTriangulosAdjacentes[1] = UltimoUnido->Id;
                        TR->OrientacionesTriangulosAdjacentes[1] = 1;
                        UltimoUnido->TriangulosAdjacentes[1] = TR;//este 1 deberia ser en funcion de al caso que sea, si el anterio era del lado derecho, pues es otro ya no 1
                        UltimoUnido->IdTriangulosAdjacentes[1] = TR->Id;//este 1 deberia ser en funcion de al caso que sea, si el anterio era del lado derecho, pues es otro ya no 1
                        UltimoUnido->OrientacionesTriangulosAdjacentes[1] = 1;//revisar ben esta linea
                    }
                }
                UltimoUnido = TR;
                LadoUltimoUnido = true;
                TR = TR->TriangulosAdjacentes[2];
            }
        }
    }

    //cout << "Primera parte del mezclaro terminado" << endl;
    //en estos bucles tambien debo comprar si los tringulos son delaunay
	while (TL != TLNext) {
	//while (TL != Triangulacion1[TLNext]) {
        if (eje) {
            while(!EsDelaunayConLaOtraTriangulacion(TL->TriangulosAdjacentes[0], ini, med, AristaIntersectada)) {
                cout << TL->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullLeft(*TL, *TL->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullLeft(*TL->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TL->TriangulosAdjacentes[0]);
                TLBottom = TLNext->TriangulosAdjacentes[2];//si elimino por casualidad el tl bottom, osea si tl es tl bottom, este tlbotton deberia cambiar, o la mezcla podria terminar con erroes
            }
        }
        else {
            while(!EsDelaunayConLaOtraTriangulacion(TL->TriangulosAdjacentes[0], med + 1, fin, AristaIntersectada)) {
                cout << TL->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullLeft(*TL, *TL->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullLeft(*TL->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TL->TriangulosAdjacentes[0]);
                TLBottom = TLNext->TriangulosAdjacentes[2];//si elimino por casualidad el tl bottom, osea si tl es tl bottom, este tlbotton deberia cambiar, o la mezcla podria terminar con erroes
            }
        }
		//se supone que debio terminar algunode los dos, unir lo que falte al ultimo
		//unir
        //aqui tambien deberia hacer eliminaciones
		//luego actualizar el puntero
		TL->bGhost = false;
		TL->Vertices[2] = TR->Vertices[1];
		TL->IdVertices[2] = TR->IdVertices[1];
		//para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
		//esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
		//aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
        if (UltimoUnido) {
            if (TL->TriangulosAdjacentes[2] == UltimoUnido) {//el aterio es vecino
                //en realidad no deberia hacer nada
            }
            else {//ultimo unido no es mi adyacente, es del otro hull
                //aqui sei debo hacer cambios
                TL->TriangulosAdjacentes[2] = UltimoUnido;
                TL->IdTriangulosAdjacentes[2] = UltimoUnido->Id;
                TL->OrientacionesTriangulosAdjacentes[2] = 2;
                UltimoUnido->TriangulosAdjacentes[2] = TL;
                UltimoUnido->IdTriangulosAdjacentes[2] = TL->Id;
                UltimoUnido->OrientacionesTriangulosAdjacentes[2] = 2;//revisar vien esto
            }
        }
        UltimoUnido = TL;
        LadoUltimoUnido = false;
        TL = TL->TriangulosAdjacentes[1];

	}
	while (TR != TRNext) {
	//while (TR != Triangulacion2[TRNext]) {
        if (eje) {
            while(!EsDelaunayConLaOtraTriangulacion(TR->TriangulosAdjacentes[0], med+1, fin, AristaIntersectada)) {
                cout << TR->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullRight(*TR, *TR->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullRight(*TR->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TR->TriangulosAdjacentes[0]);
                TRBottom = TRNext->TriangulosAdjacentes[1];
                //esta cosa se arreglaria si hiciera un calculo para elimnar estas cosas en una funcion aparte antes de inicar el merge
                //tal vez tambien deba hacerlo con el top
            }
        }
        else {
            while(!EsDelaunayConLaOtraTriangulacion(TR->TriangulosAdjacentes[0], ini, med, AristaIntersectada)) {
                cout << TR->IdTriangulosAdjacentes[0] << " no cumple delaunay." << endl;
                //ConvertirEnFantasmaTrianguloHullRight(*TR, *TR->TriangulosAdjacentes[0]);
                ConvertirEnFantasmaTrianguloHullRight(*TR->TriangulosAdjacentes[0]->TriangulosAdjacentes[AristaIntersectada], *TR->TriangulosAdjacentes[0]);
                TRBottom = TRNext->TriangulosAdjacentes[1];
            }
        }
		//se supone que debio terminar algunode los dos, unir lo que falte al ultimo
		//unir
		//luego actualizar el puntero
		TR->bGhost = false;
		TR->Vertices[2] = TL->Vertices[0];
		TR->IdVertices[2] = TL->IdVertices[0];
		//para saber si el tulimo unido, es del lado derecho basta ver si comparto un vertice
		//esto es necesario para saber con que orientacion me estoy uniondo a Ultimo unido, y saber a cual de sus puntero se asigno TR
		//aun que tampoc o es necesario ver el vertice,ses decir los triangulos fantasma estaban conectados, puegdo ver si eltriagnulo fantas al que estoy conecto es el ultio unido o no
        if (UltimoUnido) {
            if (TR->TriangulosAdjacentes[1] == UltimoUnido) {//el aterio es vecino
                //en realidad no deberia hacer nada
            }
            else {//ultimo unido no es mi adyacente, es del otro hull
                //aqui sei debo hacer cambios
                TR->TriangulosAdjacentes[1] = UltimoUnido;
                TR->IdTriangulosAdjacentes[1] = UltimoUnido->Id;
                TR->OrientacionesTriangulosAdjacentes[1] = 1;
                UltimoUnido->TriangulosAdjacentes[1] = TR;
                UltimoUnido->IdTriangulosAdjacentes[1] = TR->Id;
                UltimoUnido->OrientacionesTriangulosAdjacentes[1] = 1;//revisar ben esta linea
            }
        }
        UltimoUnido = TR;
        LadoUltimoUnido = true;
        TR = TR->TriangulosAdjacentes[2];
	}

    //cout << "Segunda parte del mezclaro terminado" << endl;
	//necesito crear los dos triangulos fastama nuevos, adyacente al primero que se unio, y al ultimo unido
	//si primer unido, tiene le triangulo adjacente 1 como fantasma, entonces es un triangulo del hull, derecho, pero si tieen el triangulo adjacente 2 es del hull izquierdo
	//asi se en donde crear el triangulo fantasma, tambine encesito este nuevo triangulo, unirlo al anterior fantasma del primero unido
	//osea el del top,
	//tambien podria revisar directamente los tops, y revisar sus punteros a las caras anteriores, si alguno es fantasma quiere decir que ese es el top con el cual debo crear el triangulo fantasma
	int idTrianguloTop;
	int idTrianguloBottom;
    TriangleFace * TrianguloTop = nullptr;
    TriangleFace * TrianguloBottom = nullptr;
    //ubico quien es el que se unio primero identifican si esta unido a un fantasma
	//if (TRTop->TriangulosAdjacentes[1]->bGhost) {//mal criterio para saber cual fue el ultimo unitno, o en donde debo crear el triangulo fantasma, no cumple el if, ni el else if, por lo tanto no crea
    if (LadoPrimerUnido) {//se unio el lado derecho osea se unio primero TRTop
		TrianguloTop = new TriangleFace;
		TrianguloTop->bGhost = true;
		TrianguloTop->Vertices[0] = TRTop->Vertices[2];
		TrianguloTop->IdVertices[0] = TRTop->IdVertices[2];//cual es su id?
		TrianguloTop->Vertices[1] = TRTop->Vertices[1];
		TrianguloTop->IdVertices[1] = TRTop->IdVertices[1];
		TrianguloTop->Vertices[2] = nullptr;
		TrianguloTop->IdVertices[2] = -1;
		//despues de crear el triangulo, agregarlo al 
		//pero no necesito el ide de cada triangulo
		Triangulacion.push_back(TrianguloTop);
		idTrianguloTop = Triangulacion.size() - 1;
        TrianguloTop->Id = idTrianguloTop;

		TrianguloTop->TriangulosAdjacentes[0] = TRTop;
		TrianguloTop->IdTriangulosAdjacentes[0] = TRTop->Id;
		TrianguloTop->OrientacionesTriangulosAdjacentes[0] = 1;//esta arista se conecta con la arista 0 del triangulo2

		TRTop->IdTriangulosAdjacentes[1] = idTrianguloTop;
        TRTop->TriangulosAdjacentes[1] = TrianguloTop;
		TRTop->OrientacionesTriangulosAdjacentes[1] = 0;
    }
    else {
		TrianguloTop = new TriangleFace;
		TrianguloTop->bGhost = true;
		TrianguloTop->Vertices[0] = TLTop->Vertices[0];
		TrianguloTop->IdVertices[0] = TLTop->IdVertices[0];//cual es su id?
		TrianguloTop->Vertices[1] = TLTop->Vertices[2];
		TrianguloTop->IdVertices[1] = TLTop->IdVertices[2];
		TrianguloTop->Vertices[2] = nullptr;
		TrianguloTop->IdVertices[2] = -1;

		Triangulacion.push_back(TrianguloTop);
		idTrianguloTop = Triangulacion.size() - 1;
        TrianguloTop->Id = idTrianguloTop;

		TrianguloTop->TriangulosAdjacentes[0] = TLTop;
		TrianguloTop->IdTriangulosAdjacentes[0] = TLTop->Id;
		TrianguloTop->OrientacionesTriangulosAdjacentes[0] = 2;//esta arista se conecta con la arista 0 del triangulo2

		TLTop->IdTriangulosAdjacentes[2] = idTrianguloTop;
		TLTop->TriangulosAdjacentes[2] = TrianguloTop;
		TLTop->OrientacionesTriangulosAdjacentes[2] = 0;
    }

	//if (TRBottom->TriangulosAdjacentes[2]->bGhost) {
	if (LadoUltimoUnido) {//se unio el triangulo derecho com ultimo
	//if (Triangulacion[Triangulacion[TRBottom].IdTriangulosAdjacentes[2]].bGhost) {
		TrianguloBottom = new TriangleFace;
		TrianguloBottom->bGhost = true;
		TrianguloBottom->Vertices[0] = TRBottom->Vertices[0];
		TrianguloBottom->IdVertices[0] = TRBottom->IdVertices[0];//cual es su id?
		TrianguloBottom->Vertices[1] = TRBottom->Vertices[2];
		TrianguloBottom->IdVertices[1] = TRBottom->IdVertices[2];
		TrianguloBottom->Vertices[2] = nullptr;
		TrianguloBottom->IdVertices[2] = -1;
		//despues de crear el triangulo, agregarlo al 
		//pero no necesito el ide de cada triangulo
		Triangulacion.push_back(TrianguloBottom);
		idTrianguloBottom = Triangulacion.size() - 1;
        TrianguloBottom->Id = idTrianguloBottom;

		TrianguloBottom->TriangulosAdjacentes[0] = TRBottom;
		TrianguloBottom->IdTriangulosAdjacentes[0] = TRBottom->Id;
		TrianguloBottom->OrientacionesTriangulosAdjacentes[0] = 2;//esta arista se conecta con la arista 0 del triangulo2

		TRBottom->IdTriangulosAdjacentes[2] = idTrianguloBottom;
		TRBottom->TriangulosAdjacentes[2] = TrianguloBottom;
		TRBottom->OrientacionesTriangulosAdjacentes[2] = 0;
	}
	else {//se unio ultimo un triangulo del lado izquierdo
	//else if (TLBottom->TriangulosAdjacentes[1]->bGhost) {
		TrianguloBottom = new TriangleFace;
		TrianguloBottom->bGhost = true;
		TrianguloBottom->Vertices[0] = TLBottom->Vertices[2];
		TrianguloBottom->IdVertices[0] = TLBottom->IdVertices[2];//cual es su id?
		TrianguloBottom->Vertices[1] = TLBottom->Vertices[1];
		TrianguloBottom->IdVertices[1] = TLBottom->IdVertices[1];
		TrianguloBottom->Vertices[2] = nullptr;
		TrianguloBottom->IdVertices[2] = -1;

		Triangulacion.push_back(TrianguloBottom);
		idTrianguloBottom = Triangulacion.size() - 1;
        TrianguloBottom->Id = idTrianguloBottom;

		TrianguloBottom->TriangulosAdjacentes[0] = TLBottom;
		TrianguloBottom->IdTriangulosAdjacentes[0] = TLBottom->Id;
		TrianguloBottom->OrientacionesTriangulosAdjacentes[0] = 2;//esta arista se conecta con la arista 0 del triangulo2

		TLBottom->IdTriangulosAdjacentes[1] = idTrianguloBottom;
		TLBottom->TriangulosAdjacentes[1] = TrianguloBottom;
		TLBottom->OrientacionesTriangulosAdjacentes[1] = 0;
	}

    //Para top
    //deberia unirse a prev siempre y cuando prev sea fanstams, si no es fantasma estoy en el caso espeicia, esto aplica para los dos prevs, en ese caso debo unir con el nuevo triangulo creado para bottom
    if (TRPrev->bGhost) {
        TrianguloTop->TriangulosAdjacentes[1] = TRPrev;
        TrianguloTop->IdTriangulosAdjacentes[1] = TRPrev->Id;
        TrianguloTop->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
        TRPrev->IdTriangulosAdjacentes[2] = idTrianguloTop;
        TRPrev->TriangulosAdjacentes[2] = TrianguloTop;
        TRPrev->OrientacionesTriangulosAdjacentes[2] = 1;
    }
    else {//caso espeical, deb unir con el fanstasma recientemente creado
        TrianguloTop->TriangulosAdjacentes[1] = TrianguloBottom;
        TrianguloTop->IdTriangulosAdjacentes[1] = idTrianguloBottom;
        TrianguloTop->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
        TrianguloBottom->IdTriangulosAdjacentes[2] = idTrianguloTop;
        TrianguloBottom->TriangulosAdjacentes[2] = TrianguloTop;
        TrianguloBottom->OrientacionesTriangulosAdjacentes[2] = 1;
    }

    if (TLPrev->bGhost) {
        TrianguloTop->TriangulosAdjacentes[2] = TLPrev;
        TrianguloTop->IdTriangulosAdjacentes[2] = TLPrev->Id;
        TrianguloTop->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 1 del triangulo2
        TLPrev->IdTriangulosAdjacentes[1] = idTrianguloTop;
        TLPrev->TriangulosAdjacentes[1] = TrianguloTop;
        TLPrev->OrientacionesTriangulosAdjacentes[1] = 2;
    }
    else {//caso espeical, deb unir con el fanstasma recientemente creado
        TrianguloTop->TriangulosAdjacentes[2] = TrianguloBottom;
        TrianguloTop->IdTriangulosAdjacentes[2] = idTrianguloBottom;
        TrianguloTop->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 2 del triangulo2
        TrianguloBottom->IdTriangulosAdjacentes[1] = idTrianguloTop;
        TrianguloBottom->TriangulosAdjacentes[1] = TrianguloTop;
        TrianguloBottom->OrientacionesTriangulosAdjacentes[1] = 2;
    }

    //Para bottom
    if (TRNext->bGhost) {
        TrianguloBottom->TriangulosAdjacentes[2] = TRNext;
        TrianguloBottom->IdTriangulosAdjacentes[2] = TRNext->Id;
        TrianguloBottom->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 1 del triangulo2
        TRNext->IdTriangulosAdjacentes[1] = idTrianguloBottom;
        TRNext->TriangulosAdjacentes[1] = TrianguloBottom;
        TRNext->OrientacionesTriangulosAdjacentes[1] = 2;
    }
    else {//caso espeical, deb unir con el fanstasma recientemente creado
        TrianguloBottom->IdTriangulosAdjacentes[2] = idTrianguloTop;
        TrianguloBottom->TriangulosAdjacentes[2] = TrianguloTop;
        TrianguloBottom->OrientacionesTriangulosAdjacentes[2] = 1;
        TrianguloTop->TriangulosAdjacentes[1] = TrianguloBottom;
        TrianguloTop->IdTriangulosAdjacentes[1] = idTrianguloBottom;
        TrianguloTop->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
    }

    if (TLNext->bGhost) {
        TrianguloBottom->TriangulosAdjacentes[1] = TLNext;
        TrianguloBottom->IdTriangulosAdjacentes[1] = TLNext->Id;
        TrianguloBottom->OrientacionesTriangulosAdjacentes[1] = 2;//esta arista se conecta con la arista 2 del triangulo2
        TLNext->IdTriangulosAdjacentes[2] = idTrianguloBottom;
        TLNext->TriangulosAdjacentes[2] = TrianguloBottom;
        TLNext->OrientacionesTriangulosAdjacentes[2] = 1;
    }
    else {//caso espeical, deb unir con el fanstasma recientemente creado
        TrianguloBottom->IdTriangulosAdjacentes[1] = idTrianguloTop;
        TrianguloBottom->TriangulosAdjacentes[1] = TrianguloTop;
        TrianguloBottom->OrientacionesTriangulosAdjacentes[1] = 2;
        TrianguloTop->TriangulosAdjacentes[2] = TrianguloBottom;
        TrianguloTop->IdTriangulosAdjacentes[2] = idTrianguloBottom;
        TrianguloTop->OrientacionesTriangulosAdjacentes[2] = 1;//esta arista se conecta con la arista 2 del triangulo2
    }

    //el else es similar para los 4 casos
    //cout << "Trecera parte del mezclaro terminado" << endl;

	//ver si se pueden unificar el corte vertical y horizontal en la misma secuncia de pasos, para solo examinar una parte del codigo
	//necesito crer el retunr
	std::vector<TriangleFace *> res(Triangulacion1.size() + Triangulacion2.size() + 2);
    int i = 0;
	for (int j = 0; j < Triangulacion1.size(); j++) {
		res[i] = Triangulacion1[j];
        i++;
	}
	for (int j = 0; j < Triangulacion2.size(); j++) {
		res[i] = Triangulacion2[j];
        i++;
	}
	res[res.size() - 2] = TrianguloTop;
	res[res.size() - 1] = TrianguloBottom;

    //cout << "Copiando salida" << endl;
    //ImprimirTriangulos(Triangulacion);

    //deberia recibir vector de punteros a triangulos, no indices
	return res;
}

float Delaunay::DeteccionDeOrden(FVector V1, FVector V2, FVector Vm) {//esto regresara tambien si son colineales, lo cual no debo poder tratar
    //esto retorna un numero positivo si Vm, esta por encima de la recta que va de V1 a V2
    //retorna negativo si Vm esta por debajo de la recta que va de V1 a V2
    //esta funcion no es buna para detectar orden, pero si es buena para saber si quein esta arriba y abajo para la fase de rcreacio de triangulos
	return (Vm.Y - V1.Y) - ((V2.Y - V1.Y)/(V2.X - V1.X)) * (Vm.X - V1.X);
}

float Delaunay::DeteccionDeLadoRelativo(FVector V0, FVector V1, FVector V2) {
    //retorna positivo si el punto V2 esta a la derecha de la recta que va de V0 a V1
    //retorna negativo si el punto V2 esta a la izquierda de la recta que va de V0 a V1
    //retorna 0 si el punto V2 esta en la recta que va de V0 a V1
    //esto tambien puede servir para detectar el orden en sentido de las agujas del reloj o en contra
    //si retorna positivo los puntos estan en el sentido de las agujas del reloj
    //si retorna negativo los puntos estan en contra de las agujas del reloj
    FVector VectorAdelante = V1 - V0;
    VectorAdelante = VectorAdelante / VectorAdelante.Size();
    FVector VectorDerecha(VectorAdelante.Y, -VectorAdelante.X, VectorAdelante.Z);
    float numerador = (V2.X - V0.X)*VectorAdelante.Y - (V2.Y - V0.Y)*VectorAdelante.X;
    float denominador = (VectorDerecha.Y * VectorAdelante.X - VectorDerecha.X * VectorAdelante.Y);
    //la ecuacion usada para la recta perpendicular tiene ocmo puento de paso, a V2, por lo tanto "t" tiene el valor para la intereccion de esta recta con la que va de V0 a V1
    //por lo tanto si t es negativo, entonces V2 esta a la derecha de la revta V0-V1
    //si t es positivo, entonces V2 esta a la dereca de la recta V0-V1
    float t = numerador / denominador;
    //para que se corresponda con el valor de salida, debo multiplicar t por -1
    return -t;
}

bool Delaunay::DeteccionAristaIntersectada(FVector Baricentro, FVector Punto, FVector V0, FVector V1) {//retorna verdadero si los segmentos se intersectan, falso si no se intersectan
    //escirbir en terminos mas generales, por ahora, estan las ariables con los nombres de acuerdo a lo que necesitaba
    FVector VectorRecta = Punto - Baricentro;//no lo divido por su modulo para poder retornar un t de o a 1 si es que lo interecta entre estos puntos
    FVector VectorArista = V1 - V0;
    float numerador = (Baricentro.Y - V0.Y)*VectorArista.X - (Baricentro.X - V0.X)*VectorArista.Y;
    float denominador = (VectorRecta.X * VectorArista.Y - VectorRecta.Y * VectorArista.X);
    float t = numerador / denominador;
    float r = ((Baricentro.X - V0.X) + t * (VectorRecta.X)) / VectorArista.X;
    return (0 < t && t <= 1) && (0 < r && r <= 1);
}

int Delaunay::DeteccionAristaIntersectada(TriangleFace * Triangle, FVector Punto) {
    for (int i = 0; i < 3; i++) {
        if (DeteccionAristaIntersectada(Baricentro(*Triangle), Punto, *Triangle->Vertices[i], *Triangle->Vertices[(i+1)%3])) {
            return i;
        }
    }
    return -1;
}

float Delaunay::EnCirculo(FVector a, FVector b, FVector c, FVector d) {
    //los puntos deben ester en sentido contrario a las agujas del reloj
    //con a luego b, luego c, todo hacia la izquierda uno detras de otro
    //   a
    //b     c
    //retorna positivo si es que el punto d esta dentro del circulo
    //retorna negativo si el punto d esta fuera del circulo
    //retorna 0 si el punto estan en la linea del circulo
    /*Matriz prueba(4, 4);
    prueba.set(0, 0, a.X);
    prueba.set(0, 1, a.Y);
    prueba.set(0, 2, a.X*a.X + a.Y*a.Y);
    prueba.set(0, 3, 1);

    prueba.set(1, 0, b.X);
    prueba.set(1, 1, b.Y);
    prueba.set(1, 2, b.X*b.X + b.Y*b.Y);
    prueba.set(1, 3, 1);

    prueba.set(2, 0, c.X);
    prueba.set(2, 1, c.Y);
    prueba.set(2, 2, c.X*c.X + c.Y*c.Y);
    prueba.set(2, 3, 1);

    prueba.set(3, 0, d.X);
    prueba.set(3, 1, d.Y);
    prueba.set(3, 2, d.X*d.X + d.Y*d.Y);
    prueba.set(3, 3, 1);

    return prueba.determinante();*/
    FMatrix prueba;
    prueba.M[0][0] = a.X;
    prueba.M[0][1] = a.Y;
    prueba.M[0][2] = a.X*a.X + a.Y*a.Y;
    prueba.M[0][3] = 1;

    prueba.M[1][0] = b.X;
    prueba.M[1][1] = b.Y;
    prueba.M[1][2] = b.X*b.X + b.Y*b.Y;
    prueba.M[1][3] = 1;

    prueba.M[2][0] = c.X;
    prueba.M[2][1] = c.Y;
    prueba.M[2][2] = c.X*c.X + c.Y*c.Y;
    prueba.M[2][3] = 1;

    prueba.M[3][0] = d.X;
    prueba.M[3][1] = d.Y;
    prueba.M[3][2] = d.X*d.X + d.Y*d.Y;
    prueba.M[3][3] = 1;

    return prueba.Determinant();
}

void Delaunay::ImprimirTriangulos(std::vector<TriangleFace *>& TriangulacionIn) {
	for (int i = 0; i < TriangulacionIn.size(); i++) {
		if (!TriangulacionIn[i]->bGhost) {
            cout << "Triangulo " << i << ":" << endl;
            cout << "\tVertice 0: (" << TriangulacionIn[i]->Vertices[0]->X << ", " << TriangulacionIn[i]->Vertices[0]->Y << ", " << TriangulacionIn[i]->Vertices[0]->Z << ")" << endl;
            cout << "\tVertice 1: (" << TriangulacionIn[i]->Vertices[1]->X << ", " << TriangulacionIn[i]->Vertices[1]->Y << ", " << TriangulacionIn[i]->Vertices[1]->Z << ")" << endl;
            cout << "\tVertice 2: (" << TriangulacionIn[i]->Vertices[2]->X << ", " << TriangulacionIn[i]->Vertices[2]->Y << ", " << TriangulacionIn[i]->Vertices[2]->Z << ")" << endl;
		}
	}
}

void Delaunay::ImprimirVertices() {
    for (int i = 0; i < Vertices.size(); i++) {
        cout << "(" << Vertices[i]->X << ", " << Vertices[i]->Y << ") ";
    }
    cout << endl;
}

bool Delaunay::IsGoodTriangle(TriangleFace & Triangle) {
    return false;
}

FVector Delaunay::Circuncentro(FVector V0, FVector V1, FVector V2) {//usa las medianas, por lo tanto necesito calcular rectas perpendiculares y hayar la interseccion de 3 rectas
    //puntos de paso
    FVector P1 = (V1 + V0) / 2;
    FVector P2 = (V2 + V1) / 2;

    //vectores directores
    FVector U1 = (V1 - V0);
    U1 = U1 / U1.Size();
    FVector U2 = (V2 - V1);
    U2 = U2 / U2.Size();

    //perpendicularmente
    float temp;
    temp = U1.X;
    U1.X = U1.Y;
    U1.Y = -temp;

    temp = U2.X;
    U2.X = U2.Y;
    U2.Y = -temp;

    //hallando t para la recta 1, t es la interseccin en la recta 1
    float t = ((P1.X - P2.X) * U2.Y - (P1.Y - P2.Y) * U2.X) / (U1.Y * U2.X - U1.X * U2.Y);
    //hallando el punto usando t
    FVector direccion = U1 * t;
    FVector res = P1 + direccion;
    return res;
}

FVector Delaunay::Circuncentro(TriangleFace & Triangle) {
    return Circuncentro(*Triangle.Vertices[0], *Triangle.Vertices[1], *Triangle.Vertices[2]);
}

FVector Delaunay::Baricentro(FVector V0, FVector V1, FVector V2) {
    //puntos de paso
    FVector P1 = (V1 + V0) / 2;
    FVector P2 = (V2 + V1) / 2;

    //vectores directores
    FVector U1 = (V2 - P1);
    U1 = U1 / U1.Size();
    FVector U2 = (V0 - P2);
    U2 = U2 / U2.Size();

    //hallando t para la recta 1, t es la interseccin en la recta 1
    float t = ((P1.X - P2.X) * U2.Y - (P1.Y - P2.Y) * U2.X) / (U1.Y * U2.X - U1.X * U2.Y);
    //hallando el punto usando t
    FVector direccion = U1 * t;
    FVector res = P1 + direccion;
    return res;
}

FVector Delaunay::Baricentro(TriangleFace & Triangle) {
    return Baricentro(*Triangle.Vertices[0], *Triangle.Vertices[1], *Triangle.Vertices[2]);
}

void Delaunay::RotarTrianguloHorario(TriangleFace & Triangle) {
    FVector * VerticeTemp = Triangle.Vertices[0];
    int IdVerticeTemp = Triangle.IdVertices[0];
    Triangle.Vertices[0] = Triangle.Vertices[1];
    Triangle.IdVertices[0] = Triangle.IdVertices[1];
    Triangle.Vertices[1] = Triangle.Vertices[2];
    Triangle.IdVertices[1] = Triangle.IdVertices[2];
    Triangle.Vertices[2] = VerticeTemp;
    Triangle.IdVertices[2] = IdVerticeTemp;

    TriangleFace * TrianguloTemp = Triangle.TriangulosAdjacentes[0];
    int IdTrianguloTemp = Triangle.IdTriangulosAdjacentes[0];
    int OrientacionTrianguloTemp = Triangle.OrientacionesTriangulosAdjacentes[0];
    Triangle.TriangulosAdjacentes[0] = Triangle.TriangulosAdjacentes[1];
    Triangle.IdTriangulosAdjacentes[0] = Triangle.IdTriangulosAdjacentes[1];
    Triangle.OrientacionesTriangulosAdjacentes[0] = Triangle.OrientacionesTriangulosAdjacentes[1];
    Triangle.TriangulosAdjacentes[0]->OrientacionesTriangulosAdjacentes[Triangle.OrientacionesTriangulosAdjacentes[0]] = 0;

    Triangle.TriangulosAdjacentes[1] = Triangle.TriangulosAdjacentes[2];
    Triangle.IdTriangulosAdjacentes[1] = Triangle.IdTriangulosAdjacentes[2];
    Triangle.OrientacionesTriangulosAdjacentes[1] = Triangle.OrientacionesTriangulosAdjacentes[2];
    Triangle.TriangulosAdjacentes[1]->OrientacionesTriangulosAdjacentes[Triangle.OrientacionesTriangulosAdjacentes[1]] = 1;

    Triangle.TriangulosAdjacentes[2] = TrianguloTemp;
    Triangle.IdTriangulosAdjacentes[2] = IdTrianguloTemp;
    Triangle.OrientacionesTriangulosAdjacentes[2] = OrientacionTrianguloTemp;
    Triangle.TriangulosAdjacentes[2]->OrientacionesTriangulosAdjacentes[Triangle.OrientacionesTriangulosAdjacentes[2]] = 2;
}


int Delaunay::PerteneceAlTriangulo(TriangleFace & Triangle, FVector Punto) {
    float Val0 = DeteccionDeLadoRelativo(*Triangle.Vertices[0], *Triangle.Vertices[1], Punto);
    cout << Val0 << endl;
    float Val1 = DeteccionDeLadoRelativo(*Triangle.Vertices[1], *Triangle.Vertices[2], Punto);
    cout << Val1 << endl;
    float Val2 = DeteccionDeLadoRelativo(*Triangle.Vertices[2], *Triangle.Vertices[0], Punto);
    cout << Val2 << endl;
    if (Val0 < 0 && Val1 < 0 && Val2 < 0) {//el punto esta a la izqueirda de todos lados del triangulo
        return 3;
    }
    else if (Val0 == 0 && Val1 < 0 && Val2 < 0) {
        return 0;
    }
    else if (Val1 == 0 && Val0 < 0 && Val2 < 0) {
        return 1;
    }
    else if (Val2 == 0 && Val0 < 0 && Val1 < 0) {
        return 2;
    }
    return -1;
    //la teoria esta bien, pero debo tener cuidado con los problemas de precision, si es menor a determinado numero deberia considerarlo 0
}

bool Delaunay::EsDelaunayConElOtroHull(TriangleFace * Triangle, std::vector<TriangleFace *> & OtroHull) {
    //debo verificar que el triangulo sea delaunya, comprobando solo con los vertices del hull al otro lado del corte (hull)
    //si es delauynay retorna verdadedor, si no lo es retorna falso, no retorna con quien es falso
    
    //buscar el primer triangulo fantasma, a partir de alli recorrer usando sus punteros para ir a los otros fantasmas
    if (Triangle->bGhost) {//si es fantasma, no tiene vertice 2, es null, por lo tanto no puedo hacer comprobaciones, en este caso lo tomo que cumple delaunay,
        //esto es solo para el caso especia de que por casulaida este comprbando el caso base de 2 puntos, que forman una arita.
        return true;
    }

    for (int i = 0; i < OtroHull.size(); i++) {
        if (OtroHull[i]->bGhost && EnCirculo(*Triangle->Vertices[0], *Triangle->Vertices[1], *Triangle->Vertices[2], *OtroHull[i]->Vertices[0]) > 0) {//si el ultimo punto esta dentro del ciruclo no cumple delauynay
            return false;
        }
    }
    return true;
}

bool Delaunay::EsDelaunayConElOtroHull(TriangleFace * Triangle, std::vector<TriangleFace*>& OtroHull, int & AristaIntersectada) {
    //debo verificar que el triangulo sea delaunya, comprobando solo con los vertices del hull al otro lado del corte (hull)
    //si es delauynay retorna verdadedor, si no lo es retorna falso, no retorna con quien es falso
    
    //buscar el primer triangulo fantasma, a partir de alli recorrer usando sus punteros para ir a los otros fantasmas
    if (Triangle->bGhost) {//si es fantasma, no tiene vertice 2, es null, por lo tanto no puedo hacer comprobaciones, en este caso lo tomo que cumple delaunay,
        //esto es solo para el caso especia de que por casulaida este comprbando el caso base de 2 puntos, que forman una arita.
        AristaIntersectada = -1;
        return true;
    }

    for (int i = 0; i < OtroHull.size(); i++) {
        if (OtroHull[i]->bGhost && EnCirculo(*Triangle->Vertices[0], *Triangle->Vertices[1], *Triangle->Vertices[2], *OtroHull[i]->Vertices[0]) > 0) {//si el ultimo punto esta dentro del ciruclo no cumple delauynay
            //debo ver que arista es la que da la cara a ese vertice
            AristaIntersectada = DeteccionAristaIntersectada(Triangle, *OtroHull[i]->Vertices[0]);
            return false;
        }
    }
    AristaIntersectada = -1;
    return true;
}

bool Delaunay::EsDelaunayConLaOtraTriangulacion(TriangleFace * Triangle, int ini, int fin, int & AristaIntersectada) {
    if (Triangle->bGhost) {//si es fantasma, no tiene vertice 2, es null, por lo tanto no puedo hacer comprobaciones, en este caso lo tomo que cumple delaunay,
        //esto es solo para el caso especia de que por casulaida este comprbando el caso base de 2 puntos, que forman una arita.
        AristaIntersectada = -1;
        return true;
    }

    for (int i = ini; i <= fin; i++) {
        if (EnCirculo(*Triangle->Vertices[0], *Triangle->Vertices[1], *Triangle->Vertices[2], *Vertices[i]) > 0) {//si el ultimo punto esta dentro del ciruclo no cumple delauynay
            //debo ver que arista es la que da la cara a ese vertice
            AristaIntersectada = DeteccionAristaIntersectada(Triangle, *Vertices[i]);
            return false;
        }
    }
    AristaIntersectada = -1;
    return true;
}

bool Delaunay::EsDelaunay(TriangleFace & Triangle, std::vector<FVector>& vertices) {//esta funciona esta mal
    for (int i = 0; i < vertices.size(); i++) {
        if (i != Triangle.IdVertices[0] && i != Triangle.IdVertices[1] && i != Triangle.IdVertices[2]) {
            if (EnCirculo(*Triangle.Vertices[0], *Triangle.Vertices[0], *Triangle.Vertices[0], vertices[i])) {
                return false;
            }
        }
    }
    return false;
}

bool Delaunay::EsDelaunay(TriangleFace * Triangle, FVector * vertice) {
    return EnCirculo(*Triangle->Vertices[0], *Triangle->Vertices[1], *Triangle->Vertices[2], *vertice) < 0;//si es menor que 0 entonces el vertice esta vfuera del cincurirulo, por lo tanto es delaunay
}

void Delaunay::ConvertirEnFantasmaTrianguloHullRight(TriangleFace & TR, TriangleFace & TRE) {
    while (TRE.Vertices[0] != TR.Vertices[0]) {
        RotarTrianguloHorario(TRE);
    }
    TRE.bGhost = true;
    TR.Vertices[0] = TRE.Vertices[1];
    TR.IdVertices[0] = TRE.IdVertices[1];
    TRE.Vertices[2] = TR.Vertices[2]; //nullptr
    TRE.IdVertices[2] = TR.IdVertices[2]; //nullptr

    TR.TriangulosAdjacentes[2]->TriangulosAdjacentes[1] = &TRE;
    TR.TriangulosAdjacentes[2]->IdTriangulosAdjacentes[1] = TRE.Id;
    TR.TriangulosAdjacentes[2]->OrientacionesTriangulosAdjacentes[1] = 2;

    TRE.TriangulosAdjacentes[2] = TR.TriangulosAdjacentes[2];
    TRE.IdTriangulosAdjacentes[2] = TR.IdTriangulosAdjacentes[2];
    TRE.OrientacionesTriangulosAdjacentes[2] = 1;

    TR.TriangulosAdjacentes[0] = TRE.TriangulosAdjacentes[1];
    TR.IdTriangulosAdjacentes[0] = TRE.IdTriangulosAdjacentes[1];
    TR.OrientacionesTriangulosAdjacentes[0] = TRE.OrientacionesTriangulosAdjacentes[1];
    TR.TriangulosAdjacentes[0]->TriangulosAdjacentes[TR.OrientacionesTriangulosAdjacentes[0]] = &TR;
    TR.TriangulosAdjacentes[0]->IdTriangulosAdjacentes[TR.OrientacionesTriangulosAdjacentes[0]] = TR.Id;
    TR.TriangulosAdjacentes[0]->OrientacionesTriangulosAdjacentes[TR.OrientacionesTriangulosAdjacentes[0]] = 0;

    TR.TriangulosAdjacentes[2] = &TRE;
    TR.IdTriangulosAdjacentes[2] = TRE.Id;
    TR.OrientacionesTriangulosAdjacentes[2] = 1;
    TRE.TriangulosAdjacentes[1] = &TR;
    TRE.IdTriangulosAdjacentes[1] = TR.Id;
    TRE.OrientacionesTriangulosAdjacentes[1] = 2;
}

void Delaunay::ConvertirEnFantasmaTrianguloHullLeft(TriangleFace & TL, TriangleFace & TLE) {
    while (TLE.Vertices[1] != TL.Vertices[1]) {
        RotarTrianguloHorario(TLE);
    }
    TLE.bGhost = true;
    TL.Vertices[1] = TLE.Vertices[0];
    TL.IdVertices[1] = TLE.IdVertices[0];
    TLE.Vertices[2] = TL.Vertices[2]; //nullptr
    TLE.IdVertices[2] = TL.IdVertices[2]; //nullptr

    TL.TriangulosAdjacentes[1]->TriangulosAdjacentes[2] = &TLE;
    TL.TriangulosAdjacentes[1]->IdTriangulosAdjacentes[2] = TLE.Id;
    TL.TriangulosAdjacentes[1]->OrientacionesTriangulosAdjacentes[2] = 1;//

    TLE.TriangulosAdjacentes[1] = TL.TriangulosAdjacentes[1];
    TLE.IdTriangulosAdjacentes[1] = TL.IdTriangulosAdjacentes[1];
    TLE.OrientacionesTriangulosAdjacentes[1] = 2;

    TL.TriangulosAdjacentes[0] = TLE.TriangulosAdjacentes[2];
    TL.IdTriangulosAdjacentes[0] = TLE.IdTriangulosAdjacentes[2];
    TL.OrientacionesTriangulosAdjacentes[0] = TLE.OrientacionesTriangulosAdjacentes[2];
    TL.TriangulosAdjacentes[0]->TriangulosAdjacentes[TL.OrientacionesTriangulosAdjacentes[0]] = &TL;
    TL.TriangulosAdjacentes[0]->IdTriangulosAdjacentes[TL.OrientacionesTriangulosAdjacentes[0]] = TL.Id;
    TL.TriangulosAdjacentes[0]->OrientacionesTriangulosAdjacentes[TL.OrientacionesTriangulosAdjacentes[0]] = 0;

    TL.TriangulosAdjacentes[1] = &TLE;
    TL.IdTriangulosAdjacentes[1] = TLE.Id;
    TL.OrientacionesTriangulosAdjacentes[1] = 2;
    TLE.TriangulosAdjacentes[2] = &TL;
    TLE.IdTriangulosAdjacentes[2] = TL.Id;
    TLE.OrientacionesTriangulosAdjacentes[2] = 1;
}

/*void Delaunay::EliminarTriangulosNoDelaunayLeft(std::vector<TriangleFace*>& Hull, TriangleFace *& TLTop, TriangleFace *& TLPrev, TriangleFace *& TLBottom, TriangleFace *& TLNext, int ini, int fin) {
    //creo que no necesito recibir el hull, si ya estoy recibiendo los punteros
    //debo ir desde el inico hasta el vinal
    //si uso los punteros siempre debo garantizar una iteracion, igual que como pasa con el merge
    //que sea reccoriendo el hull de la trangulacion, garantiza elimancion y manejo de adjacencias correctos

}*/

Delaunay::Delaunay() {
}

Delaunay::~Delaunay() {
}
