// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "TriangleFace.h"
#include <vector>
//#include "FVector.h"
//#include "Matriz.h"

/**
 * 
 */
class GEOVISUALIZATION_API Delaunay {
public:
    //debo trabajar con mi propia lista de vertices
    std::vector <TriangleFace *> Triangulacion;
    //std::vector<FVector> Vertices;
    std::vector<FVector *> Vertices;
	std::vector<TriangleFace *> Triangulate(std::vector<FVector> & VerticesIn, std::vector<FVector *> & VerticesOut);

	void MergeSortA(std::vector<FVector *> & VerticesIn, int ini, int fin, bool eje);
	void MergeA(std::vector<FVector *> & VerticesIn, int ini, int med, int fin, bool eje);
	//void PrepararParticiones(std::vector<FVector *> & Vertices, int ini, int fin, bool eje);
	void PrepararParticiones(int ini, int fin, bool eje);

    void BuscarPuntosTangentesCorteHorizontal(std::vector<TriangleFace *> & Triangulacion1, std::vector<TriangleFace *> & Triangulacion2, TriangleFace * & TLTop, TriangleFace * & TLBottom, TriangleFace * & TLNext, TriangleFace * & TLPrev, TriangleFace * & TRTop, TriangleFace * & TRBottom, TriangleFace * & TRNext, TriangleFace * & TRPrev);
    void BuscarPuntosTangentesCorteVertical(std::vector<TriangleFace *> & Triangulacion1, std::vector<TriangleFace*> & Triangulacion2, TriangleFace * & TLTop, TriangleFace * & TLBottom, TriangleFace * & TLNext, TriangleFace * & TLPrev, TriangleFace * & TRTop, TriangleFace * & TRBottom, TriangleFace * & TRNext, TriangleFace * & TRPrev);

	std::vector<TriangleFace *> DivideConquerP(int ini, int fin, bool eje);//los inidice y los conrtes son en el vector de vertices, devuelvo un vector con las triangulaciones
	std::vector<TriangleFace *> MergeTriangulations(std::vector<TriangleFace *> & Triangulacion1, std::vector<TriangleFace *> & Triangulacion2, int ini, int med, int fin, bool eje);

	float DeteccionDeOrden(FVector V1, FVector V2, FVector Vm);//funcionara como la counterclockwise() de triangle??
	float DeteccionDeLadoRelativo(FVector V0, FVector V1, FVector V2);// determina a que lado de la recta que va de 0 a 1 se encuentra el punto 2, (derecha izquierda o en la recta misma)
	bool DeteccionAristaIntersectada(FVector Baricentro, FVector Punto, FVector V0, FVector V1);// determina a que lado de la recta que va de 0 a 1 se encuentra el punto 2, (derecha izquierda o en la recta misma)
    int DeteccionAristaIntersectada(TriangleFace * Triangle, FVector Punto);
    float EnCirculo(FVector a, FVector b, FVector c, FVector d);

    void ImprimirTriangulos(std::vector<TriangleFace *> & TriangulacionIn);
    void ImprimirVertices();

    bool IsGoodTriangle(TriangleFace & Triangle);
    FVector Circuncentro(FVector V0, FVector V1, FVector V2);
    FVector Circuncentro(TriangleFace & Triangle);
    FVector Baricentro(FVector V0, FVector V1, FVector V2);
    FVector Baricentro(TriangleFace & Triangle);

    void RotarTrianguloHorario(TriangleFace & Triangle);
    int PerteneceAlTriangulo(TriangleFace & Triangle, FVector Punto);
    bool EsDelaunayConElOtroHull(TriangleFace * Triangle, std::vector<TriangleFace *> & OtroHull);
    bool EsDelaunayConElOtroHull(TriangleFace * Triangle, std::vector<TriangleFace *> & OtroHull, int & AristaIntersectada);//ese int podria ser la respuesta en lugar de un bool, -1 seria el falso
    bool EsDelaunayConLaOtraTriangulacion(TriangleFace * Triangle, int ini, int fin, int & AristaIntersectada);//ese int podria ser la respuesta en lugar de un bool, -1 seria el falso, ini y fin son los indices de los vertices donde esta ubicado la otra triangulacion
    bool EsDelaunay(TriangleFace & Triangle, std::vector<FVector> & vertices);
    bool EsDelaunay(TriangleFace * Triangle, FVector * vertice);
    void ConvertirEnFantasmaTrianguloHullRight(TriangleFace & TR, TriangleFace & TRE);
    void ConvertirEnFantasmaTrianguloHullLeft(TriangleFace & TL, TriangleFace & TLE);
    //como alternativa a estas funciones de eliminacion, podria usar un while en las eliminaciones
    //void EliminarTriangulosNoDelaunayLeft(std::vector<TriangleFace *> & Hull, TriangleFace * & TLTop, TriangleFace * & TLPrev, TriangleFace * & TLBottom, TriangleFace * & TLNext, int ini, int fin);//ini y fin es para los vertices del otro hull, ya que no recibire el otro hull en si mismo
    //void EliminarTriangulosNoDelaunayRight(std::vector<);
	Delaunay();
	~Delaunay();
};

//mre pregunto si deberia conservar una copia de los arreglos, para reducir parametros en las funciones, por ejmplo en todo tengo que usar triangle, el arreglo de triangulos, al igual, de pronto los vertices

//los vertices ua vez ordenados, no deberian cambiar de posicion, por lo tanto los triangulos deben siempre apuntar a esos vertices con ese ordenamiento, esto para todas las fases de la triangulacion.
//tambien una vez triangulado se le debe pasar al precedural mesh.
//la calse del mapa tiene el arreglo de vectores y triangulos.
//tener cuidado con refinamientos o actualziaciones a esas cosas, ya que podria emzclarse y desordenarse tod
